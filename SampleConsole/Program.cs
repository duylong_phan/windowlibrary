﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LongModel.Helpers;
using System.Reflection;
using System.IO.Ports;
using System.Threading;
using LongModel.Models.Communications;
using System.Diagnostics;
using Microsoft.Win32;
using System.Collections.ObjectModel;
using System.ComponentModel;
using LongInterface.Models;
using LongModel.BaseClasses;
using LongModel.Collections;
using System.Xml.Linq;
using System.IO;
using LongModel.ExtensionMethods;
using LongWpfUI.Models.Net;
using System.Net;
using LongWpfUI.SystemHelper;
using System.Globalization;

namespace SampleConsole
{
    public enum NetPositions
    {
        Client,
        Server,
        Dummy1,
        Dummy2
    }

    public class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int value = int.MaxValue;

                byte high = (byte)(value / 256);
                byte low = (byte)(value % 256);
                Console.WriteLine(string.Format("{0}  {1}", high, low));

                Console.WriteLine("Done");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                Console.WriteLine(ex.StackTrace);
            }
            Console.ReadKey();
        }
    }
}
