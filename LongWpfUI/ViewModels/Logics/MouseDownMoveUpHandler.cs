﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LongInterface.ViewModels.Logics;
using System.Windows;
using System.Windows.Input;

namespace LongWpfUI.ViewModels.Logics
{    
    public class MouseDownMoveUpHandler : IDownMoveUpHandler<Point>
    {
        #region Field
        private bool canFocus;
        private FrameworkElement element;
        private FrameworkElement relativeElement;
        private MouseButtonEventHandler downHandler;
        private MouseEventHandler moveHandler;
        private MouseButtonEventHandler upHandler;
        #endregion

        #region Getter, Setter
        public Action<Point> DownAction { get; set; }
        public Action<Point> MoveAction { get; set; }
        public Action<Point> UpAction { get; set; }
        #endregion

        #region Constructor
        /// <summary>
        /// Initialize a new instance of MouseDownMoveUpHandler
        /// </summary>
        /// <param name="element">attacted element to this handler</param>
        /// <param name="relativeElement">relative Element for the coordinate, nullable as Window coordinate</param>
        public MouseDownMoveUpHandler(FrameworkElement element, FrameworkElement relativeElement, bool canFocus)
        {
            this.element = element;            
            this.relativeElement = relativeElement;
            this.canFocus = canFocus;

            //subscribe only Down event
            this.downHandler = (sender, e) =>
            {
                OnDown(e.GetPosition(this.relativeElement));
            };
            element.PreviewMouseLeftButtonDown += downHandler;
        }
        #endregion

        #region Public Method
        public void Dispose()
        {
            if (downHandler != null)
            {
                element.PreviewMouseLeftButtonDown -= downHandler;
                downHandler = null;
            }
            if (moveHandler != null)
            {
                element.PreviewMouseMove -= moveHandler;
                moveHandler = null;
            }
            if (upHandler != null)
            {
                element.PreviewMouseLeftButtonUp -= upHandler;
                upHandler = null;
            }
        }
        #endregion

        #region Protected Method
        protected void OnDown(Point point)
        {
            //dispose Down
            if (downHandler != null)
            {
                element.PreviewMouseLeftButtonDown -= downHandler;
                downHandler = null;
            }

            //subscribe Move Event
            this.moveHandler = (sender, e) =>
            {
                OnMove(e.GetPosition(this.relativeElement));
            };
            element.PreviewMouseMove += this.moveHandler;
            //suscribe Down Event
            this.upHandler = (sender, e) =>
            {
                OnUp(e.GetPosition(this.relativeElement));
            };            
            element.PreviewMouseLeftButtonUp += this.upHandler;
            //focus mouse to element
            if (this.canFocus)
            {
                element.CaptureMouse();
            }

            //do DownAction
            if (this.DownAction != null)
            {
                this.DownAction(point);
            }
        }

        protected void OnMove(Point point)
        {
            //Do MoveAction
            if (this.MoveAction != null)
            {
                this.MoveAction(point);
            }
        }

        protected void OnUp(Point point)
        {
            //dispose Move
            if (moveHandler != null)
            {
                element.PreviewMouseMove -= moveHandler;
                moveHandler = null;
            }
            //dispose Up
            if (upHandler != null)
            {
                element.PreviewMouseLeftButtonUp -= upHandler;
                upHandler = null;
            }
            //release if mouse was capture
            if (this.canFocus)
            {
                element.ReleaseMouseCapture();
            }

            if (this.UpAction != null)
            {
                this.UpAction(point);
            }
        } 
        #endregion
    }
}
