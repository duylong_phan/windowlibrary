﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace LongWpfUI.ViewModels.Logics
{
    public class CircleCreatorHandler : MouseDownMoveUpHandler
    {
        public CircleCreatorHandler(FrameworkElement element, FrameworkElement relativeElement, bool canFocus, Canvas panel, Action<Ellipse> doSomeThing)
            : base(element, relativeElement, canFocus)
        {
            if (panel == null || doSomeThing == null)
            {
                throw new Exception("Panel or DoSomething is null");
            }

            Point startPoint = new Point();
            TranslateTransform transform = new TranslateTransform();
            transform.X = transform.Y = 0;

            Ellipse circle = new Ellipse();
            circle.Fill = Brushes.LightSteelBlue;
            circle.Stroke = Brushes.SteelBlue;
            circle.StrokeThickness = 1;
            circle.RenderTransform = transform;

            this.DownAction = (point) =>
            {
                //center point
                startPoint = point;
                Canvas.SetLeft(circle, point.X);
                Canvas.SetTop(circle, point.Y);
                panel.Children.Add(circle);
            };

            this.MoveAction = (point) =>
            {
                //Pythagoras' Theorem 
                double width = point.X - startPoint.X;
                double height = point.Y - startPoint.Y;
                double radius = Math.Sqrt(Math.Pow(width, 2) + Math.Pow(height,2));
                double diameter = radius * 2;
                
                //new diameter
                circle.Width = circle.Height = diameter;
                //update transform to get center always as StartPoint
                transform.X = transform.Y = -radius;
            };

            this.UpAction = (point) =>
            {
                panel.Children.Remove(circle);
                Dispose();
                doSomeThing(circle);
            };
        }
    }
}
