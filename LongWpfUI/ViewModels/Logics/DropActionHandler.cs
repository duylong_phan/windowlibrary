﻿using LongWpfUI.Models.DragDrop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace LongWpfUI.ViewModels.Logics
{
    public class DropActionHandler : DropBaseHandler
    {
        #region Getter, Setter
        public Action<FrameworkElement, DragEventArgs> DragEnterAction { get; set; }
        public Action<FrameworkElement, DragEventArgs> DragLeaveAction { get; set; }
        public Action<FrameworkElement, DragEventArgs> DragOverAction { get; set; }
        public Action<FrameworkElement, DragEventArgs> DropAction { get; set; } 
        #endregion

        #region Getter
        public override DragDropEffects Effect
        {
            get
            {
                return this.effect;
            }
        }

        public override string FormatString
        {
            get
            {
                return this.formatString;
            }
        }
        #endregion

        #region Field
        protected DragDropEffects effect;
        protected string formatString;
        #endregion

        #region Constructor
        public DropActionHandler(DragDropEffects effect, string formatString)
        {
            this.effect = effect;
            this.formatString = formatString;
        } 
        #endregion

        #region Public Method
        public override void OnDragEnter(FrameworkElement element, DragEventArgs e)
        {
            if (this.DragEnterAction == null)
            {
                base.OnDragEnter(element, e);
            }
            else
            {
                this.DragEnterAction(element, e);
            }
        }

        public override void OnDragLeave(FrameworkElement element, DragEventArgs e)
        {
            if (this.DragLeaveAction == null)
            {
                base.OnDragLeave(element, e);
            }
            else
            {
                this.DragLeaveAction(element, e);
            }
        }

        public override void OnDragOver(FrameworkElement element, DragEventArgs e)
        {
            if (this.DragOverAction == null)
            {
                base.OnDragOver(element, e);
            }
            else
            {
                this.DragOverAction(element, e);
            }
        }

        public override void OnDrop(FrameworkElement element, DragEventArgs e)
        {
            if (this.DropAction != null)
            {
                this.DropAction(element, e);
            }
        } 
        #endregion
    }
}
