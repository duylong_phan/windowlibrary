﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace LongWpfUI.ViewModels.Logics
{
    public class RectCreatorHandler : MouseDownMoveUpHandler
    {
        public RectCreatorHandler(FrameworkElement element, FrameworkElement relativeElement, bool canFocus, Canvas panel, Action<Rectangle> doSomeThing)
            : base(element, relativeElement, canFocus)
        {
            if (panel == null || doSomeThing == null)
            {
                throw new Exception("Panel or DoSomething is null");
            }

            Point startPoint = new Point();
            Rectangle rect = new Rectangle();
            rect.Fill = Brushes.LightSteelBlue;
            rect.Stroke = Brushes.SteelBlue;
            rect.StrokeThickness = 1;

            this.DownAction = (point) =>
            {
                startPoint = point;
                Canvas.SetLeft(rect, point.X);
                Canvas.SetTop(rect, point.Y);
                panel.Children.Add(rect);                
            };

            this.MoveAction = (point) =>
            {
                double width = point.X - startPoint.X;
                double height = point.Y - startPoint.Y;
                if (width >= 0)
                {
                    rect.Width = width;
                }
                else
                {
                    Canvas.SetLeft(rect, startPoint.X + width);
                    rect.Width = -width;
                }

                if (height >= 0)
                {
                    rect.Height = height;
                }
                else
                {
                    Canvas.SetTop(rect, startPoint.Y + height);
                    rect.Height = -height;
                }
            };

            this.UpAction = (point) =>
            {
                panel.Children.Remove(rect);
                Dispose();
                doSomeThing(rect);
            };
        }
    }
}
