﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using LongWpfUI.Models.DragDrop;

namespace LongWpfUI.ViewModels.Logics
{
    public class DragDropActionHandler : DragDropBaseHandler
    {
        #region Getter, setter
        public Func<object, object> GetDataAction { get; set; }
        public Action<ItemsControl, DragDropEffects, object> DragDoneAction { get; set; }
        
        public Action<FrameworkElement, DragEventArgs> DragEnterAction { get; set; }
        public Action<FrameworkElement, DragEventArgs> DragLeaveAction { get; set; }
        public Action<FrameworkElement, DragEventArgs> DragOverAction { get; set; }
        public Action<FrameworkElement, DragEventArgs> DropAction { get; set; }
        #endregion       

        #region Getter
        public override DragDropEffects Effect
        {
            get
            {
                return this.effect;
            }
        }

        public override string FormatString
        {
            get
            {
                return this.formatString;
            }
        }
        #endregion

        #region Field
        private DragDropEffects effect;
        private string formatString;
        #endregion

        #region Constructor
        public DragDropActionHandler(DragDropEffects effect, string formatString)
        {
            this.effect = effect;
            this.formatString = formatString;
        } 
        #endregion

        #region Public Method
        public override object GetDragData(object rawData)
        {
            if (this.GetDataAction != null)
            {
                return this.GetDataAction(rawData);
            }
            else
            {
                return null;
            }
        }

        public override void OnDragDone(ItemsControl control, DragDropEffects finalEffect, object rawData)
        {
            if (this.DragDoneAction != null)
            {
                this.DragDoneAction(control, finalEffect, rawData);
            }
        }

        public override void OnDragEnter(FrameworkElement element, DragEventArgs e)
        {
            if (this.DragEnterAction == null)
            {
                base.OnDragEnter(element, e);
            }
            else
            {
                this.DragEnterAction(element, e);
            }
        }

        public override void OnDragLeave(FrameworkElement element, DragEventArgs e)
        {
            if (this.DragLeaveAction == null)
            {
                base.OnDragLeave(element, e);
            }
            else
            {
                this.DragLeaveAction(element, e);
            }
        }

        public override void OnDragOver(FrameworkElement element, DragEventArgs e)
        {
            if (this.DragOverAction == null)
            {
                base.OnDragOver(element, e);
            }
            else
            {
                this.DragOverAction(element, e);
            }
        }

        public override void OnDrop(FrameworkElement element, DragEventArgs e)
        {
            if (this.DropAction != null)
            {
                this.DropAction(element, e);
            }
        }
        #endregion
    }
}
