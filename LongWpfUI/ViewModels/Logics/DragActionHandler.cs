﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using LongWpfUI.Models.DragDrop;

namespace LongWpfUI.ViewModels.Logics
{
    public class DragActionHandler : DragBaseHandler
    {
        #region Getter, setter
        public Func<object, object> GetDataAction { get; set; }
        public Action<ItemsControl, DragDropEffects, object> DragDoneAction { get; set; } 
        #endregion

        #region Getter
        public override DragDropEffects Effect
        {
            get
            {
                return this.effect;
            }
        }

        public override string FormatString
        {
            get
            {
                return this.formatString;
            }
        }
        #endregion

        #region Field
        private DragDropEffects effect;
        private string formatString;
        #endregion

        #region Constructor
        public DragActionHandler(DragDropEffects effect, string formatString)
        {
            this.effect = effect;
            this.formatString = formatString;
        } 
        #endregion

        #region Public Methods
        public override object GetDragData(object rawData)
        {
            if (this.GetDataAction != null)
            {
                return this.GetDataAction(rawData);
            }
            else
            {
                return null;
            }
        }

        public override void OnDragDone(ItemsControl control, DragDropEffects finalEffect, object rawData)
        {
            if (this.DragDoneAction != null)
            {
                this.DragDoneAction(control, finalEffect, rawData);
            }
        } 
        #endregion
    }
}
