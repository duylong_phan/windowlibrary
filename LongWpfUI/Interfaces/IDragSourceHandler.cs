﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace LongWpfUI.Interfaces
{
    public interface IDragSourceHandler : IDragDrop
    {
        object GetDragData(object rawData);        
        void OnDragDone(ItemsControl control, DragDropEffects finalEffect, object rawData);
    }
}
