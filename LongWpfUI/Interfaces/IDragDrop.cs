﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace LongWpfUI.Interfaces
{
    public interface IDragDrop
    {
        string FormatString { get; }
        DragDropEffects Effect { get; }
    }
}
