﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace LongWpfUI.Interfaces
{
    public interface IDropTargetHandler : IDragDrop
    {
        DataTemplate ContentTemplate { get; set; }

        void OnDragOver(FrameworkElement element, DragEventArgs e);
        void OnDragEnter(FrameworkElement element, DragEventArgs e);
        void OnDragLeave(FrameworkElement element, DragEventArgs e);
        void OnDrop(FrameworkElement element, DragEventArgs e);
    }
}
