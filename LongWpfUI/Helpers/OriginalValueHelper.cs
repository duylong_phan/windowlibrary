﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace LongWpfUI.Helpers
{
    public class OriginalValueHelper : DependencyObject
    {
        #region AP
        public static double GetWidth(DependencyObject obj)
        {
            return (double)obj.GetValue(WidthProperty);
        }

        public static void SetWidth(DependencyObject obj, double value)
        {
            obj.SetValue(WidthProperty, value);
        }

        public static double GetHeight(DependencyObject obj)
        {
            return (double)obj.GetValue(HeightProperty);
        }

        public static void SetHeight(DependencyObject obj, double value)
        {
            obj.SetValue(HeightProperty, value);
        }

        public static double GetMinimum(DependencyObject obj)
        {
            return (double)obj.GetValue(MinimumProperty);
        }

        public static void SetMinimum(DependencyObject obj, double value)
        {
            obj.SetValue(MinimumProperty, value);
        }


        public static double GetMaximum(DependencyObject obj)
        {
            return (double)obj.GetValue(MaximumProperty);
        }

        public static void SetMaximum(DependencyObject obj, double value)
        {
            obj.SetValue(MaximumProperty, value);
        }

        public static readonly DependencyProperty WidthProperty =
            DependencyProperty.RegisterAttached("Width", typeof(double), typeof(OriginalValueHelper), new PropertyMetadata(0.0));

        public static readonly DependencyProperty HeightProperty =
            DependencyProperty.RegisterAttached("Height", typeof(double), typeof(OriginalValueHelper), new PropertyMetadata(0.0));

        public static readonly DependencyProperty MinimumProperty =
            DependencyProperty.RegisterAttached("Minimum", typeof(double), typeof(OriginalValueHelper), new PropertyMetadata(0.0));

        public static readonly DependencyProperty MaximumProperty =
            DependencyProperty.RegisterAttached("Maximum", typeof(double), typeof(OriginalValueHelper), new PropertyMetadata(0.0)); 
        #endregion
    }
}
