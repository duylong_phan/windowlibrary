﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace LongWpfUI.Helpers
{
    /// <summary>
    /// Attachted Class provides more function to TextBox Class
    /// </summary>
    public class TextBoxHelper : DependencyObject
    {
        #region DA
        /// <summary>
        /// Get SelectAllOnFocus status of given TextBox instance
        /// </summary>
        /// <param name="obj">given TextBox instance</param>
        /// <returns></returns>
        [AttachedPropertyBrowsableForType(typeof(TextBox))]
        public static bool GetSelectAllOnFocus(DependencyObject obj)
        {
            return (bool)obj.GetValue(SelectAllOnFocusProperty);
        }

        public static void SetSelectAllOnFocus(DependencyObject obj, bool value)
        {
            obj.SetValue(SelectAllOnFocusProperty, value);
        }
        [AttachedPropertyBrowsableForType(typeof(TextBox))]
        public static bool GetEnableEnterNextFocus(DependencyObject obj)
        {
            return (bool)obj.GetValue(EnableEnterNextFocusProperty);
        }

        public static void SetEnableEnterNextFocus(DependencyObject obj, bool value)
        {
            obj.SetValue(EnableEnterNextFocusProperty, value);
        }
        [AttachedPropertyBrowsableForType(typeof(TextBox))]
        public static FrameworkElement GetNextControl(DependencyObject obj)
        {
            return (FrameworkElement)obj.GetValue(NextControlProperty);
        }

        public static void SetNextControl(DependencyObject obj, FrameworkElement value)
        {
            obj.SetValue(NextControlProperty, value);
        }

        public static readonly DependencyProperty SelectAllOnFocusProperty =
            DependencyProperty.RegisterAttached("SelectAllOnFocus", typeof(bool), typeof(TextBoxHelper), new PropertyMetadata(false, OnSelectAllOnFocus));

        public static readonly DependencyProperty EnableEnterNextFocusProperty =
           DependencyProperty.RegisterAttached("EnableEnterNextFocus", typeof(bool), typeof(TextBoxHelper), new PropertyMetadata(false, OnEnableEnterNextFocusChanged));

        public static readonly DependencyProperty NextControlProperty =
             DependencyProperty.RegisterAttached("NextControl", typeof(FrameworkElement), typeof(TextBoxHelper), new PropertyMetadata(null));

        #region Private
        private static RoutedEventHandler GetGotFocus(DependencyObject obj)
        {
            return (RoutedEventHandler)obj.GetValue(GotFocusProperty);
        }

        private static void SetGotFocus(DependencyObject obj, RoutedEventHandler value)
        {
            obj.SetValue(GotFocusProperty, value);
        }

        private static MouseButtonEventHandler GetMouseDown(DependencyObject obj)
        {
            return (MouseButtonEventHandler)obj.GetValue(MouseDownProperty);
        }

        private static void SetMouseDown(DependencyObject obj, MouseButtonEventHandler value)
        {
            obj.SetValue(MouseDownProperty, value);
        }

        private static KeyEventHandler GetKeyUp(DependencyObject obj)
        {
            return (KeyEventHandler)obj.GetValue(KeyUpProperty);
        }

        private static void SetKeyUp(DependencyObject obj, KeyEventHandler value)
        {
            obj.SetValue(KeyUpProperty, value);
        }

        public static readonly DependencyProperty GotFocusProperty =
           DependencyProperty.RegisterAttached("GotFocus", typeof(RoutedEventHandler), typeof(TextBoxHelper), new PropertyMetadata(null));

        public static readonly DependencyProperty MouseDownProperty =
            DependencyProperty.RegisterAttached("MouseDown", typeof(MouseButtonEventHandler), typeof(TextBoxHelper), new PropertyMetadata(null));

        public static readonly DependencyProperty KeyUpProperty =
            DependencyProperty.RegisterAttached("KeyUp", typeof(KeyEventHandler), typeof(TextBoxHelper), new PropertyMetadata(null));
        #endregion

        #endregion

        #region Property Changed
        private static void OnSelectAllOnFocus(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is TextBox)
            {
                TextBox textBox = d as TextBox;
                bool selectAllOnFocus = TextBoxHelper.GetSelectAllOnFocus(textBox);
                if (selectAllOnFocus)
                {
                    RoutedEventHandler focusHandler = (_sender, _e) =>
                    {
                        textBox.SelectAll();                        
                    };
                    textBox.GotFocus += focusHandler;

                    MouseButtonEventHandler mouseHandler = (_sender, _e) =>
                    {
                        if (_e.ClickCount == 3)
                        {
                            textBox.SelectAll();
                        }

                        if (!textBox.IsKeyboardFocusWithin) //NOT
                        {
                            textBox.Focus();
                            _e.Handled = true;
                        }
                    };
                    textBox.PreviewMouseDown += mouseHandler;

                    TextBoxHelper.SetGotFocus(textBox, focusHandler);
                    TextBoxHelper.SetMouseDown(textBox, mouseHandler);
                }
                else
                {
                    RoutedEventHandler focusHandler = TextBoxHelper.GetGotFocus(textBox);
                    if (focusHandler != null)
                    {
                        textBox.GotFocus -= focusHandler;
                        TextBoxHelper.SetGotFocus(textBox, null);
                    }

                    MouseButtonEventHandler mouseHandler = TextBoxHelper.GetMouseDown(textBox);
                    if (mouseHandler != null)
                    {
                        textBox.PreviewMouseDown -= mouseHandler;
                    }
                }
            }
            else
            {
                throw new Exception("Support only TextBox instance");
            }
        }

        private static void OnEnableEnterNextFocusChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is FrameworkElement)
            {
                FrameworkElement element = d as FrameworkElement;
                bool enableEnterNextFocus = TextBoxHelper.GetEnableEnterNextFocus(element);
                if (enableEnterNextFocus)
                {
                    KeyEventHandler keyUp = (_sender, _e) =>
                    {
                        if (_e.Key == Key.Enter)
                        {
                            FrameworkElement nextControl = TextBoxHelper.GetNextControl(element);
                            if (nextControl != null)
                            {
                                nextControl.Focus();
                            }
                        }
                    };
                    element.PreviewKeyUp += keyUp;
                    TextBoxHelper.SetKeyUp(element, keyUp);
                }
                else
                {
                    KeyEventHandler keyUp = TextBoxHelper.GetKeyUp(element);
                    if (keyUp != null)
                    {
                        element.PreviewKeyUp -= keyUp;
                        TextBoxHelper.SetKeyUp(element, null);
                    }
                }
            }
            else
            {
                throw new Exception("Only Support FrameworkElement Instance");
            }
        }
        #endregion        
    }
}
