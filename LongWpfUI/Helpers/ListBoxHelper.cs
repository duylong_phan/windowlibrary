﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace LongWpfUI.Helpers
{
    public class ListBoxHelper : DependencyObject
    {
        #region AP
        /// <summary>
        /// Get the Status for Mono Pole for given Item
        /// </summary>
        /// <param name="obj">given Item</param>
        /// <returns></returns>
        [AttachedPropertyBrowsableForType(typeof(ListBox))]
        public static bool GetIsMonoPoleEnable(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsMonoPoleEnableProperty);
        }
        /// <summary>
        /// Set the Status for Mono Pole for given Item
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="value"></param>
        public static void SetIsMonoPoleEnable(DependencyObject obj, bool value)
        {
            obj.SetValue(IsMonoPoleEnableProperty, value);
        }


        public static readonly DependencyProperty IsMonoPoleEnableProperty =
            DependencyProperty.RegisterAttached("IsMonoPoleEnable", typeof(bool), typeof(ListBoxHelper), new PropertyMetadata(false, OnIsMonoPoleEnableChanged));



        #region Private
        private static SelectionChangedEventHandler GetSelectionChanged(DependencyObject obj)
        {
            return (SelectionChangedEventHandler)obj.GetValue(SelectionChangedProperty);
        }

        private static void SetSelectionChanged(DependencyObject obj, SelectionChangedEventHandler value)
        {
            obj.SetValue(SelectionChangedProperty, value);
        }


        public static readonly DependencyProperty SelectionChangedProperty =
            DependencyProperty.RegisterAttached("SelectionChanged", typeof(SelectionChangedEventHandler), typeof(ListBoxHelper), new PropertyMetadata(null));
        #endregion 
        #endregion

        //ListBoxItem Property="Focusable" should be False
        #region Property Changed
        private static void OnIsMonoPoleEnableChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is ListBox)
            {
                ListBox listBox = d as ListBox;
                bool isMonoPoleEnable = ListBoxHelper.GetIsMonoPoleEnable(listBox);
                if (isMonoPoleEnable)
                {
                    SelectionChangedEventHandler selectionChanged = (_sender, _e) =>
                    {
                        if (listBox.SelectedItem != null)
                        {
                            foreach (object item in listBox.Items)
                            {
                                if (item != null)
                                {
                                    ListBoxItem listBoxItem = listBox.ItemContainerGenerator.ContainerFromItem(item) as ListBoxItem;
                                    if (listBoxItem != null && listBoxItem.Content != listBox.SelectedItem)
                                    {
                                        listBoxItem.IsEnabled = false;
                                    }
                                }
                            }
                        }
                        else
                        {
                            foreach (object item in listBox.Items)
                            {
                                if (item != null)
                                {
                                    ListBoxItem listBoxItem = listBox.ItemContainerGenerator.ContainerFromItem(item) as ListBoxItem;
                                    if (listBoxItem != null)
                                    {
                                        listBoxItem.IsEnabled = true;
                                    }
                                }
                            }
                        }
                    };

                    listBox.SelectionChanged += selectionChanged;
                    ListBoxHelper.SetSelectionChanged(listBox, selectionChanged);
                }
                else
                {
                    SelectionChangedEventHandler selectionChanged = ListBoxHelper.GetSelectionChanged(listBox);
                    if (selectionChanged != null)
                    {
                        listBox.SelectionChanged -= selectionChanged;
                        ListBoxHelper.SetSelectionChanged(listBox, null);
                    }
                }
            }
            else
            {
                throw new Exception("Support only ListBox Instance");
            }
        } 
        #endregion
    }
}
