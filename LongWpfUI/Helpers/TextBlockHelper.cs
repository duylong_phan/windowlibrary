﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;

namespace LongWpfUI.Helpers
{
    public class TextBlockHelper : DependencyObject
    {
        #region DA
        [AttachedPropertyBrowsableForType(typeof(TextBlock))]
        public static bool GetIsSelected(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsSelectedProperty);
        }

        public static void SetIsSelected(DependencyObject obj, bool value)
        {
            obj.SetValue(IsSelectedProperty, value);
        }

        [AttachedPropertyBrowsableForType(typeof(TextBlock))]
        public static bool GetIsEditEnable(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsEditEnableProperty);
        }

        public static void SetIsEditEnable(DependencyObject obj, bool value)
        {
            obj.SetValue(IsEditEnableProperty, value);
        }

         [AttachedPropertyBrowsableForType(typeof(TextBlock))]
        public static Style GetEditElementStyle(DependencyObject obj)
        {
            return (Style)obj.GetValue(EditElementStyleProperty);
        }

        public static void SetEditElementStyle(DependencyObject obj, Style value)
        {
            obj.SetValue(EditElementStyleProperty, value);
        }

        private static MouseButtonEventHandler GetMouseDown(DependencyObject obj)
        {
            return (MouseButtonEventHandler)obj.GetValue(MouseDownProperty);
        }

        private static void SetMouseDown(DependencyObject obj, MouseButtonEventHandler value)
        {
            obj.SetValue(MouseDownProperty, value);
        }

        public static bool GetCanEdit(DependencyObject obj)
        {
            return (bool)obj.GetValue(CanEditProperty);
        }
       

        public static readonly DependencyProperty IsSelectedProperty =
            DependencyProperty.RegisterAttached("IsSelected", typeof(bool), typeof(TextBlockHelper), new PropertyMetadata(false, OnIsSelected));

        public static readonly DependencyProperty IsEditEnableProperty =
            DependencyProperty.RegisterAttached("IsEditEnable", typeof(bool), typeof(TextBlockHelper), new PropertyMetadata(false, OnIsEditEnable));

        public static readonly DependencyProperty EditElementStyleProperty =
           DependencyProperty.RegisterAttached("EditElementStyle", typeof(Style), typeof(TextBlockHelper), new PropertyMetadata(null));


        #region Private
        private static void SetCanEdit(DependencyObject obj, bool value)
        {
            obj.SetValue(CanEditProperty, value);
        }

        private static string GetPreviousText(DependencyObject obj)
        {
            return (string)obj.GetValue(PreviousTextProperty);
        }

        private static void SetPreviousText(DependencyObject obj, string value)
        {
            obj.SetValue(PreviousTextProperty, value);
        }

        private static Popup GetPopup(DependencyObject obj)
        {
            return (Popup)obj.GetValue(PopupProperty);
        }

        private static void SetPopup(DependencyObject obj, Popup value)
        {
            obj.SetValue(PopupProperty, value);
        }

        private static KeyEventHandler GetKeyUp(DependencyObject obj)
        {
            return (KeyEventHandler)obj.GetValue(KeyUpProperty);
        }

        private static void SetKeyUp(DependencyObject obj, KeyEventHandler value)
        {
            obj.SetValue(KeyUpProperty, value);
        }       

        private static RoutedEventHandler GetLostFocus(DependencyObject obj)
        {
            return (RoutedEventHandler)obj.GetValue(LostFocusProperty);
        }

        private static void SetLostFocus(DependencyObject obj, RoutedEventHandler value)
        {
            obj.SetValue(LostFocusProperty, value);
        }

        private static KeyEventHandler GetWindowKeyUp(DependencyObject obj)
        {
            return (KeyEventHandler)obj.GetValue(WindowKeyUpProperty);
        }

        private static void SetWindowKeyUp(DependencyObject obj, KeyEventHandler value)
        {
            obj.SetValue(WindowKeyUpProperty, value);
        }

        public static readonly DependencyProperty MouseDownProperty =
          DependencyProperty.RegisterAttached("MouseDown", typeof(MouseButtonEventHandler), typeof(TextBlockHelper), new PropertyMetadata(null));

        public static readonly DependencyProperty CanEditProperty =
           DependencyProperty.RegisterAttached("CanEdit", typeof(bool), typeof(TextBlockHelper), new PropertyMetadata(false));

        public static readonly DependencyProperty PreviousTextProperty =
           DependencyProperty.RegisterAttached("PreviousText", typeof(string), typeof(TextBlockHelper), new PropertyMetadata(string.Empty));

        public static readonly DependencyProperty PopupProperty =
           DependencyProperty.RegisterAttached("Popup", typeof(Popup), typeof(TextBlockHelper), new PropertyMetadata(null));

        public static readonly DependencyProperty KeyUpProperty =
          DependencyProperty.RegisterAttached("KeyUp", typeof(KeyEventHandler), typeof(TextBlockHelper), new PropertyMetadata(null));     

        public static readonly DependencyProperty LostFocusProperty =
            DependencyProperty.RegisterAttached("LostFocus", typeof(RoutedEventHandler), typeof(TextBlockHelper), new PropertyMetadata(null));

        public static readonly DependencyProperty WindowKeyUpProperty =
           DependencyProperty.RegisterAttached("WindowKeyUp", typeof(KeyEventHandler), typeof(TextBlockHelper), new PropertyMetadata(null));
        #endregion

        #endregion

        #region Property Changed
        private static void OnIsSelected(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is TextBlock)
            {
                TextBlock textBlock = d as TextBlock;
                bool isSelected = TextBlockHelper.GetIsSelected(textBlock);
                Window topWindow = Window.GetWindow(textBlock);

                if (topWindow == null)
                {
                    return;
                }

                if (isSelected)
                {
                    KeyEventHandler windowKeyUp = (_sender, _e) =>
                    {
                        if (_e.Key == Key.F2)
                        {
                            SwitchState(textBlock);
                            _e.Handled = true;
                        }
                    };
                    topWindow.PreviewKeyUp += windowKeyUp;
                    TextBlockHelper.SetWindowKeyUp(textBlock, windowKeyUp);
                }
                else
                {
                    KeyEventHandler windowKeyUp = TextBlockHelper.GetWindowKeyUp(textBlock);
                    if (windowKeyUp != null)
                    {
                        topWindow.PreviewKeyUp -= windowKeyUp;
                        TextBlockHelper.SetWindowKeyUp(textBlock, null);
                    }
                }
            }
            else
            {
                throw new Exception("Support only for TextBlock Instance");
            }
        }

        private static void OnIsEditEnable(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is TextBlock)
            {
                TextBlock textBlock = d as TextBlock;
                bool isEditEnable = TextBlockHelper.GetIsEditEnable(textBlock);
                if (isEditEnable)
                {
                    MouseButtonEventHandler mouseDown = (_sender, _e) =>
                    {
                        if (_e.ClickCount == 2)
                        {
                            SwitchState(textBlock);
                            _e.Handled = true;
                        }
                    };

                    textBlock.PreviewMouseDown += mouseDown;
                    TextBlockHelper.SetMouseDown(textBlock, mouseDown);
                }
                else
                {
                    MouseButtonEventHandler mouseDown = TextBlockHelper.GetMouseDown(textBlock);
                    if (mouseDown != null)
                    {
                        textBlock.PreviewMouseDown -= mouseDown;
                        TextBlockHelper.SetMouseDown(textBlock, null);
                    }
                }
            }
            else
            {
                throw new Exception("Support only for TextBlock Instance");
            }
        } 
        #endregion

        #region Sub Method
        /// <summary>
        /// Switch current state of given textBlock
        /// </summary>
        /// <param name="textBlock">given textBlock</param>
        private static void SwitchState(TextBlock textBlock, bool isReset = false)
        {
            if (textBlock != null)
            {
                bool canEdit = TextBlockHelper.GetCanEdit(textBlock);
                canEdit = !canEdit;
                TextBlockHelper.SetCanEdit(textBlock, canEdit);

                if (canEdit)
                {
                    #region TextBox
                    #region Event Definition
                    KeyEventHandler keyUp = (_sender, _e) =>
                    {
                        bool isHandled = false;
                        switch (_e.Key)
                        {
                            case Key.Enter:
                            case Key.F2:
                                SwitchState(textBlock);
                                isHandled = true;
                                break;
                            case Key.Escape:
                                ResetText(textBlock);
                                isHandled = true;
                                break;
                        }

                        _e.Handled = isHandled;
                    };                    

                    RoutedEventHandler lostFocus = (_send, _e) =>
                    {
                        SwitchState(textBlock);
                    };

                    MouseButtonEventHandler mouseDown = TextBlockHelper.GetMouseDown(textBlock);
                    #endregion

                    #region Set Property
                    TextBox textBox = new TextBox();
                    textBox.PreviewKeyUp += keyUp;                   
                    textBox.LostFocus += lostFocus;                    
                    if (mouseDown != null)
                    {
                        textBox.PreviewMouseDown += mouseDown;
                    }
                    TextBoxHelper.SetSelectAllOnFocus(textBox, true);

                    textBox.Text = textBlock.Text;
                    textBox.MinWidth = textBlock.ActualWidth;
                    textBox.Style = TextBlockHelper.GetEditElementStyle(textBlock);  
                    #endregion

                    TextBlockHelper.SetKeyUp(textBox, keyUp);                  
                    TextBlockHelper.SetLostFocus(textBox, lostFocus);
                    TextBlockHelper.SetPreviousText(textBlock, textBlock.Text);                    
                    #endregion                   

                    #region Popup
                    Popup popup = new Popup();
                    popup.AllowsTransparency = true;
                    popup.PlacementTarget = textBlock;
                    popup.Placement = PlacementMode.RelativePoint;
                    popup.Child = textBox;                  
                    popup.IsOpen = true;
                    textBox.Focus();                    

                    TextBlockHelper.SetPopup(textBlock, popup);
                    PopupHelper.SetCanPositionUpdate(popup, true);
                    #endregion
                }
                else
                {
                    Popup popup = TextBlockHelper.GetPopup(textBlock);
                    if (popup != null)
                    {
                        if (popup.Child is TextBox)
                        {
                            TextBox textBox = popup.Child as TextBox;
                            if (!isReset)   //NOT
                            {
                                textBlock.Text = textBox.Text;                                
                            }

                            #region Reset
                            KeyEventHandler keyUp = TextBlockHelper.GetKeyUp(textBox);                           
                            RoutedEventHandler lostFocus = TextBlockHelper.GetLostFocus(textBox);
                            MouseButtonEventHandler mouseDown = TextBlockHelper.GetMouseDown(textBlock);
                            textBox.PreviewKeyUp -= keyUp;                          
                            textBox.LostFocus -= lostFocus;
                            if (mouseDown != null)
                            {
                                textBox.PreviewMouseDown -= mouseDown;
                            }

                            TextBoxHelper.SetSelectAllOnFocus(textBox, false);
                            TextBlockHelper.SetKeyUp(textBox, null);                           
                            TextBlockHelper.SetLostFocus(textBox, null);
                            popup.Child = null; 
                            #endregion
                        }
                        
                        popup.IsOpen = false;
                        popup.Child = null;
                        TextBlockHelper.SetPreviousText(textBlock, string.Empty);
                        TextBlockHelper.SetPopup(textBlock, null);
                        PopupHelper.SetCanPositionUpdate(popup, false);
                    }
                }
            }
        }

        /// <summary>
        /// Reset text of given current textBlock
        /// </summary>
        /// <param name="textBlock">given textBlock</param>
        private static void ResetText(TextBlock textBlock)
        {
            if (textBlock != null)
            {
                textBlock.Text = TextBlockHelper.GetPreviousText(textBlock);
                SwitchState(textBlock, true);
            }
        } 
        #endregion
    }
}
