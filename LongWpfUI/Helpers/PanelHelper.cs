﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.ComponentModel;

namespace LongWpfUI.Helpers
{
    public class PanelHelper : DependencyObject
    {
        #region AD
        public static bool GetShowChildSize(DependencyObject obj)
        {
            return (bool)obj.GetValue(ShowChildSizeProperty);
        }

        public static void SetShowChildSize(DependencyObject obj, bool value)
        {
            obj.SetValue(ShowChildSizeProperty, value);
        }

        public static bool GetShowCanvasLeftTop(DependencyObject obj)
        {
            return (bool)obj.GetValue(ShowCanvasLeftTopProperty);
        }

        public static void SetShowCanvasLeftTop(DependencyObject obj, bool value)
        {
            obj.SetValue(ShowCanvasLeftTopProperty, value);
        }
        
        public static Style GetTextBlockStyle(DependencyObject obj)
        {
            return (Style)obj.GetValue(TextBlockStyleProperty);
        }

        public static void SetTextBlockStyle(DependencyObject obj, Style value)
        {
            obj.SetValue(TextBlockStyleProperty, value);
        }

        public static readonly DependencyProperty ShowChildSizeProperty =
            DependencyProperty.RegisterAttached("ShowChildSize", typeof(bool), typeof(PanelHelper), new PropertyMetadata(false, OnShowChildSizeChanged));

        public static readonly DependencyProperty ShowCanvasLeftTopProperty =
            DependencyProperty.RegisterAttached("ShowCanvasLeftTop", typeof(bool), typeof(PanelHelper), new PropertyMetadata(false, OnShowCanvasLeftTopChanged));

        public static readonly DependencyProperty TextBlockStyleProperty =
          DependencyProperty.RegisterAttached("TextBlockStyle", typeof(Style), typeof(PanelHelper), new PropertyMetadata(null));

        #region Private
        private static Popup GetWidthPopup(DependencyObject obj)
        {
            return (Popup)obj.GetValue(WidthPopupProperty);
        }

        private static void SetWidthPopup(DependencyObject obj, Popup value)
        {
            obj.SetValue(WidthPopupProperty, value);
        }

        private static Popup GetHeightPopup(DependencyObject obj)
        {
            return (Popup)obj.GetValue(HeightPopupProperty);
        }

        private static void SetHeightPopup(DependencyObject obj, Popup value)
        {
            obj.SetValue(HeightPopupProperty, value);
        }

        private static Popup GetLeftPopup(DependencyObject obj)
        {
            return (Popup)obj.GetValue(LeftPopupProperty);
        }

        private static void SetLeftPopup(DependencyObject obj, Popup value)
        {
            obj.SetValue(LeftPopupProperty, value);
        }

        private static Popup GetTopPopup(DependencyObject obj)
        {
            return (Popup)obj.GetValue(TopPopupProperty);
        }

        private static void SetTopPopup(DependencyObject obj, Popup value)
        {
            obj.SetValue(TopPopupProperty, value);
        }
        
        private static SizeChangedEventHandler GetSizeChanged(DependencyObject obj)
        {
            return (SizeChangedEventHandler)obj.GetValue(SizeChangedProperty);
        }

        private static void SetSizeChanged(DependencyObject obj, SizeChangedEventHandler value)
        {
            obj.SetValue(SizeChangedProperty, value);
        }

        private static EventHandler GetTopChanged(DependencyObject obj)
        {
            return (EventHandler)obj.GetValue(TopChangedProperty);
        }

        private static void SetTopChanged(DependencyObject obj, EventHandler value)
        {
            obj.SetValue(TopChangedProperty, value);
        }

        private static EventHandler GetLeftChanged(DependencyObject obj)
        {
            return (EventHandler)obj.GetValue(LeftChangedProperty);
        }

        private static void SetLeftChanged(DependencyObject obj, EventHandler value)
        {
            obj.SetValue(LeftChangedProperty, value);
        }

        public static readonly DependencyProperty WidthPopupProperty =
         DependencyProperty.RegisterAttached("WidthPopup", typeof(Popup), typeof(PanelHelper), new PropertyMetadata(null));

        public static readonly DependencyProperty HeightPopupProperty =
            DependencyProperty.RegisterAttached("HeightPopup", typeof(Popup), typeof(PanelHelper), new PropertyMetadata(null));

        public static readonly DependencyProperty LeftPopupProperty =
            DependencyProperty.RegisterAttached("LeftPopup", typeof(Popup), typeof(PanelHelper), new PropertyMetadata(null));

        public static readonly DependencyProperty TopPopupProperty =
            DependencyProperty.RegisterAttached("TopPopup", typeof(Popup), typeof(PanelHelper), new PropertyMetadata(null));

        public static readonly DependencyProperty SizeChangedProperty =
           DependencyProperty.RegisterAttached("SizeChanged", typeof(SizeChangedEventHandler), typeof(PanelHelper), new PropertyMetadata(null));

        public static readonly DependencyProperty TopChangedProperty =
            DependencyProperty.RegisterAttached("TopChanged", typeof(EventHandler), typeof(PanelHelper), new PropertyMetadata(null));

        public static readonly DependencyProperty LeftChangedProperty =
            DependencyProperty.RegisterAttached("LeftChanged", typeof(EventHandler), typeof(PanelHelper), new PropertyMetadata(null));
        #endregion

        #endregion

        private static void OnShowChildSizeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is FrameworkElement)
            {
                FrameworkElement element = d as FrameworkElement;
                bool showChildSize = PanelHelper.GetShowChildSize(element);
                if (showChildSize)
                {
                    #region Width
                    Grid topGrid = new Grid();
                    TextBlock widthTextBlock = new TextBlock();
                    topGrid.Children.Add(widthTextBlock);
                    widthTextBlock.HorizontalAlignment = HorizontalAlignment.Center;

                    Binding widthBinding = new Binding();
                    widthBinding.Source = element;
                    widthBinding.Path = new PropertyPath(FrameworkElement.ActualWidthProperty);
                    widthBinding.Mode = BindingMode.OneWay;
                    widthBinding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
                    topGrid.SetBinding(Grid.WidthProperty, widthBinding);
                    widthTextBlock.SetBinding(TextBlock.TextProperty, widthBinding); 
                    #endregion

                    #region Height
                    Grid leftGrid = new Grid();
                    TextBlock heightTextBlock = new TextBlock();
                    leftGrid.Children.Add(heightTextBlock);
                    heightTextBlock.VerticalAlignment = VerticalAlignment.Center;

                    Binding heightBinding = new Binding();
                    heightBinding.Source = element;
                    heightBinding.Path = new PropertyPath(FrameworkElement.ActualHeightProperty);
                    heightBinding.Mode = BindingMode.OneWay;
                    heightBinding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
                    heightTextBlock.SetBinding(TextBlock.TextProperty, heightBinding);
                    leftGrid.SetBinding(Grid.HeightProperty, heightBinding); 
                    #endregion

                    #region TexBlock Style
                    Style textBlockStyle = PanelHelper.GetTextBlockStyle(element);
                    if (textBlockStyle != null && textBlockStyle.TargetType == typeof(TextBlock))
                    {
                        widthTextBlock.Style = textBlockStyle;
                        heightTextBlock.Style = textBlockStyle;
                    } 
                    #endregion

                    #region Popup
                    Popup widthPopup = GetPopupInstance(element);
                    widthPopup.Placement = PlacementMode.Top;
                    widthPopup.Child = topGrid;
                    widthPopup.IsOpen = true;

                    Popup heightPopup = GetPopupInstance(element);
                    heightPopup.Placement = PlacementMode.Left;
                    heightPopup.Child = leftGrid;
                    heightPopup.IsOpen = true;

                    PanelHelper.SetWidthPopup(element, widthPopup);
                    PanelHelper.SetHeightPopup(element, heightPopup); 
                    #endregion

                    #region SizeChanged
                    SizeChangedEventHandler sizeChanged = (_sender, _e) =>
                               {
                                   widthPopup.HorizontalOffset++;
                                   widthPopup.HorizontalOffset--;

                                   heightPopup.HorizontalOffset++;
                                   heightPopup.HorizontalOffset--;
                               };

                    element.SizeChanged += sizeChanged;
                    PanelHelper.SetSizeChanged(element, sizeChanged); 
                    #endregion
                }
                else
                {
                    #region Dispose
                    Popup widthPopup = PanelHelper.GetWidthPopup(element);
                    if (widthPopup != null)
                    {
                        DisposePopup(widthPopup);
                        PanelHelper.SetWidthPopup(element, null);
                    }

                    Popup heightPopup = PanelHelper.GetHeightPopup(element);
                    if (heightPopup != null)
                    {
                        DisposePopup(heightPopup);
                        PanelHelper.SetHeightPopup(element, null);
                    }

                    SizeChangedEventHandler sizeChanged = PanelHelper.GetSizeChanged(element);
                    if (sizeChanged != null)
                    {
                        element.SizeChanged -= sizeChanged;
                        PanelHelper.SetSizeChanged(element, null);
                    } 
                    #endregion
                }
            }
            else
            {
                throw new Exception("Support only for FrameworkElement Instance");
            }
        }

        private static void OnShowCanvasLeftTopChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is FrameworkElement)
            {
                FrameworkElement element = d as FrameworkElement;
                bool showCanvasLeftTop = PanelHelper.GetShowCanvasLeftTop(element);
                if (showCanvasLeftTop)
                {
                    #region Top
                    TextBlock topTextBlock = new TextBlock();
                    topTextBlock.VerticalAlignment = VerticalAlignment.Center;                                   

                    Border topBorder = GetBorderInstance(new Thickness(1, 0, 0, 0));                  
                    topBorder.Child = topTextBlock;

                    Binding topBinding = new Binding();
                    topBinding.Source = element;
                    topBinding.Path = new PropertyPath(Canvas.TopProperty);
                    topBinding.Mode = BindingMode.OneWay;
                    topBinding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;

                    topBorder.SetBinding(Border.HeightProperty, topBinding);
                    topTextBlock.SetBinding(TextBlock.TextProperty, topBinding); 
                    #endregion

                    #region Left
                    TextBlock leftTextBlock = new TextBlock();                   
                    leftTextBlock.HorizontalAlignment = HorizontalAlignment.Center;

                    Border leftBorder = GetBorderInstance(new Thickness(0, 1, 0, 0));
                    leftBorder.Child = leftTextBlock;

                    Binding leftBinding = new Binding();
                    leftBinding.Source = element;
                    leftBinding.Path = new PropertyPath(Canvas.LeftProperty);
                    leftBinding.Mode = BindingMode.OneWay;
                    leftBinding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;

                    leftBorder.SetBinding(Border.WidthProperty, leftBinding);
                    leftTextBlock.SetBinding(TextBlock.TextProperty, leftBinding); 
                    #endregion

                    #region TextBlock Style
                    Style textBlockStyle = PanelHelper.GetTextBlockStyle(element);
                    if (textBlockStyle != null && textBlockStyle.TargetType == typeof(TextBlock))
                    {
                        topTextBlock.Style = textBlockStyle;
                        leftTextBlock.Style = textBlockStyle;
                    } 
                    #endregion

                    #region Popup
                    Popup topPopup = GetPopupInstance(element);
                    topPopup.Placement = PlacementMode.Top;
                    topPopup.Child = topBorder;
                    topPopup.IsOpen = true;
                    PopupHelper.SetCanPositionUpdate(topPopup, true);

                    Popup leftPopup = GetPopupInstance(element);
                    leftPopup.Placement = PlacementMode.Left;
                    leftPopup.Child = leftBorder;
                    leftPopup.IsOpen = true;
                    PopupHelper.SetCanPositionUpdate(leftPopup, true);

                    PanelHelper.SetTopPopup(element, topPopup);
                    PanelHelper.SetLeftPopup(element, leftPopup); 
                    #endregion

                    #region Canvas Changed
                    EventHandler topChanged = (_sender, _e) =>
                    {
                        leftPopup.HorizontalOffset++;
                        leftPopup.HorizontalOffset--;
                    };

                    EventHandler leftChanged = (_sender, _e) =>
                    {
                        topPopup.HorizontalOffset++;
                        topPopup.HorizontalOffset--;
                    };

                    DependencyPropertyDescriptor topDescriptor = DependencyPropertyDescriptor.FromProperty(Canvas.TopProperty, typeof(FrameworkElement));
                    topDescriptor.AddValueChanged(element, topChanged);

                    DependencyPropertyDescriptor leftDescriptor = DependencyPropertyDescriptor.FromProperty(Canvas.LeftProperty, typeof(FrameworkElement));
                    leftDescriptor.AddValueChanged(element, leftChanged);

                    PanelHelper.SetTopChanged(element, topChanged);
                    PanelHelper.SetLeftChanged(element, leftChanged); 
                    #endregion
                }
                else
                {
                    #region Dispose
                    Popup topPopup = PanelHelper.GetTopPopup(element);
                    if (topPopup != null)
                    {
                        PopupHelper.SetCanPositionUpdate(topPopup, false);
                        PanelHelper.SetTopPopup(element, null);
                        DisposePopup(topPopup);
                    }

                    Popup leftPopup = PanelHelper.GetLeftPopup(element);
                    if (leftPopup != null)
                    {
                        PopupHelper.SetCanPositionUpdate(leftPopup, false);
                        PanelHelper.SetTopPopup(element, null);
                        DisposePopup(leftPopup);
                    }

                    EventHandler topChanged = PanelHelper.GetTopChanged(element);
                    DependencyPropertyDescriptor topDescriptor = DependencyPropertyDescriptor.FromProperty(Canvas.TopProperty, typeof(FrameworkElement));
                    if (topChanged != null && topDescriptor != null)
                    {
                        topDescriptor.RemoveValueChanged(element, topChanged);
                        PanelHelper.SetTopChanged(element, null);
                    }

                    EventHandler leftChanged = PanelHelper.GetLeftChanged(element);
                    DependencyPropertyDescriptor leftDescriptor = DependencyPropertyDescriptor.FromProperty(Canvas.LeftProperty, typeof(FrameworkElement));
                    if (leftChanged != null && leftDescriptor != null)
                    {
                        leftDescriptor.RemoveValueChanged(element, leftChanged);
                        PanelHelper.SetLeftChanged(element, null);
                    } 
                    #endregion
                }
            }
            else
            {
                throw new Exception("Support only for FrameworkElement Instance");
            }
        }

        #region Sub Method
        private static Popup GetPopupInstance(FrameworkElement element)
        {
            Popup popup = new Popup();
            popup.PlacementTarget = element;
            popup.AllowsTransparency = true;
            popup.StaysOpen = true;

            return popup;
        }

        private static Border GetBorderInstance(Thickness borderThickness)
        {
            Border border = new Border();
            border.BorderBrush = Brushes.LightBlue;
            border.BorderThickness = borderThickness;

            return border;
        }

        private static void DisposePopup(Popup popup)
        {
            if (popup != null)
            {
                if (popup.Child is Panel)
                {
                    Panel panel = popup.Child as Panel;
                    panel.Children.Clear();
                }
                else if (popup.Child is Border)
                {
                    DisposeBorder(popup.Child as Border);
                }

                popup.Child = null;
                popup.IsOpen = false;
            }
        }

        private static void DisposeBorder(Border border)
        {
            if (border != null)
            {
                if (border.Child is Panel)
                {
                    Panel panel = border.Child as Panel;
                    panel.Children.Clear();
                }
                border.Child = null;
            }
        } 
        #endregion
    }
}
