﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;

namespace LongWpfUI.Helpers
{
    /// <summary>
    /// Attachted Class provides the Drage Function to assigned instance
    /// </summary>
    public class DragHelper : DependencyObject
    {
        #region AP
        /// <summary>
        /// Get Enable status for Draging
        /// </summary>
        /// <param name="obj">attached Item</param>
        /// <returns></returns>
        public static bool GetIsEnable(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsEnableProperty);
        }

        /// <summary>
        /// Set Enable status for Draging
        /// </summary>
        /// <param name="obj">attached Item</param>
        /// <param name="value">enable status</param>
        public static void SetIsEnable(DependencyObject obj, bool value)
        {
            obj.SetValue(IsEnableProperty, value);
        }

        /// <summary>
        /// Get Mouse Focus status for Draging
        /// </summary>
        /// <param name="obj">attached Item</param>
        /// <returns></returns>
        public static bool GetCanFocus(DependencyObject obj)
        {
            return (bool)obj.GetValue(CanFocusProperty);
        }

        /// <summary>
        /// Set Move inside Parent only for Draging
        /// </summary>
        /// <param name="obj">attached Item</param>
        /// <param name="value">focus status</param>
        public static void SetCanFocus(DependencyObject obj, bool value)
        {
            obj.SetValue(CanFocusProperty, value);
        }

        /// <summary>
        /// Get Move inside Parent only for Draging
        /// </summary>
        /// <param name="obj">attached Item</param>
        /// <returns></returns>
        public static bool GetIsInsideParent(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsInsideParentProperty);
        }

        /// <summary>
        /// Set Move inside Parent only for Draging
        /// </summary>
        /// <param name="obj">attached Item</param>
        /// <param name="value">inside parent status</param>
        public static void SetIsInsideParent(DependencyObject obj, bool value)
        {
            obj.SetValue(IsInsideParentProperty, value);
        }

        /// <summary>
        /// Get Popup, which will be supported when the item is move
        /// </summary>
        /// <param name="obj">attached Item</param>
        /// <returns></returns>
        public static Popup GetPopup(DependencyObject obj)
        {
            return (Popup)obj.GetValue(PopupProperty);
        }

        /// <summary>
        /// Get Popup, which will be supported when the item is move
        /// </summary>
        /// <param name="obj">attached Item</param>
        /// <param name="value">given popup</param>
        public static void SetPopup(DependencyObject obj, Popup value)
        {
            obj.SetValue(PopupProperty, value);
        }

        /// <summary>
        /// Get if RotateTransform in LayoutTransform is supported
        /// </summary>
        /// <param name="obj">given Item</param>
        /// <returns></returns>
        public static bool GetSupportRotateTransform(DependencyObject obj)
        {
            return (bool)obj.GetValue(SupportRotateTransformProperty);
        }

        /// <summary>
        /// Set if  RotateTransform in LayoutTransform is supported
        /// </summary>
        /// <param name="obj">given Item</param>
        /// <param name="value">given Status</param>
        public static void SetSupportRotateTransform(DependencyObject obj, bool value)
        {
            obj.SetValue(SupportRotateTransformProperty, value);
        }

        /// <summary>
        /// Get if MouseDown Event is handled after it finished, Default True
        /// </summary>
        /// <param name="obj">given Item</param>
        /// <returns></returns>
        public static bool GetIsHandledMouseDown(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsHandledMouseDownProperty);
        }

        /// <summary>
        /// Set if MouseDown Event is handled after it finished, Default True
        /// </summary>
        /// <param name="obj">given Item</param>
        /// <param name="value">given Status</param>
        public static void SetIsHandledMouseDown(DependencyObject obj, bool value)
        {
            obj.SetValue(IsHandledMouseDownProperty, value);
        }

        public static readonly DependencyProperty IsEnableProperty =
            DependencyProperty.RegisterAttached("IsEnable", typeof(bool), typeof(DragHelper), new PropertyMetadata(false, OnIsEnableChanged));

        public static readonly DependencyProperty CanFocusProperty =
            DependencyProperty.RegisterAttached("CanFocus", typeof(bool), typeof(DragHelper), new PropertyMetadata(false));

        public static readonly DependencyProperty IsInsideParentProperty =
            DependencyProperty.RegisterAttached("IsInsideParent", typeof(bool), typeof(DragHelper), new PropertyMetadata(false));

        public static readonly DependencyProperty PopupProperty =
            DependencyProperty.RegisterAttached("Popup", typeof(Popup), typeof(DragHelper), new PropertyMetadata(null));

        public static readonly DependencyProperty SupportRotateTransformProperty =
           DependencyProperty.RegisterAttached("SupportRotateTransform", typeof(bool), typeof(DragHelper), new PropertyMetadata(false));

        public static readonly DependencyProperty IsHandledMouseDownProperty =
          DependencyProperty.RegisterAttached("IsHandledMouseDown", typeof(bool), typeof(DragHelper), new PropertyMetadata(true));

        #region Private
        private static MouseButtonEventHandler GetMouseDownHandler(DependencyObject obj)
        {
            return (MouseButtonEventHandler)obj.GetValue(MouseDownHandlerProperty);
        }

        private static void SetMouseDownHandler(DependencyObject obj, MouseButtonEventHandler value)
        {
            obj.SetValue(MouseDownHandlerProperty, value);
        }

        private static MouseEventHandler GetMouseMoveHandler(DependencyObject obj)
        {
            return (MouseEventHandler)obj.GetValue(MouseMoveHandlerProperty);
        }

        private static void SetMouseMoveHandler(DependencyObject obj, MouseEventHandler value)
        {
            obj.SetValue(MouseMoveHandlerProperty, value);
        }

        private static MouseButtonEventHandler GetMouseUpHandler(DependencyObject obj)
        {
            return (MouseButtonEventHandler)obj.GetValue(MouseUpHandlerProperty);
        }

        private static void SetMouseUpHandler(DependencyObject obj, MouseButtonEventHandler value)
        {
            obj.SetValue(MouseUpHandlerProperty, value);
        }

        private static Canvas GetParent(DependencyObject obj)
        {
            return (Canvas)obj.GetValue(ParentProperty);
        }

        private static void SetParent(DependencyObject obj, Canvas value)
        {
            obj.SetValue(ParentProperty, value);
        }

        public static readonly DependencyProperty MouseDownHandlerProperty =
            DependencyProperty.RegisterAttached("MouseDownHandler", typeof(MouseButtonEventHandler), typeof(DragHelper), new PropertyMetadata(null));

        public static readonly DependencyProperty MouseMoveHandlerProperty =
            DependencyProperty.RegisterAttached("MouseMoveHandler", typeof(MouseEventHandler), typeof(DragHelper), new PropertyMetadata(null));

        public static readonly DependencyProperty MouseUpHandlerProperty =
            DependencyProperty.RegisterAttached("MouseUpHandler", typeof(MouseButtonEventHandler), typeof(DragHelper), new PropertyMetadata(null));

        public static readonly DependencyProperty ParentProperty =
           DependencyProperty.RegisterAttached("Parent", typeof(Canvas), typeof(DragHelper), new PropertyMetadata(null));
        #endregion
                
        #endregion

        #region Private Field
        private static double initialLeft;
        private static double initialTop;
        private static bool isInsideParent;
        private static bool supportRotateTranform;
        private static Popup popup;
        private static Canvas parent;
        private static Point startPoint;
        private static FrameworkElement currentElement; 
        #endregion
      
        #region Property Changed
        private static void OnIsEnableChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is FrameworkElement)
            {
                FrameworkElement element = d as FrameworkElement;
                
                bool isEnable = DragHelper.GetIsEnable(element);
                if (isEnable)
                {
                    #region Event Handler      
                    MouseButtonEventHandler mouseDown = (_sender, _e) =>
                    {
                        currentElement = element;
                        isInsideParent = DragHelper.GetIsInsideParent(currentElement);
                        supportRotateTranform = DragHelper.GetSupportRotateTransform(currentElement);
                        popup = DragHelper.GetPopup(currentElement);
                        parent = DragHelper.GetParent(currentElement);
                        initialLeft = Canvas.GetLeft(currentElement);
                        initialTop = Canvas.GetTop(currentElement);
                        startPoint = _e.GetPosition(null);

                        bool canFocus = DragHelper.GetCanFocus(currentElement);
                        if (canFocus)
                        {
                            Mouse.Capture(currentElement);
                        }

                        bool isHandledMouseDown = DragHelper.GetIsHandledMouseDown(currentElement);
                        if (isHandledMouseDown)
                        {
                            _e.Handled = true; 
                        }                                             
                    };

                    MouseButtonEventHandler mouseUp = (_sender, _e) =>
                    {
                        if (currentElement != null)
                        {
                            bool canFocus = DragHelper.GetCanFocus(currentElement);
                            if (canFocus && currentElement.IsMouseCaptured)
                            {
                                currentElement.ReleaseMouseCapture();
                            }
                            currentElement = null;                            
                        }

                        popup = null;
                        parent = null; 
                        //e.Handled is not allowed to be true => block other Event, e.g. Button
                    };

                    MouseEventHandler mouseMove = (_sender, _e) =>
                    {
                        if (currentElement == null)
                        {
                            return;
                        }

                        Point currentPoint = _e.GetPosition(null);
                        double newLeft = initialLeft + currentPoint.X - startPoint.X;
                        double newTop = initialTop + currentPoint.Y - startPoint.Y;

                        if (parent != null && isInsideParent)
                        {
                            double leftOffset = currentElement.ActualWidth;
                            double topOffset = currentElement.ActualHeight;
                            //only Support is set
                            if (supportRotateTranform && currentElement.LayoutTransform is RotateTransform)
                            {
                                RotateTransform rotateTransform = currentElement.LayoutTransform as RotateTransform;
                                int angle = (int)rotateTransform.Angle;
                                if (angle == 90 || angle == 270)
                                {
                                    leftOffset = currentElement.ActualHeight;
                                    topOffset = currentElement.ActualWidth;
                                }
                            }

                            if ((newLeft + leftOffset) <= parent.ActualWidth && newLeft >= 0)
                            {                               
                                Canvas.SetLeft(currentElement, newLeft);
                            }

                            if ((newTop + topOffset) <= parent.ActualHeight && newTop >= 0)
                            {                                
                                Canvas.SetTop(currentElement, newTop);
                            }
                        }
                        else
                        {
                            Canvas.SetLeft(currentElement, newLeft);
                            Canvas.SetTop(currentElement, newTop);
                        }

                        //update Popup Location
                        if (popup != null)
                        {
                            popup.HorizontalOffset++;
                            popup.HorizontalOffset--;
                        }
                        
                        _e.Handled = true;
                    }; 
                    #endregion

                    #region Set event
                    element.PreviewMouseDown += mouseDown;                    
                    element.PreviewMouseMove += mouseMove;
                    //prevent strange behavior when element is lost focus
                    Window topWindow = Window.GetWindow(element);
                    if (topWindow != null)
                    {
                        topWindow.PreviewMouseUp += mouseUp;
                    }
                    else
                    {
                        element.PreviewMouseUp += mouseUp;
                    }
                    #endregion

                    #region Set Property

                    #region Get & Set Parent
                    DependencyObject parentObject = VisualTreeHelper.GetParent(element);
                    //normal Panel
                    if (parentObject is Canvas)
                    {
                        DragHelper.SetParent(element, parentObject as Canvas);
                    }
                    //in case of ItemsControl
                    else if (parentObject is ItemsControl)
                    {
                        ItemsControl itemsControl = parentObject as ItemsControl;
                        Canvas panel = ControlHelper.GetChildControl<Canvas>(itemsControl);
                        DragHelper.SetParent(element, panel);
                    }    
                    #endregion
                 
                    DragHelper.SetMouseDownHandler(element, mouseDown);
                    DragHelper.SetMouseUpHandler(element, mouseUp);
                    DragHelper.SetMouseMoveHandler(element, mouseMove);  
                    #endregion

                    #region Check Initial Value
                    CheckInitialLeftTop(element);
                    #endregion
                }
                else
                {
                    #region Dispose

                    MouseButtonEventHandler mouseDown = DragHelper.GetMouseDownHandler(element);
                    if (mouseDown != null)
                    {
                        element.PreviewMouseDown -= mouseDown;
                    }

                    MouseButtonEventHandler mouseUp = DragHelper.GetMouseUpHandler(element);
                    if (mouseUp != null)
                    {
                        Window topWindow = Window.GetWindow(element);
                        if (topWindow != null)
                        {
                            topWindow.PreviewMouseUp += mouseUp;
                        }
                        else
                        {
                            element.PreviewMouseUp += mouseUp;
                        }
                    }

                    MouseEventHandler mouseMove = DragHelper.GetMouseMoveHandler(element);
                    if (mouseMove != null)
                    {
                        element.PreviewMouseMove -= mouseMove;
                    }
                    
                    DragHelper.SetParent(element, null);
                    DragHelper.SetMouseDownHandler(element, null);
                    DragHelper.SetMouseUpHandler(element, null);
                    DragHelper.SetMouseMoveHandler(element, null); 
                    #endregion
                }
            }
            else
            {
                throw new Exception("Support only for FrameworkElement instance");
            }
        }
        #endregion

        #region Public Method
        /// <summary>
        /// Check and assign initial Left Top Property of given Element to 0, if the Property were not assigned
        /// </summary>
        /// <param name="element">given element</param>
        public static void CheckInitialLeftTop(FrameworkElement element)
        {
            if (element == null)
            {
                return;
            }

            double left = Canvas.GetLeft(element);
            double top = Canvas.GetTop(element);
            if (double.IsNaN(left))
            {
                Canvas.SetLeft(element, 0.0);
            }
            if (double.IsNaN(top))
            {
                Canvas.SetTop(element, 0.0);
            }
        }
        #endregion
    }
}
