﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace LongWpfUI.Helpers
{
    public class ContainerHelper : DependencyObject
    {
        #region AP
        public static bool GetFocusFirstTextBox(DependencyObject obj)
        {
            return (bool)obj.GetValue(FocusFirstTextBoxProperty);
        }

        public static void SetFocusFirstTextBox(DependencyObject obj, bool value)
        {
            obj.SetValue(FocusFirstTextBoxProperty, value);
        }        

        public static readonly DependencyProperty FocusFirstTextBoxProperty =
            DependencyProperty.RegisterAttached("FocusFirstTextBox", typeof(bool), typeof(ContainerHelper), new PropertyMetadata(false, OnFocusFirstTextBoxChanged));       

        #endregion

        #region PropertyChanged Callback
        private static void OnFocusFirstTextBoxChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            FrameworkElement element = d as FrameworkElement;
            if (element != null)
            {
                bool focusFirstTextBox = ContainerHelper.GetFocusFirstTextBox(element);
                if (focusFirstTextBox)
                {
                    element.Loaded += Element_Loaded_Focus;
                }
                else
                {
                    element.Loaded -= Element_Loaded_Focus;
                }
            }
        }              
        #endregion

        #region Event Handler
        private static void Element_Loaded_Focus(object sender, RoutedEventArgs e)
        {
            FrameworkElement element = sender as FrameworkElement;
            if (element == null)
            {
                return;
            }

            //Check and get first TextBox
            TextBox textBox = ControlHelper.GetChildControl<TextBox>(element);
            if (textBox != null)
            {
                Keyboard.Focus(textBox);
            }
        }    
        #endregion       
    }
}
