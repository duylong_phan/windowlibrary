﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongWpfUI.Helpers
{
    public static class EnumHelper
    {
        /// <summary>
        /// Get the Collection of Enum Option
        /// </summary>
        /// <param name="type">given Enum Type</param>
        /// <returns></returns>
        public static List<string> GetItemCollection(Type type)
        {
            List<string> collection = new List<string>();

            if (type != null && type.IsEnum)
            {
                string[] items = Enum.GetNames(type);
                if (items != null)
                {
                    collection.AddRange(items);
                }
            }

            return collection;
        }

    }
}
