﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace LongWpfUI.Helpers
{
    public static class ControlHelper
    {
        #region Find Child and Children Control
        /// <summary>
        /// Get the first Control, with given Type. Otherwise null return
        /// </summary>
        /// <typeparam name="T">Given Type</typeparam>
        /// <param name="parent">Parent Control</param>
        /// <returns>Found Control</returns>
        public static T GetChildControl<T>(DependencyObject parent)
           where T : FrameworkElement
        {
            return GetChildControl<T>(parent, null);
        }

        /// <summary>
        /// Get the first Control with given Type and given Tag. Otherwise null return
        /// </summary>
        /// <typeparam name="T">Given Type</typeparam>
        /// <param name="parent">Parent Control</param>
        /// <param name="controlTagName">String in Tag Property</param>
        /// <returns>Found Control</returns>
        public static T GetChildControl<T>(DependencyObject parent, string controlTagName)
            where T : FrameworkElement
        {
            T foundChild = null;

            bool findControlName = false;
            if (!string.IsNullOrEmpty(controlTagName))  //NOT
            {
                findControlName = true;
            }

            if (parent != null && VisualTreeHelper.GetChildrenCount(parent) > 0)
            {
                //Find in Current Child
                int childCount = VisualTreeHelper.GetChildrenCount(parent);
                for (int i = 0; i < childCount; i++)
                {
                    T tmpChild = VisualTreeHelper.GetChild(parent, i) as T;
                    if (tmpChild != null)
                    {
                        if (findControlName)
                        {
                            string tag = tmpChild.Tag as string;
                            if (tag != null && tag == controlTagName)
                            {
                                foundChild = tmpChild;
                                break;
                            }
                        }
                        else
                        {
                            foundChild = tmpChild;
                            break;
                        }
                    }
                }

                //not found, find in SubChildren
                if (foundChild == null)
                {
                    for (int i = 0; i < childCount; i++)
                    {
                        DependencyObject tmpChild = VisualTreeHelper.GetChild(parent, i);
                        foundChild = GetChildControl<T>(tmpChild, controlTagName);
                        if (foundChild != null)
                        {
                            break;
                        }
                    }
                }
            }

            return foundChild;
        }

        /// <summary>
        /// Get a Collection of all the given Type Childen
        /// </summary>
        /// <typeparam name="T">Given Type</typeparam>
        /// <param name="parent">Parent Object</param>
        /// <returns></returns>
        public static List<T> GetChildrenControl<T>(DependencyObject parent)
             where T : FrameworkElement
        {
            List<T> children = new List<T>();

            if (parent != null && VisualTreeHelper.GetChildrenCount(parent) > 0)
            {
                int childCout = VisualTreeHelper.GetChildrenCount(parent);
                for (int i = 0; i < childCout; i++)
                {
                    T tmpChild = VisualTreeHelper.GetChild(parent, i) as T;
                    if (tmpChild != null)
                    {
                        children.Add(tmpChild);
                    }
                    else
                    {
                        DependencyObject tmpObject = VisualTreeHelper.GetChild(parent, i);
                        if (tmpObject != null)
                        {
                            List<T> subChildren = GetChildrenControl<T>(tmpObject);
                            if (subChildren != null && subChildren.Count > 0)
                            {
                                children.AddRange(subChildren);
                            }
                        }
                    }
                }
            }

            return children;
        } 
        #endregion

        #region Find Parent
        /// <summary>
        /// Get first Parent with given Type from given child
        /// </summary>
        /// <typeparam name="T">Parent Type</typeparam>
        /// <param name="child">given Type</param>
        /// <returns></returns>
        public static T GetParent<T>(DependencyObject child)
            where T : DependencyObject
        {
            //get parent item
            DependencyObject parentObject = VisualTreeHelper.GetParent(child);

            //we've reached the end of the tree
            if (parentObject == null) return null;

            //check if the parent matches the type we're looking for
            T parent = parentObject as T;
            if (parent != null)
                return parent;
            else
                return GetParent<T>(parentObject);
        } 
        #endregion

        #region Take Image
        /// <summary>
        /// Take Image of a Control and save with given FilePath
        /// </summary>
        /// <param name="element">given Control</param>
        /// <param name="filePath">given FilePath</param>
        /// <returns></returns>
        /// <exception cref="Null Parameter or invalid Path"/>
        public static string TakeImage(FrameworkElement element, string filePath)
        {
            if (element == null)
            {
                throw new Exception("Given Element is null");
            }

            if (string.IsNullOrEmpty(filePath))
            {
                throw new Exception("Given File Path is null or empty");
            }

            string directory = Path.GetDirectoryName(filePath);
            if (Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            BitmapSource source = TakeImageOnBitmapSource(element);
            using (FileStream stream = File.Create(filePath))
            {
                PngBitmapEncoder pngImage = new PngBitmapEncoder();
                pngImage.Frames.Add(BitmapFrame.Create(source));
                pngImage.Save(stream);                
            }

            return filePath;
        }
        
        /// <summary>
        /// Take Image of a given Control, and given a MemoryStream back
        /// </summary>
        /// <param name="element">given Control</param>
        /// <returns>MemoryStream</returns>
        /// <exception cref="Null Parameter"/>
        public static MemoryStream TakeImage(FrameworkElement element)
        {
            if (element == null)
            {
                throw new Exception("Null Parameter");
            }

            BitmapSource source = TakeImageOnBitmapSource(element);            
            PngBitmapEncoder pngImage = new PngBitmapEncoder();
            MemoryStream memoryStream = new MemoryStream();

            pngImage.Frames.Add(BitmapFrame.Create(source));
            pngImage.Save(memoryStream);

            return memoryStream;
        }

        /// <summary>
        /// Take a Image of given Control and given back a BitmapImage, better is TakeImageOnBitmapSource()
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        /// <exception cref="Null Parameter"/>
        public static BitmapImage TakeImageOnBitmap(FrameworkElement element)
        {
            if (element == null)
            {
                throw new Exception("Null Parameter");
            }

            BitmapImage bitmap = new BitmapImage();
            using (MemoryStream memoryStream = TakeImage(element))
            {
                bitmap.BeginInit();
                bitmap.CacheOption = BitmapCacheOption.OnLoad;  //sehr wichtig, sonst wird die Datei nicht speichern bevor die Stream schließen
                bitmap.StreamSource = memoryStream;
                bitmap.EndInit();
            }

            return bitmap;
        }
        
        /// <summary>
        /// Take a Image of given Control and copy it into Clipboard
        /// </summary>
        /// <param name="element"></param>
        public static void TakeImageOnClipBoard(FrameworkElement element)
        {
            if (element == null)
            {
                throw new Exception("Null Parameter");
            }

            BitmapSource source = TakeImageOnBitmapSource(element);
            Clipboard.SetImage(source);
        }

        /// <summary>
        /// Take a image of given control and given back a BitmapSource
        /// </summary>
        /// <param name="element">given Elelemt</param>
        /// <param name="dpi">given dpi</param>
        /// <param name="scale">given scale</param>
        /// <returns></returns>
        public static BitmapSource TakeImageOnBitmapSource(FrameworkElement element, int dpi = 96, double scale = 1)
        {
            if (element == null)
            {
                throw new Exception("Null Parameter");
            }

            double width = element.ActualWidth * scale;
            double height = element.ActualHeight * scale;
            RenderTargetBitmap renderBitmap = new RenderTargetBitmap((int)width, (int)height, dpi, dpi, PixelFormats.Pbgra32);

            ModifyPosition(element);
            renderBitmap.Render(element);
            ModifyPositionBack(element);
            return renderBitmap;
        }
        
        /// <summary>
        /// move the Position of FrameworkElement to top left
        /// </summary>
        /// <param name="element"></param>
        public static void ModifyPosition(FrameworkElement element)
        {
            if (element != null)
            {
                Size fs = new Size(element.ActualWidth + element.Margin.Left + element.Margin.Right, element.ActualHeight + element.Margin.Top + element.Margin.Bottom);
                element.Measure(fs);
                element.Arrange(new Rect(-element.Margin.Left, -element.Margin.Top, fs.Width, fs.Height));
            }
        }

        /// <summary>
        /// Reset original Position
        /// </summary>
        /// <param name="element"></param>
        public static void ModifyPositionBack(FrameworkElement element)
        {
            if (element != null)
            {
                element.Measure(new Size());
            }            
        }
        #endregion
    }
}
