﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace LongWpfUI.Helpers
{
    public class SliderHelper : DependencyObject
    {
        #region AP
        /// <summary>
        /// Get if Value in Range can be checked
        /// </summary>
        /// <param name="obj">given Item</param>
        /// <returns></returns>
        [AttachedPropertyBrowsableForType(typeof(Slider))]
        public static bool GetCheckValueInRange(DependencyObject obj)
        {
            return (bool)obj.GetValue(CheckValueInRangeProperty);
        }

        public static void SetCheckValueInRange(DependencyObject obj, bool value)
        {
            obj.SetValue(CheckValueInRangeProperty, value);
        }

        /// <summary>
        /// Get if Value in Range is correct, it will be only valid if CheckValueInRange = true
        /// </summary>
        /// <param name="obj">given Item</param>
        /// <returns></returns>
        [AttachedPropertyBrowsableForType(typeof(Slider))]
        public static bool GetIsValueInRange(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsValueInRangeProperty);
        }

        private static void SetIsValueInRange(DependencyObject obj, bool value)
        {
            obj.SetValue(IsValueInRangeProperty, value);
        }

        public static readonly DependencyProperty CheckValueInRangeProperty =
            DependencyProperty.RegisterAttached("CheckValueInRange", typeof(bool), typeof(SliderHelper), new PropertyMetadata(false, OnCheckValueInRangeChanged));

        public static readonly DependencyProperty IsValueInRangeProperty =
            DependencyProperty.RegisterAttached("IsValueInRange", typeof(bool), typeof(SliderHelper), new PropertyMetadata(true));

        #region Private
        private static RoutedPropertyChangedEventHandler<double> GetValueChanged(DependencyObject obj)
        {
            return (RoutedPropertyChangedEventHandler<double>)obj.GetValue(ValueChangedProperty);
        }

        private static void SetValueChanged(DependencyObject obj, RoutedPropertyChangedEventHandler<double> value)
        {
            obj.SetValue(ValueChangedProperty, value);
        }


        public static readonly DependencyProperty ValueChangedProperty =
            DependencyProperty.RegisterAttached("ValueChanged", typeof(RoutedPropertyChangedEventHandler<double>), typeof(SliderHelper), new PropertyMetadata(null));
        #endregion


        #endregion

        #region Property Changed
        private static void OnCheckValueInRangeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is Slider)
            {
                Slider slider = d as Slider;
                bool checkValueInRange = SliderHelper.GetCheckValueInRange(slider);
                if (checkValueInRange)
                {
                    RoutedPropertyChangedEventHandler<double> valueChanged = (_sender, _e) =>
                    {
                        if (slider.Value >= slider.SelectionStart && slider.Value <= slider.SelectionEnd)
                        {
                            SliderHelper.SetIsValueInRange(slider, true);
                        }
                        else
                        {
                            SliderHelper.SetIsValueInRange(slider, false);
                        }
                    };

                    slider.ValueChanged += valueChanged;
                    SliderHelper.SetValueChanged(slider, valueChanged);
                }
                else
                {
                    RoutedPropertyChangedEventHandler<double> valueChanged = SliderHelper.GetValueChanged(slider);
                    if (valueChanged != null)
                    {
                        slider.ValueChanged -= valueChanged;
                        SliderHelper.SetValueChanged(slider, null);
                        SliderHelper.SetIsValueInRange(slider, true);
                    }
                }
            }
            else
            {
                throw new Exception("Support only for Slider Instance");
            }
        } 
        #endregion
    }
}
