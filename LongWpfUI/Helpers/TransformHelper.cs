﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace LongWpfUI.Helpers
{
    public enum TransformSourceOption
    {
        Layout,
        Render
    }

    public class TransformHelper : DependencyObject
    {
        #region AP
        /// <summary>
        /// Get the Source of the Transform
        /// </summary>
        /// <param name="obj">given Item</param>
        /// <returns></returns>
        public static TransformSourceOption GetSource(DependencyObject obj)
        {
            return (TransformSourceOption)obj.GetValue(SourceProperty);
        }

        /// <summary>
        /// Set the Source of the Transform
        /// </summary>
        /// <param name="obj">given Item</param>
        /// <param name="value"></param>
        public static void SetSource(DependencyObject obj, TransformSourceOption value)
        {
            obj.SetValue(SourceProperty, value);
        }

        /// <summary>
        /// Get the Status of the 90 Rotation Transform
        /// </summary>
        /// <param name="obj">given Item</param>
        /// <returns></returns>
        public static bool GetEnable90Rotation(DependencyObject obj)
        {
            return (bool)obj.GetValue(Enable90RotationProperty);
        }

        /// <summary>
        /// the Status of the 90 Rotation Transform
        /// </summary>
        /// <param name="obj">given Item</param>
        /// <param name="value"></param>
        public static void SetEnable90Rotation(DependencyObject obj, bool value)
        {
            obj.SetValue(Enable90RotationProperty, value);
        }
        
        /// <summary>
        /// Get Status of using Animation
        /// </summary>
        /// <param name="obj">given Item</param>
        /// <returns></returns>
        public static bool GetUseAnimation(DependencyObject obj)
        {
            return (bool)obj.GetValue(UseAnimationProperty);
        }

        /// <summary>
        /// Set Status of using Animation
        /// </summary>
        /// <param name="obj">given Item</param>
        /// <param name="value"></param>
        public static void SetUseAnimation(DependencyObject obj, bool value)
        {
            obj.SetValue(UseAnimationProperty, value);
        }

        /// <summary>
        /// Get the amount of Milisecond are used for the animation
        /// </summary>
        /// <param name="obj">given Item</param>
        /// <returns></returns>
        public static double GetMsAnimationDuration(DependencyObject obj)
        {
            return (double)obj.GetValue(MsAnimationDurationProperty);
        }

        /// <summary>
        /// Set the amount of Milisecond are used for the animation
        /// </summary>
        /// <param name="obj">given Item</param>
        /// <param name="value"></param>
        public static void SetMsAnimationDuration(DependencyObject obj, double value)
        {
            obj.SetValue(MsAnimationDurationProperty, value);
        }

        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.RegisterAttached("Source", typeof(TransformSourceOption), typeof(TransformHelper), new PropertyMetadata(TransformSourceOption.Layout));
        
        public static readonly DependencyProperty Enable90RotationProperty =
            DependencyProperty.RegisterAttached("Enable90Rotation", typeof(bool), typeof(TransformHelper), new PropertyMetadata(false, OnEnable90RotationChanged));

        public static readonly DependencyProperty UseAnimationProperty =
         DependencyProperty.RegisterAttached("UseAnimation", typeof(bool), typeof(TransformHelper), new PropertyMetadata(false));

        public static readonly DependencyProperty MsAnimationDurationProperty =
           DependencyProperty.RegisterAttached("MsAnimationDuration", typeof(double), typeof(TransformHelper), new PropertyMetadata(0.0));

        #region Private
        private static MouseButtonEventHandler GetDoubleClickRotationHandler(DependencyObject obj)
        {
            return (MouseButtonEventHandler)obj.GetValue(DoubleClickRotationHandlerProperty);
        }

        private static void SetDoubleClickRotationHandler(DependencyObject obj, MouseButtonEventHandler value)
        {
            obj.SetValue(DoubleClickRotationHandlerProperty, value);
        }


        public static readonly DependencyProperty DoubleClickRotationHandlerProperty =
            DependencyProperty.RegisterAttached("DoubleClickRotationHandler", typeof(MouseButtonEventHandler), typeof(TransformHelper), new PropertyMetadata(null));
        #endregion

        #endregion

        #region Property Changed
        private static void OnEnable90RotationChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is FrameworkElement)
            {
                FrameworkElement element = d as FrameworkElement;
                bool enable90Rotation = TransformHelper.GetEnable90Rotation(element);
                if (enable90Rotation)
                {
                    MouseButtonEventHandler doubleClickRotationHandler = (_sender, _e) =>
                    {
                        if (_e.ClickCount == 2 && element.LayoutTransform is RotateTransform)
                        {
                            RotateTransform rotateTransform = element.LayoutTransform as RotateTransform;
                            bool useAnimation = TransformHelper.GetUseAnimation(element);
                            double msAnimationDuration = TransformHelper.GetMsAnimationDuration(element);

                            int currentAngle = (int)rotateTransform.Angle;
                            currentAngle = (currentAngle == 360) ? 0 : currentAngle;
                            int nextAngle = currentAngle + 90;

                            if (useAnimation)
                            {                               
                                DoubleAnimation angleAnimation = new DoubleAnimation();
                                angleAnimation.Duration = new Duration(TimeSpan.FromMilliseconds(msAnimationDuration));
                                angleAnimation.From = currentAngle;
                                angleAnimation.To = nextAngle;
                                rotateTransform.BeginAnimation(RotateTransform.AngleProperty, angleAnimation);                                
                            }
                            //update MVVM
                            rotateTransform.Angle = (nextAngle == 360) ? 0 : nextAngle;
                        }
                    };

                    element.PreviewMouseDown += doubleClickRotationHandler;
                    TransformHelper.SetDoubleClickRotationHandler(element, doubleClickRotationHandler);
                }
                else
                {
                    MouseButtonEventHandler doubleClickRotationHandler = TransformHelper.GetDoubleClickRotationHandler(element);
                    if (doubleClickRotationHandler != null)
                    {
                        element.PreviewMouseDown -= doubleClickRotationHandler;
                        TransformHelper.SetDoubleClickRotationHandler(element, null);
                    }
                }
            }
            else
            {
                throw new Exception("Support only for FrameworkElement instance");
            }
        } 
        #endregion
    }
}
