﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace LongWpfUI.Helpers
{
    /// <summary>
    /// Attached Property class support for Resize Operations
    /// </summary>
    public class ResizeHelper : DependencyObject
    {
        #region Const Value
        public const double DefaultThumbSize = 8;
        public const double DefaultMinSize = 5; 
        #endregion

        #region AP
        public static bool GetIsEnable(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsEnableProperty);
        }

        public static void SetIsEnable(DependencyObject obj, bool value)
        {
            obj.SetValue(IsEnableProperty, value);
        }

        public static bool GetSupportCrossResize(DependencyObject obj)
        {
            return (bool)obj.GetValue(SupportCrossResizeProperty);
        }

        public static void SetSupportCrossResize(DependencyObject obj, bool value)
        {
            obj.SetValue(SupportCrossResizeProperty, value);
        }

        public static Style GetThumbStyle(DependencyObject obj)
        {
            return (Style)obj.GetValue(ThumbStyleProperty);
        }

        public static void SetThumbStyle(DependencyObject obj, Style value)
        {
            obj.SetValue(ThumbStyleProperty, value);
        }

        public static readonly DependencyProperty IsEnableProperty =
            DependencyProperty.RegisterAttached("IsEnable", typeof(bool), typeof(ResizeHelper), new PropertyMetadata(false, OnIsEnableChanged));

        public static readonly DependencyProperty SupportCrossResizeProperty =
           DependencyProperty.RegisterAttached("SupportCrossResize", typeof(bool), typeof(ResizeHelper), new PropertyMetadata(false));

        public static readonly DependencyProperty ThumbStyleProperty =
           DependencyProperty.RegisterAttached("ThumbStyle", typeof(Style), typeof(ResizeHelper), new PropertyMetadata(null));

        #region Private
        /// <summary>
        /// Get currrent Popup, which attached to attached Item, can be used to support the update of DragHelper
        /// </summary>
        /// <param name="obj">attached Item</param>
        /// <returns></returns>
        public static Popup GetPopup(DependencyObject obj)
        {
            return (Popup)obj.GetValue(PopupProperty);
        }

        /// <summary>
        /// Set current Popup
        /// </summary>
        /// <param name="obj">attached Item</param>
        /// <param name="value">given popup</param>
        private static void SetPopup(DependencyObject obj, Popup value)
        {
            obj.SetValue(PopupProperty, value);
        }


        public static readonly DependencyProperty PopupProperty =
            DependencyProperty.RegisterAttached("Popup", typeof(Popup), typeof(ResizeHelper), new PropertyMetadata(null));


        #endregion 

        #endregion     

        #region Property Changed
        private static void OnIsEnableChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is FrameworkElement)
            {
                FrameworkElement element = d as FrameworkElement;
                bool isEnable = ResizeHelper.GetIsEnable(element);
                if (isEnable)
                {
                    List<Thumb> collection = GetThumbCollection(element);
                    Rectangle rect = GetRectangleInstance(element);
                    Grid grid = GetGridInstance(element);

                    grid.Children.Add(rect);
                    foreach (Thumb item in collection)
                    {
                        grid.Children.Add(item);
                    }

                    Popup popup = GetPopupInstance(element);                    
                    popup.Child = grid;
                    popup.IsOpen = true;
                    PopupHelper.SetCanPositionUpdate(popup, true);                    
                    ResizeHelper.SetPopup(element, popup);

                    DragHelper.CheckInitialLeftTop(element);
                }
                else
                {
                    Popup popup = ResizeHelper.GetPopup(element);
                    if (popup != null)
                    {
                        popup.IsOpen = false;

                        Grid grid = popup.Child as Grid;
                        if (grid != null)
                        {
                            grid.Children.Clear();
                        }
                        popup.Child = null;
                        popup.PlacementTarget = null;

                        PopupHelper.SetCanPositionUpdate(popup, false);
                        ResizeHelper.SetPopup(element, null);
                    }
                }
            }
            else
            {
                throw new Exception("Only support for FrameworkElement instance");
            }
        } 
        #endregion

        #region Private Method
        private static List<Thumb> GetThumbCollection(FrameworkElement element)
        {
            #region Style
            Style style = ResizeHelper.GetThumbStyle(element);

            //default Style
            if (style == null)
            {
                ControlTemplate controlTemplate = new ControlTemplate(typeof(Thumb));
                var ellipse = new FrameworkElementFactory(typeof(Ellipse));
                ellipse.SetValue(Ellipse.FillProperty, Brushes.SteelBlue);
                controlTemplate.VisualTree = ellipse;
                Setter template = new Setter(Thumb.TemplateProperty, controlTemplate);
                style = new Style(typeof(Thumb));

                style.Setters.Add(template);
            }
            #endregion

            #region Event Handler
            DragDeltaEventHandler dragTop = (_sender, _e) =>
            {
                ResizeTop(element, _e);
            };
            DragDeltaEventHandler dragBottom = (_sender, _e) =>
            {
                ResizeBottom(element, _e);
            };

            DragDeltaEventHandler dragLeft = (_sender, _e) =>
            {
                ResizeLeft(element, _e);
            };

            DragDeltaEventHandler dragRight = (_sender, _e) =>
            {
                ResizeRight(element, _e);
            }; 
            #endregion

            #region Basic Resize
            Thumb top = GetThumbInstance(DefaultThumbSize, dragTop, HorizontalAlignment.Center, VerticalAlignment.Top, Cursors.SizeNS, style);
            Thumb bottom = GetThumbInstance(DefaultThumbSize, dragBottom, HorizontalAlignment.Center, VerticalAlignment.Bottom, Cursors.SizeNS, style);
            Thumb left = GetThumbInstance(DefaultThumbSize, dragLeft, HorizontalAlignment.Left, VerticalAlignment.Center, Cursors.SizeWE, style);
            Thumb right = GetThumbInstance(DefaultThumbSize, dragRight, HorizontalAlignment.Right, VerticalAlignment.Center, Cursors.SizeWE, style);

            List<Thumb> collection = new List<Thumb>() { top, bottom, left, right }; 
            #endregion

            #region Cross Resize Support
            bool supportCrossResize = ResizeHelper.GetSupportCrossResize(element);
            if (supportCrossResize)
            {
                DragDeltaEventHandler dragTopLeft = (_sender, _e) =>
                {
                    ResizeTop(element, _e);
                    ResizeLeft(element, _e);
                };

                DragDeltaEventHandler dragTopRight = (_sender, _e) =>
                {
                    ResizeTop(element, _e);
                    ResizeRight(element, _e);
                };

                DragDeltaEventHandler dragBottomLeft = (_sender, _e) =>
                {
                    ResizeBottom(element, _e);
                    ResizeLeft(element, _e);
                };

                DragDeltaEventHandler dragBottomRight = (_sender, _e) =>
                {
                    ResizeBottom(element, _e);
                    ResizeRight(element, _e);
                };

                Thumb topLeft = GetThumbInstance(DefaultThumbSize, dragTopLeft, HorizontalAlignment.Left, VerticalAlignment.Top, Cursors.SizeNWSE, style);
                Thumb topRight = GetThumbInstance(DefaultThumbSize, dragTopRight, HorizontalAlignment.Right, VerticalAlignment.Top, Cursors.SizeNESW, style);
                Thumb bottomLeft = GetThumbInstance(DefaultThumbSize, dragBottomLeft, HorizontalAlignment.Left, VerticalAlignment.Bottom, Cursors.SizeNESW, style);
                Thumb bottomRight = GetThumbInstance(DefaultThumbSize, dragBottomRight, HorizontalAlignment.Right, VerticalAlignment.Bottom, Cursors.SizeNWSE, style);

                collection.Add(topLeft);
                collection.Add(topRight);
                collection.Add(bottomLeft);
                collection.Add(bottomRight);
            }
            #endregion

            return collection;
        }

        #region Resize Method
        private static void ResizeRight(FrameworkElement element, DragDeltaEventArgs _e)
        {
            double newWidth = element.ActualWidth + _e.HorizontalChange;
            if (newWidth >= DefaultMinSize)
            {
                element.Width = newWidth;
            }
        }

        private static void ResizeLeft(FrameworkElement element, DragDeltaEventArgs _e)
        {
            double newLeft = Canvas.GetLeft(element) + _e.HorizontalChange;
            double newWidth = element.ActualWidth - _e.HorizontalChange;
            if (newWidth >= DefaultMinSize)
            {
                Canvas.SetLeft(element, newLeft);
                element.Width = newWidth;
            }
        }

        private static void ResizeBottom(FrameworkElement element, DragDeltaEventArgs _e)
        {
            double newHeight = element.ActualHeight + _e.VerticalChange;
            if (newHeight >= DefaultMinSize)
            {
                element.Height = newHeight;
            }
        }

        private static void ResizeTop(FrameworkElement element, DragDeltaEventArgs _e)
        {
            double newTop = Canvas.GetTop(element) + _e.VerticalChange;
            double newHeight = element.ActualHeight - _e.VerticalChange;
            if (newHeight >= DefaultMinSize)
            {
                Canvas.SetTop(element, newTop);
                element.Height = newHeight;
            }
        } 
        #endregion

        #region Sub Method
        private static Thumb GetThumbInstance(double size, DragDeltaEventHandler handler, HorizontalAlignment horizontal, VerticalAlignment vertical, Cursor cursor, Style style)
        {
            Thumb thumb = new Thumb();
            thumb.Width = thumb.Height = size;
            thumb.DragDelta += handler;
            thumb.HorizontalAlignment = horizontal;
            thumb.VerticalAlignment = vertical;
            thumb.Cursor = cursor;
            Thickness margin = new Thickness();
            if (horizontal == HorizontalAlignment.Left)
            {
                margin.Left = -size / 2.0;
            }
            if (horizontal == HorizontalAlignment.Right)
            {
                margin.Right = -size / 2.0;
            }

            if (vertical == VerticalAlignment.Top)
            {
                margin.Top = -size / 2.0;
            }
            if (vertical == VerticalAlignment.Bottom)
            {
                margin.Bottom = -size / 2.0;
            }
            thumb.Margin = margin;

            if (style != null && style.TargetType == typeof(Thumb))
            {
                thumb.Style = style;
            }
            return thumb;
        }

        private static Rectangle GetRectangleInstance(FrameworkElement element)
        {
            Rectangle rect = new Rectangle();
            rect.Stroke = Brushes.LightGray;
            rect.StrokeThickness = 1;
            return rect;
        }

        private static Grid GetGridInstance(FrameworkElement element)
        {
            Grid grid = new Grid();
            grid.Margin = new Thickness(DefaultThumbSize);

            Binding widthBinding = new Binding();
            widthBinding.Source = element;
            widthBinding.Path = new PropertyPath(FrameworkElement.WidthProperty);
            widthBinding.Mode = BindingMode.TwoWay;
            widthBinding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            grid.SetBinding(Grid.WidthProperty, widthBinding);

            Binding heightBinding = new Binding();
            heightBinding.Source = element;
            heightBinding.Path = new PropertyPath(FrameworkElement.HeightProperty);
            heightBinding.Mode = BindingMode.TwoWay;
            heightBinding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            grid.SetBinding(Grid.HeightProperty, heightBinding);

            return grid;
        }

        private static Popup GetPopupInstance(FrameworkElement element)
        {
            Popup popup = new Popup();
            popup.PlacementTarget = element;
            popup.Placement = PlacementMode.Relative;
            popup.AllowsTransparency = true;

            return popup;
        }  
        #endregion
        #endregion
    }
}
