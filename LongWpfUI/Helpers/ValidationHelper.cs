﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace LongWpfUI.Helpers
{
    /// <summary>
    /// Class contains attacted Property for Validation instance
    /// </summary>
    public class ValidationHelper : DependencyObject
    {
        #region AP
        /// <summary>
        /// Get ErrorTemplate of given DependencyObject
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static ControlTemplate GetErrorTemplate(DependencyObject obj)
        {
            return (ControlTemplate)obj.GetValue(ErrorTemplateProperty);
        }

        public static void SetErrorTemplate(DependencyObject obj, ControlTemplate value)
        {
            obj.SetValue(ErrorTemplateProperty, value);
        }

        /// <summary>
        /// Get ValidationRule for TextBox
        /// </summary>
        /// <param name="obj">DependencyObject instance</param>
        /// <returns></returns>
        public static object GetTextBoxRule(DependencyObject obj)
        {
            return (object)obj.GetValue(TextBoxRuleProperty);
        }

        public static void SetTextBoxRule(DependencyObject obj, object value)
        {
            obj.SetValue(TextBoxRuleProperty, value);
        }

        public static readonly DependencyProperty ErrorTemplateProperty =
            DependencyProperty.RegisterAttached("ErrorTemplate", typeof(ControlTemplate), typeof(ValidationHelper), new PropertyMetadata(null, OnErrorTemplateChanged));

        public static readonly DependencyProperty TextBoxRuleProperty =
            DependencyProperty.RegisterAttached("TextBoxRule", typeof(object), typeof(ValidationHelper), new PropertyMetadata(null, OnTextBoxRule));
        #endregion

        #region Property Changed
        private static void OnTextBoxRule(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is TextBox)
            {
                TextBox textBox = d as TextBox;
                var rule = ValidationHelper.GetTextBoxRule(textBox) as ValidationRule;                
                BindingExpression binding = textBox.GetBindingExpression(TextBox.TextProperty);
                if (rule != null && binding != null)
                {
                    Binding parentBinding = binding.ParentBinding;
                    parentBinding.ValidationRules.Clear();
                    parentBinding.ValidationRules.Add(rule);
                }
            }
            else
            {
                throw new Exception("Support only for TextBox instance");
            }
        }

        private static void OnErrorTemplateChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is FrameworkElement)
            {
                FrameworkElement element = d as FrameworkElement;
                ControlTemplate template = ValidationHelper.GetErrorTemplate(element);
                if (template != null)
                {
                    Validation.SetErrorTemplate(element, template);
                }
            }
            else
            {
                throw new Exception("Support only for FrameworkElement instance");
            }
        }
        #endregion
    }
}
