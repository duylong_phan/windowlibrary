﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace LongWpfUI.Helpers
{
    public class ComboBoxHelper
    {
        #region DP
        public static bool GetEditableDropDown(DependencyObject obj)
        {
            return (bool)obj.GetValue(EditableDropDownProperty);
        }

        public static void SetEditableDropDown(DependencyObject obj, bool value)
        {
            obj.SetValue(EditableDropDownProperty, value);
        }


        public static readonly DependencyProperty EditableDropDownProperty =
            DependencyProperty.RegisterAttached("EditableDropDown", typeof(bool), typeof(ComboBoxHelper), new PropertyMetadata(false, OnEditableDropDownChanged)); 
        #endregion

        #region Property Changed
        private static void OnEditableDropDownChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ComboBox box = d as ComboBox;
            if (box == null)
            {
                return;
            }

            bool editableDropDown = ComboBoxHelper.GetEditableDropDown(box);
            if (editableDropDown)
            {
                box.IsEditable = true;
                box.GotFocus += Box_GotFocus;
                box.LostFocus += Box_LostFocus;
            }
            else
            {
                box.IsEditable = false;
                box.GotFocus -= Box_GotFocus;
                box.LostFocus -= Box_LostFocus;
            }
        } 
        #endregion

        #region Event Handler
        private static void Box_GotFocus(object sender, RoutedEventArgs e)
        {
            ComboBox box = sender as ComboBox;
            if (box == null)
            {
                return;
            }

            box.KeyDown += Box_KeyDown;
        }



        private static void Box_LostFocus(object sender, RoutedEventArgs e)
        {
            ComboBox box = sender as ComboBox;
            if (box == null)
            {
                return;
            }

            box.KeyDown -= Box_KeyDown;
        }

        private static void Box_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            ComboBox box = sender as ComboBox;
            if (box == null)
            {
                return;
            }

            if (box.Text.Length > 0)
            {
                box.IsDropDownOpen = true;
            }
        } 
        #endregion
    }
}
