﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls.Primitives;

namespace LongWpfUI.Helpers
{
    public class PopupHelper : DependencyObject
    {
        #region AP
        [AttachedPropertyBrowsableForType(typeof(Popup))]
        public static bool GetCanPositionUpdate(DependencyObject obj)
        {
            return (bool)obj.GetValue(CanPositionUpdateProperty);
        }

        public static void SetCanPositionUpdate(DependencyObject obj, bool value)
        {
            obj.SetValue(CanPositionUpdateProperty, value);
        }

        public static readonly DependencyProperty CanPositionUpdateProperty =
          DependencyProperty.RegisterAttached("CanPositionUpdate", typeof(bool), typeof(PopupHelper), new PropertyMetadata(false, OnCanPositionUpdate));

        #region Private
        private static EventHandler GetLocationChanged(DependencyObject obj)
        {
            return (EventHandler)obj.GetValue(LocationChangedProperty);
        }

        private static void SetLocationChanged(DependencyObject obj, EventHandler value)
        {
            obj.SetValue(LocationChangedProperty, value);
        }

        private static SizeChangedEventHandler GetSizeChanged(DependencyObject obj)
        {
            return (SizeChangedEventHandler)obj.GetValue(SizeChangedProperty);
        }

        private static void SetSizeChanged(DependencyObject obj, SizeChangedEventHandler value)
        {
            obj.SetValue(SizeChangedProperty, value);
        }

        public static readonly DependencyProperty LocationChangedProperty =
          DependencyProperty.RegisterAttached("LocationChanged", typeof(EventHandler), typeof(PopupHelper), new PropertyMetadata(null));

        public static readonly DependencyProperty SizeChangedProperty =
          DependencyProperty.RegisterAttached("SizeChanged", typeof(SizeChangedEventHandler), typeof(PopupHelper), new PropertyMetadata(null)); 
        #endregion

        #endregion

        #region Property Changed
        private static void OnCanPositionUpdate(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is Popup)
            {
                Popup popup = d as Popup;                
                if (popup.PlacementTarget != null)
                {
                    Window topWindow = Window.GetWindow(popup.PlacementTarget);
                    bool canPositionUpdate = PopupHelper.GetCanPositionUpdate(popup);
                    if (topWindow != null)
                    {
                        if (canPositionUpdate)
                        {
                            EventHandler locationChanged = (_sender, _e) =>
                            {
                                popup.HorizontalOffset++;
                                popup.HorizontalOffset--;
                            };

                            SizeChangedEventHandler sizeChanged = (_sender, _e) =>
                            {
                                popup.HorizontalOffset++;
                                popup.HorizontalOffset--;
                            };

                            topWindow.LocationChanged += locationChanged;
                            topWindow.SizeChanged += sizeChanged;

                            PopupHelper.SetLocationChanged(popup, locationChanged);
                            PopupHelper.SetSizeChanged(popup, sizeChanged);
                        }
                        else
                        {
                            EventHandler locationChanged = PopupHelper.GetLocationChanged(popup);
                            if (locationChanged != null)
                            {
                                topWindow.LocationChanged -= locationChanged;
                                PopupHelper.SetLocationChanged(popup, null);
                            }

                            SizeChangedEventHandler sizeChanged = PopupHelper.GetSizeChanged(popup);
                            if (sizeChanged != null)
                            {
                                topWindow.SizeChanged -= sizeChanged;
                                PopupHelper.SetSizeChanged(popup, null);
                            }
                        }
                    }
                }
            }
            else
            {
                throw new Exception("Support only for Popup instance");
            }
        } 
        #endregion
    }
}
