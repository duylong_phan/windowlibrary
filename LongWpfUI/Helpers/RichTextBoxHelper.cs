﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace LongWpfUI.Helpers
{
    /// <summary>
    /// Helper class, to load, save, and update content in RichTextBox
    /// IsEnable = True, Content = Binding
    /// </summary>
    public class RichTextBoxHelper
    {
        #region Static
        /// <summary>
        /// Get Style of Paragraph, with no space
        /// </summary>
        public static Style ParagraghStyle { get; private set; } 
        #endregion

        #region DP        
        public static bool GetIsEnable(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsEnableProperty);
        }

        public static void SetIsEnable(DependencyObject obj, bool value)
        {
            obj.SetValue(IsEnableProperty, value);
        }

        public static bool GetSelectAllOnFocus(DependencyObject obj)
        {
            return (bool)obj.GetValue(SelectAllOnFocusProperty);
        }

        public static void SetSelectAllOnFocus(DependencyObject obj, bool value)
        {
            obj.SetValue(SelectAllOnFocusProperty, value);
        }

        public static string GetContent(DependencyObject obj)
        {
            return (string)obj.GetValue(ContentProperty);
        }

        public static void SetContent(DependencyObject obj, string value)
        {
            obj.SetValue(ContentProperty, value);
        }

        private static bool GetCanUpdate(DependencyObject obj)
        {
            return (bool)obj.GetValue(CanUpdateProperty);
        }

        private static void SetCanUpdate(DependencyObject obj, bool value)
        {
            obj.SetValue(CanUpdateProperty, value);
        }

        public static readonly DependencyProperty IsEnableProperty =
            DependencyProperty.RegisterAttached("IsEnable", typeof(bool), typeof(RichTextBoxHelper), new PropertyMetadata(false, OnIsEnableChanged));

        public static readonly DependencyProperty SelectAllOnFocusProperty =
            DependencyProperty.RegisterAttached("SelectAllOnFocus", typeof(bool), typeof(RichTextBoxHelper), new PropertyMetadata(false));

        public static readonly DependencyProperty ContentProperty =
             DependencyProperty.RegisterAttached("Content", typeof(string), typeof(RichTextBoxHelper), new FrameworkPropertyMetadata(string.Empty, OnContentChanged) { BindsTwoWayByDefault = true });
              
        public static readonly DependencyProperty CanUpdateProperty =
           DependencyProperty.RegisterAttached("CanUpdate", typeof(bool), typeof(RichTextBoxHelper), new PropertyMetadata(true));


        #endregion

        #region Property Changed
        private static void OnIsEnableChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RichTextBox textBox = d as RichTextBox;
            if (textBox == null)
            {
                return;
            }

            bool isEnable = RichTextBoxHelper.GetIsEnable(textBox);
            if (isEnable)
            {
                textBox.Loaded += TextBox_Loaded;
                textBox.GotFocus += TextBox_GotFocus;
                textBox.LostFocus += TextBox_LostFocus;
            }
            else
            {
                textBox.Loaded -= TextBox_Loaded;
                textBox.GotFocus -= TextBox_GotFocus;
                textBox.LostFocus -= TextBox_LostFocus;
            }
        }

        private static void OnContentChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RichTextBox textBox = d as RichTextBox;
            if (textBox == null)
            {
                return;
            }

            bool isEnable = RichTextBoxHelper.GetIsEnable(textBox);
            if (!isEnable || !textBox.IsLoaded)  //NOT
            {
                return;
            }

            bool canUpdate = RichTextBoxHelper.GetCanUpdate(textBox);
            if (!canUpdate) //NOT
            {
                //reset
                RichTextBoxHelper.SetCanUpdate(textBox, true);
                return;
            }

            UpdateContent(textBox, RichTextBoxHelper.GetContent(textBox));
        }

        #endregion

        #region Event Handler
        private static void TextBox_Loaded(object sender, RoutedEventArgs e)
        {
            //cast, double check
            RichTextBox textBox = sender as RichTextBox;
            if (textBox == null)
            {
                return;
            }
            textBox.Loaded -= TextBox_Loaded;

            //no space between paragragh => lazy Pattern
            if (ParagraghStyle == null)
            {
                ParagraghStyle = new Style();
                ParagraghStyle.TargetType = typeof(Paragraph);
                ParagraghStyle.Setters.Add(new Setter(Paragraph.MarginProperty, new Thickness(0)));
            }

            //set style for paragraph
            if (!textBox.Resources.Contains(typeof(Paragraph))) //NOT
            {
                textBox.Resources.Add(typeof(Paragraph), ParagraghStyle);
            }

            UpdateContent(textBox, RichTextBoxHelper.GetContent(textBox));
        }

        private static void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            //cast, double check
            RichTextBox textBox = sender as RichTextBox;
            if (textBox == null)
            {
                return;
            }

            //check enable select all
            bool canSelectAll = RichTextBoxHelper.GetSelectAllOnFocus(textBox);
            if (canSelectAll)
            {
                textBox.SelectAll();
            }

            //Add event Handler
            textBox.PreviewKeyDown += TextBox_KeyDown;
        }        

        private static void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            //cast, double check
            RichTextBox textBox = sender as RichTextBox;
            if (textBox == null)
            {
                return;
            }

            //Remove event Handler
            textBox.PreviewKeyDown -= TextBox_KeyDown;

            //update Content
            string content = string.Empty;
            TextRange range = new TextRange(textBox.Document.ContentStart, textBox.Document.ContentEnd);
            using (MemoryStream stream = new MemoryStream())
            {
                range.Save(stream, DataFormats.Rtf);
                var data = stream.ToArray();
                content = Encoding.ASCII.GetString(data);
            }

            //avoid self looping
            RichTextBoxHelper.SetCanUpdate(textBox, false);
            //update
            RichTextBoxHelper.SetContent(textBox, content);
        }

        private static void TextBox_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            //Control Key
            if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
            {
                switch (e.Key)
                {
                    case Key.T:
                       e.Handled = ChangeTextProperty(sender as RichTextBox, Inline.TextDecorationsProperty, TextDecorations.Strikethrough, null);
                        break;

                    default:
                        break;
                }
            }

            //Alt Key
            if (Keyboard.IsKeyDown(Key.RightAlt))
            {
                switch (e.Key)
                {
                    case Key.R:
                        e.Handled = ChangeTextProperty(sender as RichTextBox, Inline.ForegroundProperty, Brushes.Red, Brushes.Black);
                        break;
                    case Key.G:
                        e.Handled = ChangeTextProperty(sender as RichTextBox, Inline.ForegroundProperty, Brushes.Green, Brushes.Black);
                        break;
                    case Key.B:
                        e.Handled = ChangeTextProperty(sender as RichTextBox, Inline.ForegroundProperty, Brushes.SteelBlue, Brushes.Black);
                        break;

                    default:
                        break;
                }
            }
        }

        #endregion
        
        #region Sub Function
        private static void UpdateContent(RichTextBox textBox, string content)
        {
            if (textBox == null || string.IsNullOrEmpty(content))
            {
                return;
            }

            //load Text
            TextRange range = new TextRange(textBox.Document.ContentStart, textBox.Document.ContentEnd);
            using (MemoryStream stream = new MemoryStream(Encoding.ASCII.GetBytes(content)))
            {
                range.Load(stream, DataFormats.Rtf);
            }
        }

        private static bool ChangeTextProperty(RichTextBox textBox, DependencyProperty property, object value, object defaultValue)
        {
            //check null
            if (textBox == null || property == null || value == null)
            {
                return false;
            }

            //check current Value, same => reset
            if (textBox.Selection.GetPropertyValue(property).Equals(value))
            {
                textBox.Selection.ApplyPropertyValue(property, defaultValue);
            }
            //different => set new
            else
            {
                textBox.Selection.ApplyPropertyValue(property, value);
            }
            return true;
        }
        #endregion
    }
}
