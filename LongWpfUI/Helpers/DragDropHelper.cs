﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using LongWpfUI.Interfaces;
using System.Windows.Input;
using System.Windows.Controls;

namespace LongWpfUI.Helpers
{
    public class DragDropHelper
    {
        #region AP
        public static bool GetIsDragSource(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsDragSourceProperty);
        }

        public static void SetIsDragSource(DependencyObject obj, bool value)
        {
            obj.SetValue(IsDragSourceProperty, value);
        }

        public static bool GetIsDropTarget(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsDropTargetProperty);
        }

        public static void SetIsDropTarget(DependencyObject obj, bool value)
        {
            obj.SetValue(IsDropTargetProperty, value);
        }

        public static object GetHandler(DependencyObject obj)
        {
            return (object)obj.GetValue(HandlerProperty);
        }

        public static void SetHandler(DependencyObject obj, object value)
        {
            obj.SetValue(HandlerProperty, value);
        }

        public static readonly DependencyProperty IsDragSourceProperty =
            DependencyProperty.RegisterAttached("IsDragSource", typeof(bool), typeof(DragDropHelper), new PropertyMetadata(false, OnIsDragSourceChanged));



        public static readonly DependencyProperty IsDropTargetProperty =
            DependencyProperty.RegisterAttached("IsDropTarget", typeof(bool), typeof(DragDropHelper), new PropertyMetadata(false, OnIsDropTargetChanged));



        public static readonly DependencyProperty HandlerProperty =
            DependencyProperty.RegisterAttached("Handler", typeof(object), typeof(DragDropHelper), new PropertyMetadata(null));
        #endregion

        #region Field
        private static Point startPoint; 
        #endregion

        #region PropertyChanged CallBack
        private static void OnIsDragSourceChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ItemsControl control = d as ItemsControl;
            if (control == null)
            {
                return;
            }

            if (DragDropHelper.GetIsDragSource(control))
            {                
                control.PreviewMouseLeftButtonDown += DragSource_PreviewMouseLeftButtonDown;
                control.PreviewMouseMove += DragSource_PreviewMouseMove;
            }
            else
            {
                control.PreviewMouseLeftButtonDown -= DragSource_PreviewMouseLeftButtonDown;
                control.PreviewMouseMove -= DragSource_PreviewMouseMove;
            }
        }

        private static void OnIsDropTargetChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            FrameworkElement element = d as FrameworkElement;
            if (element == null)
            {
                return;
            }

            bool isDropTarget = DragDropHelper.GetIsDropTarget(element);
            if (isDropTarget)
            {
                element.AllowDrop = true;
                element.DragEnter += DropTarget_DragEnter;
                element.DragLeave += DropTarget_DragLeave;
                element.DragOver += DropTarget_DragOver;
                element.Drop += DropTarget_Drop;
            }
            else
            {
                element.AllowDrop = false;
                element.DragEnter -= DropTarget_DragEnter;
                element.DragLeave -= DropTarget_DragLeave;
                element.DragOver -= DropTarget_DragOver;
                element.Drop -= DropTarget_Drop;
            }
        }
        #endregion

        #region Event Handler
        private static void DragSource_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            startPoint = e.GetPosition(null);
        }

        private static void DragSource_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            ItemsControl control = sender as ItemsControl;

            //check button state
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                //check drag distance
                Point currentPoint = e.GetPosition(null);
                if (control != null && IsDragging(startPoint, currentPoint))
                {
                    //check handler and Original source
                    IDragSourceHandler handler = DragDropHelper.GetHandler(control) as IDragSourceHandler;
                    DependencyObject source = e.OriginalSource as DependencyObject;
                    if (handler != null && source != null)
                    {
                        //check drag Data
                        FrameworkElement container = control.ContainerFromElement(source) as FrameworkElement;
                        if (container != null && container.DataContext != null)
                        {
                            object dragData = handler.GetDragData(container.DataContext);
                            DragDropEffects result = DragDrop.DoDragDrop(control, dragData, handler.Effect);
                            handler.OnDragDone(control, result, container.DataContext);
                        }
                    }
                }
            }
        }

        private static void DropTarget_DragEnter(object sender, DragEventArgs e)
        {
            FrameworkElement element = sender as FrameworkElement;
            IDropTargetHandler handler = DragDropHelper.GetHandler(element) as IDropTargetHandler;
            if (handler != null)
            {
                handler.OnDragEnter(element, e);
            }
            e.Handled = true;
        }

        private static void DropTarget_DragLeave(object sender, DragEventArgs e)
        {
            FrameworkElement element = sender as FrameworkElement;
            IDropTargetHandler handler = DragDropHelper.GetHandler(element) as IDropTargetHandler;
            if (handler != null)
            {
                handler.OnDragLeave(element, e);
            }
            e.Handled = true;
        }

        private static void DropTarget_DragOver(object sender, DragEventArgs e)
        {
            FrameworkElement element = sender as FrameworkElement;
            IDropTargetHandler handler = DragDropHelper.GetHandler(element) as IDropTargetHandler;
            if (handler != null)
            {
                handler.OnDragOver(element, e);
            }
            e.Handled = true;
        }

        private static void DropTarget_Drop(object sender, DragEventArgs e)
        {
            FrameworkElement element = sender as FrameworkElement;
            IDropTargetHandler handler = DragDropHelper.GetHandler(element) as IDropTargetHandler;
            if (handler != null)
            {
                handler.OnDrop(element, e);
            }
            e.Handled = true;
        }
        #endregion

        #region Public Method
        public static bool IsDragging(Point start, Point current)
        {
            bool isDragging = false;
            Vector diff = current - startPoint;

            if (Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance ||
                Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance)
            {
                isDragging = true;
            }

            return isDragging;
        } 
        #endregion
    }
}
