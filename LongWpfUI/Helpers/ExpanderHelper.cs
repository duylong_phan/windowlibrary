﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace LongWpfUI.Helpers
{
    public class ExpanderHelper
    {
        #region DP
        public static bool GetDisableCollapsed(DependencyObject obj)
        {
            return (bool)obj.GetValue(DisableCollapsedProperty);
        }

        public static void SetDisableCollapsed(DependencyObject obj, bool value)
        {
            obj.SetValue(DisableCollapsedProperty, value);
        }

        public static bool GetDisableExpanded(DependencyObject obj)
        {
            return (bool)obj.GetValue(DisableExpandedProperty);
        }

        public static void SetDisableExpanded(DependencyObject obj, bool value)
        {
            obj.SetValue(DisableExpandedProperty, value);
        }

        public static readonly DependencyProperty DisableCollapsedProperty =
            DependencyProperty.RegisterAttached("DisableCollapsed", typeof(bool), typeof(ExpanderHelper), new PropertyMetadata(false, OnDisableCollapsedChanged));

        public static readonly DependencyProperty DisableExpandedProperty =
           DependencyProperty.RegisterAttached("DisableExpanded", typeof(bool), typeof(ExpanderHelper), new PropertyMetadata(false, OnDisableExpandedChanged));
                
        #endregion



        #region Property Changed
        private static void OnDisableCollapsedChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Expander expander = d as Expander;
            if (expander == null)
            {
                return;
            }

            bool disableCollapsed = ExpanderHelper.GetDisableCollapsed(expander);
            bool disableExpanded = ExpanderHelper.GetDisableCollapsed(expander);
            if (disableCollapsed)
            {
                expander.Collapsed += Expander_Collapsed;
                //reset
                if (disableExpanded)
                {
                    ExpanderHelper.SetDisableExpanded(expander, false);
                }
            }
            else
            {
                expander.Collapsed -= Expander_Collapsed;
            }
        }

        private static void OnDisableExpandedChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Expander expander = d as Expander;
            if (expander == null)
            {
                return;
            }

            bool disableCollapsed = ExpanderHelper.GetDisableCollapsed(expander);
            bool disableExpanded = ExpanderHelper.GetDisableCollapsed(expander);
            if (disableExpanded)
            {
                expander.Expanded += Expander_Expanded;
                if (disableCollapsed)
                {
                    ExpanderHelper.SetDisableCollapsed(expander, false);
                }
            }
            else
            {
                expander.Expanded -= Expander_Expanded;
            }
        }

        

        #endregion

        #region Sub Method
        private static void Expander_Collapsed(object sender, RoutedEventArgs e)
        {
            Expander expander = sender as Expander;
            if (expander == null)
            {
                return;
            }

            expander.IsExpanded = true;
        }

        private static void Expander_Expanded(object sender, RoutedEventArgs e)
        {
            Expander expander = sender as Expander;
            if (expander == null)
            {
                return;
            }

            expander.IsExpanded = false;
        }
        #endregion
    }
}
