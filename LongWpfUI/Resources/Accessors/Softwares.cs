﻿using LongInterface.Views;
using LongInterface.Views.ControlModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongWpfUI.Resources.Accessors
{
    public partial class IconAccessor : IGetIcon
    {
        private static IconAccessor msWord;
        private static IconAccessor msExcel;
        private static IconAccessor msPowerPoint;
        private static IconAccessor msAccess;

        /// <summary>
        /// Get source for MsWord Icon
        /// </summary>
        public static IconAccessor MsWord
        {
            get
            {
                if (msWord == null)
                {
                    msWord = new IconAccessor(string.Empty,
                                              "/LongWpfUI;component/Resources/Images/Softwares/Word24.png",
                                              "/LongWpfUI;component/Resources/Images/Softwares/Word32.png",
                                              "/LongWpfUI;component/Resources/Images/Softwares/Word48.png");
                }
                return msWord;
            }
        }
        /// <summary>
        /// Get source for MsExcel Icon
        /// </summary>
        public static IconAccessor MsExcel
        {
            get
            {
                if (msExcel == null)
                {
                    msExcel = new IconAccessor(string.Empty,
                                               "/LongWpfUI;component/Resources/Images/Softwares/Excel24.png",
                                               "/LongWpfUI;component/Resources/Images/Softwares/Excel32.png",
                                               "/LongWpfUI;component/Resources/Images/Softwares/Excel48.png");
                }
                return msExcel;
            }
        }
        /// <summary>
        /// Get source for MsPowerPoint Icon
        /// </summary>
        public static IconAccessor MsPowerPoint
        {
            get
            {
                if (msPowerPoint == null)
                {
                    msPowerPoint = new IconAccessor(string.Empty,
                                                    "/LongWpfUI;component/Resources/Images/Softwares/PowerPoint24.png",
                                                    "/LongWpfUI;component/Resources/Images/Softwares/PowerPoint32.png",
                                                    "/LongWpfUI;component/Resources/Images/Softwares/PowerPoint48.png");
                }
                return msPowerPoint;
            }
        }
        /// <summary>
        /// Get source for MsAccess Icon
        /// </summary>
        public static IconAccessor MsAccess
        {
            get
            {
                if (msAccess == null)
                {
                    msAccess = new IconAccessor(string.Empty,
                                                "/LongWpfUI;component/Resources/Images/Softwares/Access24.png",
                                                "/LongWpfUI;component/Resources/Images/Softwares/Access32.png",
                                                "/LongWpfUI;component/Resources/Images/Softwares/Access48.png");
                }
                return msAccess;
            }
        }

    }
}
