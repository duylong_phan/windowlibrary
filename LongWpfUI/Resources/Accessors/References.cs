﻿using LongInterface.Views;
using LongInterface.Views.ControlModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongWpfUI.Resources.Accessors
{
    public partial class IconAccessor : IGetIcon
    {
        private static IconAccessor dll;
        private static IconAccessor software;
        private static IconAccessor license;
        private static IconAccessor reference;

        /// <summary>
        /// Get source for DLL Icon
        /// </summary>
        public static IconAccessor DLL
        {
            get
            {
                if (dll == null)
                {
                    dll = new IconAccessor(string.Empty,
                                           "/LongWpfUI;component/Resources/Images/References/DLL24.png",
                                           "/LongWpfUI;component/Resources/Images/References/DLL32.png",
                                           "/LongWpfUI;component/Resources/Images/References/DLL48.png");
                }
                return dll;
            }
        }
        /// <summary>
        /// Get source for Software Icon
        /// </summary>
        public static IconAccessor Software
        {
            get
            {
                if (software == null)
                {
                    software = new IconAccessor(string.Empty,
                                                "/LongWpfUI;component/Resources/Images/References/Software24.png",
                                                "/LongWpfUI;component/Resources/Images/References/Software32.png",
                                                "/LongWpfUI;component/Resources/Images/References/Software48.png");
                }
                return software;
            }
        }
        /// <summary>
        /// Get source for License Icon
        /// </summary>
        public static IconAccessor License
        {
            get
            {
                if (license == null)
                {
                    license = new IconAccessor(string.Empty,
                                               "/LongWpfUI;component/Resources/Images/References/License24.png",
                                               "/LongWpfUI;component/Resources/Images/References/License32.png",
                                               "/LongWpfUI;component/Resources/Images/References/License48.png");
                }
                return license;
            }
        }
        /// <summary>
        /// Get source for Reference Icon
        /// </summary>
        public static IconAccessor Reference
        {
            get
            {
                if (reference == null)
                {
                    reference = new IconAccessor(string.Empty,
                                                 "/LongWpfUI;component/Resources/Images/References/Reference24.png",
                                                 "/LongWpfUI;component/Resources/Images/References/Reference32.png",
                                                 "/LongWpfUI;component/Resources/Images/References/Reference48.png");
                }
                return reference;
            }
        }

    }
}
