﻿using LongInterface.Views;
using LongInterface.Views.ControlModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongWpfUI.Resources.Accessors
{
    public partial class IconAccessor : IGetIcon
    {
        private static IconAccessor english;
        private static IconAccessor german;

        /// <summary>
        /// Get source for English Icon
        /// </summary>
        public static IconAccessor English
        {
            get
            {
                if (english == null)
                {
                    english = new IconAccessor("/LongWpfUI;component/Resources/Images/Flags/English16.png",
                                               "/LongWpfUI;component/Resources/Images/Flags/English24.png",
                                               "/LongWpfUI;component/Resources/Images/Flags/English32.png",
                                               "/LongWpfUI;component/Resources/Images/Flags/English48.png");
                }
                return english;
            }
        }
        /// <summary>
        /// Get source for German Icon
        /// </summary>
        public static IconAccessor German
        {
            get
            {
                if (german == null)
                {
                    german = new IconAccessor("/LongWpfUI;component/Resources/Images/Flags/German16.png",
                                              "/LongWpfUI;component/Resources/Images/Flags/German24.png",
                                              "/LongWpfUI;component/Resources/Images/Flags/German32.png",
                                              "/LongWpfUI;component/Resources/Images/Flags/German48.png");
                }
                return german;
            }
        }

    }
}
