﻿using LongInterface.Views;
using LongInterface.Views.ControlModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongWpfUI.Resources.Accessors
{
    public partial class IconAccessor : IGetIcon
    {
        private static IconAccessor dataBase;
        private static IconAccessor image;
        private static IconAccessor noImage;
        private static IconAccessor printer;
        private static IconAccessor table;
        private static IconAccessor video;
        private static IconAccessor window;
        private static IconAccessor color;
        private static IconAccessor trash;
        private static IconAccessor calender;
        private static IconAccessor addressBook;
        private static IconAccessor alarm;
        private static IconAccessor barCode;

        /// <summary>
        /// Get source for DataBase Icon
        /// </summary>
        public static IconAccessor DataBase
        {
            get
            {
                if (dataBase == null)
                {
                    dataBase = new IconAccessor("/LongWpfUI;component/Resources/Images/Objects/DataBase16.png",
                                                "/LongWpfUI;component/Resources/Images/Objects/DataBase24.png",
                                                "/LongWpfUI;component/Resources/Images/Objects/DataBase32.png",
                                                "/LongWpfUI;component/Resources/Images/Objects/DataBase48.png");
                }
                return dataBase;
            }
        }
        /// <summary>
        /// Get source for Image Icon
        /// </summary>
        public static IconAccessor Image
        {
            get
            {
                if (image == null)
                {
                    image = new IconAccessor(string.Empty,
                                             string.Empty,
                                             "/LongWpfUI;component/Resources/Images/Objects/Image32.png",
                                             "/LongWpfUI;component/Resources/Images/Objects/Image48.png");
                }
                return image;
            }
        }
        /// <summary>
        /// Get source for NoImage Icon
        /// </summary>
        public static IconAccessor NoImage
        {
            get
            {
                if (noImage == null)
                {
                    noImage = new IconAccessor(string.Empty,
                                               string.Empty,
                                               "/LongWpfUI;component/Resources/Images/Objects/NoImage32.png",
                                               "/LongWpfUI;component/Resources/Images/Objects/NoImage48.png");
                }
                return noImage;
            }
        }
        /// <summary>
        /// Get source for Printer Icon
        /// </summary>
        public static IconAccessor Printer
        {
            get
            {
                if (printer == null)
                {
                    printer = new IconAccessor("/LongWpfUI;component/Resources/Images/Objects/Printer16.png",
                                               "/LongWpfUI;component/Resources/Images/Objects/Printer24.png",
                                               "/LongWpfUI;component/Resources/Images/Objects/Printer32.png",
                                               "/LongWpfUI;component/Resources/Images/Objects/Printer48.png");
                }
                return printer;
            }
        }
        /// <summary>
        /// Get source for Table Icon
        /// </summary>
        public static IconAccessor Table
        {
            get
            {
                if (table == null)
                {
                    table = new IconAccessor("/LongWpfUI;component/Resources/Images/Objects/Table16.png",
                                             "/LongWpfUI;component/Resources/Images/Objects/Table24.png",
                                             "/LongWpfUI;component/Resources/Images/Objects/Table32.png",
                                             "/LongWpfUI;component/Resources/Images/Objects/Table48.png");
                }
                return table;
            }
        }
        /// <summary>
        /// Get source for Video Icon
        /// </summary>
        public static IconAccessor Video
        {
            get
            {
                if (video == null)
                {
                    video = new IconAccessor("/LongWpfUI;component/Resources/Images/Objects/Video16.png",
                                             "/LongWpfUI;component/Resources/Images/Objects/Video24.png",
                                             "/LongWpfUI;component/Resources/Images/Objects/Video32.png",
                                             "/LongWpfUI;component/Resources/Images/Objects/Video48.png");
                }
                return video;
            }
        }
        /// <summary>
        /// Get source for Window Icon
        /// </summary>
        public static IconAccessor Window
        {
            get
            {
                if (window == null)
                {
                    window = new IconAccessor("/LongWpfUI;component/Resources/Images/Objects/Window16.png",
                                              "/LongWpfUI;component/Resources/Images/Objects/Window24.png",
                                              "/LongWpfUI;component/Resources/Images/Objects/Window32.png",
                                              "/LongWpfUI;component/Resources/Images/Objects/Window48.png");
                }
                return window;
            }
        }
        /// <summary>
        /// Get source for Color Icon
        /// </summary>
        public static IconAccessor Color
        {
            get
            {
                if (color == null)
                {
                    color = new IconAccessor("/LongWpfUI;component/Resources/Images/Objects/Color16.png",
                                             "/LongWpfUI;component/Resources/Images/Objects/Color24.png",
                                             "/LongWpfUI;component/Resources/Images/Objects/Color32.png",
                                             "/LongWpfUI;component/Resources/Images/Objects/Color48.png");
                }
                return color;
            }
        }               
        /// <summary>
        /// Get source for Trash Icon
        /// </summary>
        public static IconAccessor Trash
        {
            get
            {
                if (trash == null)
                {
                    trash = new IconAccessor(string.Empty,
                                             string.Empty,
                                             "/LongWpfUI;component/Resources/Images/Objects/Trash32.png",
                                             "/LongWpfUI;component/Resources/Images/Objects/Trash48.png");
                }
                return trash;
            }
        }        
        /// <summary>
        /// Get source for Calender Icon
        /// </summary>
        public static IconAccessor Calender
        {
            get
            {
                if (calender == null)
                {
                    calender =  new IconAccessor("/LongWpfUI;component/Resources/Images/Objects/Calender16.png",
                                                 "/LongWpfUI;component/Resources/Images/Objects/Calender24.png",
                                                 "/LongWpfUI;component/Resources/Images/Objects/Calender32.png",
                                                 "/LongWpfUI;component/Resources/Images/Objects/Calender48.png");
                }
                return calender;
            }
        }
        /// <summary>
        /// Get source for AddressBook Icon
        /// </summary>
        public static IconAccessor AddressBook
        {
            get
            {
                if (addressBook == null)
                {
                    addressBook = new IconAccessor(string.Empty,
                                                   string.Empty,
                                                   "/LongWpfUI;component/Resources/Images/Objects/AddressBook32.png",
                                                   "/LongWpfUI;component/Resources/Images/Objects/AddressBook32.png");
                }
                return addressBook;
            }
        }
        /// <summary>
        /// Get source for Alarm Icon
        /// </summary>
        public static IconAccessor Alarm
        {
            get
            {
                if (alarm == null)
                {
                    alarm = new IconAccessor(string.Empty,
                                             string.Empty,
                                             "/LongWpfUI;component/Resources/Images/Objects/Alarm32.png",
                                             "/LongWpfUI;component/Resources/Images/Objects/Alarm32.png");
                }
                return alarm;
            }
        }
        /// <summary>
        /// Get source for BarCode Icon
        /// </summary>
        public static IconAccessor BarCode
        {
            get
            {
                if (barCode == null)
                {
                    barCode = new IconAccessor(string.Empty,
                                               string.Empty,
                                               "/LongWpfUI;component/Resources/Images/Objects/BarCode32.png",
                                               "/LongWpfUI;component/Resources/Images/Objects/BarCode48.png");
                }
                return barCode;
            }
        }

        private static IconAccessor clock;

        /// <summary>
        /// Get source for Clock Icon
        /// </summary>
        public static IconAccessor Clock
        {
            get
            {
                if (clock == null)
                {
                    clock = new IconAccessor("/LongWpfUI;component/Resources/Images/Objects/Clock16.png",
                                             "/LongWpfUI;component/Resources/Images/Objects/Clock24.png",
                                             "/LongWpfUI;component/Resources/Images/Objects/Clock32.png",
                                             "/LongWpfUI;component/Resources/Images/Objects/Clock48.png");
                }
                return clock;
            }
        }

    }
}
