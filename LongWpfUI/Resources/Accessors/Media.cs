﻿using LongInterface.Views;
using LongInterface.Views.ControlModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongWpfUI.Resources.Accessors
{
    public partial class IconAccessor : IGetIcon
    {
        private static IconAccessor play;
        private static IconAccessor pause;
        private static IconAccessor stop;

        /// <summary>
        /// Get source for Play Icon
        /// </summary>
        public static IconAccessor Play
        {
            get
            {
                if (play == null)
                {
                    play = new IconAccessor("/LongWpfUI;component/Resources/Images/Media/Play16.png",
                                            "/LongWpfUI;component/Resources/Images/Media/Play24.png",
                                            "/LongWpfUI;component/Resources/Images/Media/Play32.png",
                                            "/LongWpfUI;component/Resources/Images/Media/Play48.png");
                }
                return play;
            }
        }
        
        /// <summary>
        /// Get source for Pause Icon
        /// </summary>
        public static IconAccessor Pause
        {
            get
            {
                if (pause == null)
                {
                    pause = new IconAccessor("/LongWpfUI;component/Resources/Images/Media/Pause16.png",
                                             "/LongWpfUI;component/Resources/Images/Media/Pause24.png",
                                             "/LongWpfUI;component/Resources/Images/Media/Pause32.png",
                                             "/LongWpfUI;component/Resources/Images/Media/Pause48.png");
                }
                return pause;
            }
        }        

        /// <summary>
        /// Get source for Stop Icon
        /// </summary>
        public static IconAccessor Stop
        {
            get
            {
                if (stop == null)
                {
                    stop = new IconAccessor("/LongWpfUI;component/Resources/Images/Media/Stop16.png",
                                            "/LongWpfUI;component/Resources/Images/Media/Stop24.png",
                                            "/LongWpfUI;component/Resources/Images/Media/Stop32.png",
                                            "/LongWpfUI;component/Resources/Images/Media/Stop48.png");
                }
                return stop;
            }
        }

    }
}
