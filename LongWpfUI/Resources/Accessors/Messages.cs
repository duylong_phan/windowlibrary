﻿using LongInterface.Views;
using LongInterface.Views.ControlModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongWpfUI.Resources.Accessors
{
    public partial class IconAccessor : IGetIcon
    {
        private static IconAccessor addMessage;
        private static IconAccessor deleteMessage;
        private static IconAccessor editMessage;
        private static IconAccessor infoMessage;
        private static IconAccessor messageTrash;
        private static IconAccessor validatedMessage;

        /// <summary>
        /// Get source for AddMessage Icon
        /// </summary>
        public static IconAccessor AddMessage
        {
            get
            {
                if (addMessage == null)
                {
                    addMessage = new IconAccessor("/LongWpfUI;component/Resources/Images/Messages/AddMessage16.png",
                                                  "/LongWpfUI;component/Resources/Images/Messages/AddMessage24.png",
                                                  "/LongWpfUI;component/Resources/Images/Messages/AddMessage32.png",
                                                  "/LongWpfUI;component/Resources/Images/Messages/AddMessage48.png");
                }
                return addMessage;
            }
        }
        
        /// <summary>
        /// Get source for DeleteMessage Icon
        /// </summary>
        public static IconAccessor DeleteMessage
        {
            get
            {
                if (deleteMessage == null)
                {
                    deleteMessage = new IconAccessor("/LongWpfUI;component/Resources/Images/Messages/DeleteMessage16.png",
                                                     "/LongWpfUI;component/Resources/Images/Messages/DeleteMessage24.png",
                                                     "/LongWpfUI;component/Resources/Images/Messages/DeleteMessage32.png",
                                                     "/LongWpfUI;component/Resources/Images/Messages/DeleteMessage48.png");
                }
                return deleteMessage;
            }
        }
                
        /// <summary>
        /// Get source for EditMessage Icon
        /// </summary>
        public static IconAccessor EditMessage
        {
            get
            {
                if (editMessage == null)
                {
                    editMessage = new IconAccessor("/LongWpfUI;component/Resources/Images/Messages/EditMessage16.png",
                                                   "/LongWpfUI;component/Resources/Images/Messages/EditMessage24.png",
                                                   "/LongWpfUI;component/Resources/Images/Messages/EditMessage32.png",
                                                   "/LongWpfUI;component/Resources/Images/Messages/EditMessage48.png");
                }
                return editMessage;
            }
        }
                
        /// <summary>
        /// Get source for InfoMessage Icon
        /// </summary>
        public static IconAccessor InfoMessage
        {
            get
            {
                if (infoMessage == null)
                {
                    infoMessage = new IconAccessor("/LongWpfUI;component/Resources/Images/Messages/InfoMessage16.png",
                                                   "/LongWpfUI;component/Resources/Images/Messages/InfoMessage24.png",
                                                   "/LongWpfUI;component/Resources/Images/Messages/InfoMessage32.png",
                                                   "/LongWpfUI;component/Resources/Images/Messages/InfoMessage48.png");
                }
                return infoMessage;
            }
        }
                
        /// <summary>
        /// Get source for MessageTrash Icon
        /// </summary>
        public static IconAccessor MessageTrash
        {
            get
            {
                if (messageTrash == null)
                {
                    messageTrash = new IconAccessor("/LongWpfUI;component/Resources/Images/Messages/MessageTrash16.png",
                                                    "/LongWpfUI;component/Resources/Images/Messages/MessageTrash24.png",
                                                    "/LongWpfUI;component/Resources/Images/Messages/MessageTrash32.png",
                                                    "/LongWpfUI;component/Resources/Images/Messages/MessageTrash48.png");
                }
                return messageTrash;
            }
        }
        
        /// <summary>
        /// Get source for ValidatedMessage Icon
        /// </summary>
        public static IconAccessor ValidatedMessage
        {
            get
            {
                if (validatedMessage == null)
                {
                    validatedMessage = new IconAccessor("/LongWpfUI;component/Resources/Images/Messages/ValidatedMessage16.png",
                                                        "/LongWpfUI;component/Resources/Images/Messages/ValidatedMessage24.png",
                                                        "/LongWpfUI;component/Resources/Images/Messages/ValidatedMessage32.png",
                                                        "/LongWpfUI;component/Resources/Images/Messages/ValidatedMessage48.png");
                }
                return validatedMessage;
            }
        }

    }
}
