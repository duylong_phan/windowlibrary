﻿using LongInterface.Views;
using LongInterface.Views.ControlModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongWpfUI.Resources.Accessors
{
    public partial class IconAccessor : IGetIcon
    {
        private static IconAccessor deleteFile;
        private static IconAccessor editFile;
        private static IconAccessor helpFile;
        private static IconAccessor infoFile;
        private static IconAccessor newFile;
        private static IconAccessor searchFile;
        private static IconAccessor warningFile;
        private static IconAccessor importFile;
        private static IconAccessor exportFile;

        private static IconAccessor file;

        /// <summary>
        /// Get source for File Icon
        /// </summary>
        public static IconAccessor File
        {
            get
            {
                if (file == null)
                {
                    file = new IconAccessor("/LongWpfUI;component/Resources/Images/Objects/Files/File16.png",
                                            "/LongWpfUI;component/Resources/Images/Objects/Files/File24.png",
                                            "/LongWpfUI;component/Resources/Images/Objects/Files/File32.png",
                                            "/LongWpfUI;component/Resources/Images/Objects/Files/File48.png");
                }
                return file;
            }
        }


        /// <summary>
        /// Get source for DeleteFile Icon
        /// </summary>
        public static IconAccessor DeleteFile
        {
            get
            {
                if (deleteFile == null)
                {
                    deleteFile = new IconAccessor("/LongWpfUI;component/Resources/Images/Objects/Files/DeleteFile16.png",
                                                  "/LongWpfUI;component/Resources/Images/Objects/Files/DeleteFile24.png",
                                                  "/LongWpfUI;component/Resources/Images/Objects/Files/DeleteFile32.png",
                                                  "/LongWpfUI;component/Resources/Images/Objects/Files/DeleteFile48.png");
                }
                return deleteFile;
            }
        }       
        /// <summary>
        /// Get source for EditFile Icon
        /// </summary>
        public static IconAccessor EditFile
        {
            get
            {
                if (editFile == null)
                {
                    editFile = new IconAccessor("/LongWpfUI;component/Resources/Images/Objects/Files/EditFile16.png",
                                                "/LongWpfUI;component/Resources/Images/Objects/Files/EditFile24.png",
                                                "/LongWpfUI;component/Resources/Images/Objects/Files/EditFile32.png",
                                                "/LongWpfUI;component/Resources/Images/Objects/Files/EditFile48.png");
                }
                return editFile;
            }
        }        
        /// <summary>
        /// Get source for HelpFile Icon
        /// </summary>
        public static IconAccessor HelpFile
        {
            get
            {
                if (helpFile == null)
                {
                    helpFile = new IconAccessor("/LongWpfUI;component/Resources/Images/Objects/Files/HelpFile16.png",
                                                "/LongWpfUI;component/Resources/Images/Objects/Files/HelpFile24.png",
                                                "/LongWpfUI;component/Resources/Images/Objects/Files/HelpFile32.png",
                                                "/LongWpfUI;component/Resources/Images/Objects/Files/HelpFile48.png");
                }
                return helpFile;
            }
        }        
        /// <summary>
        /// Get source for InfoFile Icon
        /// </summary>
        public static IconAccessor InfoFile
        {
            get
            {
                if (infoFile == null)
                {
                    infoFile = new IconAccessor("/LongWpfUI;component/Resources/Images/Objects/Files/InfoFile16.png",
                                                "/LongWpfUI;component/Resources/Images/Objects/Files/InfoFile24.png",
                                                "/LongWpfUI;component/Resources/Images/Objects/Files/InfoFile32.png",
                                                "/LongWpfUI;component/Resources/Images/Objects/Files/InfoFile48.png");
                }
                return infoFile;
            }
        }        
        /// <summary>
        /// Get source for NewFile Icon
        /// </summary>
        public static IconAccessor NewFile
        {
            get
            {
                if (newFile == null)
                {
                    newFile = new IconAccessor("/LongWpfUI;component/Resources/Images/Objects/Files/NewFile16.png",
                                               "/LongWpfUI;component/Resources/Images/Objects/Files/NewFile24.png",
                                               "/LongWpfUI;component/Resources/Images/Objects/Files/NewFile32.png",
                                               "/LongWpfUI;component/Resources/Images/Objects/Files/NewFile48.png");
                }
                return newFile;
            }
        }
        /// <summary>
        /// Get source for SearchFile Icon
        /// </summary>
        public static IconAccessor SearchFile
        {
            get
            {
                if (searchFile == null)
                {
                    searchFile = new IconAccessor("/LongWpfUI;component/Resources/Images/Objects/Files/SearchFile16.png",
                                                  "/LongWpfUI;component/Resources/Images/Objects/Files/SearchFile24.png",
                                                  "/LongWpfUI;component/Resources/Images/Objects/Files/SearchFile32.png",
                                                  "/LongWpfUI;component/Resources/Images/Objects/Files/SearchFile48.png");
                }
                return searchFile;
            }
        }
        /// <summary>
        /// Get source for WarningFile Icon
        /// </summary>
        public static IconAccessor WarningFile
        {
            get
            {
                if (warningFile == null)
                {
                    warningFile = new IconAccessor("/LongWpfUI;component/Resources/Images/Objects/Files/WarningFile16.png",
                                                   "/LongWpfUI;component/Resources/Images/Objects/Files/WarningFile24.png",
                                                   "/LongWpfUI;component/Resources/Images/Objects/Files/WarningFile32.png",
                                                   "/LongWpfUI;component/Resources/Images/Objects/Files/WarningFile48.png");
                }
                return warningFile;
            }
        }

        
        /// <summary>
        /// Get source for ImportFile Icon
        /// </summary>
        public static IconAccessor ImportFile
        {
            get
            {
                if (importFile == null)
                {
                    importFile = new IconAccessor(string.Empty,
                                                  "/LongWpfUI;component/Resources/Images/Objects/Files/ImportFile24.png",
                                                  "/LongWpfUI;component/Resources/Images/Objects/Files/ImportFile32.png",
                                                  "/LongWpfUI;component/Resources/Images/Objects/Files/ImportFile48.png");
                }
                return importFile;
            }
        }
        /// <summary>
        /// Get source for ExportFile Icon
        /// </summary>
        public static IconAccessor ExportFile
        {
            get
            {
                if (exportFile == null)
                {
                    exportFile = new IconAccessor(string.Empty,
                                                  "/LongWpfUI;component/Resources/Images/Objects/Files/ExportFile24.png",
                                                  "/LongWpfUI;component/Resources/Images/Objects/Files/ExportFile32.png",
                                                  "/LongWpfUI;component/Resources/Images/Objects/Files/ExportFile48.png");
                }
                return exportFile;
            }
        }

        private static IconAccessor attachment;

        /// <summary>
        /// Get source for Attachment Icon
        /// </summary>
        public static IconAccessor Attachment
        {
            get
            {
                if (attachment == null)
                {
                    attachment = new IconAccessor("/LongWpfUI;component/Resources/Images/Objects/Files/Attachment16.png",
                                                  "/LongWpfUI;component/Resources/Images/Objects/Files/Attachment24.png",
                                                  "/LongWpfUI;component/Resources/Images/Objects/Files/Attachment32.png",
                                                  "/LongWpfUI;component/Resources/Images/Objects/Files/Attachment48.png");
                }
                return attachment;
            }
        }


    }
}
