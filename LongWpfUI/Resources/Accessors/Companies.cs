﻿using LongInterface.Views;
using LongInterface.Views.ControlModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongWpfUI.Resources.Accessors
{
    public partial class IconAccessor : IGetIcon
    {
        private static IconAccessor windowOs;

        /// <summary>
        /// Get source for WindowOS Icon
        /// </summary>
        public static IconAccessor WindowOS
        {
            get
            {
                if (windowOs == null)
                {
                    windowOs = new IconAccessor(string.Empty, string.Empty, string.Empty,
                                                "/LongWpfUI;component/Resources/Images/Companies/WindowOS48.png");
                }
                return windowOs;
            }
        }

    }
}
