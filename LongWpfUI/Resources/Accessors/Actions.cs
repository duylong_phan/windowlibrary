﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LongInterface.Views;
using LongInterface.Views.ControlModels;

namespace LongWpfUI.Resources.Accessors
{
    public partial class IconAccessor : IGetIcon
    {
        #region Instance 
        private string icon16;
        private string icon24;
        private string icon32;
        private string icon48;

        #region Getter & Setter
        /// <summary>
        /// Get Icon 16x16 source , always check the instance with HasIcon16
        /// </summary>
        public string Icon16
        {
            get
            {
                return this.icon16;
            }
        }
        /// <summary>
        /// Get Icon 24x24 source , always check the instance with HasIcon16
        /// </summary>
        public string Icon24
        {
            get
            {
                return this.icon24;
            }
        }
        /// <summary>
        /// Get Icon 48x48 source 
        /// </summary>
        public string Icon32
        {
            get
            {
                return this.icon32;
            }
        }
        /// <summary>
        /// Get Icon 48x48 source 
        /// </summary>
        public string Icon48
        {
            get
            {
                return this.icon48;
            }
        }
        #endregion

        #region Getter
        /// <summary>
        /// Get if the current instance support Icon size 16x16
        /// </summary>
        public bool HasIcon16
        {
            get
            {
                return (string.IsNullOrEmpty(this.icon16)) ? false : true;
            }
        }
        /// <summary>
        /// Get if the current instance support Icon size 24x24
        /// </summary>
        public bool HasIcon24
        {
            get
            {
                return (string.IsNullOrEmpty(this.icon24)) ? false : true;
            }
        }
        /// <summary>
        /// Get if the current instance support Icon size 32x32
        /// </summary>
        public bool HasIcon32
        {
            get
            {
                return (string.IsNullOrEmpty(this.icon32)) ? false : true;
            }
        }
        /// <summary>
        /// Get if the current instance support Icon size 48x48
        /// </summary>
        public bool HasIcon48
        {
            get
            {
                return (string.IsNullOrEmpty(this.icon48)) ? false : true;
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Initialize a new instance of IconAccessor
        /// </summary>
        /// <param name="icon16">initial 16x16</param>
        /// <param name="icon32">initial 32x32</param>
        /// <param name="icon48">initial 48x48</param>
        private IconAccessor(string icon16, string icon24, string icon32, string icon48)
        {
            this.icon16 = icon16;
            this.icon24 = icon24;
            this.icon32 = icon32;
            this.icon48 = icon48;
        }
        #endregion 
        #endregion

        #region Static
        private static IconAccessor ok;
        private static IconAccessor cancel;
        private static IconAccessor load;
        private static IconAccessor save;
        private static IconAccessor saveAs;  
        private static IconAccessor add;
        private static IconAccessor remove;
        private static IconAccessor setting;
        private static IconAccessor edit;
        private static IconAccessor search;
        private static IconAccessor close;
        private static IconAccessor send;
        private static IconAccessor receive;
        private static IconAccessor connect;
        private static IconAccessor disconnect;
        private static IconAccessor refresh;
        private static IconAccessor copy;
        private static IconAccessor cut;
        private static IconAccessor paste;
        private static IconAccessor configure;
        private static IconAccessor generate;
        private static IconAccessor upload;
        private static IconAccessor download;
        private static IconAccessor filter;

        





        /// <summary>
        /// Get source for Ok Icon
        /// </summary>
        public static IconAccessor Ok
        {
            get
            {
                if (ok == null)
                {
                    ok = new IconAccessor("/LongWpfUI;component/Resources/Images/Actions/Check16.png",
                                          "/LongWpfUI;component/Resources/Images/Actions/Check24.png",
                                          "/LongWpfUI;component/Resources/Images/Actions/Check32.png",
                                          "/LongWpfUI;component/Resources/Images/Actions/Check48.png");
                }
                return ok;
            }
        }
        /// <summary>
        /// Get source for Cancel Icon
        /// </summary>
        public static IconAccessor Cancel
        {
            get
            {
                if (cancel == null)
                {
                    cancel = new IconAccessor("/LongWpfUI;component/Resources/Images/Actions/Cancel16.png",
                                              "/LongWpfUI;component/Resources/Images/Actions/Cancel24.png",
                                              "/LongWpfUI;component/Resources/Images/Actions/Cancel32.png",
                                              "/LongWpfUI;component/Resources/Images/Actions/Cancel48.png");
                }
                return cancel;
            }
        }        
        /// <summary>
        /// Get source for Add Icon
        /// </summary>
        public static IconAccessor Add
        {
            get
            {
                if (add == null)
                {
                    add = new IconAccessor("/LongWpfUI;component/Resources/Images/Actions/Add16.png",
                                           "/LongWpfUI;component/Resources/Images/Actions/Add24.png",
                                           "/LongWpfUI;component/Resources/Images/Actions/Add32.png",
                                           "/LongWpfUI;component/Resources/Images/Actions/Add48.png");
                }
                return add;
            }
        }        
        /// <summary>
        /// Get source for Remove Icon
        /// </summary>
        public static IconAccessor Remove
        {
            get
            {
                if (remove == null)
                {
                    remove = new IconAccessor("/LongWpfUI;component/Resources/Images/Actions/Remove16.png",
                                              "/LongWpfUI;component/Resources/Images/Actions/Remove24.png",
                                              "/LongWpfUI;component/Resources/Images/Actions/Remove32.png",
                                              "/LongWpfUI;component/Resources/Images/Actions/Remove48.png");
                }
                return remove;
            }
        }       
        /// <summary>
        /// Get source for Setting Icon
        /// </summary>
        public static IconAccessor Setting
        {
            get
            {
                if (setting == null)
                {
                    setting = new IconAccessor("/LongWpfUI;component/Resources/Images/Actions/Settings16.png",
                                               "/LongWpfUI;component/Resources/Images/Actions/Settings24.png",
                                               "/LongWpfUI;component/Resources/Images/Actions/Settings32.png",
                                               "/LongWpfUI;component/Resources/Images/Actions/Settings48.png");
                }
                return setting;
            }
        }      
        /// <summary>
        /// Get source for Edit Icon
        /// </summary>
        public static IconAccessor Edit
        {
            get
            {
                if (edit == null)
                {
                    edit = new IconAccessor("/LongWpfUI;component/Resources/Images/Actions/Edit16.png",
                                            "/LongWpfUI;component/Resources/Images/Actions/Edit24.png",
                                            "/LongWpfUI;component/Resources/Images/Actions/Edit32.png",
                                            "/LongWpfUI;component/Resources/Images/Actions/Edit48.png");
                }
                return edit;
            }
        }
        /// <summary>
        /// Get source for Search Icon
        /// </summary>
        public static IconAccessor Search
        {
            get
            {
                if (search == null)
                {
                    search = new IconAccessor("/LongWpfUI;component/Resources/Images/Actions/Search16.png",
                                              "/LongWpfUI;component/Resources/Images/Actions/Search24.png",
                                              "/LongWpfUI;component/Resources/Images/Actions/Search32.png",
                                              "/LongWpfUI;component/Resources/Images/Actions/Search48.png");
                }
                return search;
            }
        }      
        /// <summary>
        /// Get source for Load Icon
        /// </summary>
        public static IconAccessor Load
        {
            get
            {
                if (load == null)
                {
                    load = new IconAccessor("/LongWpfUI;component/Resources/Images/Actions/Load16.png",
                                            "/LongWpfUI;component/Resources/Images/Actions/Load24.png",
                                            "/LongWpfUI;component/Resources/Images/Actions/Load32.png",
                                            "/LongWpfUI;component/Resources/Images/Actions/Load48.png");
                }
                return load;
            }
        }
        /// <summary>
        /// Get source for Close Icon
        /// </summary>
        public static IconAccessor Close
        {
            get
            {
                if (close == null)
                {
                    close = new IconAccessor("/LongWpfUI;component/Resources/Images/Actions/Close16.png",
                                             "/LongWpfUI;component/Resources/Images/Actions/Close24.png",
                                             "/LongWpfUI;component/Resources/Images/Actions/Close32.png",
                                             "/LongWpfUI;component/Resources/Images/Actions/Close48.png");
                }
                return close;
            }
        }
        /// <summary>
        /// Get source for Save Icon
        /// </summary>
        public static IconAccessor Save
        {
            get
            {
                if (save == null)
                {
                    save = new IconAccessor("/LongWpfUI;component/Resources/Images/Actions/Save16.png",
                                            "/LongWpfUI;component/Resources/Images/Actions/Save24.png",
                                            "/LongWpfUI;component/Resources/Images/Actions/Save32.png",
                                            "/LongWpfUI;component/Resources/Images/Actions/Save48.png");
                }
                return save;
            }
        }
        /// <summary>
        /// Get source for Name Icon
        /// </summary>
        public static IconAccessor SaveAs
        {
            get
            {
                if (saveAs == null)
                {
                    saveAs = new IconAccessor("/LongWpfUI;component/Resources/Images/Actions/SaveAs16.png",
                                              "/LongWpfUI;component/Resources/Images/Actions/SaveAs24.png",
                                              "/LongWpfUI;component/Resources/Images/Actions/SaveAs32.png",
                                              "/LongWpfUI;component/Resources/Images/Actions/SaveAs48.png");
                }
                return saveAs;
            }
        }        
        /// <summary>
        /// Get source for Send Icon
        /// </summary>
        public static IconAccessor Send
        {
            get
            {
                if (send == null)
                {
                    send = new IconAccessor(string.Empty,
                                            "/LongWpfUI;component/Resources/Images/Actions/Send24.png",
                                            "/LongWpfUI;component/Resources/Images/Actions/Send32.png",
                                            "/LongWpfUI;component/Resources/Images/Actions/Send48.png");
                }
                return send;
            }
        }        
        /// <summary>
        /// Get source for Receive Icon
        /// </summary>
        public static IconAccessor Receive
        {
            get
            {
                if (receive == null)
                {
                    receive = new IconAccessor(string.Empty,
                                               "/LongWpfUI;component/Resources/Images/Actions/Receive24.png",
                                               "/LongWpfUI;component/Resources/Images/Actions/Receive32.png",
                                               "/LongWpfUI;component/Resources/Images/Actions/Receive48.png");
                }
                return receive;
            }
        } 
        /// <summary>
        /// Get source for Connect Icon
        /// </summary>
        public static IconAccessor Connect
        {
            get
            {
                if (connect == null)
                {
                    connect = new IconAccessor("/LongWpfUI;component/Resources/Images/Actions/Connect16.png",
                                               "/LongWpfUI;component/Resources/Images/Actions/Connect24.png",
                                               "/LongWpfUI;component/Resources/Images/Actions/Connect32.png",
                                               "/LongWpfUI;component/Resources/Images/Actions/Connect48.png");
                }
                return connect;
            }
        }
        /// <summary>
        /// Get source for Disconnect Icon
        /// </summary>
        public static IconAccessor Disconnect
        {
            get
            {
                if (disconnect == null)
                {
                    disconnect = new IconAccessor(string.Empty,
                                                  "/LongWpfUI;component/Resources/Images/Actions/Disconnect24.png",
                                                  "/LongWpfUI;component/Resources/Images/Actions/Disconnect32.png",
                                                  string.Empty);
                }
                return disconnect;
            }
        }     
        /// <summary>
        /// Get source for Refresh Icon
        /// </summary>
        public static IconAccessor Refresh
        {
            get
            {
                if (refresh == null)
                {
                    refresh = new IconAccessor("/LongWpfUI;component/Resources/Images/Actions/Refresh16.png",
                                               "/LongWpfUI;component/Resources/Images/Actions/Refresh24.png",
                                               "/LongWpfUI;component/Resources/Images/Actions/Refresh32.png",
                                               "/LongWpfUI;component/Resources/Images/Actions/Refresh48.png");
                }
                return refresh;
            }
        }
        /// <summary>
        /// Get source for Copy Icon
        /// </summary>
        public static IconAccessor Copy
        {
            get
            {
                if (copy == null)
                {
                    copy = new IconAccessor("/LongWpfUI;component/Resources/Images/Actions/Copy16.png",
                                            "/LongWpfUI;component/Resources/Images/Actions/Copy24.png",
                                            "/LongWpfUI;component/Resources/Images/Actions/Copy32.png",
                                            "/LongWpfUI;component/Resources/Images/Actions/Copy48.png");
                }
                return copy;
            }
        }
        /// <summary>
        /// Get source for Cut Icon
        /// </summary>
        public static IconAccessor Cut
        {
            get
            {
                if (cut == null)
                {
                    cut = new IconAccessor("/LongWpfUI;component/Resources/Images/Actions/Cut16.png",
                                           "/LongWpfUI;component/Resources/Images/Actions/Cut24.png",
                                           "/LongWpfUI;component/Resources/Images/Actions/Cut32.png",
                                           "/LongWpfUI;component/Resources/Images/Actions/Cut48.png");
                }
                return cut;
            }
        }
        /// <summary>
        /// Get source for Paste Icon
        /// </summary>
        public static IconAccessor Paste
        {
            get
            {
                if (paste == null)
                {
                    paste = new IconAccessor("/LongWpfUI;component/Resources/Images/Actions/Paste16.png",
                                             "/LongWpfUI;component/Resources/Images/Actions/Paste24.png",
                                             "/LongWpfUI;component/Resources/Images/Actions/Paste32.png",
                                             "/LongWpfUI;component/Resources/Images/Actions/Paste48.png");
                }
                return paste;
            }
        }
        /// <summary>
        /// Get source for Configure Icon
        /// </summary>
        public static IconAccessor Configure
        {
            get
            {
                if (configure == null)
                {
                    configure = new IconAccessor("/LongWpfUI;component/Resources/Images/Actions/Configure16.png",
                                                 "/LongWpfUI;component/Resources/Images/Actions/Configure24.png",
                                                 "/LongWpfUI;component/Resources/Images/Actions/Configure32.png",
                                                 "/LongWpfUI;component/Resources/Images/Actions/Configure48.png");
                }
                return configure;
            }
        }
        /// <summary>
        /// Get source for Generate Icon
        /// </summary>
        public static IconAccessor Generate
        {
            get
            {
                if (generate == null)
                {
                    generate = new IconAccessor("/LongWpfUI;component/Resources/Images/Actions/Generate16.png",
                                                "/LongWpfUI;component/Resources/Images/Actions/Generate24.png",
                                                "/LongWpfUI;component/Resources/Images/Actions/Generate32.png",
                                                "/LongWpfUI;component/Resources/Images/Actions/Generate48.png");
                }
                return generate;
            }
        }
        /// <summary>
        /// Get source for Upload Icon
        /// </summary>
        public static IconAccessor Upload
        {
            get
            {
                if (upload == null)
                {
                    upload = new IconAccessor("/LongWpfUI;component/Resources/Images/Actions/Upload16.png",
                                              "/LongWpfUI;component/Resources/Images/Actions/Upload24.png",
                                              "/LongWpfUI;component/Resources/Images/Actions/Upload32.png",
                                              "/LongWpfUI;component/Resources/Images/Actions/Upload48.png");
                }
                return upload;
            }
        }
        /// <summary>
        /// Get source for Download Icon
        /// </summary>
        public static IconAccessor Download
        {
            get
            {
                if (download == null)
                {
                    download = new IconAccessor("/LongWpfUI;component/Resources/Images/Actions/Download16.png",
                                                "/LongWpfUI;component/Resources/Images/Actions/Download24.png",
                                                "/LongWpfUI;component/Resources/Images/Actions/Download32.png",
                                                "/LongWpfUI;component/Resources/Images/Actions/Download48.png");
                }
                return download;
            }
        }
        /// <summary>
        /// Get source for Filter Icon
        /// </summary>
        public static IconAccessor Filter
        {
            get
            {
                if (filter == null)
                {
                    filter = new IconAccessor("/LongWpfUI;component/Resources/Images/Actions/Filter16.png",
                                              "/LongWpfUI;component/Resources/Images/Actions/Filter24.png",
                                              "/LongWpfUI;component/Resources/Images/Actions/Filter32.png",
                                              "/LongWpfUI;component/Resources/Images/Actions/Filter48.png");
                }
                return filter;
            }
        }
        #endregion
    }
}
