﻿using LongInterface.Views;
using LongInterface.Views.ControlModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongWpfUI.Resources.Accessors
{
    public partial class IconAccessor : IGetIcon
    {
        private static IconAccessor ceres;
        private static IconAccessor earth;
        private static IconAccessor eris;
        private static IconAccessor jupiter;
        private static IconAccessor mars;
        private static IconAccessor mercury;
        private static IconAccessor moon;
        private static IconAccessor neptune;
        private static IconAccessor pluto;
        private static IconAccessor saturn;
        private static IconAccessor sun;
        private static IconAccessor uranus;
        private static IconAccessor venus;

        /// <summary>
        /// Get source for Ceres Icon
        /// </summary>
        public static IconAccessor Ceres
        {
            get
            {
                if (ceres == null)
                {
                    ceres = new IconAccessor(string.Empty, string.Empty, string.Empty,
                                             "/LongWpfUI;component/Resources/Images/Objects/Planets/Ceres48.png");
                }
                return ceres;
            }
        }        
        /// <summary>
        /// Get source for Earth Icon
        /// </summary>
        public static IconAccessor Earth
        {
            get
            {
                if (earth == null)
                {
                    earth = new IconAccessor(string.Empty, string.Empty, string.Empty,
                                             "/LongWpfUI;component/Resources/Images/Objects/Planets/Earth48.png");
                }
                return earth;
            }
        }
        /// <summary>
        /// Get source for Eris Icon
        /// </summary>
        public static IconAccessor Eris
        {
            get
            {
                if (eris == null)
                {
                    eris = new IconAccessor(string.Empty, string.Empty, string.Empty,
                                             "/LongWpfUI;component/Resources/Images/Objects/Planets/Eris48.png");
                }
                return eris;
            }
        }
        /// <summary>
        /// Get source for Jupiter Icon
        /// </summary>
        public static IconAccessor Jupiter
        {
            get
            {
                if (jupiter == null)
                {
                    jupiter = new IconAccessor(string.Empty, string.Empty, string.Empty,
                                             "/LongWpfUI;component/Resources/Images/Objects/Planets/Jupiter48.png");
                }
                return jupiter;
            }
        }
        /// <summary>
        /// Get source for Mars Icon
        /// </summary>
        public static IconAccessor Mars
        {
            get
            {
                if (mars == null)
                {
                    mars = new IconAccessor(string.Empty, string.Empty, string.Empty,
                                             "/LongWpfUI;component/Resources/Images/Objects/Planets/Mars48.png");
                }
                return mars;
            }
        }
        /// <summary>
        /// Get source for Mercury Icon
        /// </summary>
        public static IconAccessor Mercury
        {
            get
            {
                if (mercury == null)
                {
                    mercury = new IconAccessor(string.Empty, string.Empty, string.Empty,
                                             "/LongWpfUI;component/Resources/Images/Objects/Planets/Mercury48.png");
                }
                return mercury;
            }
        }
        /// <summary>
        /// Get source for Moon Icon
        /// </summary>
        public static IconAccessor Moon
        {
            get
            {
                if (moon == null)
                {
                    moon = new IconAccessor(string.Empty, string.Empty, string.Empty,
                                             "/LongWpfUI;component/Resources/Images/Objects/Planets/Moon48.png");
                }
                return moon;
            }
        }
        /// <summary>
        /// Get source for Neptune Icon
        /// </summary>
        public static IconAccessor Neptune
        {
            get
            {
                if (neptune == null)
                {
                    neptune = new IconAccessor(string.Empty, string.Empty, string.Empty,
                                             "/LongWpfUI;component/Resources/Images/Objects/Planets/Neptune48.png");
                }
                return neptune;
            }
        }
        /// <summary>
        /// Get source for Pluto Icon
        /// </summary>
        public static IconAccessor Pluto
        {
            get
            {
                if (pluto == null)
                {
                    pluto = new IconAccessor(string.Empty, string.Empty, string.Empty,
                                             "/LongWpfUI;component/Resources/Images/Objects/Planets/Pluto48.png");
                }
                return pluto;
            }
        }
        /// <summary>
        /// Get source for Saturn Icon
        /// </summary>
        public static IconAccessor Saturn
        {
            get
            {
                if (saturn == null)
                {
                    saturn = new IconAccessor(string.Empty, string.Empty, string.Empty,
                                             "/LongWpfUI;component/Resources/Images/Objects/Planets/Saturn48.png");
                }
                return saturn;
            }
        }
        /// <summary>
        /// Get source for Sun Icon
        /// </summary>
        public static IconAccessor Sun
        {
            get
            {
                if (sun == null)
                {
                    sun = new IconAccessor(string.Empty, string.Empty, string.Empty,
                                             "/LongWpfUI;component/Resources/Images/Objects/Planets/Sun48.png");
                }
                return sun;
            }
        }
        /// <summary>
        /// Get source for Uranus Icon
        /// </summary>
        public static IconAccessor Uranus
        {
            get
            {
                if (uranus == null)
                {
                    uranus = new IconAccessor(string.Empty, string.Empty, string.Empty,
                                             "/LongWpfUI;component/Resources/Images/Objects/Planets/Uranus48.png");
                }
                return uranus;
            }
        }
        /// <summary>
        /// Get source for Venus Icon
        /// </summary>
        public static IconAccessor Venus
        {
            get
            {
                if (venus == null)
                {
                    venus = new IconAccessor(string.Empty, string.Empty, string.Empty,
                                             "/LongWpfUI;component/Resources/Images/Objects/Planets/Venus48.png");
                }
                return venus;
            }
        }
    }
}
