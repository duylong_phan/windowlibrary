﻿using LongInterface.Views;
using LongInterface.Views.ControlModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongWpfUI.Resources.Accessors
{
    public partial class IconAccessor : IGetIcon
    {
        private static IconAccessor camera;
        private static IconAccessor cellPhone;
        private static IconAccessor keyBoard;
        private static IconAccessor monitor;
        private static IconAccessor mouse;
        private static IconAccessor noteBooks;
        private static IconAccessor pc;
        private static IconAccessor tablet;
        private static IconAccessor oscilloscope;
        private static IconAccessor usb;
        private static IconAccessor wlan;
        private static IconAccessor chip;

        /// <summary>
        /// Get source for Camera Icon
        /// </summary>
        public static IconAccessor Camera
        {
            get
            {
                if (camera == null)
                {
                    camera = new IconAccessor("/LongWpfUI;component/Resources/Images/Devices/Camera16.png",
                                              "/LongWpfUI;component/Resources/Images/Devices/Camera24.png",
                                              "/LongWpfUI;component/Resources/Images/Devices/Camera32.png",
                                              "/LongWpfUI;component/Resources/Images/Devices/Camera48.png");
                }
                return camera;
            }
        }
        /// <summary>
        /// Get source for CellPhone Icon
        /// </summary>
        public static IconAccessor CellPhone
        {
            get
            {
                if (cellPhone == null)
                {
                    cellPhone = new IconAccessor(string.Empty,
                                                 string.Empty,
                                                 "/LongWpfUI;component/Resources/Images/Devices/CellPhone32.png",
                                                 "/LongWpfUI;component/Resources/Images/Devices/CellPhone48.png");
                }
                return cellPhone;
            }
        }
        /// <summary>
        /// Get source for KeyBoard Icon
        /// </summary>
        public static IconAccessor KeyBoard
        {
            get
            {
                if (keyBoard == null)
                {
                    keyBoard = new IconAccessor(string.Empty,
                                                string.Empty,
                                                "/LongWpfUI;component/Resources/Images/Devices/KeyBoard32.png",
                                                "/LongWpfUI;component/Resources/Images/Devices/Camera48.png");
                }
                return keyBoard;
            }
        }
        /// <summary>
        /// Get source for Monitor Icon
        /// </summary>
        public static IconAccessor Monitor
        {
            get
            {
                if (monitor == null)
                {
                    monitor = new IconAccessor(string.Empty,
                                               string.Empty,
                                               "/LongWpfUI;component/Resources/Images/Devices/Monitor32.png",
                                               "/LongWpfUI;component/Resources/Images/Devices/Monitor48.png");
                }
                return monitor;
            }
        }
        /// <summary>
        /// Get source for Mouse Icon
        /// </summary>
        public static IconAccessor Mouse
        {
            get
            {
                if (mouse == null)
                {
                    mouse = new IconAccessor(string.Empty,
                                             string.Empty,
                                             "/LongWpfUI;component/Resources/Images/Devices/Mouse32.png",
                                             "/LongWpfUI;component/Resources/Images/Devices/Mouse48.png");
                }
                return mouse;
            }
        }
        /// <summary>
        /// Get source for NoteBooks Icon
        /// </summary>
        public static IconAccessor NoteBooks
        {
            get
            {
                if (noteBooks == null)
                {
                    noteBooks = new IconAccessor(string.Empty,
                                                 string.Empty,
                                                 "/LongWpfUI;component/Resources/Images/Devices/NoteBooks32.png",
                                                 "/LongWpfUI;component/Resources/Images/Devices/NoteBooks48.png");
                }
                return noteBooks;
            }
        }
        /// <summary>
        /// Get source for PC Icon
        /// </summary>
        public static IconAccessor PC
        {
            get
            {
                if (pc == null)
                {
                    pc = new IconAccessor(string.Empty,
                                          string.Empty,
                                          "/LongWpfUI;component/Resources/Images/Devices/PC32.png",
                                          "/LongWpfUI;component/Resources/Images/Devices/PC48.png");
                }
                return pc;
            }
        }
        /// <summary>
        /// Get source for Tablet Icon
        /// </summary>
        public static IconAccessor Tablet
        {
            get
            {
                if (tablet == null)
                {
                    tablet = new IconAccessor(string.Empty,
                                              string.Empty,
                                              "/LongWpfUI;component/Resources/Images/Devices/Tablet32.png",
                                              "/LongWpfUI;component/Resources/Images/Devices/Tablet48.png");
                }
                return tablet;
            }
        }
        /// <summary>
        /// Get source for Oscilloscope Icon
        /// </summary>
        public static IconAccessor Oscilloscope
        {
            get
            {
                if (oscilloscope == null)
                {
                    oscilloscope = new IconAccessor(string.Empty,
                                                    string.Empty,
                                                    "/LongWpfUI;component/Resources/Images/Devices/Oscilloscope32.png",
                                                    "/LongWpfUI;component/Resources/Images/Devices/Oscilloscope48.png");
                }
                return oscilloscope;
            }
        }
        /// <summary>
        /// Get source for USB Icon
        /// </summary>
        public static IconAccessor USB
        {
            get
            {
                if (usb == null)
                {
                    usb = new IconAccessor(string.Empty,
                                           string.Empty,
                                           "/LongWpfUI;component/Resources/Images/Devices/USB32.png",
                                           "/LongWpfUI;component/Resources/Images/Devices/USB48.png");
                }
                return usb;
            }
        }
        /// <summary>
        /// Get source for WLAN Icon
        /// </summary>
        public static IconAccessor WLAN
        {
            get
            {
                if (wlan == null)
                {
                    wlan = new IconAccessor(string.Empty,
                                            string.Empty,
                                            "/LongWpfUI;component/Resources/Images/Devices/WLAN32.png",
                                            "/LongWpfUI;component/Resources/Images/Devices/WLAN48.png");
                }
                return wlan;
            }
        }
        /// <summary>
        /// Get source for Chip Icon
        /// </summary>
        public static IconAccessor Chip
        {
            get
            {
                if (chip == null)
                {
                    chip = new IconAccessor(string.Empty,
                                            string.Empty,
                                            "/LongWpfUI;component/Resources/Images/Devices/Chip32.png",
                                            "/LongWpfUI;component/Resources/Images/Devices/Chip48.png");
                }
                return chip;
            }
        }
    }
}
