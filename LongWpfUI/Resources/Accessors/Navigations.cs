﻿using LongInterface.Views;
using LongInterface.Views.ControlModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongWpfUI.Resources.Accessors
{
    public partial class IconAccessor : IGetIcon
    {
        private static IconAccessor left;
        private static IconAccessor right;
        private static IconAccessor top;
        private static IconAccessor bottom;
        private static IconAccessor warning;
        private static IconAccessor openFolder;
        private static IconAccessor simpleFolder;

        /// <summary>
        /// Get source for Left Icon
        /// </summary>
        public static IconAccessor Left
        {
            get
            {
                if (left == null)
                {
                    left = new IconAccessor("/LongWpfUI;component/Resources/Images/Navigations/Left16.png",
                                            "/LongWpfUI;component/Resources/Images/Navigations/Left24.png",
                                            "/LongWpfUI;component/Resources/Images/Navigations/Left32.png",
                                            "/LongWpfUI;component/Resources/Images/Navigations/Left48.png");
                }
                return left;
            }
        }        
        /// <summary>
        /// Get source for Right Icon
        /// </summary>
        public static IconAccessor Right
        {
            get
            {
                if (right == null)
                {
                    right = new IconAccessor("/LongWpfUI;component/Resources/Images/Navigations/Right16.png",
                                             "/LongWpfUI;component/Resources/Images/Navigations/Right24.png",
                                            "/LongWpfUI;component/Resources/Images/Navigations/Right32.png",
                                            "/LongWpfUI;component/Resources/Images/Navigations/Right48.png");
                }
                return right;
            }
        }        
        /// <summary>
        /// Get source for Top Icon
        /// </summary>
        public static IconAccessor Top
        {
            get
            {
                if (top == null)
                {
                    top = new IconAccessor("/LongWpfUI;component/Resources/Images/Navigations/Top16.png",
                                           "/LongWpfUI;component/Resources/Images/Navigations/Top24.png",
                                           "/LongWpfUI;component/Resources/Images/Navigations/Top32.png",
                                           "/LongWpfUI;component/Resources/Images/Navigations/Top48.png");
                }
                return top;
            }
        }      
        /// <summary>
        /// Get source for Bottom Icon
        /// </summary>
        public static IconAccessor Bottom
        {
            get
            {
                if (bottom == null)
                {
                    bottom = new IconAccessor("/LongWpfUI;component/Resources/Images/Navigations/Bottom16.png",
                                              "/LongWpfUI;component/Resources/Images/Navigations/Bottom24.png",
                                              "/LongWpfUI;component/Resources/Images/Navigations/Bottom32.png",
                                              "/LongWpfUI;component/Resources/Images/Navigations/Bottom48.png");
                }
                return bottom;
            }
        }
        /// <summary>
        /// Get source for Warning Icon
        /// </summary>
        public static IconAccessor Warning
        {
            get
            {
                if (warning == null)
                {
                    warning = new IconAccessor("/LongWpfUI;component/Resources/Images/Navigations/Warning16.png",
                                               "/LongWpfUI;component/Resources/Images/Navigations/Warning24.png",
                                               "/LongWpfUI;component/Resources/Images/Navigations/Warning32.png",
                                               "/LongWpfUI;component/Resources/Images/Navigations/Warning48.png");
                }
                return warning;
            }
        }
        /// <summary>
        /// Get source for OpenFolder Icon
        /// </summary>
        public static IconAccessor OpenFolder
        {
            get
            {
                if (openFolder == null)
                {
                    openFolder = new IconAccessor("/LongWpfUI;component/Resources/Images/Navigations/OpenFolder16.png",
                                                  "/LongWpfUI;component/Resources/Images/Navigations/OpenFolder24.png",
                                                  "/LongWpfUI;component/Resources/Images/Navigations/OpenFolder32.png",
                                                  "/LongWpfUI;component/Resources/Images/Navigations/OpenFolder48.png");
                }
                return openFolder;
            }
        }
        /// <summary>
        /// Get source for SimpleFolder Icon
        /// </summary>
        public static IconAccessor SimpleFolder
        {
            get
            {
                if (simpleFolder == null)
                {
                    simpleFolder = new IconAccessor("/LongWpfUI;component/Resources/Images/Navigations/SimpleFolder16.png",
                                                    "/LongWpfUI;component/Resources/Images/Navigations/SimpleFolder24.png",
                                                    "/LongWpfUI;component/Resources/Images/Navigations/SimpleFolder32.png",
                                                    "/LongWpfUI;component/Resources/Images/Navigations/SimpleFolder48.png");
                }
                return simpleFolder;
            }
        }

    }
}
