﻿using LongInterface.Views;
using LongInterface.Views.ControlModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongWpfUI.Resources.Accessors
{
    public partial class IconAccessor : IGetIcon
    {
        private static IconAccessor help;
        private static IconAccessor support;
        private static IconAccessor info;

        /// <summary>
        /// Get source for Help Icon
        /// </summary>
        public static IconAccessor Help
        {
            get
            {
                if (help == null)
                {
                    help = new IconAccessor("/LongWpfUI;component/Resources/Images/Helpers/Help16.png",
                                            "/LongWpfUI;component/Resources/Images/Helpers/Help24.png",
                                            "/LongWpfUI;component/Resources/Images/Helpers/Help32.png",
                                            "/LongWpfUI;component/Resources/Images/Helpers/Help48.png");
                }
                return help;
            }
        }
        /// <summary>
        /// Get source for Support Icon
        /// </summary>
        public static IconAccessor Support
        {
            get
            {
                if (support == null)
                {
                    support = new IconAccessor("/LongWpfUI;component/Resources/Images/Helpers/Support16.png",
                                               "/LongWpfUI;component/Resources/Images/Helpers/Support24.png",
                                               "/LongWpfUI;component/Resources/Images/Helpers/Support32.png",
                                               "/LongWpfUI;component/Resources/Images/Helpers/Support48.png");
                }
                return support;
            }
        }
        /// <summary>
        /// Get source for Info Icon
        /// </summary>
        public static IconAccessor Info
        {
            get
            {
                if (info == null)
                {
                    info = new IconAccessor("/LongWpfUI;component/Resources/Images/Helpers/Info16.png",
                                            "/LongWpfUI;component/Resources/Images/Helpers/Info24.png",
                                            "/LongWpfUI;component/Resources/Images/Helpers/Info32.png",
                                            "/LongWpfUI;component/Resources/Images/Helpers/Info48.png");
                }
                return info;
            }
        }

    }
}
