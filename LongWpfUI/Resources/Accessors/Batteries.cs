﻿using LongInterface.Views;
using LongInterface.Views.ControlModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongWpfUI.Resources.Accessors
{
    public partial class IconAccessor : IGetIcon
    {
        private static IconAccessor almostFullBattery;
        private static IconAccessor chargedFullBattery;
        private static IconAccessor chargingBattery;
        private static IconAccessor fullBattery;
        private static IconAccessor halfBattery;

        /// <summary>
        /// Get source for AlmostFullBattery Icon
        /// </summary>
        public static IconAccessor AlmostFullBattery
        {
            get
            {
                if (almostFullBattery == null)
                {
                    almostFullBattery = new IconAccessor(string.Empty, string.Empty,
                                                         "/LongWpfUI;component/Resources/Images/Objects/Battery/AlmostFullBattery32.png",
                                                         "/LongWpfUI;component/Resources/Images/Objects/Battery/AlmostFullBattery48.png");
                }
                return almostFullBattery;
            }
        }
                
        /// <summary>
        /// Get source for ChargedFullBattery Icon
        /// </summary>
        public static IconAccessor ChargedFullBattery
        {
            get
            {
                if (chargedFullBattery == null)
                {
                    chargedFullBattery = new IconAccessor(string.Empty, string.Empty,
                                                         "/LongWpfUI;component/Resources/Images/Objects/Battery/ChargedFullBattery32.png",
                                                         "/LongWpfUI;component/Resources/Images/Objects/Battery/ChargedFullBattery48.png");
                }
                return chargedFullBattery;
            }
        }
                
        /// <summary>
        /// Get source for ChargingBattery Icon
        /// </summary>
        public static IconAccessor ChargingBattery
        {
            get
            {
                if (chargingBattery == null)
                {
                    chargingBattery = new IconAccessor(string.Empty, string.Empty,
                                                       "/LongWpfUI;component/Resources/Images/Objects/Battery/ChargingBattery32.png",
                                                       "/LongWpfUI;component/Resources/Images/Objects/Battery/ChargingBattery48.png");
                }
                return chargingBattery;
            }
        }
        
        /// <summary>
        /// Get source for FullBattery Icon
        /// </summary>
        public static IconAccessor FullBattery
        {
            get
            {
                if (fullBattery == null)
                {
                    fullBattery = new IconAccessor(string.Empty, string.Empty,
                                                   "/LongWpfUI;component/Resources/Images/Objects/Battery/FullBattery32.png",
                                                   "/LongWpfUI;component/Resources/Images/Objects/Battery/FullBattery48.png");
                }
                return fullBattery;
            }
        }
        
        /// <summary>
        /// Get source for HalfBattery Icon
        /// </summary>
        public static IconAccessor HalfBattery
        {
            get
            {
                if (halfBattery == null)
                {
                    halfBattery = new IconAccessor(string.Empty, string.Empty,
                                                   "/LongWpfUI;component/Resources/Images/Objects/Battery/HalfBattery32.png",
                                                   "/LongWpfUI;component/Resources/Images/Objects/Battery/HalfBattery48.png");
                }
                return halfBattery;
            }
        }

    }
}
