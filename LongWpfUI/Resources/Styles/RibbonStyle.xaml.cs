﻿using LongWpfUI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace LongWpfUI.Resources.Styles
{
    public partial class RibbonStyle
    {
        #region Event handler
        private void Ribbon_Loaded(object sender, RoutedEventArgs e)
        {
            var element = sender as FrameworkElement;
            if (element != null)
            {
                var grid = ControlHelper.GetChildControl<Grid>(element);
                if (grid != null && grid.RowDefinitions.Count > 0)
                {
                    grid.RowDefinitions[0].Height = new GridLength(0);
                }
            }
        }
        #endregion
    }
}
