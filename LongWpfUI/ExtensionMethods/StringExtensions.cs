﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace LongWpfUI.ExtensionMethods
{
    public static class StringExtensions
    {
        /// <summary>
        /// Create new directory if it does not exist
        /// </summary>
        /// <param name="value"></param>
        public static void CreateDirWhenNoExist(this string value)
        {
            if (!Directory.Exists(value))   //NOT
            {
                Directory.CreateDirectory(value);
            }
        }

        /// <summary>
        /// Get all sub file paths
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static List<string> GetSubFilePaths(this string value)
        {
            List<string> collection = new List<string>();

            if (Directory.Exists(value))
            {
                collection.AddRange(Directory.GetFiles(value));
                var subFolders = Directory.GetDirectories(value);
                foreach (var item in subFolders)
                {
                    collection.AddRange(item.GetSubFilePaths());
                }
            }

            return collection;
        }

        
    }
}
