﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongWpfUI.ExtensionMethods
{
    public static class DirectoryInfoExtensions
    {
        public static void DeleteAll(this DirectoryInfo value)
        {
            var files = value.GetFiles();
            foreach (var item in files)
            {
                item.Delete();
            }

            var folders = value.GetDirectories();
            foreach (var item in folders)
            {
                item.DeleteAll();
            }

            value.Delete();
        }
    }
}
