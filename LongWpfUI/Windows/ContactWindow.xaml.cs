﻿using LongModel.Views.Infos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LongWpfUI.Windows
{    
    public partial class ContactWindow : Window
    {
        #region Getter
        public List<ContactInfo> Contacts
        {
            get { return (List<ContactInfo>)GetValue(ContactsProperty); }
            set { SetValue(ContactsProperty, value); }
        }
        public string CompanyImage
        {
            get { return (string)GetValue(CompanyImageProperty); }
            set { SetValue(CompanyImageProperty, value); }
        }

        public static readonly DependencyProperty ContactsProperty =
            DependencyProperty.Register("Contacts", typeof(List<ContactInfo>), typeof(ContactWindow), new PropertyMetadata(null));
        
        public static readonly DependencyProperty CompanyImageProperty =
            DependencyProperty.Register("CompanyImage", typeof(string), typeof(ContactWindow), new PropertyMetadata(null));
        #endregion

        #region Constructor
        public ContactWindow(Window owner, List<ContactInfo> contacts, string companyImage)
        {
            InitializeComponent();
            this.Owner = owner;
            this.Contacts = contacts;
            this.CompanyImage = companyImage;
        }
        #endregion
    }
}
