﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LongWpfUI.Windows
{
    public partial class ProgressingWindow : Window
    {
        #region Const
        public const int DelayTime = 50;
        #endregion

        #region Static Field
        private static ProgressingWindow instance;
        #endregion

        #region Static Method
        public static bool HasWindow
        {
            get
            {
                return instance != null;
            }
        }

        public static async void ShowDialog(Window owner, string message, double width)
        {
            if (instance == null)
            {
                await Task.Delay(DelayTime);
                instance = new ProgressingWindow(owner, message, width);
                instance.ShowDialog();
            }
        }

        public static async void ShowDialog(Window owner, string message)
        {
            if (instance == null)
            {
                await Task.Delay(DelayTime);
                instance = new ProgressingWindow(owner, message);
                instance.ShowDialog();
            }
        }

        public static void CloseDialog()
        {
            if (instance != null)
            {
                instance.Close();
                instance = null;
            }
        }
        #endregion


        #region private Constructor
        private ProgressingWindow(Window owner, string message, double width)
            : this(owner, message)
        {
            this.Width = width;
        }

        private ProgressingWindow(Window owner, string message)
        {
            InitializeComponent();
            this.Owner = owner;
            this.textBlock.Text = message;
        }
        #endregion
    }
}
