﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using LongWpfUI.Helpers;
using System.Windows.Input;

namespace LongWpfUI.Windows
{
    public class DialogBase : Window
    {
        #region DP
        /// <summary>
        /// Get, Set SaveCommand
        /// </summary>
        public ICommand SaveCommand
        {
            get { return (ICommand)GetValue(SaveCommandProperty); }
            set { SetValue(SaveCommandProperty, value); }
        }

        /// <summary>
        /// Get, Set CancelCommand
        /// </summary>
        public ICommand CancelCommand
        {
            get { return (ICommand)GetValue(CancelCommandProperty); }
            set { SetValue(CancelCommandProperty, value); }
        }

        /// <summary>
        /// Get, set MainPanel
        /// </summary>
        public Panel MainPanel
        {
            get { return (Panel)GetValue(MainPanelProperty); }
            set { SetValue(MainPanelProperty, value); }
        }

        public static readonly DependencyProperty SaveCommandProperty =
             DependencyProperty.Register("SaveCommand", typeof(ICommand), typeof(DialogBase), new PropertyMetadata(null));

        public static readonly DependencyProperty CancelCommandProperty =
            DependencyProperty.Register("CancelCommand", typeof(ICommand), typeof(DialogBase), new PropertyMetadata(null));

        public static readonly DependencyProperty MainPanelProperty =
            DependencyProperty.Register("MainPanel", typeof(Panel), typeof(DialogBase), new PropertyMetadata(null));
        #endregion

        #region Protected Method
        /// <summary>
        /// Default action for save
        /// </summary>
        protected virtual void Exe_Save()
        {
            string message = string.Empty;
            bool hasError = this.HasInputError(this.MainPanel, out message);
            if (hasError)
            {
                MessageBox.Show("Input values are invalid: " + message, "Invalid input", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            this.DialogResult = true;
            this.Close();
        }

        /// <summary>
        /// Default action for cancel
        /// </summary>
        protected virtual void Exe_Cancel()
        {
            this.DialogResult = false;
            this.Close();
        }

        /// <summary>
        /// Check if input has corrent
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        protected virtual bool HasInputError(DependencyObject parent, out string message)
        {
            bool hasError = false;
            message = string.Empty;

            if (parent != null)
            {
                List<TextBox> collection = ControlHelper.GetChildrenControl<TextBox>(parent);
                foreach (TextBox item in collection)
                {
                    if (item != null && Validation.GetHasError(item))
                    {
                        hasError = true;
                        message = item.Text;
                        break;
                    }
                }
            }
            else
            {
                hasError = true;
                message = "Given parent instance is null";
            }

            return hasError;
        }
        #endregion

        #region Protected Event
        /// <summary>
        /// Event when panel is loaded, which is used to check input error
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void MainPanel_Loaded(object sender, RoutedEventArgs e)
        {
            this.MainPanel = sender as Panel;
        }

        /// <summary>
        /// Event when input selection is about to change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void InputSelection_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {

        } 
        #endregion
    }
}
