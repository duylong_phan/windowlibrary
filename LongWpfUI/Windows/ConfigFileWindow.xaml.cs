﻿using System.Windows;
using System.Windows.Input;
using System.Collections;
using LongInterface.ViewModels.Logics;
using LongWpfUI.Interfaces;
using LongWpfUI.Models.Commands;

namespace LongWpfUI.Windows
{
    public partial class ConfigFileWindow : DialogBase
    {
        #region DP
        public string Title1
        {
            get { return (string)GetValue(Title1Property); }
            set { SetValue(Title1Property, value); }
        }

        public string Title2
        {
            get { return (string)GetValue(Title2Property); }
            set { SetValue(Title2Property, value); }
        }

        public IEnumerable ItemsSource
        {
            get { return (IEnumerable)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        public object SelectedItem
        {
            get { return (object)GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }

        public Style ListBoxStyle
        {
            get { return (Style)GetValue(ListBoxStyleProperty); }
            set { SetValue(ListBoxStyleProperty, value); }
        }

        public Style ContentControlStyle
        {
            get { return (Style)GetValue(ContentControlStyleProperty); }
            set { SetValue(ContentControlStyleProperty, value); }
        }

        public IAddRemoveHandler AddRemoveHandler
        {
            get { return (IAddRemoveHandler)GetValue(AddRemoveHandlerProperty); }
            set { SetValue(AddRemoveHandlerProperty, value); }
        }

        public IDropTargetHandler DropTargetHandler
        {
            get { return (IDropTargetHandler)GetValue(DropTargetHandlerProperty); }
            set { SetValue(DropTargetHandlerProperty, value); }
        }

        public ICommand LoadFile
        {
            get { return (ICommand)GetValue(LoadFileProperty); }
            set { SetValue(LoadFileProperty, value); }
        }

        public static readonly DependencyProperty Title1Property =
            DependencyProperty.Register("Title1", typeof(string), typeof(ConfigFileWindow), new PropertyMetadata("Setting collection"));

        public static readonly DependencyProperty Title2Property =
            DependencyProperty.Register("Title2", typeof(string), typeof(ConfigFileWindow), new PropertyMetadata("Setting detail"));

        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register("ItemsSource", typeof(IEnumerable), typeof(ConfigFileWindow), 
                                        new FrameworkPropertyMetadata(null) { BindsTwoWayByDefault = true });

        public static readonly DependencyProperty SelectedItemProperty =
            DependencyProperty.Register("SelectedItem", typeof(object), typeof(ConfigFileWindow), new PropertyMetadata(null));

        public static readonly DependencyProperty ListBoxStyleProperty =
            DependencyProperty.Register("ListBoxStyle", typeof(Style), typeof(ConfigFileWindow), new PropertyMetadata(null));

        public static readonly DependencyProperty ContentControlStyleProperty =
            DependencyProperty.Register("ContentControlStyle", typeof(Style), typeof(ConfigFileWindow), new PropertyMetadata(null));

        public static readonly DependencyProperty AddRemoveHandlerProperty =
             DependencyProperty.Register("AddRemoveHandler", typeof(IAddRemoveHandler), typeof(ConfigFileWindow), new PropertyMetadata(null));

        public static readonly DependencyProperty DropTargetHandlerProperty =
            DependencyProperty.Register("DropTargetHandler", typeof(IDropTargetHandler), typeof(ConfigFileWindow), new PropertyMetadata(null));

        public static readonly DependencyProperty LoadFileProperty =
            DependencyProperty.Register("LoadFile", typeof(ICommand), typeof(ConfigFileWindow), new PropertyMetadata(null));
        #endregion

        #region Constructor
        public ConfigFileWindow()
            : this(null, null, null, null, null, null)
        {

        }

        public ConfigFileWindow(Window owner, IDropTargetHandler dropTargetHandler, IAddRemoveHandler addRemoveHandler, ICommand loadFile, Style listBoxStyle, Style contentControlStyle)
        {
            InitializeComponent();
            this.Owner = owner;
            this.DropTargetHandler = dropTargetHandler;
            this.AddRemoveHandler = addRemoveHandler;
            this.LoadFile = loadFile;
            this.ListBoxStyle = listBoxStyle;
            this.ContentControlStyle = contentControlStyle;

            this.SaveCommand = new WpfRelayCommand(Exe_Save);
            this.CancelCommand = new WpfRelayCommand(Exe_Cancel);
        }
        #endregion

        #region Override Method
        protected override void Exe_Save()
        {
            this.DialogResult = true;
            this.Close();
        }

        protected override void Exe_Cancel()
        {
            this.DialogResult = false;
            this.Close();
        } 
        #endregion
    }
}
