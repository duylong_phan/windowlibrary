﻿using LongInterface.Views.Documents;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LongWpfUI.Windows
{
    public partial class PrintTestWindow : Window
    {
        #region Field
        private LongWpfUI.UserControls.DocumentViewer documentViewer;
        private IList dataCollection;
        #endregion

        #region Constructor
        /// <summary>
        /// Create a Dummy Window, in order to measure the Item size
        /// </summary>
        /// <param name="dataCollection">given data collection</param>
        /// <param name="documentViewer">given documentViewer</param>
        /// <param name="viewDictResource">given dictResource</param>
        public PrintTestWindow(IList dataCollection, LongWpfUI.UserControls.DocumentViewer documentViewer, IEnumerable<ResourceDictionary> viewDictResource)
        {
            InitializeComponent();
            this.documentViewer = documentViewer;
            this.dataCollection = dataCollection;
            this.DataContext = dataCollection;
            if (viewDictResource != null)
            {
                foreach (var item in viewDictResource)
                {
                    this.Resources.MergedDictionaries.Add(item);
                }
            }
            this.ContentRendered += PrintTestWindow_ContentRendered;
        }
        #endregion

        #region Event Handler
        private void PrintTestWindow_ContentRendered(object sender, EventArgs e)
        {
            this.ContentRendered -= PrintTestWindow_ContentRendered;
            if (this.itemsControl == null || this.documentViewer == null || this.dataCollection == null)
            {
                return;
            }

            LengthConverter converter = new LengthConverter();
            double maxHeight = (double)converter.ConvertFrom(LongWpfUI.UserControls.DocumentViewer.MaxWriteHeight);

            List<IDocumentPage> pageCollection = new List<IDocumentPage>();

            int pageIndex = 1;
            IDocumentPage currentPage = this.documentViewer.CreateDocumentPage(this.documentViewer.Header, this.documentViewer.Footer, pageIndex);
            double currentHeight = 0;

            //Check real size of Template
            for (int i = 0; i < this.dataCollection.Count; i++)
            {
                ContentPresenter control = this.itemsControl.ItemContainerGenerator.ContainerFromIndex(i) as ContentPresenter;
                if (control != null)
                {
                    currentHeight += control.ActualHeight;

                    if (currentHeight > maxHeight)
                    {
                        //add page
                        pageCollection.Add(currentPage);

                        //create new page
                        pageIndex++;
                        currentPage = this.documentViewer.CreateDocumentPage(this.documentViewer.Header, this.documentViewer.Footer, pageIndex);
                        currentHeight = control.ActualHeight;
                    }
                    currentPage.Content.Add(this.dataCollection[i]);
                }
            }

            //check last page
            if (currentPage.Content.Count > 0)
            {
                pageCollection.Add(currentPage);
            }

            //assign page Collection
            this.documentViewer.ItemsSource = pageCollection;

            //dispose
            this.Resources.MergedDictionaries.Clear();
            this.Close();
        }
        #endregion
    }
}
