﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using LongModel.Views.Infos.HelpInfos;
using LongWpfUI.Models.Selectors;

namespace LongWpfUI.Windows
{
    public partial class HelpWindow : Window
    {
        #region Constructor
        public HelpWindow(Window owner, TaskHelpInfo info)
        {
            this.Initialized += HelpWindow_Initialized;
            InitializeComponent();
            this.Owner = owner;
            this.DataContext = info;
        }
        #endregion

        #region Event Handler
        private void HelpWindow_Initialized(object sender, EventArgs e)
        {
            var selector = this.Resources["helpTemplateSelector"] as HelpTemplateSelector;
            if (selector != null)
            {
                selector.TextContent = this.Resources["helpTextContentTemplate"] as DataTemplate;
                selector.BulletContent = this.Resources["helpBulletTextTemplate"] as DataTemplate;
                selector.ImageContent = this.Resources["helpImageContentTemplate"] as DataTemplate;
            }
        }
        #endregion
    }
}
