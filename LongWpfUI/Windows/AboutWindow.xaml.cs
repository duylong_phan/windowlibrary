﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using LongModel.Views.Infos;
using LongInterface.Views.Infos;

namespace LongWpfUI.Windows
{
    public partial class AboutWindow : Window
    {
        #region DP


        public string SoftwareName
        {
            get { return (string)GetValue(SoftwareNameProperty); }
            set { SetValue(SoftwareNameProperty, value); }
        }

        
       


        public ISectionInfo Description
        {
            get { return (ISectionInfo)GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }
        public ISectionInfo Function
        {
            get { return (ISectionInfo)GetValue(FunctionProperty); }
            set { SetValue(FunctionProperty, value); }
        }
        public ISectionInfo Reference
        {
            get { return (ISectionInfo)GetValue(ReferenceProperty); }
            set { SetValue(ReferenceProperty, value); }
        }
        public ISectionInfo Support
        {
            get { return (ISectionInfo)GetValue(SupportProperty); }
            set { SetValue(SupportProperty, value); }
        }

        public static readonly DependencyProperty SoftwareNameProperty =
           DependencyProperty.Register("SoftwareName", typeof(string), typeof(AboutWindow), new PropertyMetadata(string.Empty));

        public static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register("Description", typeof(ISectionInfo), typeof(AboutWindow), new PropertyMetadata(null));
                
        public static readonly DependencyProperty FunctionProperty =
            DependencyProperty.Register("Function", typeof(ISectionInfo), typeof(AboutWindow), new PropertyMetadata(null));
        
        public static readonly DependencyProperty ReferenceProperty =
            DependencyProperty.Register("Reference", typeof(ISectionInfo), typeof(AboutWindow), new PropertyMetadata(null));
               
        public static readonly DependencyProperty SupportProperty =
            DependencyProperty.Register("Support", typeof(ISectionInfo), typeof(AboutWindow), new PropertyMetadata(null));
        #endregion

        #region Constructor
        public AboutWindow(Window owner, string softwareName, ISectionInfo description, ISectionInfo function, ISectionInfo reference, ISectionInfo support)
        {
            InitializeComponent();
            this.Owner = owner;
            this.SoftwareName = softwareName;
            this.Description = description;
            this.Function = function;
            this.Reference = reference;
            this.Support = support;
        }
        #endregion
    }
}
