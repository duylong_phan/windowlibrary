﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;

namespace LongWpfUI.SystemHelper
{
    public static class FileAssociation
    {
        /// <summary>
        /// Check if given key is available
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool IsKeyAvailable(string key)
        {
            bool isAvailable = false;

            using (RegistryKey softwareClasses = GetSoftwareClasses())
            using(RegistryKey registry = softwareClasses.OpenSubKey(key))
            {
                isAvailable = (registry != null);
            }
            return isAvailable;
        }

        /// <summary>
        /// Check if given file extension is available
        /// </summary>
        /// <param name="extension"></param>
        /// <returns></returns>
        public static bool IsAssociated(string extension)
        {
            if (!extension.StartsWith("."))     //NOT
            {
                throw new Exception("Extension has to start with dot(.)");
            }

            return IsKeyAvailable(extension);
        }

        /// <summary>
        /// Create a new file Extension, which associate with given software
        /// </summary>
        /// <param name="executePath">absolute *.exe path</param>
        /// <param name="fileExtension">file extention with dot on beginning</param>
        /// <param name="progID">unique ID</param>
        /// <param name="description">short description, ex: CPM Database</param>
        /// <param name="iconPath">absolute *.icon path</param>
        public static void CreateAssociation(string executePath, string fileExtension, string progID, string description, string iconPath)
        {            
            using (RegistryKey softwareClasses = GetSoftwareClasses())            
            {
                //check for progID Key
                //a progID has: description, Icon, and shell command
                RegistryKey progKey = softwareClasses.OpenSubKey(progID);
                if (progKey == null)
                {
                    using (progKey = softwareClasses.CreateSubKey(progID))
                    {
                        progKey.SetValue(null, description);

                        RegistryKey iconKey = progKey.CreateSubKey("DefaultIcon");
                        iconKey.SetValue(null, iconPath);

                        RegistryKey shellKey = progKey.CreateSubKey(@"shell\open\command");
                        string shellCommand = string.Format("\"{0}\" \"%1\"", executePath);         //%1 for double-clicked file
                        shellKey.SetValue(null, shellCommand);
                    }
                }

                //assign value to progID
                //when file is double clicked, the software with progID is opened
                using (RegistryKey extensionKey = softwareClasses.CreateSubKey(fileExtension))
                {
                    extensionKey.SetValue(null, progID);
                }
            }
        }

        /// <summary>
        /// Delete given file extension, check with IsAssociated, before using
        /// </summary>
        /// <param name="extension">given file extension</param>
        /// <exception cref="Exception"></exception>
        public static void DeleteAssociation(string extension)
        {
            using (RegistryKey softwareClasses = GetSoftwareClasses())
            {               
                softwareClasses.DeleteSubKey(extension, true);
            }
        }

        /// <summary>
        /// Delete given progID, check with IsKeyAvailable, before using
        /// </summary>
        /// <param name="progID"></param>
        /// <exception cref="Exception"></exception>
        public static void DeleteProgID(string progID)
        {
            using (RegistryKey softwareClasses = GetSoftwareClasses())
            {
                softwareClasses.DeleteSubKeyTree(progID, true);
            }
        }

        private static RegistryKey GetSoftwareClasses()
        {
            //true: allow to edit
            return Registry.CurrentUser.OpenSubKey("Software").OpenSubKey("Classes", true);
        }
    }
}
