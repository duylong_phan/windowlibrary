﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongWpfUI.SystemHelper
{
    public static class ConsoleHelper
    {
        public static string RunCommand(string program, string argument, bool isAdmin)
        {
            var output = string.Empty;
            var info = new ProcessStartInfo();
            info.FileName = program;
            info.Arguments = argument;
            info.UseShellExecute = false;
            info.RedirectStandardOutput = true;
            if (isAdmin)
            {
                info.Verb = "runas";
            }
            using (var process = Process.Start(info))
            using (var reader = process.StandardOutput)            
            {
                output = reader.ReadToEnd();                
            }

            return output;
        }
    }
}
