﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace LongWpfUI.SystemHelper
{
    public static class FileFolderHelper
    {
        /// <summary>
        /// Check if given Directory is editable
        /// </summary>
        /// <param name="directory">given Directory</param>
        /// <returns></returns>
        public static bool IsDirectoryEditable(string directory)
        {
            bool isEditable = false;

            if (string.IsNullOrEmpty(directory) || !Directory.Exists(directory))    //NOT
            {
                isEditable = false;
            }
            else
            {
                try
                {   //try create
                    string filePath = Path.Combine(directory, "dummy.txt");
                    using (StreamWriter writer = new StreamWriter(File.Create(filePath)))
                    {
                        writer.Write("Dummy");
                    }

                    //try delete
                    if (File.Exists(filePath))
                    {
                        File.Delete(filePath);
                    }

                    isEditable = true;
                }
                catch (Exception)
                {
                    isEditable = false;
                }
            }

            return isEditable;
        }

        /// <summary>
        /// Get all files and subFiles from given Directory
        /// </summary>
        /// <param name="directory">given directory</param>
        /// <returns></returns>
        public static List<string> GetAllFiles(string directory)
        {
            var collection = new List<string>();

            //take files
            collection.AddRange(Directory.GetFiles(directory));

            //file in subFolder
            var subFolders = Directory.GetDirectories(directory);
            foreach (var item in subFolders)
            {
                collection.AddRange(GetAllFiles(item));
            }

            return collection;
        }

        /// <summary>
        /// Get all subFolder from given Directory
        /// </summary>
        /// <param name="directory"></param>
        /// <returns></returns>
        public static List<string> GetAllDirectory(string directory)
        {
            var collection = new List<string>();
            var folders = Directory.GetDirectories(directory);
            foreach (var item in folders)
            {
                collection.Add(item);
                collection.AddRange(GetAllDirectory(item));
            }            

            return collection;
        }

        /// <summary>
        /// Delete all file and folder in given Directory
        /// </summary>
        /// <param name="directory">given directory</param>
        public static void DeleteAll(string directory)
        {
            var files = Directory.GetFiles(directory);
            foreach (var item in files)
            {
                File.Delete(item);
            }

            var folders = Directory.GetDirectories(directory);
            foreach (var item in folders)
            {
                DeleteAll(item);
                Directory.Delete(item);
            }
        }

        /// <summary>
        /// Create a shortcut file
        /// </summary>
        /// <param name="shortcutName">given shortcut name</param>
        /// <param name="targetPath">given sourcePath</param>
        public static void CreateShortCut(string shortcutName, string targetPath, string iconPath)
        {
            string deskDir = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            string path = Path.Combine(deskDir, shortcutName + ".lnk");

            IWshRuntimeLibrary.WshShell shell = new IWshRuntimeLibrary.WshShell();
            IWshRuntimeLibrary.IWshShortcut shortcut = (IWshRuntimeLibrary.IWshShortcut)shell.CreateShortcut(path);
            //name
            shortcut.Description = shortcutName;
            //icon
            shortcut.IconLocation = iconPath;
            //executable file
            shortcut.TargetPath = targetPath;
            //WorkingDirectory => important
            //in order to load file, Folder from relative Path
            shortcut.WorkingDirectory = Path.GetDirectoryName(targetPath);
            shortcut.Save();
        }

        /// <summary>
        /// Enable EveryOne Access for given Folder
        /// </summary>
        /// <param name="folderPath"></param>
        public static void EnableEveryOneAccess(string folderPath)
        {
            if (!Directory.Exists(folderPath))  //NOT
            {
                return;
            }

            #region Get Security, ID
            var security = Directory.GetAccessControl(folderPath);
            var secID = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
            var everyone = secID.Translate(typeof(NTAccount));
            #endregion

            #region remove old Rule for Everyone ID
            FileSystemAccessRule foundItem = null;
            var collection = security.GetAccessRules(true, true, typeof(NTAccount));
            foreach (var item in collection)
            {
                var rule = item as FileSystemAccessRule;
                if (rule != null)
                {
                    var id = rule.IdentityReference as IdentityReference;
                    if (id == everyone)
                    {
                        foundItem = item as FileSystemAccessRule;
                        break;
                    }
                }
            }
            if (foundItem != null)
            {
                security.RemoveAccessRule(foundItem);
            }
            #endregion

            #region Add rule, update security
            security.AddAccessRule(new FileSystemAccessRule(everyone,
                                                            FileSystemRights.Modify | FileSystemRights.Synchronize,
                                                            InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit,
                                                            PropagationFlags.None, AccessControlType.Allow));
            Directory.SetAccessControl(folderPath, security);
            #endregion
        }
    }
}
