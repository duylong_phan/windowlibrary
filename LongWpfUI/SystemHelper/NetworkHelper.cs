﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Diagnostics;

namespace LongWpfUI.SystemHelper
{
    public static class NetworkHelper
    {
        /// <summary>
        /// Register given Address for local user
        /// </summary>
        /// <param name="prefix">given Prefix address</param>
        public static void RegisterPrefix(string prefix)
        {
            RegisterPrefix(prefix, Environment.UserDomainName, Environment.UserName);
        }

        /// <summary>
        /// Register given address for given domain, and user
        /// </summary>
        /// <param name="prefix">given Prefix</param>
        /// <param name="domain">given Userdomain</param>
        /// <param name="user">given user</param>
        public static void RegisterPrefix(string prefix, string domain, string user)
        {
            string args = string.Format(@"http add urlacl url={0} user={1}\{2}", prefix, domain, user);

            ProcessStartInfo info = new ProcessStartInfo("netsh", args);
            info.Verb = "runas";
            //no show console Window
            info.CreateNoWindow = true;
            info.WindowStyle = ProcessWindowStyle.Hidden;
            info.UseShellExecute = true;
            Process.Start(info).WaitForExit();
        }

        /// <summary>
        /// Get Local Ip address
        /// </summary>
        /// <returns></returns>
        public static string GetIpV4()
        {
            string address = null;

            if (NetworkInterface.GetIsNetworkAvailable())
            {
                var host = Dns.GetHostEntry(Dns.GetHostName());
                foreach (var item in host.AddressList)
                {
                    if (item.AddressFamily == AddressFamily.InterNetwork)
                    {
                        address = item.ToString();
                        break;
                    }
                }
            }

            return address;
        }

        /// <summary>
       /// Get Preffix for HttpListener
       /// </summary>
       /// <param name="ipAddress">given IP Address</param>
       /// <param name="port">given Port</param>
       /// <returns></returns>
        public static string GetHttpPrefix(string ipAddress,int port)
        {
            return string.Format("http://{0}:{1}/", ipAddress, port);
        }

        /// <summary>
        /// Check if given Prefix is available in Computer Domain
        /// </summary>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public static bool CheckHttpPrefix(string prefix)
        {
            bool isOk = true;

            try
            {
                var listener = new HttpListener();
                listener.Prefixes.Add(prefix);
                listener.Start();
                listener.Stop();
            }
            catch (Exception)
            {
                isOk = false;
            }

            return isOk;
        }
    }
}
