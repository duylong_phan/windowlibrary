﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using LongWpfUI.Interfaces;

namespace LongWpfUI.Models.DragDrop
{
    public abstract class DropBaseHandler : IDropTargetHandler
    {
        #region Properties
        public DataTemplate ContentTemplate { get; set; }
        #endregion

        #region Abstract Properties
        public abstract DragDropEffects Effect { get; }
        public abstract string FormatString { get; }
        #endregion

        #region Virtual Method
        public virtual void OnDragEnter(FrameworkElement element, DragEventArgs e)
        {

        }

        public virtual void OnDragLeave(FrameworkElement element, DragEventArgs e)
        {

        }

        public virtual void OnDragOver(FrameworkElement element, DragEventArgs e)
        {
            bool hasData = e.Data.GetDataPresent(this.FormatString);
            e.Effects = (hasData) ? this.Effect : DragDropEffects.None;
        }
        #endregion

        #region Abstract Method
        public abstract void OnDrop(FrameworkElement element, DragEventArgs e);
        #endregion
    }
}
