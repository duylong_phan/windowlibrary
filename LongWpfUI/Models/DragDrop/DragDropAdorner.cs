﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace LongWpfUI.Models.DragDrop
{
    public class DragDropAdorner : Adorner, IDisposable
    {
        public const double Offset = 10;

        #region Field
        private AdornerLayer layer;
        private ContentPresenter contentPresenter;
        private double left;
        private double top;
        #endregion

        #region Constructor
        public DragDropAdorner(UIElement adornedElement, AdornerLayer layer, DataTemplate contentTemplate, object data)
            : base(adornedElement)
        {
            this.contentPresenter = new ContentPresenter() { Content = data, ContentTemplate = contentTemplate, Opacity = 0.8 };
            this.layer = layer;
            this.layer.Add(this);
        } 
        #endregion

        #region Protected Method
        protected override Size MeasureOverride(Size constraint)
        {
            this.contentPresenter.Measure(constraint);
            return this.contentPresenter.DesiredSize;
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            this.contentPresenter.Arrange(new Rect(finalSize));
            return finalSize;
        }

        protected override Visual GetVisualChild(int index)
        {
            return this.contentPresenter;
        }

        protected override int VisualChildrenCount
        {
            get { return 1; }
        }
        #endregion

        #region Public Method
        public override GeneralTransform GetDesiredTransform(GeneralTransform transform)
        {
            GeneralTransformGroup group = new GeneralTransformGroup();
            group.Children.Add(transform);
            group.Children.Add(new TranslateTransform(this.left, this.top));

            return group;
        }

        public void UpdatePosition(double left, double top)
        {
            this.left = left - Offset;
            this.top = top - Offset;
            if (this.layer != null)
            {
                this.layer.Update(this.AdornedElement);
            }
        }

        public void Dispose()
        {
            this.layer.Remove(this);
        } 
        #endregion
    }
}
