﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using LongWpfUI.Interfaces;

namespace LongWpfUI.Models.DragDrop
{
    public abstract class DragBaseHandler : IDragSourceHandler
    {
        #region Abstract Property
        public abstract DragDropEffects Effect { get; }
        public abstract string FormatString { get; }
        #endregion

        #region Abstract Method
        public abstract object GetDragData(object rawData);
        public abstract void OnDragDone(ItemsControl control, DragDropEffects finalEffect, object rawData); 
        #endregion
    }
}
