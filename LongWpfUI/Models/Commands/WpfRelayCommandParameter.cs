﻿using LongModel.Models.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace LongWpfUI.Models.Commands
{
    public class WpfRelayCommandParameter : RelayCommandParameter
    {
        #region Static
        public static void UpdateCanExecuteStatus()
        {
            CommandManager.InvalidateRequerySuggested();
        }
        #endregion

        #region Event
        public override event EventHandler CanExecuteChanged
        {
            add
            {
                //when the CanExecute == null, no need to call it again => always true
                if (this.canExecute != null)
                {
                    CommandManager.RequerySuggested += value;
                }
            }
            remove
            {
                if (this.canExecute != null)
                {
                    CommandManager.RequerySuggested -= value;
                }
            }
        }
        #endregion

        #region Constructor
        public WpfRelayCommandParameter(Action<object> execute)
           : base(execute)
        {

        }

        public WpfRelayCommandParameter(Action<object> execute, Func<object, bool> canExecute)
            : base(execute, canExecute)
        {

        } 
        #endregion
    }
}
