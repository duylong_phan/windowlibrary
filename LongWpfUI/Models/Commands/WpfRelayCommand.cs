﻿using LongModel.Models.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace LongWpfUI.Models.Commands
{
    public class WpfRelayCommand : RelayCommand
    {
        #region Static
        public static void UpdateCanExecuteStatus()
        {
            CommandManager.InvalidateRequerySuggested();
        } 
        #endregion

        #region Event
        public override event EventHandler CanExecuteChanged
        {
            add
            {
                //when the CanExecute == null, no need to call it again => always true
                if (this.canExecute != null)
                {
                    CommandManager.RequerySuggested += value;
                }
            }
            remove
            {
                if (this.canExecute != null)
                {
                    CommandManager.RequerySuggested -= value;
                }
            }
        } 
        #endregion

        #region Constructor
        public WpfRelayCommand(Action execute)
           : base(execute)
        {

        }

        public WpfRelayCommand(Action execute, Func<bool> canExe)
            : base(execute, canExe)
        {

        } 
        #endregion
    }
}
