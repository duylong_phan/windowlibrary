﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using LongInterface.Models;

namespace LongWpfUI.Models.Selectors
{
    /// <summary>
    /// Class for selecting different DataTemplate for normal and selected Item, , Support for ISelectable Item only
    /// Only support when the Item is loaded, after loaded the Template will not be applied
    /// For dynamic changed Property =>Style.Trigger => Trigger oder DataTrigger => ItemContainerStyle.ContentTemplate 
    /// </summary>
    public class ListBoxTemplateSelector : DataTemplateSelector
    {
        #region Getter & Setter
        /// <summary>
        /// Get & Set Normal Item Template
        /// </summary>
        public DataTemplate NormalItem { get; set; }
        /// <summary>
        /// Get & Set Selected Item Template
        /// </summary>
        public DataTemplate SelectedItem { get; set; } 
        #endregion

        /// <summary>
        /// Select Template for different selected status
        /// </summary>
        /// <param name="item">given item</param>
        /// <param name="container">given container</param>
        /// <returns>Template result</returns>
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            DataTemplate result = null;

            if (item is ISelectable)
            {
                ISelectable obj = item as ISelectable;
                if (obj.IsSelected)
                {
                    result = this.SelectedItem;
                }
                else
                {
                    result = NormalItem;
                }
            }

            return result;
        }
    }
}
