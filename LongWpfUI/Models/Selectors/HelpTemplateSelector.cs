﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using LongModel.Views.Infos.HelpInfos;

namespace LongWpfUI.Models.Selectors
{
    public class HelpTemplateSelector : DataTemplateSelector
    {
        #region Getter, setter
        public DataTemplate TextContent { get; set; }
        public DataTemplate BulletContent { get; set; }
        public DataTemplate ImageContent { get; set; }
        #endregion

        #region Public Method
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            DataTemplate template = null;
            
            if (item is TextContentHelpInfo)            template = this.TextContent;
            else if (item is ImageContentHelpInfo)      template = this.ImageContent;
            else if(item is BulletTextContentHelpInfo)  template = this.BulletContent;
            
            return template;
        }
        #endregion
    }
}
