﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace LongWpfUI.Models.Selectors
{
    /// <summary>
    /// Class for selecting Combobox Template, Support for ISelectable Item only
    /// Only support when the Item is loaded, after loaded the Template will not be applied
    /// For dynamic changed Property =>Style.Trigger => Trigger oder DataTrigger => ItemContainerStyle.ContentTemplate 
    /// </summary>
    public class ComboBoxTemplateSelector : ListBoxTemplateSelector
    {
        #region Getter & Setter
        /// <summary>
        /// Get & Set ContentItem Template
        /// </summary>
        public DataTemplate ContentItem { get; set; } 
        #endregion

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            //take from base
            DataTemplate result = base.SelectTemplate(item, container);

            //container is the ContentPresenter => TemplatedParent is valid => item inside the ComboBox Content
            FrameworkElement element = container as FrameworkElement;
            if (element != null)
            {
                DependencyObject parent = element.TemplatedParent;
                if (parent != null && parent is ComboBox)
                {
                    result = this.ContentItem;
                }
            }

            return result;
        }
    }
}
