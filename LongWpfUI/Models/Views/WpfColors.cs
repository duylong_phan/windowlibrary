﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Reflection;

namespace LongWpfUI.Models.Views
{
    public class WpfColors : List<Color>
    {
        #region Static Property
        private static List<Color> colorCollection;
        public static List<Color> ColorCollection
        {
            get
            {
                if (colorCollection == null)
                {
                    colorCollection = new List<Color>();
                    var collection = typeof(Colors).GetTypeInfo().DeclaredProperties;
                    foreach (var item in collection)
                    {
                        var value = item.GetValue(null);
                        if (value is Color)
                        {
                            colorCollection.Add((Color)value);
                        }
                    }
                }
                return colorCollection;
            }
        }
        #endregion

        #region Constructor
        public WpfColors()
        {
            this.AddRange(WpfColors.ColorCollection);
        }
        #endregion
    }
}
