﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LongWpfUI.Models.Converters.Base;
using System.Windows.Data;
using LongInterface.Views;
using LongInterface.Views.ControlModels;

namespace LongWpfUI.Models.Converters
{
    public class BasicIconConverter : BoolConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool && this.TrueValue is IBasicIcon && this.FalseValue is IBasicIcon)
            {
                bool input = (bool)value;
                IBasicIcon trueValue = this.TrueValue as IBasicIcon;
                IBasicIcon falseValue = this.FalseValue as IBasicIcon;
                return (input) ? trueValue.Icon : falseValue.Icon;
            }
            else
            {
                return value;
            }
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
