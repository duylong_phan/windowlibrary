﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LongWpfUI.Models.Converters.Base;
using System.Windows.Data;
using System.Windows;

namespace LongWpfUI.Models.Converters
{
    public class VisibilityConverter : BoolConverterBase
    {
        #region Getter, Setter
        private bool canCollapse;

        public bool CanCollapse
        {
            get
            {
                return this.canCollapse;
            }
            set
            {
                this.canCollapse = value;
                this.FalseValue = (canCollapse) ? Visibility.Collapsed : Visibility.Hidden;
            }
        }
        public bool IsReversed { get; set; }
        #endregion

        #region Constructor
        public VisibilityConverter()
        {
            this.canCollapse = false;
            this.IsReversed = false;
            this.TrueValue = Visibility.Visible;
            this.FalseValue = Visibility.Hidden;
        }
        #endregion

        #region Override Method
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool)
            {
                bool input = (bool)value;
                object output = (input) ? this.TrueValue : this.FalseValue;

                if (this.IsReversed)
                {
                    output = (!input) ? this.TrueValue : this.FalseValue;   //NOT
                }

                return output;
            }
            else
            {
                return value;
            }
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        } 
        #endregion
    }
}
