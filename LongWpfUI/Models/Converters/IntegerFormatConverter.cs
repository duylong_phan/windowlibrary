﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LongWpfUI.Models.Converters.Base;
using System.Globalization;

namespace LongWpfUI.Models.Converters
{   
    /// <summary>
    /// Class for Converting a integer Number to given Format
    /// </summary>
    public class IntegerFormatConverter : NumberFormatConverter
    {
        public override object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is int)
            {
                int number = (int)value;
                if (this.cultureInfo == null)
                {
                    try
                    {
                        this.cultureInfo = new CultureInfo(this.Culture);
                    }
                    catch (Exception)
                    {
                        this.cultureInfo = CultureInfo.InvariantCulture;
                    }
                }

                string numberText = string.Empty;
                try
                {
                    numberText = number.ToString(this.FormatNumber, this.cultureInfo);
                }
                catch (Exception)
                {
                    numberText = string.Empty;
                }

                return numberText;
            }
            else
            {
                return value;
            }
        }
    }
}
