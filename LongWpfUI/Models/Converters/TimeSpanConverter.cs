﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using LongInterface.Enums;

namespace LongWpfUI.Models.Converters
{
    /// <summary>
    /// Class for converting a number to Time Text Format
    /// </summary>
    public class TimeSpanConverter : IValueConverter
    {
        #region Getter & Setter
        /// <summary>
        /// Get & Set format for displaying the Time
        /// </summary>
        public string TextFormat { get; set; }
        /// <summary>
        /// Get & Set Type of UnitOption, default Second
        /// </summary>
        public TimeUnitOptions UnitOption { get; set; } 
        #endregion

        #region Constructor
        public TimeSpanConverter()
        {
            this.TextFormat = @"hh\:mm\:ss";
            this.UnitOption = TimeUnitOptions.Second;
        } 
        #endregion

        #region IValueConverter Method
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            long input = -1;

            if (value is int)
            {
                input = (int)value;
            }
            else if (value is long)
            {
                input = (long)value;
            }

            if (input >= 0)
            {
                TimeSpan timeSpan = TimeSpan.FromSeconds(0);

                switch (this.UnitOption)
                {
                    case TimeUnitOptions.Tick:
                        timeSpan = TimeSpan.FromTicks(input);
                        break;
                    case TimeUnitOptions.MiliSecond:
                        timeSpan = TimeSpan.FromMilliseconds(input);
                        break;
                    case TimeUnitOptions.Second:
                        timeSpan = TimeSpan.FromSeconds(input);
                        break;
                    case TimeUnitOptions.Minute:
                        timeSpan = TimeSpan.FromMinutes(input);
                        break;
                    case TimeUnitOptions.Hour:
                        timeSpan = TimeSpan.FromHours(input);
                        break;
                }

                try
                {
                    return timeSpan.ToString(this.TextFormat);
                }
                catch (Exception)
                {
                    throw new Exception("TimeSpan toString() Error: TextFormat is invalid");
                }
            }
            else
            {
                return value;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return Binding.DoNothing;
        } 
        #endregion
    }
}
