﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LongWpfUI.Models.Converters.Base;
using System.Globalization;

namespace LongWpfUI.Models.Converters
{
    /// <summary>
    /// Class for converting text to double number, support for validation
    /// </summary>
    public class DoubleValidationConverter : NumberValidationConverter
    {
        #region Constructor
        /// <summary>
        /// Initialize a new instance of DoubleValidationConverter
        /// </summary>
        public DoubleValidationConverter()
        {

        } 
        #endregion

        #region Abstract Method
        public override object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is double)
            {
                double number = (double)value;
                string numberText = this.LastInputText;   

                //check availibility
                if (numberText == null)
                {
                    numberText = number.ToString(this.NumberFormat, this.CurrentCulture);
                }

                return numberText;
            }
            else
            {
                return null;
            }
        }

        public override object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            //expect only string
            if (value is string)
            {
                double number = 0;
                string numberText = value as string;
                if (double.TryParse(numberText, NumberStyles.Any, this.CurrentCulture, out number))
                {
                    this.LastInputText = numberText;
                    return number;
                }
                else
                {   //validation Error = true
                    return null;
                }
            }
            else
            {
                return null;
            }
        } 
        #endregion
    }
}
