﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace LongWpfUI.Models.Converters
{
    public class DoubleToDoubleConverter  : IValueConverter
    {
        #region Getter & Setter
        /// <summary>
        /// Get & Set Factor,
        /// Default 1
        /// </summary>
        public double Factor { get; set; }
        /// <summary>
        /// Get & Set Offset
        /// Default 0
        /// </summary>
        public double Offset { get; set; }
        /// <summary>
        /// Get & Set Invert the result number to negative
        /// Default false
        /// </summary>
        public bool IsNegative { get; set; } 
        #endregion

        #region Constructor
        public DoubleToDoubleConverter()
        {
            this.Factor = 1;
            this.Offset = 0;
            this.IsNegative = false;
        } 
        #endregion

        #region IValueConverter Method
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is double)
            {
                double input = (double)value;
                double result = input * this.Factor + this.Offset;

                return (this.IsNegative) ? -result : result;
            }
            else
            {
                return value;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return Binding.DoNothing;
        } 
        #endregion
    }
}
