﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using LongInterface.Models;

namespace LongWpfUI.Models.Converters
{
    /// <summary>
    /// Class for converting given text to lower Letter
    /// </summary>
    public class LowerLetterConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is string)
            {
                string text = value as string;
                return text.ToLower();
            }
            else if (value is IText)
            {
                IText instance = value as IText;
                if (instance.Text != null)
                {
                    return instance.Text.ToLower();
                }
                else
                {
                    return value;
                }
            }
            else
            {
                return value;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
