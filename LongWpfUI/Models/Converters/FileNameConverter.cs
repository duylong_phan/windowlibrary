﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace LongWpfUI.Models.Converters
{
    public class FileNameConverter : IValueConverter
    {
        public bool HasExtentsion { get; set; }

        public FileNameConverter()
        {
            this.HasExtentsion = true;
        }


        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string)
            {
                string filePath = value as string;
                string fileName = string.Empty;

                if (this.HasExtentsion)
                {
                    fileName = Path.GetFileName(filePath);
                }
                else
                {
                    fileName = Path.GetFileNameWithoutExtension(filePath);
                }

                return fileName;
            }
            else
            {
                return value;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
