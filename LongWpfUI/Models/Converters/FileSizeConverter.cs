﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace LongWpfUI.Models.Converters
{
    public enum FileSizeTypes
    {
        Kbyte,
        Mbyte,
        Gbyte
    }

    public class FileSizeConverter : IValueConverter
    {
        #region Getter, setter
        public FileSizeTypes Type { get; set; }
        public string NumberFormat { get; set; }
        #endregion

        #region Constructor
        public FileSizeConverter()
        {
            this.Type = FileSizeTypes.Kbyte;
            this.NumberFormat = "0.##";
        }
        #endregion

        #region Public method
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is long)
            {
                var input = (long)value;

                string output = string.Empty;
                switch (this.Type)
                {

                    case FileSizeTypes.Mbyte:
                        output = (input / (1024.0 * 1024.0)).ToString(this.NumberFormat);
                        break;

                    case FileSizeTypes.Gbyte:
                        output = (input / (1024.0 * 1024.0 * 1024.0)).ToString(this.NumberFormat);
                        break;

                    case FileSizeTypes.Kbyte:
                    default:
                        output = (input / 1024.0).ToString(this.NumberFormat);
                        break;
                }
                return output;
            }
            else
            {
                return value;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
        #endregion
    }
}
