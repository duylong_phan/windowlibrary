﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using LongInterface.Models;

namespace LongWpfUI.Models.Converters
{
    /// <summary>
    /// Converter for checking input Value is a Text with length > 0
    /// </summary>
    public class HasTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool hasText = false;

            if (value is string)
            {
                string text = value as string;
                if (text.Length > 0)
                {
                    hasText = true;
                }
            }
            else if (value is IText)
            {
                IText instance = value as IText;
                if (instance.Text != null && instance.Text.Length > 0)
                {
                    hasText = true;
                }
            }

            return hasText;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
