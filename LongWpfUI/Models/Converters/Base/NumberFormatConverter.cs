﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace LongWpfUI.Models.Converters.Base
{
    /// <summary>
    /// Base Class for Number Formating
    /// </summary>
    public abstract class NumberFormatConverter : IValueConverter
    {
        #region Protected Field
        /// <summary>
        /// Get & set the cultureInfo instance for formating
        /// </summary>
        protected CultureInfo cultureInfo; 
        #endregion

        #region Getter & Setter
        /// <summary>
        /// Get & Set Culture Code
        /// </summary>
        public string Culture { get; set; }
        /// <summary>
        /// Get & Set Format of Number
        /// </summary>
        public string FormatNumber { get; set; }
        #endregion  

        #region Abstract Method
        public abstract object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture); 
        #endregion

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
