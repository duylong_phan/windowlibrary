﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace LongWpfUI.Models.Converters.Base
{
    public abstract class BoolConverterBase : IValueConverter
    {
        #region Getter & Setter
        /// <summary>
        /// Get & Set Value for true case
        /// </summary>
        public object TrueValue { get; set; }
        /// <summary>
        /// Get & Set Value for false case
        /// </summary>
        public object FalseValue { get; set; }
        #endregion

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public BoolConverterBase()
        {
            this.TrueValue = this.FalseValue = null;
        }

        /// <summary>
        /// Initialize a new instance of BoolConverterBase
        /// </summary>
        /// <param name="trueValue">initialze trueValue</param>
        /// <param name="falseValue">initialze falseValue</param>
        public BoolConverterBase(object trueValue, object falseValue)
        {
            this.TrueValue = trueValue;
            this.FalseValue = falseValue;
        } 
        #endregion

        #region IValueConverter Method
        public virtual object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is bool)
            {
                bool input = (bool)value;
                return (input) ? this.TrueValue : this.FalseValue;
            }
            else
            {
                return value;
            }
        }

        public virtual object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (object.ReferenceEquals(value, this.TrueValue) || object.Equals(value, this.TrueValue))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
    }
}
