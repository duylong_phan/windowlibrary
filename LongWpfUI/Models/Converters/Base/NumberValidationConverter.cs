﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace LongWpfUI.Models.Converters.Base
{
    /// <summary>
    /// Base class for Number Validation Converter
    /// </summary>
    public abstract class NumberValidationConverter : IValueConverter
    {
        #region Getter & Setter      
        /// <summary>
        /// Get & set Culture code
        /// Default en-US
        /// </summary>
        public string CultureCode { get; set; }
        /// <summary>
        /// Get & Set Number Format
        /// Default 0.##
        /// </summary>
        public string NumberFormat { get; set; }
        /// <summary>
        /// Get & Set last input Text
        /// </summary>
        protected string LastInputText { get; set; }
        #endregion

        #region Getter
        private CultureInfo currentCulture;
        /// <summary>
        /// Get the current Culture for formating Number Text
        /// It is depended on CultureCode
        /// </summary>
        protected virtual CultureInfo CurrentCulture
        {
            get
            {
                if (currentCulture == null)
                {
                    try
                    {
                        currentCulture = new CultureInfo(this.CultureCode);
                    }
                    catch (Exception)
                    {
                        currentCulture = CultureInfo.InvariantCulture;
                    }
                }

                return this.currentCulture;
            }
        }     
        #endregion

        #region Constructor
        /// <summary>
        /// Initialize a new instance of NumberValidationConverter
        /// </summary>
        public NumberValidationConverter()
        {
            this.currentCulture = null;            
            this.CultureCode = "en-US";
            this.NumberFormat = "0.##";
            this.LastInputText = null;
        } 
        #endregion

        #region IValueConverter Method
        public abstract object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture);

        public abstract object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture); 
        #endregion
    }
}
