﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace LongWpfUI.Models.Converters
{
    public enum PixelUnits
    {
        Inch,
        Centimeter,
        Point,
    }

    /// <summary>
    /// Convert/Convert back pixel value to given unit in Parameter
    /// </summary>
    public class PixelSizeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is int && parameter is PixelUnits)
            {
                var input = (int)value;
                var unit = (PixelUnits)parameter;
                double output = 0;

                switch (unit)
                {
                    case PixelUnits.Inch:
                        output = input / 96.0;
                        break;

                    case PixelUnits.Point:
                        output = input * 72.0 / 96.0;
                        break;

                    case PixelUnits.Centimeter:
                    default:
                        output = input * 2.54 / 96.0;
                        break;
                }

                return output;
            }
            else
            {
                return value;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double input = -1;
            if (value is int)
            {
                input = (int)value;
            }
            else if (value is double)
            {
                input = (double)value;
            }

            if (input > 0 && parameter is PixelUnits)
            {
                double output = 0;
                var unit = (PixelUnits)parameter;
                switch (unit)
                {
                    case PixelUnits.Inch:
                        output = input * 96.0;
                        break;
                        
                    case PixelUnits.Point:
                        output = input * 96.0 / 72.0;
                        break;

                    case PixelUnits.Centimeter:
                    default:
                        output = input * 96.0 / 2.54;
                        break;
                }

                return output;
            }
            else
            {
                return value;
            }
        }
    }
}
