﻿using LongInterface.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace LongWpfUI.Models.Converters
{
    public class TextSeparatorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is string || value is IText)
            {
                string inputText = null;
                if (value is string)
                {
                    inputText = value as string;
                }
                else if (value is IText)
                {
                    inputText = (value as IText).Text;
                }

                StringBuilder builder = new StringBuilder();

                for (int i = 0; i < inputText.Length; i++)
                {
                    if (char.IsUpper(inputText[i]) && i != 0)
                    {
                        builder.Append(' ');                        
                    }
                    builder.Append(inputText[i]);
                }

                return builder.ToString();
            }
            else
            {
                return value;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
