﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LongWpfUI.Models.Converters.Base;

namespace LongWpfUI.Models.Converters
{
    /// <summary>
    /// Class for converting text to integer, support for validation
    /// </summary>
    public class IntegerValidationConverter : NumberValidationConverter
    {
        #region Abstract Method
        public override object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is int)
            {
                int number = (int)value;
                string numberText = this.LastInputText;

                //check availibility => available in case of callback
                if (numberText == null)
                {
                    numberText = number.ToString(this.NumberFormat, this.CurrentCulture);
                }

                return numberText;
            }
            else
            {
                return null;
            }
        }

        public override object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            //expect only string
            if (value is string)
            {
                int number = 0;
                string numberText = value as string;
                if (int.TryParse(numberText, out number))
                {
                    this.LastInputText = numberText;
                    return number;
                }
                else
                {   //validation Error = true
                    return null;
                }
            }
            else
            {
                return null;
            }
        } 
        #endregion
    }
}
