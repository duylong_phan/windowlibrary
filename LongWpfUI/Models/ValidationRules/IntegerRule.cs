﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LongWpfUI.Models.ValidationRules.Base;
using System.Windows.Controls;
using System.Globalization;

namespace LongWpfUI.Models.ValidationRules
{
    public class IntegerRule : NumberRule<int>
    {
        #region Getter & Setter
        /// <summary>
        /// Initialize a new instance of IntegerRule
        /// </summary>
        public IntegerRule()
            : this(int.MinValue)
        {

        }
        /// <summary>
        /// Initialize a new instance of IntegerRule, with given min
        /// </summary>
        /// <param name="min">initial min</param>
        public IntegerRule(int min)
            : this(min, int.MaxValue)
        {

        }
        /// <summary>
        /// Initialize a new instance of IntegerRule, with given min and max
        /// </summary>
        /// <param name="min">initial min</param>
        /// <param name="max">initial Max</param>
        public IntegerRule(int min, int max)
            : base(min, max)
        {

        } 
        #endregion

        #region Abstract Method
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            ValidationResult result = new ValidationResult(false, this.InitialMessage);

            if (value is string)
            {
                string inputText = value as string;
                try
                {
                    int number = int.Parse(inputText);
                    result = new ValidationResult(true, string.Empty);
                    if (number < this.Min)
                    {
                        result = new ValidationResult(false, this.MinMessage);
                    }
                    if (number > this.Max)
                    {
                        result = new ValidationResult(false, this.MaxMessage);
                    }
                }
                catch (Exception)
                {
                    result = new ValidationResult(false, this.InvalidMessage);
                }
            }

            return result;
        } 
        #endregion
    }
}
