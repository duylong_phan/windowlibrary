﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using LongWpfUI.Models.ValidationRules.Base;
using System.Net;

namespace LongWpfUI.Models.ValidationRules
{
    public class IPAddressRule : RuleBase
    {
        public IPAddressRule()
        {
            this.InvalidMessage = "Invalid IP address";
        }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            ValidationResult result = new ValidationResult(false, this.InitialMessage);

            try
            {
                string input = value as string;
                string[] element = input.Split('.');
                
                if (element.Length == 4)
                {
                    IPAddress address = IPAddress.Parse(input);
                    result = new ValidationResult(true, string.Empty);
                }
            }
            catch (Exception)
            {
                result = new ValidationResult(false, this.InvalidMessage);
            }

            return result;
        }
    }
}
