﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LongWpfUI.Models.ValidationRules.Base;
using System.Windows.Controls;

namespace LongWpfUI.Models.ValidationRules
{
    public class NonEmptyTextRule : RuleBase
    {
        #region Contructor
        /// <summary>
        /// Initialize a new instance of NonEmptyTextRule
        /// </summary>
        public NonEmptyTextRule()
        {
            this.InitialMessage = "Invalid Input";
            this.InvalidMessage = "Not allowed empty";
        } 
        #endregion

        #region Abstract Method
        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            ValidationResult result = new ValidationResult(false, this.InitialMessage);

            if (value is string)
            {
                string input = value as string;
                if (input.Length == 0)
                {
                    result = new ValidationResult(false, this.InvalidMessage);
                }
                else
                {
                    result = new ValidationResult(true, string.Empty);
                }
            }
            else
            {
                result = new ValidationResult(false, this.InvalidMessage);
            }

            return result;
        } 
        #endregion
    }
}
