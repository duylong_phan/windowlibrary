﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace LongWpfUI.Models.ValidationRules.Base
{
    public abstract class RuleBase : ValidationRule
    {
        #region Getter & Setter
        /// <summary>
        /// Get & Set Initial Message
        /// </summary>
        public string InitialMessage { get; set; }
        /// <summary>
        /// Get & Set Message for invalid input
        /// </summary>
        public string InvalidMessage { get; set; } 
        #endregion

        #region Constructor
        /// <summary>
        /// Initialize a new instance of RuleBase
        /// </summary>
        public RuleBase()
        {
            this.InitialMessage = string.Empty;
            this.InvalidMessage = "Input instance type is not expected";
        } 
        #endregion
    }
}
