﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace LongWpfUI.Models.ValidationRules.Base
{
    public abstract class NumberRule<T> : RuleBase
    {
        #region Getter, Setter
        /// <summary>
        /// Get, Set Min value of current Rule
        /// </summary>
        public T Min { get; set; }
        /// <summary>
        /// Get, Set Max value of current Rule
        /// </summary>
        public T Max { get; set; }

        #endregion

        #region Getter
        /// <summary>
        /// Get Message for invalid Min
        /// </summary>
        public string MinMessage
        {
            get
            {
                return string.Format("{0} {1}", this.minMessage, this.Min);
            }
        }
        /// <summary>
        /// Get Message invalid Max
        /// </summary>
        public string MaxMessage
        {
            get
            {
                return string.Format("{0} {1}", this.maxMessage, this.Max);
            }
        }
        #endregion

        #region Field
        private string minMessage;
        private string maxMessage; 
        #endregion

        #region Constructor
        /// <summary>
        /// Initialize a new instance of NumberRule
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        public NumberRule(T min, T max)
        {
            this.Min = min;
            this.Max = max;
            SetMessage("Invalid Input", "Invalid Number", "muss ≥", "muss ≤");
        } 
        #endregion

        #region Virtual Method
        /// <summary>
        /// Set all the Message for current Rule
        /// </summary>
        /// <param name="initialMessage">initialMessage</param>
        /// <param name="invalidMessage">invalidMessage</param>
        /// <param name="minMessage">minMessage</param>
        /// <param name="maxMessage">maxMessage</param>
        public virtual void SetMessage(string initialMessage, string invalidMessage, string minMessage, string maxMessage)
        {
            this.InitialMessage = initialMessage;
            this.InvalidMessage = invalidMessage;
            this.minMessage = minMessage;
            this.maxMessage = maxMessage;
        } 
        #endregion
    }
}
