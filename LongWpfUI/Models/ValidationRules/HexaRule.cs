﻿using LongWpfUI.Models.ValidationRules.Base;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace LongWpfUI.Models.ValidationRules
{
    public class HexaRule : NumberRule<string>
    {
        #region Field
        private long minValue;
        private long maxValue;
        #endregion

        #region Constructor
        public HexaRule(string min, string max)
           : base(min, max)
        {
            this.minValue = long.Parse(min, NumberStyles.HexNumber);
            this.maxValue = long.Parse(max, NumberStyles.HexNumber);
            this.InvalidMessage = "Invalid Hexa number format";
        }
        #endregion

        #region Override Method
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            ValidationResult result = new ValidationResult(false, this.InitialMessage);
            string input = value as string;
            if (!string.IsNullOrEmpty(input))   //NOT
            {
                long number = 0;

                try
                {
                    number = long.Parse(input, NumberStyles.HexNumber);
                    result = new ValidationResult(true, string.Empty);
                    if (number > this.maxValue)
                    {
                        result = new ValidationResult(false, this.MaxMessage);
                    }
                    else if (number < this.minValue)
                    {
                        result = new ValidationResult(false, this.MinMessage);
                    }
                }
                catch (Exception)
                {
                    result = new ValidationResult(false, this.InvalidMessage);
                }
            }
            return result;
        }
        #endregion
    }
}
