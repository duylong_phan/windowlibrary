﻿using LongWpfUI.Models.ValidationRules.Base;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;


namespace LongWpfUI.Models.ValidationRules
{
    public class FileNameRule : RuleBase
    {
        #region Static
        private static char[] invalidChars;
        private static string invalidCharsString;

        /// <summary>
        /// Get Array of all invalid characters
        /// </summary>
        public static char[] InvalidChars
        {
            get
            {
                if (invalidChars == null)
                {
                    invalidChars = new char[9] { '>', '<', ':', '*', '?', '\"', '\\', '/', '|' };            
                }

                return invalidChars;
            }
        }
        /// <summary>
        /// Get string of all invalid characters
        /// </summary>
        public static string InvalidCharsString
        {
            get
            {
                if (invalidCharsString == null)
                {
                    StringBuilder builder = new StringBuilder();
                    for (int i = 0; i < InvalidChars.Length; i++)
                    {
                        builder.Append(InvalidChars[i]);
                        if (i != (InvalidChars.Length - 1))
                        {
                            builder.Append(", ");
                        }
                    }
                    invalidCharsString = builder.ToString();
                }

                return invalidCharsString;
            }
        }
        
        #endregion

        #region Getter & Setter
        /// <summary>
        /// Get, Set if Allow empty input string, Default: true
        /// </summary>
        public bool AllowEmpty { get; set; }
        /// <summary>
        /// Get, set if space is allow, Default: True
        /// </summary>
        public bool AllowSpace { get; set; }
        /// <summary>
        /// Get, Set Message for empty input string
        /// </summary>
        public string EmptyMessage { get; set; }
        /// <summary>
        /// Get, Set Message for Space input string
        /// </summary>
        public string SpaceMessage { get; set; }
        #endregion

        #region Constructor
        /// <summary>
        /// Initialize a new instance of FileNameRule
        /// </summary>
        public FileNameRule()
        {
            this.AllowEmpty = true;
            this.AllowSpace = true;
            this.InitialMessage = "Invalid Input";
            this.InvalidMessage = "Invalid characters: ";
            this.EmptyMessage = "Not allowed empty";
            this.SpaceMessage = "Not allowed space";
        } 
        #endregion
        
        #region Abstract Method
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            ValidationResult result = new ValidationResult(false, this.InitialMessage);

            if (value is string)
            {
                string input = value as string;

                if (!this.AllowEmpty && input.Length == 0)  //NOT
                {
                    result = new ValidationResult(false, this.EmptyMessage);
                }
                else if(!this.AllowSpace && input.IndexOf(' ') > -1)    //NOT
                {
                    result = new ValidationResult(false, this.SpaceMessage);
                }
                else if (input.IndexOfAny(InvalidChars) >= 0)
                {
                    result = new ValidationResult(false, this.InvalidMessage + FileNameRule.InvalidCharsString);
                }                
                else
                {
                    result = new ValidationResult(true, string.Empty);
                }
            }
            else
            {
                result = new ValidationResult(false, this.InvalidMessage);
            }

            return result;
        } 
        #endregion
    }
}
