﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LongWpfUI.Models.ValidationRules.Base;
using System.Windows.Controls;
using System.Globalization;

namespace LongWpfUI.Models.ValidationRules
{
    /// <summary>
    /// Rule class for checking if input can be matlab variable name
    /// </summary>
    public class MatVariableRule : RuleBase
    {
        #region Static
        private static char[] matOperator;
        private static FileNameRule fileNameRule;

        /// <summary>
        /// Get typical mat operator
        /// </summary>
        public static char[] MatOperator
        {
            get
            {
                if (matOperator == null)
                {
                    matOperator = new char[10] { '-', '+', '*', '/', '&', '|', '~', '^', '%', '!' };
                }

                return matOperator;
            }
        }
        /// <summary>
        /// Get FileNameRule instance in case CheckAsFileName = true
        /// </summary>
        public static FileNameRule FileNameRule
        {
            get
            {
                if (fileNameRule == null)
                {
                    fileNameRule = new FileNameRule();
                }

                return fileNameRule;
            }
        } 
        #endregion

        #region Getter & Setter
        /// <summary>
        /// Get, Set if input is checked as FileName
        /// </summary>
        public bool CheckAsFileName { get; set; } 
        #endregion

        #region Constructor
        /// <summary>
        /// Initialize a new instance of MatVariableRule
        /// </summary>
        public MatVariableRule()
        {
            this.CheckAsFileName = false;
        }
        #endregion

        #region Override Method
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            ValidationResult result = new ValidationResult(false, this.InitialMessage);

            if (value is string)
            {
                string input = value as string;
                if (string.IsNullOrEmpty(input))
                {
                    result = new ValidationResult(false, "Variable name muss be given");
                }
                else if (!char.IsLetter(input[0]))   //NOT
                {
                    result = new ValidationResult(false, "Variable name muss start with letter");
                }
                else if (input.IndexOf(' ') >= 0)
                {
                    result = new ValidationResult(false, "Variable name muss not have space letter");
                }
                else if (input.IndexOfAny(MatOperator) >= 0)
                {
                    result = new ValidationResult(false, "Variable name muss not have operator letter");
                }
                else if (this.CheckAsFileName)
                {
                    result = FileNameRule.Validate(input, cultureInfo);
                }
                else
                {
                    result = new ValidationResult(true, string.Empty);
                }
            }
            else
            {
                result = new ValidationResult(false, this.InvalidMessage);
            }

            return result;
        } 
        #endregion
    }
}
