﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LongWpfUI.Models.ValidationRules.Base;
using System.Windows.Controls;
using LongInterface.Enums;
using System.Globalization;

namespace LongWpfUI.Models.ValidationRules
{
    public class DoubleRule : NumberRule<double>
    {
        #region Getter & Setter
        /// <summary>
        /// Get & Set Number Separator.
        /// Default Dot
        /// </summary>
        public NumberSeparatorOptions SeparatorOption { get; set; }
        /// <summary>
        /// Get & Set Message for Invalid Separator
        /// </summary>
        public string SeparatorMessage { get; set; } 
        #endregion

        #region Getter
        private CultureInfo currentCulture;
        /// <summary>
        /// Get CultureInfo for current Rule
        /// </summary>
        public virtual CultureInfo CurrentCulture
        {
            get
            {
                if (this.currentCulture == null)
                {
                    this.currentCulture = (this.SeparatorOption == NumberSeparatorOptions.Dot) ? CultureInfo.InvariantCulture : new CultureInfo("de-DE");
                }

                return this.currentCulture;
            }
        } 
        #endregion

        #region Constructor
        /// <summary>
        /// Initialize a new instance of DoubleRule
        /// </summary>
        public DoubleRule()
            : this(double.MinValue)
        {

        }

        /// <summary>
        /// Initialize a new instance of DoubleRule, with given min
        /// </summary>
        /// <param name="min">initial min</param>
        public DoubleRule(double min)
            : this(min, double.MaxValue)
        {

        }

        /// <summary>
        /// Initialize a new instance of DoubleRule, with given min and max
        /// </summary>
        /// <param name="min">initial min</param>
        /// <param name="max">initial max</param>
        public DoubleRule(double min, double max)
            : base(min, max)
        {
            this.SeparatorOption = NumberSeparatorOptions.Dot;
            this.SeparatorMessage = string.Empty;
        } 
        #endregion

        #region Public Method
        /// <summary>
        /// Set all the Message for current Rule
        /// </summary>
        /// <param name="initialMessage">initialMessage</param>
        /// <param name="invalidMessage">invalidMessage</param>
        /// <param name="minMessage">minMessage</param>
        /// <param name="maxMessage">maxMessage</param>
        /// <param name="separatorMessage">separatorMessage</param>
        public virtual void SetMessage(string initialMessage, string invalidMessage, string minMessage, string maxMessage, string separatorMessage)
        {
            SetMessage(initialMessage, invalidMessage, minMessage, maxMessage);
            this.SeparatorMessage = separatorMessage;
        }

        /// <summary>
        /// Validate if the input Value is suit the requirement
        /// </summary>
        /// <param name="value">given Value</param>
        /// <param name="cultureInfo">given Culture</param>
        /// <returns></returns>
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            ValidationResult result = new ValidationResult(false, this.InitialMessage);

            if (value is string)
            {
                string text = value as string;
                if (text.Contains('.') && this.SeparatorOption != NumberSeparatorOptions.Dot)
                {
                    result = new ValidationResult(false, this.SeparatorMessage);
                }
                else
                {
                    double number = 0;
                    try
                    {
                        number = double.Parse(text, this.CurrentCulture);
                        result = new ValidationResult(true, string.Empty);

                        if (number < this.Min)
                        {
                            result = new ValidationResult(false, this.MinMessage);
                        }

                        if (number > this.Max)
                        {
                            result = new ValidationResult(false, this.MaxMessage);
                        }
                    }
                    catch (Exception)
                    {
                        result = new ValidationResult(false, this.InvalidMessage);
                    }
                }
            }

            return result;
        } 
        #endregion
    }
}
