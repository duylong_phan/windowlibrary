﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LongWpfUI.Models.ValidationRules.Base;
using System.Windows.Controls;
using System.Globalization;

namespace LongWpfUI.Models.ValidationRules
{
    public class ByteRule : NumberRule<byte>
    {
        public ByteRule()
            : base(byte.MinValue, byte.MaxValue)
        {
            
        }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            ValidationResult result = new ValidationResult(false, this.InitialMessage);

            if (value is string)
            {
                string inputText = value as string;
                try
                {
                    byte number = byte.Parse(inputText);
                    result = new ValidationResult(true, string.Empty);
                    if (number < this.Min)
                    {
                        result = new ValidationResult(false, this.MinMessage);
                    }
                    if (number > this.Max)
                    {
                        result = new ValidationResult(false, this.MaxMessage);
                    }
                }
                catch (Exception)
                {
                    result = new ValidationResult(false, this.InvalidMessage);
                }
            }

            return result;
        }
    }
}
