﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace LongWpfUI.Models.Net
{
    public class HttpServer : IDisposable
    {
        #region Getter
        public bool IsEnabled { get; private set; }
        #endregion

        #region Field
        private HttpListener listener;
        private bool isDisposed;
        private Action<HttpListenerRequest, HttpListenerResponse> action;
        #endregion

        #region Constructor
        public HttpServer()
        {
            this.listener = new HttpListener();
            this.isDisposed = false;
            this.IsEnabled = false;
        }
        #endregion

        #region Public Method
        public void Start(string url, Action<HttpListenerRequest, HttpListenerResponse> action)
        {
            if (this.IsEnabled)
            {
                return;
            }

            if (!url.EndsWith("/")) //NOT
            {
                throw new Exception("Url has to be ended width \"/\"");
            }

            if (action == null)
            {
                throw new Exception("Invalid Action");
            }

            this.IsEnabled = true;
            this.listener.Prefixes.Add(url);
            this.action = action;
            this.listener.Start();
            this.listener.BeginGetContext(OnRequested, this.listener);
        }

        public void Stop()
        {
            if (this.IsEnabled)
            {
                this.IsEnabled = false;
                this.listener.Stop();
            }
        }

        public void Dispose()
        {
            if (this.isDisposed)
            {
                throw new Exception("Object has been disposed");
            }

            Stop();
            this.listener = null;
            this.isDisposed = true;
        }
        #endregion

        #region Private Method
        private void OnRequested(IAsyncResult ar)
        {
            var listener = ar.AsyncState as HttpListener;
            if (listener != null)
            {
                HttpListenerContext context = null;
                try
                {
                    context = listener.EndGetContext(ar);
                }
                catch (Exception)
                {
                    //HttpListener is stoped
                    return;
                }
                //Important => wait next request
                listener.BeginGetContext(OnRequested, listener);

                //Call Action
                this.action(context.Request, context.Response);
            }
        }
        #endregion
    }
}
