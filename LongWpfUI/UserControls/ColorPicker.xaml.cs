﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LongWpfUI.UserControls
{
    public enum ShapeStyles
    {
        Rectangle,
        Circle,
    }

    public partial class ColorPicker : UserControl
    {
        #region DP
        public Color SelectedColor
        {
            get { return (Color)GetValue(SelectedColorProperty); }
            set { SetValue(SelectedColorProperty, value); }
        }

        public ShapeStyles ShapeStyle
        {
            get { return (ShapeStyles)GetValue(ShapeStyleProperty); }
            set { SetValue(ShapeStyleProperty, value); }
        }

        public static readonly DependencyProperty SelectedColorProperty =
            DependencyProperty.Register("SelectedColor", typeof(Color), typeof(ColorPicker), new FrameworkPropertyMetadata(Colors.Black, OnSelectedColorChanged) { BindsTwoWayByDefault = true });

        public static readonly DependencyProperty ShapeStyleProperty =
            DependencyProperty.Register("ShapeStyle", typeof(ShapeStyles), typeof(ColorPicker), new PropertyMetadata(ShapeStyles.Rectangle, OnShapeStyleChanged));

        #endregion

        #region DP Changed
        private static void OnSelectedColorChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = d as ColorPicker;
            if (control != null && control.colorButton != null && control.listBox != null)
            {
                control.colorButton.Content = control.SelectedColor;
                control.colorButton.IsChecked = false;
                control.listBox.SelectedItem = control.SelectedColor;
            }
        }

        private static void OnShapeStyleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = d as ColorPicker;
            if (control != null && control.colorButton != null && control.listBox != null)
            {
                DataTemplate template = null;
                switch (control.ShapeStyle)
                {
                    case ShapeStyles.Circle:
                        template = control.Resources["circleTemplate"] as DataTemplate;
                        break;

                    case ShapeStyles.Rectangle:
                    default:
                        template = control.Resources["rectTemplate"] as DataTemplate;
                        break;
                }
                control.colorButton.ContentTemplate = template;
                control.listBox.ItemTemplate = template;
            }
        }
        #endregion

        #region Field
        private ListBox listBox;
        #endregion

        #region Constructor
        public ColorPicker()
        {
            InitializeComponent();
        }
        #endregion

        #region Event Handler       

        private void colorButton_Initialized(object sender, EventArgs e)
        {
            this.colorButton.Initialized -= colorButton_Initialized;
            this.colorButton.Content = this.SelectedColor;
        }

        private void ListBox_Initialized(object sender, EventArgs e)
        {
            this.listBox = sender as ListBox;
            if (this.listBox != null)
            {
                this.listBox.SelectedItem = this.SelectedColor;
                this.listBox.Initialized -= ListBox_Initialized;
                this.listBox.SelectionChanged += ListBox_SelectionChanged;
            }
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.colorButton != null && this.listBox != null)
            {
                if (this.listBox.SelectedItem is Color)
                {
                    Color color = (Color)this.listBox.SelectedItem;
                    this.SelectedColor = color;
                }
            }
        }
        #endregion
    }
}
