﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LongModel.ExtensionMethods;

namespace LongWpfUI.UserControls
{
    public enum DateTimePickerModes
    {
        DateTime,
        Date,
        Time
    }

    public partial class DateTimePicker : UserControl
    {
        #region DP
        public DateTimePickerModes Mode
        {
            get { return (DateTimePickerModes)GetValue(ModeProperty); }
            set { SetValue(ModeProperty, value); }
        }
        public DateTime SelectedDateTime
        {
            get { return (DateTime)GetValue(SelectedDateTimeProperty); }
            set { SetValue(SelectedDateTimeProperty, value); }
        }
        public List<int> HourCollection
        {
            get { return (List<int>)GetValue(HourCollectionProperty); }
            set { SetValue(HourCollectionProperty, value); }
        }
        public List<int> MinuteCollection
        {
            get { return (List<int>)GetValue(MinuteCollectionProperty); }
            set { SetValue(MinuteCollectionProperty, value); }
        }
        public int MinuteStep
        {
            get { return (int)GetValue(MinuteStepProperty); }
            set { SetValue(MinuteStepProperty, value); }
        }

        public static readonly DependencyProperty ModeProperty =
            DependencyProperty.Register("Mode", typeof(DateTimePickerModes), typeof(DateTimePicker), new PropertyMetadata(DateTimePickerModes.DateTime, OnModeChanged));
        
        public static readonly DependencyProperty SelectedDateTimeProperty =
            DependencyProperty.Register("SelectedDateTime", typeof(DateTime), typeof(DateTimePicker), new FrameworkPropertyMetadata(DateTime.Now, OnSelectedDateTimeChanged) { BindsTwoWayByDefault = true});

        public static readonly DependencyProperty HourCollectionProperty =
            DependencyProperty.Register("HourCollection", typeof(List<int>), typeof(DateTimePicker), new PropertyMetadata(null));

        public static readonly DependencyProperty MinuteCollectionProperty =
            DependencyProperty.Register("MinuteCollection", typeof(List<int>), typeof(DateTimePicker), new PropertyMetadata(null));

        public static readonly DependencyProperty MinuteStepProperty =
           DependencyProperty.Register("MinuteStep", typeof(int), typeof(DateTimePicker), new PropertyMetadata(1));

        private static void OnModeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = d as DateTimePicker;
            if (control != null)
            {
                switch (control.Mode)
                {
                    
                    case DateTimePickerModes.Date:
                        control.datePicker.Visibility = Visibility.Visible;
                        control.timePicker.Visibility = Visibility.Collapsed;
                        break;

                    case DateTimePickerModes.Time:
                        control.datePicker.Visibility = Visibility.Collapsed;
                        control.timePicker.Visibility = Visibility.Visible;
                        break;

                    case DateTimePickerModes.DateTime:
                    default:
                        control.datePicker.Visibility = Visibility.Visible;
                        control.timePicker.Visibility = Visibility.Visible;
                        break;
                }
            }
        }

        private static void OnSelectedDateTimeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = d as DateTimePicker;
            if (control != null)
            {
                control.datePicker.SelectedDate = control.SelectedDateTime;
                control.hourComboBox.SelectedItem = control.SelectedDateTime.Hour;
                control.minuteComboBox.SelectedItem = control.SelectedDateTime.Minute;
            }
        }

        #endregion

        #region Constructor
        public DateTimePicker()
        {
            InitializeComponent();
            this.Loaded += DateTimePicker_Loaded;
        }
        #endregion

        #region Event Handler

        private void DateTimePicker_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= DateTimePicker_Loaded;
            var collection = new List<int>();
            for (int i = 0; i < 24; i++)
            {
                collection.Add(i);
            }
            this.HourCollection = collection;

            collection = new List<int>();
            for (int i = 0; i < 60; i+= this.MinuteStep)
            {
                collection.Add(i);
            }
            this.MinuteCollection = collection;
        }

        private void hourComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (hourComboBox.SelectedItem is int)
            {
                var hour = (int)hourComboBox.SelectedItem;
                this.SelectedDateTime = this.SelectedDateTime.EditHour(hour);
            }
        }

        private void minuteComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (minuteComboBox.SelectedItem is int)
            {
                var minute = (int)minuteComboBox.SelectedItem;
                this.SelectedDateTime = this.SelectedDateTime.EditMinute(minute);
            }
        }

        private void datePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.datePicker.SelectedDate != null)
            {
                var date = this.datePicker.SelectedDate.Value;
                this.SelectedDateTime = this.SelectedDateTime.EditYear(date.Year).EditMonth(date.Month).EditDay(date.Day);
            }
            
        }
        #endregion
    }
}
