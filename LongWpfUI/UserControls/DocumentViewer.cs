﻿using LongInterface.Views.Documents;
using LongModel.Views.Documents;
using LongWpfUI.Helpers;
using LongWpfUI.Windows;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Windows.Media.Imaging;
using LongWpfUI.Models.Converters;

namespace LongWpfUI.UserControls
{
    public class DocumentViewer : ItemsControl
    {
        #region Const Value
        public const string MaxWriteWidth = "19cm";
        public const string MaxWriteHeight = "23cm"; 
        #endregion

        #region DP
        public IDocumentItem Header
        {
            get { return (IDocumentItem)GetValue(HeaderProperty); }
            set { SetValue(HeaderProperty, value); }
        }
        public IDocumentItem Footer
        {
            get { return (IDocumentItem)GetValue(FooterProperty); }
            set { SetValue(FooterProperty, value); }
        }
        public IList RawCollection
        {
            get { return (IList)GetValue(RawCollectionProperty); }
            set { SetValue(RawCollectionProperty, value); }
        }
        public IEnumerable<ResourceDictionary> ViewDictResource
        {
            get { return (IEnumerable<ResourceDictionary>)GetValue(ViewDictResourceProperty); }
            set { SetValue(ViewDictResourceProperty, value); }
        }
        
        public static readonly DependencyProperty HeaderProperty =
            DependencyProperty.Register("Header", typeof(IDocumentItem), typeof(DocumentViewer), new PropertyMetadata(null));

        public static readonly DependencyProperty FooterProperty =
            DependencyProperty.Register("Footer", typeof(IDocumentItem), typeof(DocumentViewer), new PropertyMetadata(null));

        public static readonly DependencyProperty RawCollectionProperty =
            DependencyProperty.Register("RawCollection", typeof(IList), typeof(DocumentViewer), new PropertyMetadata(null));

        public static readonly DependencyProperty ViewDictResourceProperty =
            DependencyProperty.Register("ViewDictResource", typeof(IEnumerable<ResourceDictionary>), typeof(DocumentViewer), new PropertyMetadata(null));


        #endregion

        #region Constructor
        public DocumentViewer()
        {
            ResourceDictionary dict = new ResourceDictionary();
            dict.Source = new Uri("/LongWpfUI;component/Resources/Styles/ItemsControlStyle.xaml", UriKind.Relative);
            this.Style = dict["xDocumentViewerStyle"] as Style;
        }
        #endregion

        #region Public Method
        /// <summary>
        /// Load all DocumentItems, calculate number of page and content in page
        /// </summary>
        public void LoadDocumentItems()
        {
            if (this.RawCollection == null || this.Header == null || this.Footer == null)
            {
                return;
            }

            //real render => find real size
            PrintTestWindow window = new PrintTestWindow(this.RawCollection, this, this.ViewDictResource);
            window.ShowDialog();
        }

        /// <summary>
        /// Print all the page in current viewer
        /// </summary>
        /// <param name="fileName">given fileName</param>
        /// <exception cref="Exception"/>
        public void PrintAllPages(string fileName)
        {
            IList pageCollection = this.ItemsSource as IList;
            if (pageCollection == null)
            {
                throw new Exception("ItemsSource is not a IList instance. Please check Logic in LongWPFUI.Windows.PrintTestWindow.xaml.cs");
            }

            //better resolution
            int dpi = 300;
            double scale = dpi / 96.0;
            //A4 size
            Size pageSize = new Size(8.26 * 96, 11.69 * 96);

            //Multi page document
            FixedDocument document = new FixedDocument();
            document.DocumentPaginator.PageSize = pageSize;

            try
            {
                for (int i = 0; i < pageCollection.Count; i++)
                {
                    //take each page control
                    ContentPresenter control = this.ItemContainerGenerator.ContainerFromIndex(i) as ContentPresenter;
                    if (control == null)
                    {
                        continue;
                    }

                    BitmapSource source = ControlHelper.TakeImageOnBitmapSource(control, dpi, scale);

                    //wrapper
                    FixedPage printPage = new FixedPage() { Width = pageSize.Width, Height = pageSize.Height };
                    printPage.Children.Add(new Image() { Source = source });
                    printPage.Measure(pageSize);
                    printPage.Arrange(new Rect(new Point(), pageSize));

                    //another wrapper
                    PageContent pageContent = new PageContent();
                    IAddChild addChild = pageContent as IAddChild;
                    if (addChild != null)
                    {
                        addChild.AddChild(printPage);
                    }
                    document.Pages.Add(pageContent);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Getting page error", ex);
            }

            try
            {
                PrintDialog dialog = new PrintDialog();
                if (dialog.ShowDialog() == true)
                {
                    dialog.PrintDocument(document.DocumentPaginator, fileName);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("PrintDialog API error", ex);
            }
        }

        public IDocumentPage CreateDocumentPage(IDocumentItem header, IDocumentItem footer, int index)
        {
            return new LongModel.Views.Documents.DocumentPage(header, new DocumentItemFooter(footer.Content, footer.View, index), new List<DocumentItem>());
        }
        #endregion
    }
}
