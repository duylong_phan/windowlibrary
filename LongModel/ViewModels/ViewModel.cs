﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LongInterface.ViewModels;
using LongInterface.Views;
using LongInterface.Models;
using LongModel.BaseClasses;

namespace LongModel.ViewModels
{
    /// <summary>
    /// Base class fo ViewModel
    /// </summary>
    public abstract class ViewModel : ObservableObject
    {
        /// <summary>
        /// Check if the resource is disposed
        /// </summary>
        public bool IsDisposed { get; protected set; }
    }

    /// <summary>
    /// Class for ViewModel, support View
    /// </summary>
    /// <typeparam name="T">IView implimented class</typeparam>
    public abstract class ViewModel<T> : ViewModel, IViewModel<T>
        where T : IView
    {
        #region Getter
        public T View { get; protected set; } 
        #endregion

        #region Constructor
        public ViewModel(T view)
        {
            this.View = view;
        } 
        #endregion    
    
        #region Absstract Method
        public abstract void InitializeParameter();

        public abstract void InitializeMVVM(); 
        #endregion
    }

    /// <summary>
    /// Class for ViewModel, support View and single Model
    /// </summary>
    /// <typeparam name="T">IView implimented class</typeparam>
    /// <typeparam name="K">IModel implimented class</typeparam>
    public abstract class ViewModel<T, K> : ViewModel<T>, IViewModel<T, K>
        where T : IView
        where K : IModel
    {
        #region Getter
        /// <summary>
        /// Get current Model binding to current Instance
        /// </summary>
        public K Model { get; private set; }
        #endregion

        #region Constructor
        /// <summary>
        /// Initialize a new Instance of ViewModel, support View, and Model
        /// </summary>
        /// <param name="view">initial IView</param>
        /// <param name="model">initial IModel</param>
        public ViewModel(T view, K model)
            : base(view)
        {
            this.Model = model;
        } 
        #endregion
    }

    /// <summary>
    /// Class for ViewModel, support View and I/O Model and Command/Validation container
    /// </summary>
    /// <typeparam name="T">IView implimented class</typeparam>
    /// <typeparam name="K">IModel implimented class</typeparam>
    /// <typeparam name="L">IModel implimented class</typeparam>
    /// <typeparam name="M">IViewModelHelper implimented class</typeparam>
    /// <typeparam name="N">IViewModelHelper implimented class</typeparam>
    public abstract class ViewModel<T, K, L, M, N> : ViewModel<T>, IViewModel<T,K,L,M,N>
        where T : IView
        where K : IModel
        where L : IModel
        where M : IViewModelHelper
        where N : IViewModelHelper
    {
        #region Getter
        /// <summary>
        /// Get & Set Model for InputParameter, zB: UI, File, Stream
        /// </summary>
        public K InputParameter { get; protected set; }

        /// <summary>
        /// Get & Set Model for OutputParameter, zB: UI, File, Stream
        /// </summary>
        public L OutputParameter { get; protected set; }

        /// <summary>
        /// Get & Set Container for CommandInfo
        /// </summary>
        public M CommandInfoContainer { get; protected set; }

        /// <summary>
        /// Get & Set Container for ValidationRule
        /// </summary>
        public N ValidationRuleContainer { get; protected set; }
        #endregion

        #region Constructor
        /// <summary>
        /// Initialize a new instance of ViewModel
        /// </summary>
        /// <param name="view">initial IView</param>
        public ViewModel(T view)
            : base(view)
        {

        } 
        #endregion
    }
}
