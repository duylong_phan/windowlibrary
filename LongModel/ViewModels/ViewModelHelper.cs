﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LongInterface.ViewModels;
using LongModel.BaseClasses;

namespace LongModel.ViewModels
{
    /// <summary>
    /// Base class for ViewModelHelper
    /// </summary>
    /// <typeparam name="T">IViewModel implimented class</typeparam>
    public abstract class ViewModelHelper<T> : ObservableObject, IViewModelHelper<T>
        where T : IViewModel
    {
        #region Getter
        /// <summary>
        /// Get ViewModel for current Instance
        /// </summary>
        public T ViewModel { get; private set; } 
        #endregion

        #region Constructor
        /// <summary>
        /// Initialize new instance of ViewModelHelper
        /// </summary>
        /// <param name="viewModel"></param>
        /// <param name="isPropertyChangedUpdate"></param>
        public ViewModelHelper(T viewModel, bool isPropertyChangedUpdate)
        {
            this.ViewModel = viewModel;
            this.IsPropertyChangedUpdate = isPropertyChangedUpdate;
        } 
        #endregion
    }
}
