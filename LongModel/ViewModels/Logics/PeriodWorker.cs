﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LongModel.ViewModels.Logics
{
    public class UpdatedEventArgs : EventArgs
    {

    }

    public delegate void UpdatedEventHandler(object sender, UpdatedEventArgs e);

    /// <summary>
    /// Class to work given job periodly, and notify when it has been done, update available or has error during execution
    /// </summary>
    public class PeriodWorker : WorkerBase<Func<bool>>
    {
        #region Event
        public event UpdatedEventHandler Updated;
        protected void OnUpdated(UpdatedEventArgs e)
        {
            UpdatedEventHandler handler = Updated;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        #endregion

        #region Field
        private CancellationTokenSource cancelSource;
        #endregion

        #region Public Method
        public async override void Start()
        {
            if (this.IsEnable || this.action == null)
            {
                return;
            }

            //set status
            this.IsEnable = true;
            this.cancelSource = new CancellationTokenSource();

            //do work
            await Task.Factory.StartNew(() => DoWork());

            //reset status
            this.IsEnable = false;
            this.cancelSource.Dispose();

            //notify stopped
            this.OnStoped(new StopedEventArgs());
        }

        public void Stop()
        {
            if (this.IsEnable && this.cancelSource != null)
            {
                this.cancelSource.Cancel();
            }
        }
        #endregion

        #region Private Method
        private void DoWork()
        {
            bool canUpdate = false;
            try
            {
                while (true)
                {
                    if (this.cancelSource.Token.IsCancellationRequested)
                    {
                        break;
                    }

                    if (this.action == null)
                    {
                        continue;
                    }

                    canUpdate = this.action();
                    if (canUpdate)
                    {
                        this.OnUpdated(new UpdatedEventArgs());
                    }
                }
            }
            catch (Exception ex)
            {
                this.OnHasError(new HasErrorEventArgs(ex));
            }
        } 
        #endregion
    }
}
