﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.ViewModels.Logics
{
    /// <summary>
    /// Class to work given job, and notify when it has been done or has error during execution
    /// </summary>
    public class SingleWorker : WorkerBase<Action>
    {
        public async override void Start()
        {
            if (this.IsEnable || this.action == null)
            {
                return;
            }

            this.IsEnable = true;
            try
            {
                await Task.Factory.StartNew(this.action);
            }
            catch (Exception ex)
            {
                this.OnHasError(new HasErrorEventArgs(ex));
            }

            this.IsEnable = false;
            this.OnStoped(new StopedEventArgs());
        }        
    }
}
