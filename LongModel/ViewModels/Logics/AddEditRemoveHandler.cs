﻿using LongInterface.ViewModels.Logics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace LongModel.ViewModels.Logics
{
    public class AddEditRemoveHandler : AddRemoveHandler, IAddEditRemoveHandler
    {
        public ICommand Edit { get; set; }
    }
}
