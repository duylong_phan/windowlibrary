﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.ViewModels.Logics
{
    public class DataConsumer
    {
        #region Getter
        /// <summary>
        /// Get if Update action can be executed
        /// </summary>
        public bool CanUpdate
        {
            get
            {
                return this.canUpdate;
            }
        }
        #endregion

        #region Field
        private Func<object, bool> consumFunc;
        private Action updateAction;
        private bool canUpdate;
        #endregion

        #region Constructor
        /// <summary>
        /// Initialize a new instance of DataConsumer
        /// </summary>
        /// <param name="consumFunc">given func to process data</param>
        /// <param name="updateAction">given action to update</param>
        public DataConsumer(Func<object, bool> consumFunc, Action updateAction)
        {
            this.consumFunc = consumFunc;
            this.updateAction = updateAction;
        }
        #endregion

        #region Public Method
        /// <summary>
        /// Take the given data to process
        /// </summary>
        /// <param name="data"></param>
        public void Take(object data)
        {
            if (this.consumFunc != null)
            {
                this.canUpdate = this.consumFunc(data);
            }
        }

        /// <summary>
        /// Update result
        /// </summary>
        public void Update()
        {
            if (this.updateAction != null)
            {
                this.canUpdate = false;
                this.updateAction();
            }
        }
        #endregion
    }
}
