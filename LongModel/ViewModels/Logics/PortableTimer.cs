﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.ViewModels.Logics
{
    public class TickedEventArgs : EventArgs
    {
        public int TickCount { get; private set; }

        public TickedEventArgs(int tickCount)
        {
            this.TickCount = tickCount;
        }
    }

    public delegate void TickedEventHandler(object sender, TickedEventArgs e);

    public class PortableTimer
    {
        #region Event
        public event TickedEventHandler Ticked;
        protected void OnTicked()
        {
            var handler = Ticked;
            if (handler != null)
            {
                handler(this, new TickedEventArgs(this.tickCount));
            }
            tickCount++;
        }
        #endregion

        #region Getter, setter
        public bool IsEnabled { get; set; }
        public int Interval { get; set; }
        #endregion

        #region Field
        private SingleWorker worker;
        private int tickCount;
        #endregion

        #region Constructor
        public PortableTimer(int interval)
        {
            this.Interval = interval;
            this.IsEnabled = false;
            this.tickCount = 0;
        }
        #endregion

        #region Public Method
        public void Start()
        {
            if (this.IsEnabled)
            {
                return;
            }

            this.IsEnabled = true;
            this.worker = new SingleWorker();
            this.worker.SetAction(Loop);
            this.worker.Start();
        }

        public void Stop()
        {
            if (this.IsEnabled)
            {
                this.IsEnabled = false;
            }
        }
        #endregion

        #region Private Method
        private void Loop()
        {
            while (true)
            {
                Task.Delay(this.Interval).Wait();
                if (this.IsEnabled)
                {
                    OnTicked();
                }
                else
                {
                    break;
                }
            }
        }
        #endregion
    }
}
