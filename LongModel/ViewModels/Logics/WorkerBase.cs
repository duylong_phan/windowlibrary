﻿using System;

namespace LongModel.ViewModels.Logics
{
    public class StopedEventArgs : EventArgs
    {
        
    }

    public class HasErrorEventArgs : EventArgs
    {
        public Exception Error { get; private set; }

        public HasErrorEventArgs(Exception error)
        {
            this.Error = error;
        }
    }

    public delegate void StopedEventHandler(object sender, StopedEventArgs e);
    public delegate void HasErrorEventHandler(object sender, HasErrorEventArgs e);

    public abstract class WorkerBase<T>
    {
        #region Event
        public event StopedEventHandler Stoped;
        public event HasErrorEventHandler HasError;

        protected void OnStoped(StopedEventArgs e)
        {
            StopedEventHandler handler = Stoped;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        protected void OnHasError(HasErrorEventArgs e)
        {
            HasErrorEventHandler handler = HasError;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        
        #endregion

        #region Getter
        public bool IsEnable { get; protected set; }
        #endregion

        #region Field
        protected T action; 
        #endregion

        #region Constructor
        public WorkerBase()
        {
            this.IsEnable = false;
        }
        #endregion

        #region Public Method
        public virtual void SetAction(T action)
        {
            this.action = action;
        }
        #endregion

        #region Abstract Method
        public abstract void Start();                
        #endregion
    }
}
