﻿using LongInterface.ViewModels.Logics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace LongModel.ViewModels.Logics
{
    public class AddRemoveHandler : IAddRemoveHandler
    {
        public ICommand Add { get; set; }
        public ICommand Remove { get; set; }
    }
}
