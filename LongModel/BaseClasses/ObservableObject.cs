﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Xml.Serialization;
using LongInterface.Models;

namespace LongModel.BaseClasses
{
    /// <summary>
    /// Base Class for all Class, with support for notification
    /// </summary>
    public abstract class ObservableObject : INotifyPropertyChanged, IDisposable
    {
        #region Event
        /// <summary>
        /// Event fire when the inner Property is changed
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;        
        #endregion

        #region Getter & Setter
        [XmlIgnore]
        /// <summary>
        /// Get & Set if the Instance nofities when a property is changed
        /// Default is true
        /// </summary>
        public bool IsPropertyChangedUpdate { get; set; } 
        #endregion

        #region Constructor
        /// <summary>
        /// Initialize new instance of ObservableObject
        /// </summary>
        public ObservableObject()
        {
            this.IsPropertyChangedUpdate = true;
        } 
        #endregion

        #region Public Method
        public virtual void Dispose()
        {
            //Do nothing
        } 
        #endregion

        #region Protected Method
        /// <summary>
        /// Notify if there is a changed property
        /// </summary>
        /// <param name="proName"></param>
        protected void OnPropertyChanged(string proName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(proName));
            }
        }

        /// <summary>
        /// Set the Property and Check if the given Value is new
        /// </summary>
        /// <typeparam name="T">Instance Type</typeparam>
        /// <param name="field">instance als field</param>
        /// <param name="input">new Value</param>
        /// <param name="proName">Property Name</param>
        /// <returns>Indicate if the Property is new</returns>
        protected bool SetProperty<T>(ref T field, T input, [CallerMemberName] string proName = null)
        {
            bool canEdit = false;
            if (!Object.Equals(field, input))   //NOT
            {
                //dispose Subscription, when the new object override the old one
                //avoid memory leak, and unexpected behaviour
                if (field is IDataChanged)
                {
                    IDataChanged temp = field as IDataChanged;
                    if (temp.HasSubscription)
                    {
                        temp.UnsubscribeChanged();
                    }
                }

                //override old object
                field = input;

                //check if Notification is enable
                if (this.IsPropertyChangedUpdate)
                {
                    OnPropertyChanged(proName);
                }
                canEdit = true;
            }
            return canEdit;
        }  
        #endregion
    }
}
