﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace LongModel.BaseClasses
{
    public class ActionSetting : ObservableObject
    {
        #region Getter, Setter
        private bool captureOnStart;        
        private bool isEnabled;

        [XmlAttribute("IsEnabled")]
        public bool IsEnabled
        {
            get { return this.isEnabled; }
            set
            {
                this.SetProperty(ref this.isEnabled, value);
            }
        }
        [XmlAttribute("CaptureOnStart")]
        public bool CaptureOnStart
        {
            get { return this.captureOnStart; }
            set
            {
                this.SetProperty(ref this.captureOnStart, value);
            }
        }
        #endregion

        #region Constructor
        public ActionSetting(bool isEnabled, bool captureOnStart)
        {
            this.isEnabled = isEnabled;
            this.captureOnStart = captureOnStart;
        }
        #endregion
    }
}
