﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Reflection;
using LongInterface.Models;

namespace LongModel.BaseClasses
{
    public abstract class DataObject : ObservableObject, IDataChanged
    {
        #region Protected Properties
        /// <summary>
        /// Get, Set the action, when change happened
        /// </summary>
        protected Action EventAction { get; set; }
        #endregion

        #region Constructor, Deconstructor
        public DataObject()
        {
            this.HasSubscription = false;
        }

        ~DataObject()
        {
            //avoid memory leak
            UnsubscribeChanged();
        }
        #endregion

        #region IDataChanged
        /// <summary>
        /// Get if current instance has been subscribed for changed Event
        /// </summary>
        [XmlIgnore]
        public bool HasSubscription { get; private set; }

        /// <summary>
        /// Subscribe for the inner property notification, only when HasSubscription is false
        /// </summary>
        /// <param name="eventAction">given Action</param>
        public void SubscribeChanged(Action eventAction)
        {
            if (this.HasSubscription)
            {
                return;
            }

            //self
            this.HasSubscription = true;
            this.EventAction = eventAction;
            this.PropertyChanged += DataObject_PropertyChanged;

            //Child 
            var type = this.GetType();
            var typeInfo = type.GetTypeInfo();            
            foreach (var item in typeInfo.DeclaredProperties)
            {
                var value = item.GetValue(this) as IDataChanged;
                if (value != null)
                {
                    value.SubscribeChanged(this.EventAction);
                }
            }
        }
        
        /// <summary>
        /// Unsubscribe for inner notifiication, only when HasSubscription is true
        /// </summary>
        public void UnsubscribeChanged()
        {
            if (this.HasSubscription)
            {
                //self
                this.HasSubscription = false;
                this.EventAction = null;
                this.PropertyChanged -= DataObject_PropertyChanged;

                //Child 
                var type = this.GetType();
                var typeInfo = type.GetTypeInfo();
                foreach (var item in typeInfo.DeclaredProperties)
                {
                    if (item != null && item.GetMethod != null && item.SetMethod != null)
                    {
                        var value = item.GetValue(this) as IDataChanged;
                        if (value != null)
                        {
                            value.UnsubscribeChanged();
                        }
                    }
                }
            }
        }
        #endregion

        #region Event Handler
        private void DataObject_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (this.EventAction != null)
            {
                this.EventAction();
            }
        } 
        #endregion
    }
}
