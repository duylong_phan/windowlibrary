﻿using LongInterface.Models.IO.Files;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Xml.Serialization;

namespace LongModel.BaseClasses
{
    public abstract class FileSettingBase : ActionSetting, IFileSettingBase
    {
        #region Getter, setter        
        private bool useDuration;
        private string directory;
        private int duration;
                
        [XmlAttribute("UseDuration")]
        public bool UseDuration
        {
            get { return this.useDuration; }
            set
            {
                this.SetProperty(ref this.useDuration, value);
            }
        }
        [XmlAttribute("Directory")]
        public string Directory
        {
            get { return this.directory; }
            set
            {
                this.SetProperty(ref this.directory, value);
            }
        }
        [XmlAttribute("Duration")]
        public int Duration
        {
            get { return this.duration; }
            set
            {
                this.SetProperty(ref this.duration, value);
            }
        }    

        [XmlIgnore]
        public object Tag { get; set; }
        [XmlIgnore]
        public object DurationRule { get; set; }
        [XmlIgnore]
        public ICommand SelectDirectory { get; set; }
        #endregion

        #region Constructor
        public FileSettingBase( string directory, int duration, bool useDuration, bool isEnabled, bool captureOnStart)
            : base(isEnabled, captureOnStart)
        {
            this.directory = directory;
            this.duration = duration;
            this.UseDuration = useDuration;
        }
        #endregion

        #region Abstract Method
        public abstract object CloneModel();
        #endregion
    }
}
