﻿using LongInterface.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.ExtensionMethods
{
    public static partial class IListExtensions
    {
        /// <summary>
        /// Clone current collection
        /// </summary>
        /// <typeparam name="T">item type</typeparam>
        /// <typeparam name="K">current collection type</typeparam>
        /// <param name="value">current collection</param>
        /// <param name="collection">new collection</param>
        public static void CloneCollection<T,K>(this K value, ref K collection)
            where T : ICloneModel
            where K : IList<T>
        {
            foreach (var item in value)
            {
                collection.Add((T)item.CloneModel());
            }
        }

        /// <summary>
        /// Add multiple Items into current instance
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">current instance</param>
        /// <param name="collection">items</param>
        public static void AddItems<T>(this IList<T> value, params T[] collection)
        {
            foreach (var item in collection)
            {
                value.Add(item);
            }
        }
    }
}
