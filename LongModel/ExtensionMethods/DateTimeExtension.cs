﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.ExtensionMethods
{
    public static class DateTimeExtension
    {
        #region Public Method
        public static DateTime EditSecond(this DateTime value, int second)
        {
            return new DateTime(value.Year, value.Month, value.Day,
                                value.Hour, value.Minute, second);
        }

        public static DateTime EditMinute(this DateTime value, int minute)
        {
            return new DateTime(value.Year, value.Month, value.Day,
                                value.Hour, minute, value.Second);
        }

        public static DateTime EditHour(this DateTime value, int hour)
        {
            return new DateTime(value.Year, value.Month, value.Day,
                                hour, value.Minute, value.Second);
        }

        public static DateTime EditDay(this DateTime value, int day)
        {
            return new DateTime(value.Year, value.Month, day,
                                value.Hour, value.Minute, value.Second);
        }

        public static DateTime EditMonth(this DateTime value, int month)
        {
            return new DateTime(value.Year, month, value.Day,
                                value.Hour, value.Minute, value.Second);
        }

        public static DateTime EditYear(this DateTime value, int year)
        {
            return new DateTime(year, value.Month, value.Day,
                                value.Hour, value.Minute, value.Second);
        }
        #endregion
    }
}
