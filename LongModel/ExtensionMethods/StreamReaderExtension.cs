﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.ExtensionMethods
{
    public static class StreamReaderExtension
    {
        /// <summary>
        /// Read a XML Element with given Tag
        /// </summary>
        /// <param name="value">StreamReader instance</param>
        /// <param name="tag">given tag</param>
        /// <returns></returns>
        public static string ReadXmlElement(this StreamReader value, string tag)
        {
            var builder = new StringBuilder();
            var header = string.Format("<{0}", tag);
            var ender = string.Format("</{0}>", tag);
            var hasHeader = false;
            var hasEnder = false;
            string element = null;

            var line = value.ReadLine();
            while (line != null)
            {
                if (!hasHeader && line.StartsWith(header))  //NOT
                {
                    hasHeader = true;
                }
                else if (!hasEnder && line.StartsWith(ender))   //NOT
                {
                    hasEnder = true;
                }

                //start Adding
                if (hasHeader)
                {
                    builder.AppendLine(line);
                    //Stop
                    if (hasEnder)
                    {
                        break;
                    }
                }
                line = value.ReadLine();
            }

            if (builder.Length > 0)
            {
                element = builder.ToString();
            }

            return element;
        }

        /// <summary>
        /// Read All XML Element with given Tag
        /// </summary>
        /// <param name="value">StreamReader instance</param>
        /// <param name="tag">given tag</param>
        /// <returns></returns>
        public static List<string> ReadAllXmlElement(this StreamReader value, string tag)
        {
            var collection = new List<string>();

            var element = value.ReadXmlElement(tag);
            while (element != null)
            {
                collection.Add(element);
                element = value.ReadXmlElement(tag);
            }

            return collection;
        }

        /// <summary>
        /// Read a XML Element with given Tag reversely
        /// </summary>
        /// <param name="value">StreamReader instance</param>
        /// <param name="tag">given tag</param>
        /// <param name="canDispose">dispose after read</param>
        /// <returns></returns>
        public static string ReadXmlElementReverse(this StreamReader value, string tag, bool canDispose = false)
        {
            var header = string.Format("<{0}", tag);
            var ender = string.Format("</{0}>", tag);
            var hasHeader = false;
            var hasEnder = false;
            var collection = new List<string>();
            string element = null;

            var line = value.BaseStream.ReadLineReverse();
            while (line != null)
            {
                if (!hasEnder && line.StartsWith(ender))   //NOT
                {
                    hasEnder = true;
                }
                else if (!hasHeader && line.StartsWith(header))  //NOT
                {
                    hasHeader = true;
                }

                //start Adding
                if (hasEnder)
                {
                    collection.Add(line);
                    //Stop
                    if (hasHeader)
                    {
                        break;
                    }
                }
                line = value.BaseStream.ReadLineReverse();
            }

            if (collection.Count > 0)
            {
                var builder = new StringBuilder();
                collection.Reverse();
                foreach (var item in collection)
                {
                    builder.AppendLine(item);
                }
                element = builder.ToString();
            }

            if (canDispose)
            {
                value.DisposeExtension();
            }
            return element;
        }

        /// <summary>
        /// Dispose resource in Extension method
        /// </summary>
        /// <param name="value">StreamReader Instance</param>
        public static void DisposeExtension(this StreamReader value)
        {
            if (value.BaseStream != null)
            {
                value.BaseStream.DisposeExtension();
            }
        }
    }
}
