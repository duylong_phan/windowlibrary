﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.ExtensionMethods
{
    public static class StreamExtension
    {
        #region Static Properties
        private static Dictionary<Stream, int> dict;

        public static Dictionary<Stream, int> Dict
        {
            get
            {
                if (dict == null)
                {
                    dict = new Dictionary<Stream, int>();
                }
                return dict;
            }
        }
        #endregion

        #region Public Method
        /// <summary>
        /// Read a Line from Stream reversely
        /// </summary>
        /// <param name="value">Stream Instance</param>
        /// <param name="canDispose">dispose after read</param>
        /// <returns></returns>
        public static string ReadLineReverse(this Stream value, bool canDispose = false)
        {
            var length = 0;
            var hasLine = false;
            string line = null;
            byte[] buffer = null;

            //Add dict, when value is not available
            if (!Dict.ContainsKey(value))   //NOT
            {
                Dict.Add(value, 1);
            }

            //Find New Line
            while (Dict[value] <= value.Length)
            {
                value.Seek(-Dict[value], SeekOrigin.End);
                Dict[value]++;
                var tmpValue = value.ReadByte();
                if (tmpValue == '\n')
                {
                    if (Dict[value] > 2)
                    {
                        hasLine = true;
                        break;
                    }
                }
                else if (tmpValue != '\r')
                {
                    length++;
                }
            }

            //Content is available
            if (length > 0)
            {
                buffer = new byte[length];
                if (hasLine)
                {
                    value.Seek(-(Dict[value] - 2), SeekOrigin.End);
                }
                else
                {
                    value.Seek(0, SeekOrigin.Begin);
                }
                value.Read(buffer, 0, buffer.Length);
                line = Encoding.UTF8.GetString(buffer, 0, buffer.Length);
            }

            if (canDispose)
            {
                value.DisposeExtension();
            }

            return line;
        }

        /// <summary>
        /// Read byte from End
        /// </summary>
        /// <param name="value">Stream instance</param>
        /// <param name="buffer">given buffer</param>
        /// <param name="offset">given offset</param>
        /// <param name="length">given length</param>
        public static void ReadReverse(this Stream value, byte[] buffer, int offset, int length)
        {
            value.Seek(-length, SeekOrigin.End);
            value.Read(buffer, offset, length);
        }

        /// <summary>
        /// Dispose resource in Extension method
        /// </summary>
        /// <param name="value">Stream Instance</param>
        public static void DisposeExtension(this Stream value)
        {
            if (Dict.ContainsKey(value))
            {
                Dict.Remove(value);
            }
        }
        #endregion
    }
}
