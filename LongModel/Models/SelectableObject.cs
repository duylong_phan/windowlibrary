﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LongInterface.Models;
using LongModel.BaseClasses;

namespace LongModel.Models
{
    public class SelectableObject : ObservableObject, ISelectable
    {
        #region Getter, Setter
        private bool isSelected;

        /// <summary>
        /// Get & Set observable select status of current Instance
        /// </summary>
        public bool IsSelected
        {
            get
            {
                return this.isSelected;
            }
            set
            {
                this.SetProperty(ref this.isSelected, value);
            }
        } 
        #endregion

        #region Constructor
        /// <summary>
        /// Initialize a new instance of SelectableObject
        /// </summary>
        /// <param name="isSelected">intial select status</param>
        public SelectableObject(bool isSelected)
        {
            this.isSelected = isSelected;
        } 
        #endregion
    }
}
