﻿using LongModel.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.Models.Status
{
    public abstract class DataUpdater<T> : ObservableObject
    {
        #region Getter, Setter
        private T value;

        public T Value
        {
            get { return this.value; }
            set
            {
                this.SetProperty(ref this.value, value);
            }
        }

        public T TmpValue { get; set; }
        #endregion

        #region Constructor
        public DataUpdater(T value, T tempValue)
        {
            this.value = value;
            this.TmpValue = tempValue;
        } 
        #endregion
    }
}
