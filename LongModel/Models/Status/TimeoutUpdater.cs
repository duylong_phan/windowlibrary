﻿using LongModel.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.Models.Status
{
    public class TimeoutUpdater 
    {
        #region Getter
        public int MaxIndex { get; private set; }
        #endregion

        #region Field
        private int Index;
        private Action timeoutAction;
        #endregion

        #region Constructor
        public TimeoutUpdater()
        {
           
        } 
        #endregion

        #region Public Methods
        public void UpdateIndex()
        {
            UpdateIndex(1);
        }

        public void UpdateIndex(int value)
        {
            this.Index += value;
            if (this.Index >= this.MaxIndex && this.timeoutAction != null)
            {
                this.Index = 0;
                this.timeoutAction();
            }
        }

        public void UpdateExplicit()
        {
            UpdateIndex(this.MaxIndex);
        }

        public void SetAction(Action timeoutAction, int maxIndex)
        {
            this.Index = 0;
            this.MaxIndex = maxIndex;
            this.timeoutAction = timeoutAction;
        } 
        #endregion
    }
}
