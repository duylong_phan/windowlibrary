﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LongModel.BaseClasses;
using LongInterface.Models;
using LongInterface.Models.Status;

namespace LongModel.Models.Status
{
    public class ProgressInfo<T> : ObservableObject, IProgressInfo<T>
    {
        #region Getter & Setter
        private string text;
        private T percent;
        private int timeLeft;
        private int timeRemain;
        private int duration;
        private bool isIndeterminate;
        private bool hasError;

        public string Text
        {
            get { return this.text; }
            set
            {
                this.SetProperty(ref this.text, value);
            }
        }
        public T Percent
        {
            get { return this.percent; }
            set
            {
                this.SetProperty(ref this.percent, value);
            }
        }
        public int TimeLeft
        {
            get { return this.timeLeft; }
            set
            {
                this.SetProperty(ref this.timeLeft, value);
            }
        }
        public int TimeRemain
        {
            get { return this.timeRemain; }
            set
            {
                this.SetProperty(ref this.timeRemain, value);
            }
        }
        public int Duration
        {
            get { return this.duration; }
            set
            {
                this.SetProperty(ref this.duration, value);
            }
        }
        public bool IsIndeterminate
        {
            get { return this.isIndeterminate; }
            set
            {
                this.SetProperty(ref this.isIndeterminate, value);
            }
        }
        public bool HasError
        {
            get { return this.hasError; }
            set
            {
                this.SetProperty(ref this.hasError, value);
            }
        } 
        #endregion

        #region Constructor
        /// <summary>
        /// Initialize a new instance of ProgressInfo
        /// </summary>
        public ProgressInfo()
        {
            this.text = string.Empty;
            this.timeLeft = this.timeRemain = this.duration = 0;
            this.isIndeterminate = this.hasError = false;
        } 
        #endregion
    }
}
