﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.Models.Communications
{
    public static class Base64
    {
        #region Field
        private static byte[] encodedTable;
        private static byte[] decodedTable; 
        #endregion

        #region Getter
        /// <summary>
        /// Get Array of Encoded Table
        /// </summary>
        public static byte[] EncodedTable
        {
            get
            {
                if (encodedTable == null)
                {
                    encodedTable = new byte[64];
                    for (int i = 0; i < 26; i++)
                    {
                        encodedTable[i] = (byte)('a' + i);
                        encodedTable[i + 26] = (byte)('A' + i);
                    }

                    for (int i = 0; i < 10; i++)
                    {
                        encodedTable[i + 52] = (byte)('0' + i);
                    }

                    encodedTable[62] = (byte)('+');
                    encodedTable[63] = (byte)('/');
                }

                return encodedTable;
            }
        }

        /// <summary>
        /// Get Array of Decoded Table
        /// </summary>
        public static byte[] DecodedTable
        {
            get
            {
                if (decodedTable == null)
                {
                    decodedTable = new byte[256];
                    for (int i = 0; i < EncodedTable.Length; i++)
                    {
                        decodedTable[EncodedTable[i]] = (byte)i;
                    }

                }

                return decodedTable;
            }
        } 
        #endregion

        #region Public Method

        /// <summary>
        /// Encode the given Array in Base64 Format
        /// </summary>
        /// <param name="dataBuffer">given Data</param>
        /// <param name="offset">given offset (do nothing)</param>
        /// <exception cref="Input, Calculation Exception"/>
        /// <returns></returns>
        public static byte[] Encode(byte[] dataBuffer, byte offset = 0)
        {
            if (dataBuffer == null)
            {
                throw new Exception("Input array instance is invalid");
            }

            int length = 0;
            if((dataBuffer.Length % 3) == 0)
            {
                length = (dataBuffer.Length / 3) * 4;
            }
            else
            {
                length = dataBuffer.Length / 3;
                length = (length + 1) * 4;
            }

            byte[] encodeBuffer = new byte[length];
            int newLength = Encode(dataBuffer, dataBuffer.Length, encodeBuffer, encodeBuffer.Length);
            if (newLength < 0)
            {
                throw new Exception("Error with Buffer Length");
            }

            return encodeBuffer;
        } 

        /// <summary>
        /// Convert ASCII data buffer into Base64 encode buffer
        /// </summary>
        /// <param name="dataBuffer">given data buffer</param>
        /// <param name="dataLength">given data length</param>
        /// <param name="encodeBuffer">given encode buffer</param>
        /// <param name="encodeLength">given encode length</param>
        /// <returns></returns>
        public static int Encode(byte[] dataBuffer, int dataLength, byte[] encodeBuffer, int encodeLength)
        {
            if (dataBuffer == null || encodeBuffer == null)
            {
                throw new Exception("Input array instance is invalid");
            }

            byte x, y, z, tmpValue;
            int length = 0;
            int i = 0;
            int k = 0;
            int emptyByte = 0;

            if ((dataLength % 3) == 0)
            {
                length = (dataLength / 3) * 4;
            }
            else
            {
                length = (dataLength / 3) + 1;
                length = length * 4;
                emptyByte = 3 - (dataLength % 3);
            }

            if (length > encodeLength)
            {
                length = -1;
            }
            else
            {
                i = 0;
                k = 0;
                //add encoded bytes
                while (i < dataLength)
                {                    
                    x = (i < dataLength) ? dataBuffer[i] : byte.MinValue; i++;
                    y = (i < dataLength) ? dataBuffer[i] : byte.MinValue; i++;
                    z = (i < dataLength) ? dataBuffer[i] : byte.MinValue; i++;

                    tmpValue = (byte)((x & 0xFC) >> 2);
                    encodeBuffer[k] = EncodedTable[tmpValue]; k++;

                    tmpValue = (byte)(((x & 0x03) << 4) + ((y & 0xF0) >> 4));
                    encodeBuffer[k] = EncodedTable[tmpValue]; k++;

                    tmpValue = (byte)(((y & 0x0F) << 2) + ((z & 0xC0) >> 6));
                    encodeBuffer[k] = EncodedTable[tmpValue]; k++;

                    tmpValue = (byte)(z & 0x3F);
                    encodeBuffer[k] = EncodedTable[tmpValue]; k++;
                }

                //add empty bytes
                for (i = 0; i < emptyByte; i++)
                {
                    encodeBuffer[length - i - 1] = (byte)'=';
                }
            }

            return length;
        }

        /// <summary>
        /// Decode the given Array in normal Data Format
        /// </summary>
        /// <param name="encodeBuffer">given encodedData</param>
        /// <param name="offset">given offset</param>
        /// <exception cref="Input, Calculation Exception"/>
        /// <returns></returns>
        public static byte[] Decode(byte[] encodeBuffer, byte offset = 0)
        {
            int length = (encodeBuffer.Length / 4) * 3;
            byte[] dataBuffer = new byte[length];
            int newLength = Decode(dataBuffer, length, encodeBuffer, encodeBuffer.Length);

            //check result
            if (newLength < 0)
            {
                throw new Exception("Error with Buffer Length");
            }
            else if (newLength != length)
            {
                Array.Resize(ref dataBuffer, newLength);
            }
            return dataBuffer;
        }

        /// <summary>
        /// Convert Base64 encode buffer into ASCII encode buffer
        /// </summary>
        /// <param name="dataBuffer">given data buffer</param>
        /// <param name="dataLength">given data length</param>
        /// <param name="encodeBuffer">given encode buffer</param>
        /// <param name="encodeLength">given encode length</param>
        /// <returns></returns>
        public static int Decode(byte[] dataBuffer, int dataLength, byte[] encodeBuffer, int encodeLength)
        {
            byte a, b, c, d;
            int length = 0;
            int i = 0;
            int k = 0;

            //check empty bytes
            for (i = 0; i < 3; i++)
            {
                if (encodeBuffer[encodeLength - i - 1] == '=')
                {
                    k++;
                }
                else
                {
                    break;
                }
            }

            length = (encodeLength / 4) * 3 - k;
            if (length > dataLength)
            {
                length = -1;
            }
            else
            {
                i = 0;
                k = 0;
                //add decoded bytes
                while (i < encodeLength)
                {
                    a = (i < encodeLength) ? DecodedTable[encodeBuffer[i]] : byte.MinValue; i++;
                    b = (i < encodeLength) ? DecodedTable[encodeBuffer[i]] : byte.MinValue; i++;
                    c = (i < encodeLength) ? DecodedTable[encodeBuffer[i]] : byte.MinValue; i++;
                    d = (i < encodeLength) ? DecodedTable[encodeBuffer[i]] : byte.MinValue; i++;                   

                    if (k < length)
                    {
                        dataBuffer[k] = (byte)((a << 2) + (b >> 4));
                        k++;
                    }

                    if (k < length)
                    {
                        dataBuffer[k] = (byte)(((b & 0x0F) << 4) + (c >> 2));
                        k++;
                    }

                    if (k < length)
                    {
                        dataBuffer[k] = (byte)(((c & 0x03) << 6) + d);
                        k++;
                    }
                }
            }
            return length;
        }
        #endregion
    }
}
