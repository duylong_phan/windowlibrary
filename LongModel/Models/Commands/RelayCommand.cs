﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.Models.Commands
{
    public class RelayCommand : CommandBase
    {
        #region Field
        protected Func<bool> canExecute;
        protected Action execute;
        #endregion

        #region Constructor
        public RelayCommand(Action execute)
            : this(execute, null)
        {

        }

        public RelayCommand(Action execute, Func<bool> canExecute)
        {
            if (execute == null)
            {
                throw new Exception("Execute action has to be given");
            }

            this.execute = execute;
            this.canExecute = canExecute;
        }
        #endregion

        #region ICommand        

        public override bool CanExecute(object parameter)
        {
            return (this.canExecute == null) ? true : this.canExecute();
        }

        public override void Execute(object parameter)
        {
            if (this.BeforeAction != null)
            {
                this.BeforeAction();
            }

            this.execute();
            this.IsManualExecuted = true;

            if (this.AfterAction != null)
            {
                this.AfterAction();
            }
        }
        #endregion

        #region Public Method
        public void TryToExecute()
        {
            if (CanExecute(null))
            {
                Execute(null);
            }
        }
        #endregion
    }
}
