﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.Models.Commands
{
    public class RelayCommandParameter : CommandBase
    {
        #region Field
        protected Func<object, bool> canExecute;
        protected Action<object> execute;
        #endregion

        #region Constructor
        public RelayCommandParameter(Action<object> execute)
            : this(execute, null)
        {

        }

        public RelayCommandParameter(Action<object> execute, Func<object, bool> canExecute)
        {
            if (execute == null)
            {
                throw new Exception("Execute action has to be given");
            }
            this.execute = execute;
            this.canExecute = canExecute;
        }
        #endregion

        #region ICommand
        public override bool CanExecute(object parameter)
        {
            return (this.canExecute == null) ? true : this.canExecute(parameter);
        }

        public override void Execute(object parameter)
        {
            if (this.BeforeAction != null)
            {
                this.BeforeAction();
            }

            this.execute(parameter);
            this.IsManualExecuted = true;

            if (this.AfterAction != null)
            {
                this.AfterAction();
            }
        }
        #endregion

        #region Public Method
        public void TryToExecute(object parameter)
        {
            if (CanExecute(parameter))
            {
                Execute(parameter);
            }
        }
        #endregion
    }
}
