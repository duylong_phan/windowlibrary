﻿using LongInterface.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.Models.Commands
{
    public abstract class CommandBase : ICommandTarget
    {
        #region Event
        public virtual event EventHandler CanExecuteChanged;

        protected void OnCanExecuteChanged()
        {
            var handler = CanExecuteChanged;
            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }
        #endregion

        #region Getter, setter
        /// <summary>
        /// Get & Set if the Command is done  manual trigger or program trigger
        /// </summary>
        public bool IsManualExecuted { get; set; }
        /// <summary>
        ///  Get, set Action before command is done
        /// </summary>
        public Action BeforeAction { get; set; }
        /// <summary>
        /// Get, set Action after command is done
        /// </summary>
        public Action AfterAction { get; set; }
        #endregion

        #region Public Method
        public void RefreshStatus()
        {
            OnCanExecuteChanged();
        }
        #endregion

        #region Abstract Method        
        public abstract bool CanExecute(object parameter);

        public abstract void Execute(object parameter);
        #endregion
    }
}
