﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LongInterface.Models;
using LongModel.BaseClasses;

namespace LongModel.Models
{
    /// <summary>
    /// Base Class for Model class
    /// </summary>
    public abstract class Model : ObservableObject, IModel
    {
        #region Event
        public event UpdateRequestedEventHandler UpdateRequested;

        /// <summary>
        /// Notify when a UpdateRequest is called
        /// </summary>
        /// <param name="option"></param>
        protected void OnUpdateRequested(object option)
        {
            UpdateRequestedEventHandler handler = UpdateRequested;
            if (handler != null)
            {
                handler(this, new UpdateRequestedEventArgs(option));
            }
        } 
        #endregion

        #region Virtual Method
        public virtual void Load(string text)
        {

        }

        public virtual string Save()
        {
            return string.Empty;
        } 
        #endregion
    }
}
