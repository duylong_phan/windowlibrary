﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using LongInterface.Models.IO.Files;

namespace LongModel.Models.IO.Files
{
    public class FormatInfo : IFormatInfo
    {
        #region Getter, Setter
        [XmlAttribute("Type")]
        public string Type { get; set; }
        [XmlAttribute("Version")]
        public string Version { get; set; }
        [XmlAttribute("Author")]
        public string Author { get; set; }
        [XmlAttribute("Software")]
        public string Software { get; set; }
        #endregion

        #region Constructor
        public FormatInfo()
            : this(string.Empty, string.Empty, string.Empty, string.Empty)
        {

        }

        public FormatInfo(string type, string version, string author, string software)
        {
            this.Type = type;
            this.Version = version;
            this.Author = author;
            this.Software = software;
        } 
        #endregion
    }
}
