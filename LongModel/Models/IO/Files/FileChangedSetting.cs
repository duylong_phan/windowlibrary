﻿using LongInterface.Models;
using LongInterface.Models.IO.Files;
using LongModel.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Xml.Serialization;

namespace LongModel.Models.IO.Files
{
    public class FileChangedSetting : ActionSetting, IFileChangedSetting, IFilePath
    {
        #region Getter, Setter
        private string filePath;
        private string xmlTag;
        private int length;
        private FileChangedResponses response;

        [XmlAttribute("FilePath")]
        public string FilePath
        {
            get { return this.filePath; }
            set
            {
                this.SetProperty(ref this.filePath, value);
            }
        }
        [XmlAttribute("Response")]
        public FileChangedResponses Response
        {
            get { return this.response; }
            set
            {
                this.SetProperty(ref this.response, value);
            }
        }
        [XmlAttribute("XmlTag")]
        public string XmlTag
        {
            get { return this.xmlTag; }
            set
            {
                this.SetProperty(ref this.xmlTag, value);
            }
        }
        [XmlAttribute("Length")]
        public int Length
        {
            get { return this.length; }
            set
            {
                this.SetProperty(ref this.length, value);
            }
        }

        [XmlIgnore]
        public object ResponseCollection { get; set; }
        [XmlIgnore]
        public object LengthRule { get; set; }
        [XmlIgnore]
        public ICommand SelectFile { get; set; }
        [XmlIgnore]
        public object Tag { get; set; }
        #endregion

        #region Constructor
        public FileChangedSetting()
            : this(false, false, string.Empty, FileChangedResponses.NewLine, "Info", 0)
        {

        }

        public FileChangedSetting(bool isEnabled, bool captureOnState, string filePath, FileChangedResponses response, string xmlTag, int length)
            : base(isEnabled, captureOnState)
        {
            this.filePath = filePath;
            this.response = response;
            this.xmlTag = xmlTag;
            this.length = length;
        }
        #endregion
    }
}
