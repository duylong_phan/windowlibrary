﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using LongModel.BaseClasses;
using LongInterface.Models.IO.Files;
using LongInterface.Models;
using LongModel.Helpers;

namespace LongModel.Models.IO.Files
{
    public class FileSingleSetting : FileSettingBase, IFileSingleSetting, ICopyable<IFileSingleSetting>
    {
        #region Getter, setter
        private int index;
        private int sampleAmount;
        private bool useIndex;
        private string fileExtension;
        private string fileName;

        [XmlAttribute("Index")]
        public int Index
        {
            get { return this.index; }
            set
            {
                this.SetProperty(ref this.index, value);
            }
        }
        [XmlAttribute("SampleAmount")]
        public int SampleAmount
        {
            get { return this.sampleAmount; }
            set
            {
                this.SetProperty(ref this.sampleAmount, value);
            }
        }
        [XmlAttribute("UseIndex")]
        public bool UseIndex
        {
            get { return this.useIndex; }
            set
            {
                this.SetProperty(ref this.useIndex, value);
            }
        }
        [XmlAttribute("FileExtension")]
        public string FileExtension
        {
            get { return this.fileExtension; }
            set
            {
                this.SetProperty(ref this.fileExtension, value);
            }
        }
        [XmlAttribute("FileName")]
        public string FileName
        {
            get { return this.fileName; }
            set
            {
                this.SetProperty(ref this.fileName, value);
            }
        }

        [XmlIgnore]
        public object IndexRule { get; set; }
        [XmlIgnore]
        public object SampleAmountRule { get; set; }
        [XmlIgnore]
        public object FileNameRule { get; set; }        
        #endregion

        #region Constructor
        public FileSingleSetting()
            : this(string.Empty, string.Empty, string.Empty, 0, 0, false, 0, false, false, false)
        {

        }

        public FileSingleSetting(string directory, string fileName, string fileExtension, int sampleAmount, int index, bool useIndex, int duration, bool useDuration, bool isEnabled, bool captureOnStart)
            : base(directory, duration, useDuration, isEnabled, captureOnStart)
        {
            this.fileName = fileName;
            this.fileExtension = fileExtension;
            this.sampleAmount = sampleAmount;
            this.index = index;
            this.useIndex = useIndex;
        }
        #endregion

        #region Public Method
        public override object CloneModel()
        {
            var setting = new FileSingleSetting();
            setting.CopyProperties(this);
            return setting;
        }

        public void CopyProperties(IFileSingleSetting item)
        {
            if (item != null)
            {
                ModelHelper.CopyProperties(item, this);
            }
        }
        #endregion
    }
}
