﻿using LongInterface.Models;
using LongInterface.Models.IO.Files;
using LongModel.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using LongModel.BaseClasses;

namespace LongModel.Models.IO.Files
{
    public class ExtensionSetting : ObservableObject, IExtensionSetting, ICloneModel, ICopyable<IExtensionSetting>
    {
        #region Getter, setter
        private string description;
        private string executePath;
        private string fileExtension;
        private string iconPath;
        private string progID;

        [XmlAttribute("Description")]
        public string Description
        {
            get { return this.description; }
            set
            {
                this.SetProperty(ref this.description, value);
            }
        }
        [XmlAttribute("ExecutePath")]
        public string ExecutePath
        {
            get { return this.executePath; }
            set
            {
                this.SetProperty(ref this.executePath, value);
            }
        }
        [XmlAttribute("FileExtension")]
        public string FileExtension
        {
            get { return this.fileExtension; }
            set
            {
                this.SetProperty(ref this.fileExtension, value);
            }
        }
        [XmlAttribute("IconPath")]
        public string IconPath
        {
            get { return this.iconPath; }
            set
            {
                this.SetProperty(ref this.iconPath, value);
            }
        }
        [XmlAttribute("ProgID")]
        public string ProgID
        {
            get { return this.progID; }
            set
            {
                this.SetProperty(ref this.progID, value);
            }
        }

        [XmlIgnore]
        public object Tag { get; set; }
        #endregion


        #region Constructor
        public ExtensionSetting()
            : this(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty)
        {

        }

        public ExtensionSetting(string executePath, string fileExtension, string progID, string description, string iconPath)
        {
            this.ExecutePath = executePath;
            this.FileExtension = fileExtension;
            this.ProgID = progID;
            this.Description = description;
            this.IconPath = iconPath;
        }

        #endregion

        #region Interface
        public object CloneModel()
        {
            ExtensionSetting setting = new ExtensionSetting();
            setting.CopyProperties(this);
            return setting;
        }

        public void CopyProperties(IExtensionSetting item)
        {
            if (item != null)
            {
                ModelHelper.CopyProperties(item, this);
            }
        } 
        #endregion
    }
}
