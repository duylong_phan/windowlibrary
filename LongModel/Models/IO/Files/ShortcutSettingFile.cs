﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace LongModel.Models.IO.Files
{
    public class ShortcutSettingFile
    {
        #region Getter, setter
        [XmlElement("FormatInfo")]
        public FormatInfo FormatInfo { get; set; }
        [XmlArray("Settings"), XmlArrayItem("ShortcutSetting", typeof(ShortcutSetting))]
        public List<ShortcutSetting> Settings { get; set; }
        #endregion

        #region Constructor
        public ShortcutSettingFile()
        {
            this.FormatInfo = new FormatInfo();
            this.Settings = new List<ShortcutSetting>();
        } 
        #endregion
    }
}
