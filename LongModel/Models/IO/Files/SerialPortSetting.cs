﻿using LongInterface.Models.IO.Files;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using LongModel.BaseClasses;
using LongInterface.Models;
using System.Windows.Input;
using LongModel.Helpers;

namespace LongModel.Models.IO.Files
{
    public class SerialPortSetting : ObservableObject, ISerialPortSetting, ICloneModel, ICopyable<ISerialPortSetting>
    {
        #region Getter, Setter
        private string port;
        private int baudRate;
        private int dataBit;
        private int parityIndex;
        private int stopBitsIndex;
        private int thresholdAmount;
        private bool enableDTR;
        private bool enableRTS;
        private bool useThreshold;

        [XmlAttribute("Port")]
        public string Port
        {
            get { return this.port; }
            set
            {
                this.SetProperty(ref this.port, value);
            }
        }
        [XmlAttribute("BaudRate")]
        public int BaudRate
        {
            get { return this.baudRate; }
            set
            {
                this.SetProperty(ref this.baudRate, value);
            }
        }
        [XmlAttribute("DataBit")]
        public int DataBit
        {
            get { return this.dataBit; }
            set
            {
                this.SetProperty(ref this.dataBit, value);
            }
        }
        [XmlAttribute("ParityIndex")]
        public int ParityIndex
        {
            get { return this.parityIndex; }
            set
            {
                this.SetProperty(ref this.parityIndex, value);
            }
        }
        [XmlAttribute("StopBitsIndex")]
        public int StopBitsIndex
        {
            get { return this.stopBitsIndex; }
            set
            {
                this.SetProperty(ref this.stopBitsIndex, value);
            }
        }
        [XmlAttribute("ThresholdAmount")]
        public int ThresholdAmount
        {
            get { return this.thresholdAmount; }
            set
            {
                this.SetProperty(ref this.thresholdAmount, value);
            }
        }
        [XmlAttribute("UseThreshold")]
        public bool UseThreshold
        {
            get { return this.useThreshold; }
            set
            {
                this.SetProperty(ref this.useThreshold, value);
            }
        }
        [XmlAttribute("EnableDTR")]
        public bool EnableDTR
        {
            get { return this.enableDTR; }
            set
            {
                this.SetProperty(ref this.enableDTR, value);
            }
        }
        [XmlAttribute("EnableRTS")]
        public bool EnableRTS
        {
            get { return this.enableRTS; }
            set
            {
                this.SetProperty(ref this.enableRTS, value);
            }
        }

        [XmlIgnore]
        public object PortCollection { get; set; }
        [XmlIgnore]
        public object BaudRateCollection { get; set; }
        [XmlIgnore]
        public object DataBitCollection { get; set; }
        [XmlIgnore]
        public object ParityCollection { get; set; }
        [XmlIgnore]
        public object StopBitsCollection { get; set; }
        [XmlIgnore]
        public ICommand Refresh { get; set; }
        [XmlIgnore]
        public object ThresholdAmountRule { get; set; }
        #endregion

        #region Constructor
        public SerialPortSetting()
            : this("COM1", 9600, 8, 0, 0, 1, false, false, false)
        {

        }

        public SerialPortSetting(string port, int baudRate, int dataBit, int parityIndex, int stopBitsIndex, int thresholdAmount, bool useThreshold, bool enableDTR, bool enableRTS)
        {
            this.port = port;
            this.baudRate = baudRate;
            this.dataBit = dataBit;
            this.parityIndex = parityIndex;
            this.stopBitsIndex = stopBitsIndex;
            this.thresholdAmount = thresholdAmount;
            this.useThreshold = useThreshold;
            this.enableDTR = enableDTR;
            this.enableRTS = enableRTS;
        }
        #endregion

        #region Public Method
        public object CloneModel()
        {
            var setting = new SerialPortSetting();
            setting.CopyProperties(this);
            return setting;
        }

        public void CopyProperties(ISerialPortSetting item)
        {
            if (item != null)
            {
                ModelHelper.CopyProperties(item, this);
            }
        }
        #endregion
    }
}
