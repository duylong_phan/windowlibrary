﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace LongModel.Models.IO.Files
{
    public class ExtensionSettingFile
    {
        #region Getter, setter
        [XmlElement("FormatInfo")]
        public FormatInfo FormatInfo { get; set; }
        [XmlArray("Settings"), XmlArrayItem("ExtensionSetting", typeof(ExtensionSetting))]
        public List<ExtensionSetting> Settings { get; set; }
        #endregion

        #region Constructor
        public ExtensionSettingFile()
        {
            this.FormatInfo = new FormatInfo("Extention Setting", "1", "Long", string.Empty);
            this.Settings = new List<ExtensionSetting>();
        } 
        #endregion
    }
}
