﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using LongModel.BaseClasses;
using LongInterface.Models.IO.Files;

namespace LongModel.Models.IO.Files
{
    public class ShortcutSetting : ObservableObject, IShortcutSetting
    {
        #region Getter, Setter
        private string description;
        private string executePath;
        private string iconPath;
        private string name;
        private string placementDirectory;
        private string workingDirectory;

        [XmlAttribute("Description")]
        public string Description
        {
            get { return this.description; }
            set
            {
                this.SetProperty(ref this.description, value);
            }
        }
        [XmlAttribute("ExecutePath")]
        public string ExecutePath
        {
            get { return this.executePath; }
            set
            {
                this.SetProperty(ref this.executePath, value);
            }
        }
        [XmlAttribute("IconPath")]
        public string IconPath
        {
            get { return this.iconPath; }
            set
            {
                this.SetProperty(ref this.iconPath, value);
            }
        }
        [XmlAttribute("Name")]
        public string Name
        {
            get { return this.name; }
            set
            {
                this.SetProperty(ref this.name, value);
            }
        }
        [XmlAttribute("PlacementDirectory")]
        public string PlacementDirectory
        {
            get { return this.placementDirectory; }
            set
            {
                this.SetProperty(ref this.placementDirectory, value);
            }
        }
        [XmlAttribute("WorkingDirectory")]
        public string WorkingDirectory
        {
            get { return this.workingDirectory; }
            set
            {
                this.SetProperty(ref this.workingDirectory, value);
            }
        } 
        #endregion

        #region Constructor
        public ShortcutSetting()
            : this(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty)
        {

        }

        public ShortcutSetting(string name, string description, string executePath, string iconPath, string placementDirectory, string workingDirectory)
        {
            this.name = name;
            this.description = description;
            this.executePath = executePath;
            this.iconPath = iconPath;
            this.placementDirectory = placementDirectory;
            this.workingDirectory = workingDirectory;
        } 
        #endregion
    }
}
