﻿using LongInterface.Models;
using LongInterface.Models.IO.Files;
using LongModel.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Xml.Serialization;

namespace LongModel.Models.IO.Files
{
    public interface ICameraFileSetting : IFileSingleSetting
    {
        int CameraIndex { get; set; }
        int ResolutionIndex { get; set; }
    }

    public class CameraFileSetting : FileSingleSetting, ICameraFileSetting, ICopyable<ICameraFileSetting>
    {
        #region Getter, setter
        private int cameraIndex;
        private int resolutionIndex;

        [XmlAttribute("CameraIndex")]
        public int CameraIndex
        {
            get { return this.cameraIndex; }
            set
            {
                this.SetProperty(ref this.cameraIndex, value);
            }
        }
        [XmlAttribute("ResolutionIndex")]
        public int ResolutionIndex
        {
            get { return this.resolutionIndex; }
            set
            {
                this.SetProperty(ref this.resolutionIndex, value);
            }
        }

        [XmlIgnore]
        public object CameraCollection { get; set; }
        [XmlIgnore]
        public ICommand Refresh { get; set; }
        #endregion

        #region Constructor
        public CameraFileSetting()
            : this(0, 0, string.Empty, string.Empty, string.Empty, 0, 0, false, 0, false, false, false)
        {

        }

        public CameraFileSetting(int cameraIndex, int resolutionIndex, 
                                 string directory, string fileName, string fileExtension, int sampleAmount, int index, bool useIndex, int duration, bool useDuration, bool isEnabled, bool captureOnStart)
            : base(directory, fileName, fileExtension, sampleAmount, index, useIndex, duration, useDuration, isEnabled, captureOnStart)
        {
            this.cameraIndex = cameraIndex;
            this.resolutionIndex = resolutionIndex;
        }
        #endregion

        #region Public Method

        public override object CloneModel()
        {
            var setting = new CameraFileSetting();
            setting.CopyProperties(this);
            return setting;
        }

        public void CopyProperties(ICameraFileSetting item)
        {
            if (item != null)
            {
                ModelHelper.CopyProperties(item, this);
            }
        } 
        #endregion
    }
}
