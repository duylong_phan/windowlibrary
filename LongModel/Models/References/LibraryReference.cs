﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.Models.References
{
    public enum LibrarySources
    {
        csmatio,
        GifWPF,
        LinqToDB,
        NCalc,
        Ookii,
        OxyPlot,
        SpreadsheetLight
    }
    
    /// <summary>
    /// Class contains Info of Library for Reference
    /// </summary>
    public class LibraryReference : ApplicationReference
    {
        #region Static Method
        /// <summary>
        /// Get a Library Reference from default Source
        /// </summary>
        /// <param name="sourcce">Source Type</param>
        /// <returns></returns>
        public static LibraryReference GetReference(LibrarySources sourcce)
        {
            LibraryReference reference = null;

            switch (sourcce)
            {
                case LibrarySources.csmatio:
                    reference = new LibraryReference("CSMatIO", "Copyright (c) 2007-2009, David Zier" + Environment.NewLine +
                                                                "All rights reserved.");
                    break;
                case LibrarySources.GifWPF:
                    reference = new LibraryReference("WPF Animated GIF", "Apache License" + Environment.NewLine +
                                                                         "Version 2.0, January 2004" + Environment.NewLine +
                                                                         "http://www.apache.org/licenses/");
                    break;
                case LibrarySources.LinqToDB:
                    reference = new LibraryReference("LINQ to DB", "Copyright (c) 2014, Igor Tkachev" + Environment.NewLine +
                                                                   "it@bltoolkit.net");
                    break;
                case LibrarySources.NCalc:
                    reference = new LibraryReference("NCalc", "Copyright (c) 2011 Sebastien Ros" + Environment.NewLine +
                                                              "https://ncalc.codeplex.com");
                    break;
                case LibrarySources.Ookii:
                    reference = new LibraryReference("ookii", "Copyright (c) 2015, Sven Groot" + Environment.NewLine +
                                                              "http://www.ookii.org/");
                    break;

                case LibrarySources.OxyPlot:
                    reference = new LibraryReference("OxyPlot", "Copyright (c) 2014 OxyPlot contributors" + Environment.NewLine +
                                                                "http://docs.oxyplot.org/");
                    break;

                case LibrarySources.SpreadsheetLight:
                    reference = new LibraryReference("SpreadsheetLight", "Copyright (c) 2011 Vincent Tan Wai Lip" + Environment.NewLine +
                                                                         "http://spreadsheetlight.com/");
                    break;

                default:
                    reference = new LibraryReference("Unknown", "Unknown");
                    break;
            }

            return reference;
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Create new Instance of LibraryReference
        /// </summary>
        /// <param name="name">Application Name</param>
        /// <param name="copyRight">Application Copyright</param>
        public LibraryReference(string name, string copyRight)
            : this(name, copyRight, string.Empty)
        {

        }

        /// <summary>
        /// Create new Instance of LibraryReference
        /// </summary>
        /// <param name="name">Application Name</param>
        /// <param name="copyRight">Application Copyright</param>
        /// <param name="notification">Application Notification</param>
        public LibraryReference(string name, string copyRight, string notification)
            : base(ApplicationTypes.Library, name, copyRight, notification)
        {

        }
        #endregion
    }
}
