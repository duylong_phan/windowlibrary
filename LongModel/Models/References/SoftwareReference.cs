﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.Models.References
{
    public enum SoftwareSources
    {
        VisualStudio,
        SandCastle,
        InstallCreator
    }

    /// <summary>
    /// Class contains Info of Software for Reference
    /// </summary>
    public class SoftwareReference : ApplicationReference
    {
        #region Static
        /// <summary>
        /// Get Software Reference from given Software source
        /// </summary>
        /// <param name="source">Source Type</param>
        /// <returns></returns>
        public static SoftwareReference GetReference(SoftwareSources source)
        {
            SoftwareReference reference = null;

            switch (source)
            {
                case SoftwareSources.VisualStudio:
                    reference = new SoftwareReference("Microsoft Visual Studio 2013", "(C) 2013 Microsoft Coorporation" + Environment.NewLine +
                                                                                      "All rights reserved.");
                    break;
                case SoftwareSources.SandCastle:
                    reference = new SoftwareReference("SandCastle", "Microsoft Public License (Ms-PL)");
                    break;

                case SoftwareSources.InstallCreator:
                    reference = new SoftwareReference("Install Creator", "Clickteam Install Creator" + Environment.NewLine +
                                                                        "Copyright (C) 1999-2013 Clickteam" + Environment.NewLine +
                                                                        "All Rights Reserved");
                    break;
                default:
                    reference = new SoftwareReference("Unkwown", "Unknown");
                    break;
            }

            return reference;
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Create new Instance of SoftwareReference
        /// </summary>
        /// <param name="name">Application Name</param>
        /// <param name="copyRight">Application Copyright</param>
        public SoftwareReference(string name, string copyRight)
            : this(name, copyRight, string.Empty)
        {

        }

        /// <summary>
        /// Create new Instance of SoftwareReference
        /// </summary>
        /// <param name="name">Application Name</param>
        /// <param name="copyRight">Application Copyright</param>
        /// <param name="notification">Application Notification</param>
        public SoftwareReference(string name, string copyRight, string notification)
            : base(ApplicationTypes.Software, name, copyRight, notification)
        {

        }
        #endregion
    }
}
