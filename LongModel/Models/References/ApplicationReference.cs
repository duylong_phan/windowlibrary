﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.Models.References
{
    public enum ApplicationTypes
    {
        Software,
        Library
    }

    /// <summary>
    /// Base class for Application Reference
    /// </summary>
    public class ApplicationReference : ReferenceBase
    {
        #region Getter & Setter
        /// <summary>
        /// Get Application Type
        /// </summary>
        public ApplicationTypes Type { get; protected set; }

        /// <summary>
        /// Get Application Copyright
        /// </summary>
        public string CopyRight { get; protected set; }

        /// <summary>
        /// Get Application Notification
        /// </summary>
        public string Notification { get; protected set; } 
        #endregion

        #region Constructor
        /// <summary>
        /// Initialize a new instance of Reference
        /// </summary>
        /// <param name="type">initial type</param>
        /// <param name="name">initial name</param>
        /// <param name="copyRight">initial copyRight</param>
        /// <param name="notification">initial notification</param>
        public ApplicationReference(ApplicationTypes type, string name, string copyRight, string notification)
            : base(name)
        {
            this.Type = type;
            this.CopyRight = copyRight;
            this.Notification = notification;
        } 
        #endregion
    }
}
