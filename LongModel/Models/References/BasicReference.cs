﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.Models.References
{
    public class BasicReference : ReferenceBase
    {
        #region Getter
        public string Info { get; private set; }
        public string Image { get; private set; }
        #endregion

        #region Constructor
        public BasicReference(string name, string info, string image)
            : base(name)
        {
            this.Info = info;
            this.Image = image;
        }
        #endregion
    }
}
