﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.Models.References
{
    public enum PersonJobs
    {
        Designer,
        Documentation,
        Programmer,
        Suppervisor,
    }

    /// <summary>
    /// Class for containing Information for Person
    /// </summary>
    public class PersonReference : ReferenceBase
    {
        #region Getter
        /// <summary>
        /// Get Job Position
        /// </summary>
        public PersonJobs Job { get; protected set; }
        /// <summary>
        /// Get Contact
        /// </summary>
        public string Contact { get; protected set; } 
        #endregion

        #region Constructor
        /// <summary>
        /// Initialize a new instance of PersonReference
        /// </summary>
        /// <param name="name">initial name</param>
        /// <param name="job">initial Job</param>
        /// <param name="contact">initial contact</param>
        public PersonReference(string name, PersonJobs job, string contact)
            : base(name)
        {
            this.Job = job;
            this.Contact = contact;
        } 
        #endregion
    }
}
