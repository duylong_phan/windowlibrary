﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LongInterface.Models;
using LongModel.BaseClasses;

namespace LongModel.Models.References
{
    public abstract class ReferenceBase
    {
        #region Getter & Setter
        /// <summary>
        /// Get Name of the Reference instance
        /// </summary>
        public string Name { get; protected set; }
        #endregion

        #region Constructor
        public ReferenceBase(string name)
        {
            this.Name = name;          
        } 
        #endregion
    }
}
