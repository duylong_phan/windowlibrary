﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.Collections
{
    public class ManualObservableCollection<T> : ObservableCollection<T>
    {
        #region Public Method
        /// <summary>
        /// Add range of items to the end of the collection
        /// </summary>
        /// <param name="collection"></param>
        public void AddRange(IEnumerable collection)
        {
            foreach (var item in collection)
            {
                if (item is T)
                {
                    Items.Add((T)item);
                }
            }
        }

        /// <summary>
        /// Remove items from the given start position
        /// </summary>
        /// <param name="start"></param>
        /// <param name="count"></param>
        public void RemoveRange(int start, int count)
        {
            for (int i = 0; i < count; i++)
            {
                Items.RemoveAt(start);
            }
        }

        /// <summary>
        /// Explicit request update
        /// </summary>
        public void RequestUpdateEvent()
        {
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        } 
        #endregion
    }
}
