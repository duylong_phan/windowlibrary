﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.Collections
{
    public class FixConcurrentQueue<T>
    {
        #region Getter
        public int Capacity { get; private set; }
        public int Count
        {
            get
            {
                return this.innerQueue.Count;
            }
        }
        #endregion

        #region Field
        private ConcurrentQueue<T> innerQueue;
        #endregion

        #region Constructor
        public FixConcurrentQueue(int capacity)
        {
            this.Capacity = capacity;
            this.innerQueue = new ConcurrentQueue<T>();
        }
        #endregion

        #region Public Method
        public void Enqueue(T item)
        {
            if (this.innerQueue.Count >= this.Capacity)
            {
                Dequeue();
            }  
            this.innerQueue.Enqueue(item);
        }

        public T Dequeue()
        {
            T item = default(T);

            this.innerQueue.TryDequeue(out item);

            return item;
        }

        public void Clear()
        {
            while (this.Count > 0)
            {
                Dequeue();
            }
        }
        #endregion
    }
}
