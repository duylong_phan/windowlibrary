﻿using LongModel.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.Collections.MultiChoice
{
    public abstract class MultiChoiceBase<T> : ObservableObject
    {
        #region Getter
        public IEnumerable<T> FullCollection { get; private set; }
        public T NonItem { get; private set; }
        #endregion

        #region Constructor
        public MultiChoiceBase(IEnumerable<T> fullCollection, T nonItem)
        {
            this.FullCollection = fullCollection;
            this.NonItem = nonItem;
        }
        #endregion

        #region Protected Methods
        protected virtual void CompareRemoveItem(List<T> collection, T item)
        {
            if (!object.Equals(item, this.NonItem)) //NOT
            {
                collection.Remove(item);
            }
        }

        protected virtual void SortCollection(List<T> collection)
        {

        }
        #endregion
    }
}
