﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.Collections.MultiChoice
{
    public class MultiChoice3Option<T> : MultiChoice2Option<T>
    {
        #region Getter, setter
        private T selectedItem3;

        public T SelectedItem3
        {
            get { return this.selectedItem3; }
            set
            {
                bool isChanged = this.SetProperty(ref this.selectedItem3, value);
                if (isChanged)
                {
                    this.OnItem3Changed();
                }
            }
        }
        #endregion

        #region Getter
        public List<T> Collection3
        {
            get
            {
                if (tmpCollection3 != null)
                {
                    tmpCollection3.Clear();
                }
                tmpCollection3 = new List<T>();
                tmpCollection3.AddRange(this.FullCollection);

                OnCollection3Requested(tmpCollection3);
                return tmpCollection3;
            }
        }
        #endregion

        private List<T> tmpCollection3;

        #region Constructor
        public MultiChoice3Option(List<T> fullCollection, T nonItem)
            : base(fullCollection, nonItem)
        {
            this.selectedItem3 = nonItem;
        }
        #endregion

        #region Override
        protected override void OnItem1Changed()
        {
            base.OnItem1Changed();
            this.OnPropertyChanged("Collection3");
        }

        protected override void OnItem2Changed()
        {
            base.OnItem2Changed();
            this.OnPropertyChanged("Collection3");
        }

        protected override void OnCollection1Requested(List<T> collection)
        {
            base.OnCollection1Requested(collection);
            CompareRemoveItem(collection, this.selectedItem3);
        }

        protected override void OnCollection2Requested(List<T> collection)
        {
            base.OnCollection2Requested(collection);
            CompareRemoveItem(collection, this.selectedItem3);
        }
        #endregion

        #region Protected Methods
        protected virtual void OnItem3Changed()
        {
            //request update
            this.OnPropertyChanged("Collection1");
            this.OnPropertyChanged("Collection2");
        }

        protected virtual void OnCollection3Requested(List<T> collection)
        {
            //sort and remove selected value
            SortCollection(collection);
            CompareRemoveItem(collection, this.SelectedItem1);
            CompareRemoveItem(collection, this.SelectedItem2);
        }
        #endregion
    }
}
