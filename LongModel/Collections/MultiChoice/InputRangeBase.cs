﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.Collections.MultiChoice
{
    public class InputRangeBase<T> : MultiChoiceBase<T>
    {
        #region Getter, Setter
        private T selectedMin;
        private T selectedMax;

        public T SelectedMin
        {
            get { return this.selectedMin; }
            set
            {
                bool isChanged = this.SetProperty(ref this.selectedMin, value);
                if (isChanged)
                {
                    OnMinChanged();
                }
            }
        }
        public T SelectedMax
        {
            get { return this.selectedMax; }
            set
            {
                bool isChanged = this.SetProperty(ref this.selectedMax, value);
                if (isChanged)
                {
                    OnMaxChanged();
                }
            }
        }
        #endregion

        #region Getter
        public List<T> MinCollection
        {
            get
            {
                tmpMinCollection.Clear();
                tmpMinCollection.AddRange(this.FullCollection);
                OnMinCollectionRequested(tmpMinCollection);
                return tmpMinCollection;
            }
        }
        public List<T> MaxCollection
        {
            get
            {
                tmpMaxCollection.Clear();
                tmpMaxCollection.AddRange(this.FullCollection);
                OnMaxCollectionRequested(tmpMaxCollection);
                return tmpMaxCollection;
            }
        }
        #endregion

        #region Field
        private List<T> tmpMinCollection;
        private List<T> tmpMaxCollection; 
        #endregion

        #region Constructor
        public InputRangeBase(IEnumerable<T> fullCollection, T nonItem)
           : base(fullCollection, nonItem)
        {
            this.selectedMin = this.selectedMax = nonItem;
            this.tmpMinCollection = new List<T>();
            this.tmpMaxCollection = new List<T>();
        }
        #endregion

        #region Protected Method
        protected virtual void OnMinChanged()
        {
            this.OnPropertyChanged("MaxCollection");
        }

        protected virtual void OnMaxChanged()
        {
            this.OnPropertyChanged("MinCollection");
        }

        protected virtual void OnMinCollectionRequested(List<T> collection)
        {
            SortCollection(collection);
            if (object.Equals(this.selectedMax, this.NonItem))
            {
                return;
            }

            int index = collection.IndexOf(this.selectedMax);
            int count = collection.Count - index;
            if (index >= 0 && count > 0)
            {
                collection.RemoveRange(index, count);
            }
        }

        protected virtual void OnMaxCollectionRequested(List<T> collection)
        {
            SortCollection(collection);
            if (object.Equals(this.selectedMin, this.NonItem))
            {
                return;
            }

            int index = collection.IndexOf(this.selectedMin);
            if (index >= 0)
            {
                collection.RemoveRange(1, index);
            }            
        } 
        #endregion
    }
}
