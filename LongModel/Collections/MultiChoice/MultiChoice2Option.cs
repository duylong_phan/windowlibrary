﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.Collections.MultiChoice
{
    public class MultiChoice2Option<T> : MultiChoiceBase<T>
    {
        #region Getter, setter
        private T selectedItem1;
        private T selectedItem2;

        public T SelectedItem1
        {
            get { return this.selectedItem1; }
            set
            {
                bool isChanged = this.SetProperty(ref this.selectedItem1, value);
                if (isChanged)
                {
                    OnItem1Changed();
                }
            }
        }
        public T SelectedItem2
        {
            get { return this.selectedItem2; }
            set
            {
                bool isChanged = this.SetProperty(ref this.selectedItem2, value);
                if (isChanged)
                {
                    OnItem2Changed();
                }
            }
        }
        #endregion

        #region Getter
        public List<T> Collection1
        {
            get
            {
                if (tmpCollection1 != null)
                {
                    tmpCollection1.Clear();
                }
                tmpCollection1 = new List<T>();
                tmpCollection1.AddRange(this.FullCollection);

                OnCollection1Requested(tmpCollection1);
                return tmpCollection1;
            }
        }

        public List<T> Collection2
        {
            get
            {
                if (tmpCollection2 != null)
                {
                    tmpCollection2.Clear();
                }
                tmpCollection2 = new List<T>();
                tmpCollection2.AddRange(this.FullCollection);

                OnCollection2Requested(tmpCollection2);
                return tmpCollection2;
            }
        }
        #endregion

        #region Field
        private List<T> tmpCollection1;
        private List<T> tmpCollection2; 
        #endregion

        #region Constructor
        public MultiChoice2Option(IEnumerable<T> fullCollection, T nonItem)
            : base(fullCollection, nonItem)
        {
            this.selectedItem1 = this.selectedItem2 = nonItem;
        }
        #endregion

        #region Protected Methods
        protected virtual void OnItem1Changed()
        {
            //request update
            this.OnPropertyChanged("Collection2");
        }

        protected virtual void OnItem2Changed()
        {
            //request update
            this.OnPropertyChanged("Collection1");
        }

        protected virtual void OnCollection1Requested(List<T> collection)
        {
            //sort and remove selected value
            SortCollection(collection);
            CompareRemoveItem(collection, this.selectedItem2);
        }

        protected virtual void OnCollection2Requested(List<T> collection)
        {
            //sort and remove selected value
            SortCollection(collection);
            CompareRemoveItem(collection, this.SelectedItem1);
        }
        #endregion
    }
}
