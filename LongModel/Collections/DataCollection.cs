﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using LongInterface.Models;

namespace LongModel.Collections
{
    public class DataCollection<T> : ObservableCollection<T>, IDataChanged
    {
        #region Protected Properties
        /// <summary>
        /// Get, Set the action, when change happened
        /// </summary>
        protected Action EventAction { get; set; }
        #endregion

        #region Constructor, Deconstructor
        public DataCollection()
        {
            this.HasSubscription = false;
        }

        ~DataCollection()
        {
            //avoid memory leak
            UnsubscribeChanged();
        } 
        #endregion

        #region IDataChanged
        /// <summary>
        /// Get if current instance has been subscribed for changed Event
        /// </summary>
        public bool HasSubscription { get; private set; }

        /// <summary>
        /// Subscribe for the inner property notification, only when HasSubscription is false
        /// </summary>
        /// <param name="eventAction">given Action</param>
        public void SubscribeChanged(Action eventAction)
        {
            if (this.HasSubscription)
            {
                return;
            }
            
            this.HasSubscription = true;
            this.EventAction = eventAction;
            this.CollectionChanged += DataCollection_CollectionChanged;

            foreach (var item in this)
            {
                IDataChanged dataItem = item as IDataChanged;
                if (dataItem != null)
                {
                    dataItem.SubscribeChanged(this.EventAction);
                }
            }
        }

        /// <summary>
        /// Unsubscribe for inner notifiication, only when HasSubscription is true
        /// </summary>
        public void UnsubscribeChanged()
        {
            if (this.HasSubscription)
            {
                this.HasSubscription = false;
                this.EventAction = null;
                this.CollectionChanged -= DataCollection_CollectionChanged;

                foreach (var item in this)
                {
                    IDataChanged dataItem = item as IDataChanged;
                    if (dataItem != null)
                    {
                        dataItem.UnsubscribeChanged();
                    }
                }
            }
        }
        #endregion

        #region Event Handler
        private void DataCollection_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (this.EventAction != null)
            {
                this.EventAction();
            }

            if (e.OldItems != null)
            {
                //unsubscribe removed Items
                foreach (var item in e.OldItems)
                {
                    IDataChanged dataItem = item as IDataChanged;
                    if (dataItem != null)
                    {
                        dataItem.UnsubscribeChanged();
                    }
                }
            }
        } 
        #endregion
    }
}
