﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.Views.Infos
{
    public class ColumnInfo
    {
        #region Getter
        public string Text { get; private set; }
        public int Index { get; private set; }
        public string Description { get; private set; }
        public string Unit { get; private set; }
        #endregion

        #region Constructor
        public ColumnInfo(string text, int index, string description, string unit)
        {
            this.Text = text;
            this.Index = index;
            this.Description = description;
            this.Unit = unit;
        }
        #endregion
    }
}
