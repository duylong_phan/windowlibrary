﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.Views.Infos.HelpInfos
{
    public class SectionHelpInfo : HeaderInfo
    {
        #region Getter       
        public List<HeaderInfo> ContentInfos { get; private set; }
        #endregion

        #region Constructor
        public SectionHelpInfo(object header)
            : base(header)
        {
            this.ContentInfos = new List<HeaderInfo>();
        }
        #endregion
    }
}
