﻿using LongInterface.Models;
using LongInterface.Views.Infos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.Views.Infos.HelpInfos
{
    public class BulletTextContentHelpInfo : HeaderInfo, IGetBullet
    {
        #region Getter
        public BulletTypes Bullet { get; private set; }        
        public List<string> Notes { get; private set; }
        #endregion

        #region Constructor
        public BulletTextContentHelpInfo(object header, BulletTypes bullet)
            : base(header)
        {
            this.Bullet = bullet;
            this.Notes = new List<string>();
        }
        #endregion
    }
}
