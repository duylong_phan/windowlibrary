﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.Views.Infos.HelpInfos
{
    public class TaskHelpInfo : HeaderInfo
    {
        #region Getter
        public List<SectionHelpInfo> SectionInfos { get; private set; }
        #endregion

        #region Constructor
        public TaskHelpInfo()
            : this(string.Empty)
        {

        }

        public TaskHelpInfo(object header)
            : base(header)
        {
            this.SectionInfos = new List<SectionHelpInfo>();
        }
        #endregion
    }
}
