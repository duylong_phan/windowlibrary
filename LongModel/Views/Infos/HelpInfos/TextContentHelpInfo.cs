﻿using LongInterface.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.Views.Infos.HelpInfos
{
    public class TextContentHelpInfo : HeaderInfo, IGetText
    {
        #region Getter
        public string Text { get; private set; }
        #endregion

        #region Constructor
        public TextContentHelpInfo(object header, string text)
            : base(header)
        {
            this.Text = text;
        }
        #endregion
    }
}
