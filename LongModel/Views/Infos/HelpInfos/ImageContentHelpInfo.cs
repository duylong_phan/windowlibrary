﻿using LongInterface.Views.Infos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.Views.Infos.HelpInfos
{
    public class ImageContentHelpInfo : BulletTextContentHelpInfo
    {
        #region Getter
        public string Image { get; private set; }
        public List<TextIndexInfo> IndexInfos { get; private set; }        
        #endregion

        #region Constructor
        public ImageContentHelpInfo(object header, string image)
            : base(header, BulletTypes.Circle)
        {
            this.Image = image;
            this.IndexInfos = new List<TextIndexInfo>();            
        }
        #endregion
    }
}
