﻿using LongInterface.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.Views.Infos
{
    public class HeaderInfo : IGetHeader
    {
        #region Getter
        public object Header { get; private set; }
        #endregion

        #region Constructor
        public HeaderInfo(object header)
        {
            this.Header = header;
        }
        #endregion
    }
}
