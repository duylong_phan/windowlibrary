﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.Views.Infos
{
    public class RowInfo : ColumnInfo
    {
        public RowInfo(string text, int index, string description, string unit)
            : base(text, index, description, unit)
        {

        }
    }
}
