﻿using LongInterface.Views.Infos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.Views.Infos
{
    public class SectionInfo : ISectionInfo
    {
        #region Getter        
        public string Name { get; private set; }
        public List<object> Collection { get; private set; }
        #endregion

        #region Constructor
        public SectionInfo(string name)
        {
            this.Name = name;
            this.Collection = new List<object>();
        }
        #endregion
    }
}
