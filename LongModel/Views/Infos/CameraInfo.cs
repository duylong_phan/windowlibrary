﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using LongInterface.Views.Infos;

namespace LongModel.Views.Infos
{
    public class CameraInfo : ICameraInfo
    {
        public int Index { get; private set; }
        public string Name { get; private set; }
        public IList ResolutionCollection { get; private set; }

        public CameraInfo(int index, string name, IList resolutionCollection)
        {
            this.Index = index;
            this.Name = name;
            this.ResolutionCollection = resolutionCollection;
        }
    }
}
