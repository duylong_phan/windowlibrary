﻿using LongInterface.Views.Infos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.Views.Infos
{
    public class ResolutionInfo : IResolutionInfo
    {
        #region Getter
        public int Index { get; private set; }
        public int Width { get; private set; }
        public int Height { get; private set; }
        public int FrameRate { get; private set; }
        #endregion

        #region Constructor
        public ResolutionInfo(int index, int width, int height, int frameRate)
        {
            this.Index = index;
            this.Width = width;
            this.Height = height;
            this.FrameRate = frameRate;
        }
        #endregion
    }
}
