﻿using LongInterface.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.Views.Infos
{
    public class ContentInfo : IGetContent
    {
        #region Getter
        public object Content { get; private set; }
        #endregion

        #region Constructor
        public ContentInfo(object content)
        {
            this.Content = content;
        }
        #endregion
    }
}
