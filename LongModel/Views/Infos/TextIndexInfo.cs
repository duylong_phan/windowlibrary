﻿using LongInterface.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.Views.Infos
{
    public class TextIndexInfo : IGetText, IGetIndex
    {
        #region Getter
        public int Index { get; private set; }
        public string Text { get; private set; }
        #endregion
        
        #region Constructor
        public TextIndexInfo(int index, string text)
        {
            this.Index = index;
            this.Text = text;
        }
        #endregion
    }
}
