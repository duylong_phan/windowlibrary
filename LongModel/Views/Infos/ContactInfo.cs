﻿using LongInterface.Models.DataModels;
using LongInterface.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.Views.Infos
{
    public class ContactInfo : IGetContactInfo
    {
        #region Getter
        /// <summary>
        /// Get Name
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// Get Email
        /// </summary>
        public string Email { get; private set; }
        /// <summary>
        /// Get Phone
        /// </summary>
        public string Phone { get; private set; }
        /// <summary>
        /// Get Group
        /// </summary>
        public object Group { get; private set; } 
        #endregion

        #region Constructor
        public ContactInfo(string name, string email, string phone)
           : this(name, email, phone, string.Empty)
        {

        }

        public ContactInfo(string name, string email, string phone, object group)
        {
            this.Name = name;
            this.Email = email;
            this.Phone = phone;
            this.Group = group;
        } 
        #endregion
    }
}
