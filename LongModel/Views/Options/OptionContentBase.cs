﻿using LongInterface.Views.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.Views.Options
{
    public abstract class OptionContentBase<T> : OptionBase<T>, IOptionContent<T>
    {
        #region Getter
        private object content;

        public object Content
        {
            get { return this.content; }
            set
            {
                this.SetProperty(ref this.content, value);
            }
        }

        #endregion

        #region Constructor
        public OptionContentBase(string text, T type)
            : this(text, type, null)
        {

        }

        public OptionContentBase(string text, T type, object content)
            : base(text, type)
        {
            this.Content = content;
        } 
        #endregion
    }
}
