﻿using LongInterface.Views.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LongModel.BaseClasses;
namespace LongModel.Views.Options
{
    public abstract class OptionBase<T> : ObservableObject, IOption<T>
    {
        #region Getter
        public string Text { get; private set; }
        public T Type { get; private set; }
        #endregion

        #region Constructor
        public OptionBase(string text, T type)
        {
            this.Text = text;
            this.Type = type;
        } 
        #endregion
    }
}
