﻿using LongInterface.Models.DataModels;
using LongInterface.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.Views.Options
{
    public class LanguageOption : IGetLanguageOption
    {
        #region Getter
        /// <summary>
        /// Get Name
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// Get Image
        /// </summary>
        public string Image { get; private set; }
        /// <summary>
        /// Get Language code
        /// </summary>
        public string Code { get; private set; }
        /// <summary>
        /// Get Tooltip
        /// </summary>
        public object ToolTip { get; private set; } 
        #endregion

        #region Constructor
        public LanguageOption(string name, string code)
            : this(name, code, string.Empty, string.Empty)
        {

        }

        public LanguageOption(string name, string code, string image)
            : this(name, code, image, string.Empty)
        {

        }

        public LanguageOption(string name, string code, string image, object toolTip)
        {
            this.Name = name;
            this.Code = code;
            this.Image = image;
            this.ToolTip = toolTip;
        } 
        #endregion
    }
}
