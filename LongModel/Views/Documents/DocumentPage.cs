﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LongInterface.Views.Documents;

namespace LongModel.Views.Documents
{
    public class DocumentPage : IDocumentPage
    {
        #region Getter
        /// <summary>
        /// Get Header
        /// </summary>
        public IDocumentItem Header { get; private set; }
        /// <summary>
        /// Get Footer
        /// </summary>
        public IDocumentItemFooter Footer { get; private set; }
        /// <summary>
        /// Get Content 
        /// </summary>
        public IList Content { get; private set; } 
        #endregion

        #region Constructor
        /// <summary>
        /// Initialize a new instance of DocumentPage
        /// </summary>
        /// <param name="header">given header</param>
        /// <param name="footer">given footer</param>
        /// <param name="content">given content</param>
        public DocumentPage(IDocumentItem header, IDocumentItemFooter footer, IList content)
        {
            this.Header = header;
            this.Footer = footer;
            this.Content = content;
        } 
        #endregion
    }
}
