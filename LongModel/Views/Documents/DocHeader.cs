﻿using LongModel.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace LongModel.Views.Documents
{
    public class DocHeader : ObservableObject
    {
        #region Getter, Setter
        private string text;
        private string image;

        public string Text
        {
            get { return this.text; }
            set
            {
                this.SetProperty(ref this.text, value);
            }
        }
        public string Image
        {
            get { return this.image; }
            set
            {
                this.SetProperty(ref this.image, value);
            }
        }
        public ICommand SelectImage { get; set; }
        #endregion

        #region Constructor
        public DocHeader(string text, string image)
        {
            this.text = text;
            this.image = image;
        }
        #endregion
    }
}
