﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LongInterface.Views.Documents;

namespace LongModel.Views.Documents
{
    public class DocumentItem : IDocumentItem
    {
        #region Getter
        /// <summary>
        /// Get data 
        /// </summary>
        public object Content { get; private set; }
        /// <summary>
        /// Get View
        /// </summary>
        public object View { get; private set; }

        #endregion

        #region Constructor
        /// <summary>
        /// Initialize a new instance of DocumentItem
        /// </summary>
        /// <param name="content">data content</param>
        /// <param name="view">view</param>
        public DocumentItem(object content, object view)
        {           
            this.Content = content;
            this.View = view;
        } 
        #endregion
    }
}
