﻿using LongModel.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.Views.Documents
{
    public class DocFooter : ObservableObject
    {
        #region Getter, Setter
        private string text;
        private DateTime date;

        public string Text
        {
            get { return this.text; }
            set
            {
                this.SetProperty(ref this.text, value);
            }
        }
        public DateTime Date
        {
            get { return this.date; }
            set
            {
                this.SetProperty(ref this.date, value);
            }
        }
        #endregion

        #region Constructor
        public DocFooter(string text, DateTime date)
        {
            this.text = text;
            this.date = date;
        }
        #endregion
    }
}
