﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LongInterface.Views.Documents;

namespace LongModel.Views.Documents
{
    public class DocumentItemFooter : DocumentItem, IDocumentItemFooter
    {
        #region Getter
        /// <summary>
        /// Get Page Index
        /// </summary>
        public int PageIndex { get; private set; }
        #endregion

        #region Constructor
        public DocumentItemFooter(object content, object view, int index)
            : base(content, view)
        {
            this.PageIndex = index;
        } 
        #endregion
    }
}
