﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.Views.ControlModels
{
    public class ButtonInfoPara : ButtonInfo
    {
        #region Getter, Setter
        public object Parameter { get; set; }
        #endregion

        #region Constructor
        public ButtonInfoPara(ButtonInfo info, object parameter)
        {
            this.Text = info.Text;
            this.Command = info.Command;
            this.IconInfo = info.IconInfo;
            this.ToolTipInfo = info.ToolTipInfo;
            this.Parameter = parameter;
        }
        #endregion
    }
}
