﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace LongModel.Views.ControlModels
{
    public class HeaderItemInfo : ContentItemInfo
    {
        #region Getter, Setter
        private object header;
        public object Header
        {
            get { return this.header; }
            set
            {
                this.SetProperty(ref this.header, value);
            }
        }
        #endregion

        #region Constructor
        public HeaderItemInfo(object header, object content, ICommand command)
            : base(content, command)
        {
            this.header = header;
        }
        #endregion
    }
}
