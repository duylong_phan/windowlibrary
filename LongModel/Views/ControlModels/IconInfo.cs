﻿using LongInterface.Views.ControlModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.Views.ControlModels
{
    public class IconInfo : IGetIconInfo
    {
        #region Getter, Setter
        public object Content { get; set; }
        public double Height { get; set; }
        public double Width { get; set; }
        #endregion

        #region Getter
        public bool HasInfo
        {
            get
            {
                return this.Content != null;
            }
        }
        #endregion

        #region Constructor
        public IconInfo()
            : this(null, 0, 0)
        {

        }

        public IconInfo(object content, double width, double height)
        {
            this.Content = content;
            this.Width = width;
            this.Height = height;
        } 
        #endregion
    }
}
