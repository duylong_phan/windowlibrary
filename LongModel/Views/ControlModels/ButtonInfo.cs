﻿using LongInterface.Views.ControlModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using LongModel.BaseClasses;

namespace LongModel.Views.ControlModels
{
    public class ButtonInfo : ObservableObject, ICommandItemInfo
    {
        #region Getter
        public ICommand Command { get; protected set; }
        public string Text { get; protected set; }
        #endregion

        #region Getter, Setter
        public IGetIconInfo IconInfo { get; set; }
        public IGetToolTipInfo ToolTipInfo { get; set; }
        #endregion

        #region Constructor
        public ButtonInfo()
            : this(string.Empty, null)
        {

        }

        public ButtonInfo(string text, ICommand command)
            : this(text, command, null, null)
        {

        }

        public ButtonInfo(string text, ICommand command, IGetIconInfo iconInfo, IGetToolTipInfo toolTipInfo)
        {
            this.Text = text;
            this.Command = command;
            this.IconInfo = iconInfo;
            this.ToolTipInfo = toolTipInfo;
        } 
        #endregion
    }
}
