﻿using LongInterface.Views.ControlModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.Views.ControlModels
{
    public class ToolTipInfo : IGetToolTipInfo
    {
        #region Getter, Setter
        public object Content { get; set; }
        #endregion

        #region Getter
        public bool HasInfo
        {
            get
            {
                return this.Content != null;
            }
        }
        #endregion

        #region Constructor
        public ToolTipInfo()
            : this(null)
        {

        }

        public ToolTipInfo(object content)
        {
            this.Content = content;
        } 
        #endregion
    }
}
