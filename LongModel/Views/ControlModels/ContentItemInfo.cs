﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LongModel.BaseClasses;
using System.Windows.Input;

namespace LongModel.Views.ControlModels
{
    public class ContentItemInfo : ObservableObject
    {
        #region Getter, Setter
        private object content;
        public object Content
        {
            get { return this.content; }
            set
            {
                this.SetProperty(ref this.content, value);
            }
        }

        public ICommand Command { get; set; }
        #endregion

        #region Constructor
        public ContentItemInfo(object content, ICommand command)
        {
            this.content = content;
            this.Command = command;
        }
        #endregion
    }
}
