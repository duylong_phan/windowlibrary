﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.Helpers
{
    public static class WebHelper
    {
        #region Const
        public const int BufferLength = 2056;
        public const char ParaSeparator = '&';
        public const char ValueSeparator = '=';

        public const string Post_Binary = "multipart/form-data; boundary=---------------";
        public const string Post_Text = "application/x-www-form-urlencoded";

        //Request Suffix
        public const string Req_Ping = "Ping";
        public const string Req_Info = "Info";
        public const string Req_Input = "Input";
        public const string Req_Output = "Output";
        public const string Req_Ack = "Ack";
        public const string Req_Dispose = "Dispose";

        //Response
        public const string Res_Ok = "Ok";
        public const string Res_NotAvailable = "NotAvailable";
        public const string Res_Invalid = "Invalid";

        //Parameter
        public const string Para_Action = "Action";
        public const string Para_Name = "Name";
        public const string Para_Date = "Date";

        public const string Action_Request = "Request";
        public const string Action_Send = "Send";
        #endregion

        #region Public Method        
        /// <summary>
        /// Request Content from a Web Link
        /// </summary>
        /// <param name="url">root URL</param>
        /// <param name="suffix">suffix of Web Link</param>
        /// <param name="action">CallBack Method</param>
        /// <param name="timeout">Timeout in ms</param>
        public static void Request(string url, string suffix, Action<string, byte[]> action, int timeout)
        {
            RequestSend(url, suffix, action, timeout, null, null);
        }

        /// <summary>
        /// Request Content from a Web Link with Binary Post content
        /// </summary>
        /// <param name="url">root URL</param>
        /// <param name="suffix">suffix of Web Link</param>
        /// <param name="action">CallBack Method</param>
        /// <param name="timeout">Timeout in ms</param>
        /// <param name="buffer">request Buffer</param>
        public static void RequestSendBinary(string url, string suffix, Action<string, byte[]> action, int timeout, byte[] buffer)
        {
            RequestSend(url, suffix, action, timeout, buffer, Post_Binary);
        }

        /// <summary>
        /// Request Content from a Web Link with Text Post content
        /// </summary>
        /// <param name="url">root URL</param>
        /// <param name="suffix">suffix of Web Link</param>
        /// <param name="action">CallBack Method</param>
        /// <param name="timeout">Timeout in ms</param>
        /// <param name="buffer">request Buffer</param>
        public static void RequestSendText(string url, string suffix, Action<string, byte[]> action, int timeout, byte[] buffer)
        {
            RequestSend(url, suffix, action, timeout, buffer, Post_Text);
        }

        /// <summary>
        /// Get Parameter from GET Request suffix link
        /// </summary>
        /// <param name="text">Link without root URL</param>
        /// <param name="offset">offset Index</param>
        /// <returns></returns>
        public static Dictionary<string, string> GetParameters(string text, int offset = 1)
        {
            var dict = new Dictionary<string, string>();

            if (offset > 0)
            {
                text = text.Substring(offset, text.Length - offset);
            }

            var paras = text.Split(ParaSeparator);
            if (paras.Length > 0)
            {
                foreach (var item in paras)
                {
                    var subElements = item.Split(ValueSeparator);
                    if (subElements.Length > 1)
                    {
                        dict.Add(subElements[0], subElements[1]);
                    }
                }
            }
            return dict;
        }

        /// <summary>
        /// Get GET Request suffix link from given Dict
        /// </summary>
        /// <param name="dict">parameter dictionary</param>
        /// <returns></returns>
        public static string GetParametersText(Dictionary<string, string> dict)
        {
            var builder = new StringBuilder();
            int index = 0;

            foreach (var key in dict.Keys)
            {
                if (index != 0)
                {
                    builder.Append(ParaSeparator);
                }
                builder.Append(key);
                builder.Append(ValueSeparator);
                builder.Append(dict[key]);
                index++;
            }
            return builder.ToString();
        }

        /// <summary>
        /// Get Prefix from IP and Port
        /// </summary>
        /// <param name="ipAddress"></param>
        /// <param name="port"></param>
        /// <returns></returns>
        public static string GetHttpPrefix(string ipAddress, int port)
        {
            return string.Format("http://{0}:{1}/", ipAddress, port);
        }
        #endregion

        #region Private Method  
        private static void RequestSend(string url, string suffix, Action<string, byte[]> action, int timeout, byte[] buffer, string type)
        {
            var request = (HttpWebRequest)WebRequest.Create(url + suffix);
            //Always => shorten the time for first Request
            request.Proxy = null;

            //-------Important => xxxAsync() should be called only once----------------

            //Buffer is available => POST request
            if (buffer != null)
            {
                request.Method = "POST";
                request.ContentType = type;

                //Get Stream => xxxAsync is called only once
                var requestTask = request.GetRequestStreamAsync();                                
                var isOk = requestTask.Wait(timeout);
                if (isOk)
                {
                    using (var stream = requestTask.Result)
                    {
                        stream.Write(buffer, 0, buffer.Length);
                    }
                }
                else
                {
                    //Important
                    request.Abort();
                    throw new Exception("No response from Server");
                }                
                //automatic Set => by .Net API => no need for this line
                //request.ContentLength = buffer.Length;
            }

            //wait response => xxxAsync is called only once
            var responseTask = request.GetResponseAsync();
            bool isDone = responseTask.Wait(timeout);
            if (isDone)
            {
                var response = responseTask.Result;
                var data = GetResponseBuffer(response);
                action(suffix, data);
            }
            else
            {
                //Important
                request.Abort();
                throw new Exception("No response from Server");
            }
        }

        private static byte[] GetResponseBuffer(WebResponse response)
        {
            byte[] data = null;
            if (response != null)
            {
                using (var stream = response.GetResponseStream())
                using (var memoStream = new MemoryStream())
                {
                    data = new byte[BufferLength];
                    int length = stream.Read(data, 0, data.Length);
                    while (length > 0)
                    {
                        memoStream.Write(data, 0, length);
                        length = stream.Read(data, 0, data.Length);
                    }
                    data = memoStream.ToArray();
                }
            }
            return data;
        }
        #endregion
    }
}
