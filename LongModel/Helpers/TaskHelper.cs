﻿using System;
using System.Threading.Tasks;

namespace LongModel.Helpers
{
    public enum TaskResults
    {
        /// <summary>Task is done</summary>
        Done,
        /// <summary>Task is canceled</summary>
        Cancel,
        /// <summary>Error happened during task operation</summary>
        Error
    }

    public enum TaskStates
    {
        /// <summary>Task is allowed to execute</summary>
        Enable,
        /// <summary>Task is not allowed to execute</summary>
        Disable,
        /// <summary>Task is woring</summary>
        Working
    }

    public static class TaskHelper
    {
        /// <summary>
        /// Try to do given Action with given Timeout
        /// </summary>
        /// <param name="action">given action</param>
        /// <param name="timeout">given Timeout</param>
        /// <returns>Indicate if Action is done</returns>
        public static async Task<bool> DoActionTimeout(Action action, int timeout)
        {
            bool isDone = false;

            if (action != null)
            {
                var mainTask = Task.Factory.StartNew(action);
                var waitTask = Task.Delay(timeout);

                if (await Task.WhenAny(mainTask, waitTask) == mainTask)
                {
                    isDone = true;
                }
            }            

            return isDone;
        }

        /// <summary>
        /// Try to do given function with given Timeout
        /// </summary>
        /// <typeparam name="T">output Type</typeparam>
        /// <param name="function">given function</param>
        /// <param name="timeout">given timeout</param>
        /// <param name="timeoutValue">default value for Timeout</param>
        /// <returns>output result</returns>
        public static async Task<T> DoFunctionTimeout<T>(Func<T> function, int timeout, T timeoutValue)
        {
            T result = timeoutValue;

            if (function != null)
            {
                var mainTask = Task.Factory.StartNew<T>(function);
                var waitTask = Task.Delay(timeout);

                if (await Task.WhenAny(mainTask, waitTask) == mainTask)
                {
                    result = mainTask.Result;
                }
            }

            return result;
        }
    }
}
