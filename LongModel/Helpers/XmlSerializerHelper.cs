﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace LongModel.Helpers
{
    public static class XmlSerializerHelper
    {
        #region Static
        private static Dictionary<string, string> xmlDict;

        public static Dictionary<string, string> XmlDict
        {
            get
            {
                if (xmlDict == null)
                {
                    xmlDict = new Dictionary<string, string>();
                    xmlDict.Add("xsi", "http://www.w3.org/2001/XMLSchema-instance");
                    xmlDict.Add("xsd", "http://www.w3.org/2001/XMLSchema");
                }
                return xmlDict;
            }
        }
        #endregion

        #region Public Methods
        public static string GetXML(object dataModel)
        {
            string text = string.Empty;

            using (var stream = new MemoryStream())
            {
                XmlSerializer serialize = new XmlSerializer(dataModel.GetType());
                serialize.Serialize(stream, dataModel);
                stream.Position = 0;

                using (var reader = new StreamReader(stream))
                {
                    XDocument doc = XDocument.Parse(reader.ReadToEnd());
                    foreach (var key in XmlDict.Keys)
                    {
                        doc.Root.Attribute(XNamespace.Xmlns + key).Remove();
                    }
                    text = doc.Root.ToString();
                }
            }

            return text;
        }

        public static object GetData(string text, Type dataType)
        {
            object data = null;
            XElement element = XElement.Parse(text);
            foreach (var key in XmlDict.Keys)
            {
                var att = new XAttribute(XNamespace.Xmlns + key, XmlDict[key]);
                element.Add(att);
            }

            using (var stream = new MemoryStream())
            using (var writer = new StreamWriter(stream))
            {
                writer.Write(element.ToString());
                writer.Flush();
                stream.Position = 0;

                XmlSerializer serialize = new XmlSerializer(dataType);
                data = serialize.Deserialize(stream);
            }
            return data;
        }
        #endregion
    }
}
