﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.Helpers
{
    public static class LanguageHelper
    {
        #region Private Field
        private static object source;
        private static Func<object, string, string> getlanguage;
        #endregion

        #region Public Method
        public static void SetSource(object source)
        {
            LanguageHelper.source = source;
        }

        public static void SetAction(Func<object, string, string> getlanguage)
        {
            LanguageHelper.getlanguage = getlanguage;
        }

        public static string GetTranslation(string key)
        {
            string translation = string.Empty;
            try
            {
                if (key == null)
                {
                    throw new NullReferenceException();
                }

                translation = getlanguage(source, key);
                translation = RawTextTranslator.Translate(translation);

                if (translation == null)
                {
                    throw new FormatException();
                }
            }
            catch (Exception ex)
            {
                if (ex is NullReferenceException)
                {
                    translation = "Invalid key";
                }
                else if (ex is FormatException)
                {
                    translation = "Resource is not existed";
                }
            }
            return translation;
        }
        #endregion
    }
}
