﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using LongInterface.Models;

namespace LongModel.Helpers
{    
    /// <summary>
    /// Helper Class for providing more function for Model
    /// </summary>
    public static class ModelHelper
    {
        /// <summary>
        /// Copy all available Property Value from Source to destination Object
        /// Only for Getter & Setter & public Property
        /// Only the Class Instance Property will be supported, for Interface in LongWpfUI, Helper/ModelHelper
        /// </summary>
        /// <typeparam name="T">Base Class or Interface</typeparam>
        /// <param name="source">given Source</param>
        /// <param name="dest">given Destination</param>
        public static void CopyProperties<T>(T source, T dest)
        {
            if (source == null || dest == null)
            {
                return;
            }

            Type type = typeof(T);
            if (type == null)
            {
                return;
            }

            List<PropertyInfo> collection = GetPublicProperties(type);
            foreach (var item in collection)
            {
                object value = item.GetValue(source);
                if (value is ICloneModel)
                {
                    ICloneModel instance = value as ICloneModel;
                    value = instance.CloneModel();
                }                
                item.SetValue(dest, value);
            }
        }
        
        #region Private Method
        private static List<PropertyInfo> GetPublicProperties(Type type)
        {
            List<PropertyInfo> collection = new List<PropertyInfo>();
            TypeInfo info = type.GetTypeInfo();

            //check if it is interface
            if (info.IsInterface)
            {
                List<Type> checkTypes = new List<Type>();
                Queue<Type> queue = new Queue<Type>();
                queue.Enqueue(type);

                while (queue.Count > 0)
                {
                    Type subType = queue.Dequeue();
                    foreach (var item in subType.GetTypeInfo().ImplementedInterfaces)
                    {
                        if (!checkTypes.Contains(item)) //NOT
                        {
                            checkTypes.Add(item);
                            queue.Enqueue(item);
                        }
                    }

                    //just get Property from given interface
                    List<PropertyInfo> newProps = SortGetSetProperties(subType.GetTypeInfo().DeclaredProperties);
                    foreach (var item in newProps)
                    {
                        if (!collection.Contains(item)) //NOT
                        {
                            collection.Add(item);
                        }
                    }
                }
            }
            //normal class, struct
            else
            {
                //Get ALL Properties
                collection.AddRange(SortGetSetProperties(type.GetRuntimeProperties()));
            }

            return collection;
        }

        private static List<PropertyInfo> SortGetSetProperties(IEnumerable<PropertyInfo> collection)
        {
            List<PropertyInfo> newCollection = new List<PropertyInfo>();
            
            foreach (var item in collection)
            {                
                //only Getter, Setter Properties are interested
                if (item.GetMethod != null && item.SetMethod != null)
                {
                    //static property is skipped
                    if( item.GetMethod.IsStatic || item.SetMethod.IsStatic)
                    {
                        continue;
                    }
                    newCollection.Add(item);
                }
            }

            return newCollection;
        } 
        #endregion
    }
}
