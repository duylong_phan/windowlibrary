﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongModel.Helpers
{
    public static class RawTextTranslator
    {
        #region Getter
        private static Dictionary<string,string> replaceDict;
        public static Dictionary<string,string> ReplaceDict
        {
            get
            {
                if (replaceDict == null)
                {
                    replaceDict = new Dictionary<string,string>();
                    replaceDict.Add("\\n ", Environment.NewLine);
                    replaceDict.Add("\\n", Environment.NewLine);
                    replaceDict.Add("\\t", "\t");
                }
                return replaceDict;
            }
        }
        #endregion

        #region Method
        public static string Translate(string text)
        {
            if (text != null)
            {
                foreach (var key in ReplaceDict.Keys)
                {
                    if (text.Contains(key))
                    {
                        text = text.Replace(key, ReplaceDict[key]);
                    }
                }
            }

            return text;
        }
        #endregion
    }
}
