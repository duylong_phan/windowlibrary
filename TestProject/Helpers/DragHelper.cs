﻿using LongWpfUI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace TestProject.Helpers
{
    public class DragHelper : DependencyObject
    {
        #region Const
        public const double ParentPadding = 2;
        #endregion

        #region Static Field
        private static Point _startPoint;
        private static double _startLeft;
        private static double _startTop;
        private static bool _canDrag;
        private static Canvas _parentPanel;
        
        static DragHelper()
        {
            _canDrag = false;
            _startLeft = 0;
            _startTop = 0;
        }
        #endregion

        #region AP
       
        public static bool GetIsEnable(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsEnableProperty);
        }

        public static void SetIsEnable(DependencyObject obj, bool value)
        {
            obj.SetValue(IsEnableProperty, value);
        }

        public static bool GetCanFocus(DependencyObject obj)
        {
            return (bool)obj.GetValue(CanFocusProperty);
        }

        public static void SetCanFocus(DependencyObject obj, bool value)
        {
            obj.SetValue(CanFocusProperty, value);
        }

        public static bool GetIsInsideParent(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsInsideParentProperty);
        }

        public static void SetIsInsideParent(DependencyObject obj, bool value)
        {
            obj.SetValue(IsInsideParentProperty, value);
        }

        public static bool GetSupportRotateTransform(DependencyObject obj)
        {
            return (bool)obj.GetValue(SupportRotateTransformProperty);
        }

        public static void SetSupportRotateTransform(DependencyObject obj, bool value)
        {
            obj.SetValue(SupportRotateTransformProperty, value);
        }

        public static bool GetIsHandledMouseDown(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsHandledMouseDownProperty);
        }

        public static void SetIsHandledMouseDown(DependencyObject obj, bool value)
        {
            obj.SetValue(IsHandledMouseDownProperty, value);
        }

        public static Popup GetPopup(DependencyObject obj)
        {
            return (Popup)obj.GetValue(PopupProperty);
        }

        public static void SetPopup(DependencyObject obj, Popup value)
        {
            obj.SetValue(PopupProperty, value);
        }

        public static readonly DependencyProperty IsEnableProperty =
           DependencyProperty.RegisterAttached("IsEnable", typeof(bool), typeof(DragHelper), new PropertyMetadata(false, OnIsEnableChanged));
               
        public static readonly DependencyProperty CanFocusProperty =
            DependencyProperty.RegisterAttached("CanFocus", typeof(bool), typeof(DragHelper), new PropertyMetadata(true));

        public static readonly DependencyProperty IsInsideParentProperty =
            DependencyProperty.RegisterAttached("IsInsideParent", typeof(bool), typeof(DragHelper), new PropertyMetadata(true));

        public static readonly DependencyProperty SupportRotateTransformProperty =
            DependencyProperty.RegisterAttached("SupportRotateTransform", typeof(bool), typeof(DragHelper), new PropertyMetadata(true));
        
        public static readonly DependencyProperty IsHandledMouseDownProperty =
            DependencyProperty.RegisterAttached("IsHandledMouseDown", typeof(bool), typeof(DragHelper), new PropertyMetadata(true));

        public static readonly DependencyProperty PopupProperty =
            DependencyProperty.RegisterAttached("Popup", typeof(Popup), typeof(DragHelper), new PropertyMetadata(null));
        #endregion

        #region Property Changed
        private static void OnIsEnableChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var element = d as FrameworkElement;
            if (element == null)
            {
                return;
            }
            var isEnable = DragHelper.GetIsEnable(element);
            //enable function
            if (isEnable)
            {
                AddElementEvents(element);
            }
            //disable function
            else
            {
                RemoveElementEvents(element);
            }
        }
        #endregion

        #region Private Method

        private static void AddElementEvents(FrameworkElement element)
        {
            if (element != null)
            {
                //Subscribe Event
                element.PreviewMouseDown += Element_PreviewMouseDown;
                element.PreviewMouseMove += Element_PreviewMouseMove;

                //Rotation Support ?
                if (DragHelper.GetSupportRotateTransform(element))
                {
                    element.PreviewMouseLeftButtonDown += Element_PreviewMouseLeftButtonDown;
                }

                //Child in Panel muss have a valid position value
                var offset = DragHelper.GetIsInsideParent(element) ? DragHelper.ParentPadding : 0.0;
                if (double.IsNaN(Canvas.GetLeft(element)))
                {
                    Canvas.SetLeft(element, offset);
                }
                if (double.IsNaN(Canvas.GetTop(element)))
                {
                    Canvas.SetTop(element, offset);
                }
            }
        }

        private static void RemoveElementEvents(FrameworkElement element)
        {
            if (element != null)
            {
                //Subscribe Event
                element.PreviewMouseDown -= Element_PreviewMouseDown;
                element.PreviewMouseMove -= Element_PreviewMouseMove;

                //Rotation Support ?
                if (DragHelper.GetSupportRotateTransform(element))
                {
                    element.PreviewMouseLeftButtonDown -= Element_PreviewMouseLeftButtonDown;
                }
            }
        }

        private static void UpdateElementPosition(FrameworkElement element, double newLeft, double newTop)
        {
            if (element == null)
            {
                return;
            }

            #region In parent ?
            if (DragHelper.GetIsInsideParent(element) && _parentPanel != null)
            {
                #region Calculate Max value
                double maxLeft = 0;
                double maxTop = 0;

                //Support Rotation ?
                if (DragHelper.GetSupportRotateTransform(element) && element.DataContext is IAngle)
                {
                    var tmpModel = element.DataContext as IAngle;
                    int angle = (int)tmpModel.Angle;
                    switch (angle)
                    {
                        case 90:
                        case 270:
                            maxLeft = _parentPanel.ActualWidth - element.ActualHeight - DragHelper.ParentPadding;
                            maxTop = _parentPanel.ActualHeight - element.ActualWidth - DragHelper.ParentPadding;
                            break;

                        case 0:
                        case 180:
                        default:
                            maxLeft = _parentPanel.ActualWidth - element.ActualWidth - DragHelper.ParentPadding;
                            maxTop = _parentPanel.ActualHeight - element.ActualHeight - DragHelper.ParentPadding;
                            break;
                    }
                }
                //Normal
                else
                {
                    maxLeft = _parentPanel.ActualWidth - element.ActualWidth - DragHelper.ParentPadding;
                    maxTop = _parentPanel.ActualHeight - element.ActualHeight - DragHelper.ParentPadding;
                }
                #endregion

                #region Make sure value in Range
                if (newLeft > maxLeft)
                {
                    newLeft = maxLeft;
                }
                else if (newLeft < DragHelper.ParentPadding)
                {
                    newLeft = DragHelper.ParentPadding;
                }

                if (newTop > maxTop)
                {
                    newTop = maxTop;
                }
                else if (newTop < DragHelper.ParentPadding)
                {
                    newTop = DragHelper.ParentPadding;
                }
                #endregion
            }
            #endregion

            #region Update UI
            var dataModel = element.DataContext as IPositionable;
            //in DataModel
            if (dataModel != null)
            {
                dataModel.Left = newLeft;
                dataModel.Top = newTop;
            }
            //in View
            else
            {
                Canvas.SetLeft(element, newLeft);
                Canvas.SetTop(element, newTop);
            }

            //attached Popup ?
            var popup = DragHelper.GetPopup(element);
            if (popup != null)
            {
                popup.HorizontalOffset++;
                popup.HorizontalOffset--;
            }
            #endregion
        }

        private static void UpdateElementAngle(FrameworkElement element, double angle)
        {
            if (element == null)
            {
                return;
            }

            var dataModel = element.DataContext as IAngle;
            if (dataModel != null)
            {
                dataModel.Angle = angle;
            }
        }
        #endregion

        #region Event Handler
        private static void Element_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            var element = sender as FrameworkElement;
            if (element != null)
            {
                //Subscribe Event
                element.PreviewMouseUp += Element_PreviewMouseUp;

                //Get intialize variable
                _startPoint = e.GetPosition(null);
                _startLeft = Canvas.GetLeft(element);
                _startTop = Canvas.GetTop(element);
                _canDrag = true;

                //Capture Mouse ?
                if (DragHelper.GetCanFocus(element))
                {
                    element.CaptureMouse();
                }

                //Inside Parent ?
                if (DragHelper.GetIsInsideParent(element))
                {
                    _parentPanel = ControlHelper.GetParent<Canvas>(element);
                }

                //MouseDown is handled ?
                if (DragHelper.GetIsHandledMouseDown(element))
                {
                    e.Handled = true;
                    //alternative update
                    if (element.DataContext is ISelectable)
                    {
                        var dataModel = element.DataContext as ISelectable;
                        dataModel.IsSelected = true;
                    }
                }
            }
        }

        private static void Element_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            var element = sender as FrameworkElement;
            if (element != null)
            {
                //Unsubcribe Event
                element.PreviewMouseUp -= Element_PreviewMouseUp;

                //reset
                _canDrag = false;                

                //CaptureMouse
                if (DragHelper.GetCanFocus(element))
                {
                    element.ReleaseMouseCapture();
                }
            }
        }

        private static void Element_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            var element = sender as FrameworkElement;
            if (element != null && _canDrag)
            {
                //calculate current Position
                var currentPoint = e.GetPosition(null);                
                var newLeft = (currentPoint.X - _startPoint.X) + _startLeft;
                var newTop = (currentPoint.Y - _startPoint.Y) + _startTop;

                UpdateElementPosition(element, newLeft, newTop);
            }
        }

        private static void Element_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var element = sender as FrameworkElement;
            if (element != null && e.ClickCount == 2 && element.DataContext is IAngle)
            {
                var dataModel = element.DataContext as IAngle;
                var lastValue = dataModel.Angle;
                var nextValue = lastValue + 90;
                var transform = element.LayoutTransform as RotateTransform;

                //UI is supported
                if (transform != null)
                {
                    var ani = new DoubleAnimation(lastValue, nextValue, new Duration(TimeSpan.FromMilliseconds(100)));
                    //animation is done => remove animation effect, update dataModel
                    ani.Completed += (_sender, _e) =>
                    {
                        transform.BeginAnimation(RotateTransform.AngleProperty, null);
                        
                        UpdateElementAngle(element, (nextValue > 359) ? 0 : nextValue);
                        UpdateElementPosition(element, Canvas.GetLeft(element), Canvas.GetTop(element));
                    };
                    //start
                    transform.BeginAnimation(RotateTransform.AngleProperty, ani);
                }
                //update DataModel, if UI is not supported
                else
                {
                    UpdateElementAngle(element, (nextValue > 359) ? 0 : nextValue);
                    UpdateElementPosition(element, Canvas.GetLeft(element), Canvas.GetTop(element));
                }
            }
        }
        #endregion
    }
}
