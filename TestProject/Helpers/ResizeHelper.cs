﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace TestProject.Helpers
{
    public class ResizeHelper : DependencyObject
    {
        #region AP
        public static bool GetIsEnable(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsEnableProperty);
        }

        public static void SetIsEnable(DependencyObject obj, bool value)
        {
            obj.SetValue(IsEnableProperty, value);
        }

        public static bool GetSupportCrossResize(DependencyObject obj)
        {
            return (bool)obj.GetValue(SupportCrossResizeProperty);
        }

        public static void SetSupportCrossResize(DependencyObject obj, bool value)
        {
            obj.SetValue(SupportCrossResizeProperty, value);
        }

        public static Style GetThumbStyle(DependencyObject obj)
        {
            return (Style)obj.GetValue(ThumbStyleProperty);
        }

        public static void SetThumbStyle(DependencyObject obj, Style value)
        {
            obj.SetValue(ThumbStyleProperty, value);
        }

        public static readonly DependencyProperty IsEnableProperty =
            DependencyProperty.RegisterAttached("IsEnable", typeof(bool), typeof(ResizeHelper), new PropertyMetadata(false, OnIsEnableChanged));

        public static readonly DependencyProperty SupportCrossResizeProperty =
            DependencyProperty.RegisterAttached("SupportCrossResize", typeof(bool), typeof(ResizeHelper), new PropertyMetadata(false));

        public static readonly DependencyProperty ThumbStyleProperty =
            DependencyProperty.RegisterAttached("ThumbStyle", typeof(Style), typeof(ResizeHelper), new PropertyMetadata(null));

        #endregion

        private static void OnIsEnableChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            
        }
    }
}
