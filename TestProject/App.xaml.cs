﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using LongWpfUI.Windows;
using LongModel.Views.Infos;
using LongModel.Models.References;
using LongWpfUI.Resources.Accessors;
using System.Windows.Threading;
using LongModel.Views.Infos.HelpInfos;

namespace TestProject
{   
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            /*
            var introduction = new SectionHelpInfo("Introduction");
            introduction.ContentInfos.Add(new TextContentHelpInfo("Description", "First Paragragh, which is used to describe the task." + Environment.NewLine + "Second Paragragh, which is used to test"));
            introduction.ContentInfos.Add(new TextContentHelpInfo("Description", "First Paragragh, which is used to describe the task." + Environment.NewLine + "Second Paragragh, which is used to test"));
            introduction.ContentInfos.Add(new TextContentHelpInfo("Description", "First Paragragh, which is used to describe the task." + Environment.NewLine + "Second Paragragh, which is used to test"));
            introduction.ContentInfos.Add(new TextContentHelpInfo("Description", "First Paragragh, which is used to describe the task." + Environment.NewLine + "Second Paragragh, which is used to test"));
            var connection = new SectionHelpInfo("Connection");
            var serialInfo = new ImageContentHelpInfo("Serial Port Setting", "/TestProject;component/Resources/Images/Note_Serial.png");
            serialInfo.IndexInfos.Add(new TextIndexInfo(1, "Computer interface connection type"));
            serialInfo.IndexInfos.Add(new TextIndexInfo(2, "Serial Port Index"));
            serialInfo.IndexInfos.Add(new TextIndexInfo(3, "Baurate speed"));
            serialInfo.IndexInfos.Add(new TextIndexInfo(4, "Bits per Data"));
            serialInfo.IndexInfos.Add(new TextIndexInfo(5, "Error detection method"));
            serialInfo.IndexInfos.Add(new TextIndexInfo(6, "Bits after transmited Data"));
            serialInfo.IndexInfos.Add(new TextIndexInfo(7, "Bytes per update"));
            serialInfo.IndexInfos.Add(new TextIndexInfo(8, "Use Data Transmition Ready"));
            serialInfo.IndexInfos.Add(new TextIndexInfo(9, "Use Request to send"));
            connection.ContentInfos.Add(serialInfo);

            var usbInfo = new ImageContentHelpInfo("USB Port Setting", "/TestProject;component/Resources/Images/Note_USB.png");
            usbInfo.IndexInfos.Add(new TextIndexInfo(1, "Computer interface connection type"));
            usbInfo.IndexInfos.Add(new TextIndexInfo(2, "Manufacture Indentity"));
            usbInfo.IndexInfos.Add(new TextIndexInfo(3, "Product Indentity"));
            connection.ContentInfos.Add(usbInfo);

            var graphic = new SectionHelpInfo("Graphic");

            var taskInfo = new TaskHelpInfo("Sample Software");
            taskInfo.SectionInfos.AddRange(new List<SectionHelpInfo>() { introduction, connection, graphic });
            var window = new HelpWindow(null, taskInfo);
            window.ShowDialog();
            */

            this.MainWindow = new MainWindow();
            this.MainWindow.ShowDialog();
        }
    }
}
