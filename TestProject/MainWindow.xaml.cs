﻿using LongModel.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TestProject
{
    public interface IAngle
    {
        double Angle { get; set; }
    }

    public interface IWidth
    {
        double Width { get; set; }
    }

    public interface IHeight
    {
        double Height { get; set; }
    }

    public interface ILeft
    {
        double Left { get; set; }
    }

    public interface ITop
    {
        double Top { get; set; }
    }

    public interface ISelectable
    {
        bool IsSelected { get; set; }
    }

    
    

    public interface IScalable
    {
        double ScaleX { get; set; }
        double ScaleY { get; set; }
    }

    public interface ISize : IWidth, IHeight
    {

    }

    public interface IPositionable : ILeft, ITop
    {

    }

    public interface ISizable : ISize, IPositionable
    {

    }

    

    public class DataModel : ObservableObject, IAngle, ISizable, ISelectable
    {
        #region Getter, setter
        private double angle;
        private double width;
        private double height;
        private double left;
        private double top;
        private bool isSelected;

        public double Angle
        {
            get { return this.angle; }
            set
            {
                this.SetProperty(ref this.angle, value);
            }
        }
        public double Width
        {
            get { return this.width; }
            set
            {
                this.SetProperty(ref this.width, value);
            }
        }
        public double Height
        {
            get { return this.height; }
            set
            {
                this.SetProperty(ref this.height, value);
            }
        }
        public double Left
        {
            get { return this.left; }
            set
            {
                this.SetProperty(ref this.left, value);
            }
        }
        public double Top
        {
            get { return this.top; }
            set
            {
                this.SetProperty(ref this.top, value);
            }
        }
        public bool IsSelected
        {
            get { return this.isSelected; }
            set
            {
                this.SetProperty(ref this.isSelected, value);
            }
        }
        #endregion

        #region Text
        public string Text { get; private set; }
        public SolidColorBrush Brush { get; private set; }
        #endregion

        public DataModel(string text, double width, double height, SolidColorBrush brush)
        {
            this.Text = text;
            this.width = width;
            this.height = height;
            this.Brush = brush;
            this.isSelected = false;
        }
    }

    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            var collection = new List<DataModel>();
            collection.Add(new DataModel("1", 100, 20, Brushes.Gray));
            collection.Add(new DataModel("2", 200, 50, Brushes.Blue));
            collection.Add(new DataModel("3", 150, 100, Brushes.Green));
            collection.Add(new DataModel("4", 50, 50, Brushes.LightCoral));
            collection.Add(new DataModel("5", 25, 100, Brushes.Orange));
            this.DataContext = collection;            
        }
    }
}
