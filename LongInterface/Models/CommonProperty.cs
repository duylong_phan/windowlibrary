﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongInterface.Models
{
    public interface IIsEnabled
    {
        bool IsEnabled { get; set; }
    }

    public interface IIsWorking
    {
        bool IsWorking { get; set; }
    }

    public interface ITag
    {
        object Tag { get; set; }
    }

    public interface IName
    {
        string Name { get; set; }
    }

    public interface IText
    {
        string Text { get; set; }
    }

    public interface IIndex
    {
        int Index { get; set; }
    }

    public interface IContent
    {
        object Content { get; set; }
    }

    public interface ISelectable
    {
        bool IsSelected { get; set; }
    }

    public interface IValue<T>
    {
        T Value { get; set; }
    }

    public interface IFileName : ITag
    {
        string FileName { get; set; }
    }

    public interface IDirectory : ITag
    {
        string Directory { get; set; }
    }

    public interface IFilePath : ITag
    {
        string FilePath { get; set; }
    }

    public interface IFileExtension : ITag
    {
        string FileExtension { get; set; }
    }

    public interface IPort
    {
        int Port { get; set; }
    }

    public interface IAngle
    {
        double Angle { get; set; }
    }

    public interface IWidth
    {
        double Width { get; set; }
    }

    public interface IHeight
    {
        double Height { get; set; }
    }

    public interface ILeft
    {
        double Left { get; set; }
    }

    public interface ITop
    {
        double Top { get; set; }
    }

    public interface ISize : IWidth, IHeight
    {

    }

    public interface IPositionable : ILeft, ITop
    {

    }

    public interface ISizable : ISize, IPositionable
    {

    }
}
