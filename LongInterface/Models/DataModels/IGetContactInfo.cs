﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongInterface.Models.DataModels
{
    public interface IGetContactInfo
    {
        string Name { get; }
        string Email { get; }
        string Phone { get; }
        object Group { get; }
    }
}
