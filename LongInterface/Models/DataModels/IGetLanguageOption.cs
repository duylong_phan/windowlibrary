﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongInterface.Models.DataModels
{
    public interface IGetLanguageOption
    {
        string Name { get; }
        string Code { get; }
        string Image { get; }
        object ToolTip { get; }
    }
}
