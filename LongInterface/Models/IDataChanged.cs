﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongInterface.Models
{
    public interface IDataChanged
    {
        bool HasSubscription { get; }
        void SubscribeChanged(Action eventAction);
        void UnsubscribeChanged();
    }
}
