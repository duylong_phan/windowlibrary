﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace LongInterface.Models
{
    public interface IHasInfo
    {
        bool HasInfo { get; }
    }

    public interface IGetIsEnabled
    {
        bool IsEnabled { get; }
    }

    public interface IGetIsWorking
    {
        bool IsWorking { get; }
    }

    public interface IGetText
    {
        string Text { get; }
    }

    public interface IGetName
    {
        string Name { get; }
    }

    public interface IGetIndex
    {
        int Index { get; }
    }

    public interface IGetContent
    {
        object Content { get; }
    }

    public interface IGetHeader
    {
        object Header { get; }
    }

    public interface IGetHasInfo
    {
        bool HasInfo { get; }
    }

    public interface IGetCommand
    {
        ICommand Command { get; }
    }

    public interface IGetPort
    {
        int Port { get; }
    }

    public interface IGetAngle
    {
        double Angle { get; set; }
    }

    public interface IGetWidth
    {
        double Width { get; set; }
    }

    public interface IGetHeight
    {
        double Height { get; set; }
    }

    public interface IGetLeft
    {
        double Left { get; set; }
    }

    public interface IGetTop
    {
        double Top { get; set; }
    }

    public interface IGetSize : IGetWidth, IGetHeight
    {

    }

    public interface IGetPosition : IGetLeft, IGetTop
    {

    }
}
