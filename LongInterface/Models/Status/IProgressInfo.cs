﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongInterface.Models.Status
{
    public interface IProgressInfo : IText
    {
        int TimeLeft { get; set; }
        int TimeRemain { get; set; }
        int Duration { get; set; }
        bool IsIndeterminate { get; set; }
        bool HasError { get; set; }
    }

    public interface IProgressInfo<T> : IProgressInfo
    {
        T Percent { get; set; }
    }
}
