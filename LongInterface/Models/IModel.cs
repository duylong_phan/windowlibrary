﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace LongInterface.Models
{
    /// <summary>
    /// Class contain property for UpdateRequested Event
    /// </summary>
    public class UpdateRequestedEventArgs : EventArgs
    {
        /// <summary>
        /// Get Update Option
        /// </summary>
        public object Option { get; private set; }

        /// <summary>
        /// Initialize a new instance of UpdateRequestedEventArgs
        /// </summary>
        /// <param name="option"></param>
        public UpdateRequestedEventArgs(object option)
        {
            this.Option = option;
        }
    }

    /// <summary>
    /// Delegate for UpdateRequested Event
    /// </summary>
    /// <param name="sender">event sender</param>
    /// <param name="e">event args</param>
    public delegate void UpdateRequestedEventHandler(object sender, UpdateRequestedEventArgs e);


    /// <summary>
    /// Interface for basic Model
    /// </summary>
    public interface IModel : INotifyPropertyChanged
    {
        event UpdateRequestedEventHandler UpdateRequested;
    }
}
