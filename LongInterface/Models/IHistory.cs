﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongInterface.Models
{
    public enum HistoryTypes
    {
        Edited,
        Added,
        Removed
    }

    public class HistoryAvailableEventArgs : EventArgs
    {
        #region Getter
        public string Property { get; private set; }
        public string OldValue { get; private set; }
        public object SubSender { get; private set; }
        public HistoryTypes Type { get; private set; }
        #endregion

        #region Constructor
        public HistoryAvailableEventArgs(object subSender, string property, string oldValue)
            : this(subSender, property, oldValue, HistoryTypes.Edited)
        {

        }

        public HistoryAvailableEventArgs(object subSender, string property, string oldValue, HistoryTypes type)
        {
            this.SubSender = subSender;
            this.Property = property;
            this.OldValue = oldValue;
            this.Type = type;
        }
        #endregion
    }

    public delegate void HistoryAvailableEventHandler(object sender, HistoryAvailableEventArgs e);

    public interface IHistory
    {
        event HistoryAvailableEventHandler HistoryAvailable;
        void SubscribeHistory();
        void UnsubscribeHistory();
    }

    public interface IHistoryItem : IHistory
    {
        void SetOwner(object owner);
    }
}
