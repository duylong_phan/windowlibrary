﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongInterface.Models
{
    public interface ICopyable<T>
    {
        void CopyProperties(T item);
    }

    public interface ICloneModel
    {
        object CloneModel();
    }

    public interface IRefresh
    {
        void RefreshStatus();
    }

    public interface IBuffer
    {
        byte[] GetBytes();
    }

    public interface ICalculateProperty
    {
        void CalculateProperty();
    } 
}
