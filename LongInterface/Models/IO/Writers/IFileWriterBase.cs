﻿using LongInterface.Models.IO.Files;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongInterface.Models.IO.Writers
{
    public class WrittenErrorEventArgs : EventArgs
    {
        #region Getter
        public Exception Error { get; private set; }
        #endregion

        #region Constructor
        public WrittenErrorEventArgs(Exception error)
        {
            this.Error = error;
        }
        #endregion
    }

    public class WrittenDoneEventArgs : EventArgs
    {
        #region Getter
        public string FilePath { get; private set; }
        public long FileSize { get; private set; }
        #endregion

        #region Constructor
        public WrittenDoneEventArgs(string filePath)
            : this(filePath, 0)
        {

        }

        public WrittenDoneEventArgs(string filePath, long fileSize)
        {
            this.FilePath = filePath;
            this.FileSize = fileSize;
        }
        #endregion
    }

    public delegate void WrittenErrorEventHandler(object sender, WrittenErrorEventArgs e);
    public delegate void WrittenDoneEventHandler(object sender, WrittenDoneEventArgs e);

    public interface IFileWriterBase<TData, TSetting> : IDisposable, IGetIsEnabled, IGetIsWorking
        where TSetting : IFileSettingBase
    {       
        int SampleAmount { get; }
        TSetting Setting { get; }

        event WrittenErrorEventHandler WrittenError;
        event WrittenDoneEventHandler WrittenDone;

        void Start();
        void Stop();
        void Add(TData item);        
    }
}
