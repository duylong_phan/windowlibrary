﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongInterface.Models.IO.Files
{
    public interface IFormatInfo
    {
        string Type { get; }
        string Version { get; }
        string Author { get; }
        string Software { get; }
    }

    public interface IGetFormatInfo
    {
        string Type { get; }
        string Version { get; }
        string Author { get; }
        string Software { get; }
    }
}
