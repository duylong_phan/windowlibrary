﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongInterface.Models.IO.Files
{
    public interface IFileSingleSetting : IFileSettingBase, IFileName, IFileExtension
    {
        int Index { get; set; }
        bool UseIndex { get; set; }
        
        int SampleAmount { get; set; }        
    }
}
