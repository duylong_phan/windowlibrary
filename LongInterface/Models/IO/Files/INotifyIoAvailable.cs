﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongInterface.Models.IO.Files
{
    public interface IInputAvailable
    {
        void SetInputAction(Action<object> action);
        void OnInputAvailable(object parameter);
    }

    public interface IOutputAvailable
    {
        void SetOutputAction(Action<object> action);
        void OnOutputAvailable(object parameter);
    }

    public interface INotifyIoAvailable : IInputAvailable, IOutputAvailable
    {

    }
}
