﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongInterface.Models.IO.Files
{
    public interface IUsbSetting
    {
        int VendorID { get; set; }
        int ProductID { get; set; }
    }
}
