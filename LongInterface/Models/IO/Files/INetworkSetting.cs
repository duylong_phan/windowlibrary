﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongInterface.Models.IO.Files
{
    public interface INetworkSetting
    {
        string Address { get; set; }
        int Port { get; set; }
    }

    public interface IClientSetting : INetworkSetting, IName
    {
        
    }

    public interface IServerSetting : INetworkSetting
    {

    }

    public interface IAccount
    {
        string Account { get; set; }
        string Passwork { get; set; }
    }
}
