﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongInterface.Models.IO.Files
{
    public interface IExtensionSetting : IExeIconSetting, IFileExtension
    {
        string ProgID { get; set; }        
    }
}
