﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongInterface.Models.IO.Files
{
    public interface INotifyFileChanged
    {
        void SetFileChangedAction(Action<object> action);
        void OnFileChanged(object parameter);
    }

    public interface INotifyFileChanged<T> : INotifyFileChanged
        where T : IFileChangedSetting
    {
        T FileChangedSetting { get; set; }
    }
}
