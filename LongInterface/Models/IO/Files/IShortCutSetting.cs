﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongInterface.Models.IO.Files
{
    public interface IShortcutSetting : IExeIconSetting
    {
        string Name { get; set; }
        string PlacementDirectory { get; set; }
        string WorkingDirectory { get; set; }
    }
}
