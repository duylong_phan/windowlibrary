﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongInterface.Models.IO.Files
{
    public interface IFileSettingBase : IDirectory, ICloneModel, IActionSetting
    {
        int Duration { get; set; }
        bool UseDuration { get; set; }
    }
}
