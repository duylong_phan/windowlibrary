﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongInterface.Models.IO.Files
{
    public interface IExeIconSetting
    {
        string ExecutePath { get; set; }
        string IconPath { get; set; }
        string Description { get; set; }
    }
}
