﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongInterface.Models.IO.Files
{
    public enum FileChangedResponses
    {
        NewLine,
        XmlElement,
        FixLength,
        CustomedAction
    }

    public interface IFileChangedSetting : IActionSetting
    {
        string FilePath { get; set; }
        string XmlTag { get; set; }
        int Length { get; set; }
        FileChangedResponses Response { get; set; }
    }
}
