﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongInterface.Models.IO.Files
{
    public interface ISerialPortSetting
    {
        string Port { get; set; }
        int BaudRate { get; set; }
        int DataBit { get; set; }
        int ParityIndex { get; set; }
        int StopBitsIndex { get; set; }
        int ThresholdAmount { get; set; }
        bool UseThreshold { get; set; }
        bool EnableDTR { get; set; }
        bool EnableRTS { get; set; }
    }
}
