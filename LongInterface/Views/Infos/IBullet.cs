﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongInterface.Views.Infos
{
    public enum BulletTypes
    {
        Circle,
        Rectangle,
        Number
    }

    public interface IGetBullet
    {
        BulletTypes Bullet { get; }
    }
}
