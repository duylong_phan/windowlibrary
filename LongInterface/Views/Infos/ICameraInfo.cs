﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongInterface.Views.Infos
{
    public interface ICameraInfo
    {
        int Index { get; }
        string Name { get; }
        IList ResolutionCollection { get; }
    }

    public interface IResolutionInfo
    {
        int Index { get; }
        int Width { get; }
        int Height { get; }
        int FrameRate { get; }
    }
}
