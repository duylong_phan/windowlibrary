﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongInterface.Views.Documents
{
    public interface IDocumentItem
    {
        object Content { get; }
        object View { get; }
    }
}
