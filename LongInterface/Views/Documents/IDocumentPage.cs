﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongInterface.Views.Documents
{
    public interface IDocumentPage
    {
        IDocumentItem Header { get; }
        IList Content { get; }
        IDocumentItemFooter Footer { get; }
    }
}
