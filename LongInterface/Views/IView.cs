﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LongInterface.ViewModels;

namespace LongInterface.Views
{
    /// <summary>
    /// Base Interface for View
    /// </summary>
    public interface IView
    {
        void ShowView();
        void CloseView();
    }

    /// <summary>
    /// Generic Interface for View, support ViewModel
    /// </summary>
    /// <typeparam name="T">IViewModel implimented Class</typeparam>
    public interface IView<T> : IView
        where T : IViewModel
    {
        T ViewModel { get; }
    }
}
