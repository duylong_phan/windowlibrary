﻿using LongInterface.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongInterface.Views.ControlModels
{
    public interface IGetIconInfo : IGetHasInfo, ISize<double>, IContent
    {
    }
}
