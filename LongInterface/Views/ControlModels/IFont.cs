﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongInterface.Views.ControlModels
{
    public interface IFont<T, K>
    {
        double FontSize { get; set; }
        T FontStyle { get; set; }
        K FontFamily { get; set; }
    }

    public interface IGetFont<T, K>
    {
        double FontSize { get;  }
        T FontStyle { get;  }
        K FontFamily { get; }
    }
}
