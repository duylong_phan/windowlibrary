﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongInterface.Views.ControlModels
{
    public interface IColor<T>
    {
        T Background { get; set; }
        T Foreground { get; set; }
    }

    public interface IGetColor<T>
    {
        T Background { get; }
        T Foreground { get; }
    }
}
