﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongInterface.Views.ControlModels
{
    public interface ISize<T>
    {
        T Width { get; set; }
        T Height { get; set; }
    }

    public interface IGetSize<T>
    {
        T Width { get;}
        T Height { get;}
    }

    public interface IOffset<T>
    {
        T OffsetX { get; set; }
        T OffsetY { get; set; }
    }

    public interface IGetOffset<T>
    {
        T OffsetX { get; }
        T OffsetY { get; }
    }

    public interface IPosition<T> : ISize<T>, IOffset<T>
    {

    }

    public interface IGetPosition<T> : IGetSize<T>, IGetOffset<T>
    {

    }
}
