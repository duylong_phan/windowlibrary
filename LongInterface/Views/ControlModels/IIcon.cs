﻿using LongInterface.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongInterface.Views.ControlModels
{
    public interface IBasicIcon
    {
        bool HasIcon { get; }
        string Icon { get; }
    }

    public interface IGetIcon
    {
        bool HasIcon16 { get; }
        bool HasIcon24 { get; }
        bool HasIcon32 { get; }
        bool HasIcon48 { get; }

        string Icon16 { get; }
        string Icon24 { get; }
        string Icon32 { get; }
        string Icon48 { get; }
    }
}
