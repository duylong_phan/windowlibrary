﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongInterface.Views.Options
{
    public interface IOption<T>
    {
        string Text { get; }
        T Type { get; }
    }
}
