﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongInterface.Enums
{
    public enum SortOptions
    {
        Ascending,
        Descending
    }

    public enum NumberSeparatorOptions
    {
        Comma,
        Dot
    }

    public enum TimeUnitOptions
    {
        Tick,
        MiliSecond,
        Second,
        Minute,
        Hour
    }
}
