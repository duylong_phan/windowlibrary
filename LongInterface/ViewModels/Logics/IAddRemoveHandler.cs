﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace LongInterface.ViewModels.Logics
{
    public interface IAddRemoveHandler
    {
        ICommand Add { get; set; }
        ICommand Remove { get; set; }
    }
}
