﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongInterface.ViewModels.Logics
{
    public interface IDownMoveUpHandler<T> : IDisposable
    {
        Action<T> DownAction { get; set; }
        Action<T> MoveAction { get; set; }
        Action<T> UpAction { get; set; }
    }
}
