﻿using LongInterface.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace LongInterface.ViewModels.Logics
{
    public interface IDataEditor : IGetText
    {
        ICommand AddAction { get; set; }
        ICommand EditAction { get; set; }
        ICommand RemoveAction { get; set; }
        ICommand UpAction { get; set; }
        ICommand DownAction { get; set; }
    }

    public interface IDataEditor<T> : IDataEditor
    {
        T Type { get; }
    }
}
