﻿using LongInterface.Models;
using LongInterface.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace LongInterface.ViewModels
{
    public interface ICommandTarget : ICommand
    {
        void RefreshStatus();
    }
}
