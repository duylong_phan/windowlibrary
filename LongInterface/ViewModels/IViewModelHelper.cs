﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongInterface.ViewModels
{
    /// <summary>
    /// Interface for helping Class for ViewModel
    /// </summary>
    public interface IViewModelHelper
    {

    }

    /// <summary>
    /// Generic Version Interface for helping Class for ViewModel
    /// </summary>
    public interface IViewModelHelper<T> : IViewModelHelper
        where T : IViewModel
    {
        T ViewModel { get; }
    }
}
