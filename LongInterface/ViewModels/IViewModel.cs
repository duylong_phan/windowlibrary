﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LongInterface.Views;
using LongInterface.Models;

namespace LongInterface.ViewModels
{
    /// <summary>
    /// Base Interface for ViewModel
    /// </summary>
    public interface IViewModel : IDisposable
    {
        void InitializeParameter();
        void InitializeMVVM();
    }

    /// <summary>
    /// Generic Interface for ViewModel with View
    /// </summary>
    /// <typeparam name="T">IView implimented class</typeparam>
    public interface IViewModel<T> : IViewModel
        where T : IView
    {
        T View { get; }
    }

    /// <summary>
    /// Generic Interface for ViewModel with View, and Model
    /// </summary>
    /// <typeparam name="T">IView implimented class</typeparam>
    /// <typeparam name="K">IModel implimented class</typeparam>
    public interface IViewModel<T, K> : IViewModel<T>
        where T : IView
        where K : IModel
    {
        K Model { get; }
    }

    /// <summary>
    /// Generic Interface for ViewModel with View, In/Out Model and Button, ValidationContainer
    /// </summary>
    /// <typeparam name="T">IView implimented class</typeparam>
    /// <typeparam name="K">IModel implimented class</typeparam>
    /// <typeparam name="L">IModel implimented class</typeparam>
    /// <typeparam name="M">IViewModelHelper implimented class</typeparam>
    /// <typeparam name="N">IViewModelHelper implimented class</typeparam>
    public interface IViewModel<T, K, L, M, N> : IViewModel<T>
        where T : IView
        where K : IModel
        where L : IModel
        where M : IViewModelHelper
        where N : IViewModelHelper
    {
        K InputParameter { get; }
        L OutputParameter { get; }
        M CommandInfoContainer { get; }
        N ValidationRuleContainer { get;  }
    }
}
